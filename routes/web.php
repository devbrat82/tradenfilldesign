<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
   // return view('angle');
    return view('frontend.home');
});*/
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('contact-us', ['as' => 'contact', 'uses' => 'HomeController@showContactUs']);
Route::post('/contact-us/submit','ContactusQueriesController@submitQuery')
    ->name('contactus_queries.contactus_query.query');

Route::get('post/{post_id}', ['as' => 'post.details', 'uses' => 'HomeController@showPost']);
Route::get('category/{category_id}', ['as' => 'category.posts', 'uses' => 'HomeController@showCategoryPosts']);


Route::get('test', 'TestController@show')->name('test');
Route::get('preview-mail', 'TestController@previewMail')->name('previewmail');
Route::get('unit-test', 'TestController@index')->name('unit-test');



Route::group([ 'prefix' => 'admin',], function () {
    Route::get('/', 'Admin\AdminController@showLogin')->name('admin.login');
    Route::get('login', 'Admin\AdminController@showLogin')->name('admin.login');
    Route::post('login', 'Admin\AdminController@login')->name('admin.login');
    Route::get('logout', 'Admin\AdminController@logout')->name('admin.logout');
});
//Route::group([ 'prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::group([ 'prefix' => 'admin'], function () {
    Route::get('dashboard', 'Admin\AdminController@showDashboard')->name('admin.dashboard');
    Route::get('setting', 'Admin\AdminController@setting')->name('admin.setting');
    Route::put('setting', 'Admin\AdminController@account')->name('admin.setting.account');
    Route::get('reset-password', 'Admin\AdminController@showResetPassword')->name('admin.resetpassword');
    Route::put('reset-password', 'Admin\AdminController@resetPassword')->name('admin.password.update');


    Route::group(
        [
            'prefix' => 'posts',
        ], function () {

        Route::get('/', 'PostsController@index')
            ->name('posts.post.index');

        Route::get('/create','PostsController@create')
            ->name('posts.post.create');

        Route::get('/show/{post}','PostsController@show')
            ->name('posts.post.show')
            ->where('id', '[0-9]+');

        Route::get('/{post}/edit','PostsController@edit')
            ->name('posts.post.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'PostsController@store')
            ->name('posts.post.store');

        Route::put('post/{post}', 'PostsController@update')
            ->name('posts.post.update')
            ->where('id', '[0-9]+');

        Route::delete('/post/{post}','PostsController@destroy')
            ->name('posts.post.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'categories',
        ], function () {

        Route::get('/', 'CategoriesController@index')
            ->name('categories.category.index');

        Route::get('/create','CategoriesController@create')
            ->name('categories.category.create');

        Route::get('/show/{category}','CategoriesController@show')
            ->name('categories.category.show')
            ->where('id', '[0-9]+');

        Route::get('/{category}/edit','CategoriesController@edit')
            ->name('categories.category.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'CategoriesController@store')
            ->name('categories.category.store');

        Route::put('category/{category}', 'CategoriesController@update')
            ->name('categories.category.update')
            ->where('id', '[0-9]+');

        Route::delete('/category/{category}','CategoriesController@destroy')
            ->name('categories.category.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'contactus_queries',
        ], function () {

        Route::get('/', 'ContactusQueriesController@index')
            ->name('contactus_queries.contactus_query.index');

        Route::get('/create','ContactusQueriesController@create')
            ->name('contactus_queries.contactus_query.create');

        Route::get('/show/{contactusQuery}','ContactusQueriesController@show')
            ->name('contactus_queries.contactus_query.show')
            ->where('id', '[0-9]+');

        Route::get('/{contactusQuery}/edit','ContactusQueriesController@edit')
            ->name('contactus_queries.contactus_query.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'ContactusQueriesController@store')
            ->name('contactus_queries.contactus_query.store');

        Route::put('contactus_query/{contactusQuery}', 'ContactusQueriesController@update')
            ->name('contactus_queries.contactus_query.update')
            ->where('id', '[0-9]+');

        Route::delete('/contactus_query/{contactusQuery}','ContactusQueriesController@destroy')
            ->name('contactus_queries.contactus_query.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'roles',
        ], function () {

        Route::get('/', 'RolesController@index')
            ->name('roles.roles.index');

        Route::get('/create','RolesController@create')
            ->name('roles.roles.create');

        Route::get('/show/{roles}','RolesController@show')
            ->name('roles.roles.show')
            ->where('id', '[0-9]+');

        Route::get('/{roles}/edit','RolesController@edit')
            ->name('roles.roles.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'RolesController@store')
            ->name('roles.roles.store');

        Route::put('roles/{roles}', 'RolesController@update')
            ->name('roles.roles.update')
            ->where('id', '[0-9]+');

        Route::delete('/roles/{roles}','RolesController@destroy')
            ->name('roles.roles.destroy')
            ->where('id', '[0-9]+');

    });



    Route::group(
        [
            'prefix' => 'faqs',
        ], function () {

        Route::get('/', 'FaqsController@index')
            ->name('faqs.faq.index');

        Route::get('/create','FaqsController@create')
            ->name('faqs.faq.create');

        Route::get('/show/{faq}','FaqsController@show')
            ->name('faqs.faq.show')
            ->where('id', '[0-9]+');

        Route::get('/{faq}/edit','FaqsController@edit')
            ->name('faqs.faq.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'FaqsController@store')
            ->name('faqs.faq.store');

        Route::put('faq/{faq}', 'FaqsController@update')
            ->name('faqs.faq.update')
            ->where('id', '[0-9]+');

        Route::get('faq_ajax_update/{faq}', 'FaqsController@updateAjax')
            ->name('faqs.faq.update.ajax')
            ->where('id', '[0-9]+');


        Route::delete('/faq/{faq}','FaqsController@destroy')
            ->name('faqs.faq.destroy')
            ->where('id', '[0-9]+');

    });


    Route::group(
        [
            'prefix' => 'services',
        ], function () {

        Route::get('/', 'ServicesController@index')
            ->name('services.service.index');

        Route::get('/create','ServicesController@create')
            ->name('services.service.create');

        Route::get('/show/{service}','ServicesController@show')
            ->name('services.service.show')
            ->where('id', '[0-9]+');

        Route::get('/{service}/edit','ServicesController@edit')
            ->name('services.service.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'ServicesController@store')
            ->name('services.service.store');

        Route::put('service/{service}', 'ServicesController@update')
            ->name('services.service.update')
            ->where('id', '[0-9]+');

        Route::delete('/service/{service}','ServicesController@destroy')
            ->name('services.service.destroy')
            ->where('id', '[0-9]+');

    });


    Route::group(
        [
            'prefix' => 'permissions',
        ], function () {

        Route::get('/', 'PermissionsController@index')
            ->name('permissions.permission.index');

        Route::get('/create','PermissionsController@create')
            ->name('permissions.permission.create');

        Route::get('/show/{permission}','PermissionsController@show')
            ->name('permissions.permission.show')
            ->where('id', '[0-9]+');

        Route::get('/{permission}/edit','PermissionsController@edit')
            ->name('permissions.permission.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'PermissionsController@store')
            ->name('permissions.permission.store');

        Route::put('permission/{permission}', 'PermissionsController@update')
            ->name('permissions.permission.update')
            ->where('id', '[0-9]+');

        Route::delete('/permission/{permission}','PermissionsController@destroy')
            ->name('permissions.permission.destroy')
            ->where('id', '[0-9]+');

    });


    Route::group(
        [
            'prefix' => 'document_categories',
        ], function () {

        Route::get('/', 'DocumentCategoriesController@index')
            ->name('document_categories.document_category.index');

        Route::get('/create','DocumentCategoriesController@create')
            ->name('document_categories.document_category.create');

        Route::get('/show/{documentCategory}','DocumentCategoriesController@show')
            ->name('document_categories.document_category.show')
            ->where('id', '[0-9]+');

        Route::get('/{documentCategory}/edit','DocumentCategoriesController@edit')
            ->name('document_categories.document_category.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'DocumentCategoriesController@store')
            ->name('document_categories.document_category.store');

        Route::put('document_category/{documentCategory}', 'DocumentCategoriesController@update')
            ->name('document_categories.document_category.update')
            ->where('id', '[0-9]+');

        Route::delete('/document_category/{documentCategory}','DocumentCategoriesController@destroy')
            ->name('document_categories.document_category.destroy')
            ->where('id', '[0-9]+');

    });


    Route::group(
        [
            'prefix' => 'customer_types',
        ], function () {

        Route::get('/', 'CustomerTypesController@index')
            ->name('customer_types.customer_type.index');

        Route::get('/create','CustomerTypesController@create')
            ->name('customer_types.customer_type.create');

        Route::get('/show/{customerType}','CustomerTypesController@show')
            ->name('customer_types.customer_type.show')
            ->where('id', '[0-9]+');

        Route::get('/{customerType}/edit','CustomerTypesController@edit')
            ->name('customer_types.customer_type.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'CustomerTypesController@store')
            ->name('customer_types.customer_type.store');

        Route::put('customer_type/{customerType}', 'CustomerTypesController@update')
            ->name('customer_types.customer_type.update')
            ->where('id', '[0-9]+');

        Route::delete('/customer_type/{customerType}','CustomerTypesController@destroy')
            ->name('customer_types.customer_type.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'documents',
        ], function () {

        Route::get('/', 'DocumentsController@index')
            ->name('documents.document.index');

        Route::get('/create','DocumentsController@create')
            ->name('documents.document.create');

        Route::get('/show/{document}','DocumentsController@show')
            ->name('documents.document.show')
            ->where('id', '[0-9]+');

        Route::get('/{document}/edit','DocumentsController@edit')
            ->name('documents.document.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'DocumentsController@store')
            ->name('documents.document.store');

        Route::put('document/{document}', 'DocumentsController@update')
            ->name('documents.document.update')
            ->where('id', '[0-9]+');

        Route::delete('/document/{document}','DocumentsController@destroy')
            ->name('documents.document.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'customers',
        ], function () {

        Route::get('/', 'CustomersController@index')
            ->name('customers.customer.index');

        Route::get('/create','CustomersController@create')
            ->name('customers.customer.create');

        Route::get('/show/{customer}','CustomersController@show')
            ->name('customers.customer.show')
            ->where('id', '[0-9]+');

        Route::get('/{customer}/edit','CustomersController@edit')
            ->name('customers.customer.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'CustomersController@store')
            ->name('customers.customer.store');

        Route::put('customer/{customer}', 'CustomersController@update')
            ->name('customers.customer.update')
            ->where('id', '[0-9]+');

        Route::delete('/customer/{customer}','CustomersController@destroy')
            ->name('customers.customer.destroy')
            ->where('id', '[0-9]+');

        Route::get('/customer/excel','CustomersController@excel')
            ->name('customers.customer.excel');
            

    });

    Route::group(
        [
            'prefix' => 'acts',
        ], function () {

        Route::get('/', 'ActsController@index')
            ->name('acts.act.index');

        Route::get('/create','ActsController@create')
            ->name('acts.act.create');

        Route::get('/show/{act}','ActsController@show')
            ->name('acts.act.show')
            ->where('id', '[0-9]+');

        Route::get('/{act}/edit','ActsController@edit')
            ->name('acts.act.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'ActsController@store')
            ->name('acts.act.store');

        Route::put('act/{act}', 'ActsController@update')
            ->name('acts.act.update')
            ->where('id', '[0-9]+');

        Route::delete('/act/{act}','ActsController@destroy')
            ->name('acts.act.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'formation_types',
        ], function () {

        Route::get('/', 'FormationTypesController@index')
            ->name('formation_types.formation_type.index');

        Route::get('/create','FormationTypesController@create')
            ->name('formation_types.formation_type.create');

        Route::get('/show/{formationType}','FormationTypesController@show')
            ->name('formation_types.formation_type.show')
            ->where('id', '[0-9]+');

        Route::get('/{formationType}/edit','FormationTypesController@edit')
            ->name('formation_types.formation_type.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'FormationTypesController@store')
            ->name('formation_types.formation_type.store');

        Route::put('formation_type/{formationType}', 'FormationTypesController@update')
            ->name('formation_types.formation_type.update')
            ->where('id', '[0-9]+');

        Route::delete('/formation_type/{formationType}','FormationTypesController@destroy')
            ->name('formation_types.formation_type.destroy')
            ->where('id', '[0-9]+');

    });

    Route::group(
        [
            'prefix' => 'service_documents',
        ], function () {

        Route::get('/', 'ServiceDocumentsController@index')
            ->name('service_documents.service_document.index');

        Route::get('/create','ServiceDocumentsController@create')
            ->name('service_documents.service_document.create');

        Route::get('/show/{serviceDocument}','ServiceDocumentsController@show')
            ->name('service_documents.service_document.show')
            ->where('id', '[0-9]+');

        Route::get('/{serviceDocument}/edit','ServiceDocumentsController@edit')
            ->name('service_documents.service_document.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'ServiceDocumentsController@store')
            ->name('service_documents.service_document.store');

        Route::put('service_document/{serviceDocument}', 'ServiceDocumentsController@update')
            ->name('service_documents.service_document.update')
            ->where('id', '[0-9]+');

        Route::delete('/service_document/{serviceDocument}','ServiceDocumentsController@destroy')
            ->name('service_documents.service_document.destroy')
            ->where('id', '[0-9]+');

        Route::get('/{serviceDocument}/documents','ServiceDocumentsController@servicedocs')
            ->name('service_documents.service_document.documents');

        Route::put('/{serviceDocument}/documents', 'ServiceDocumentsController@storeDocs')
            ->name('service_documents.service_document.storedocs');

        Route::get('/act-service','ServiceDocumentsController@getActService')
            ->name('service_documents.service_document.actService')
            ->where('id', '[0-9]+');

         Route::get('/service_document/excel','ServiceDocumentsController@excel')
            ->name('service_documents.service_document.excel');

    });

    Route::group(
        [
            'prefix' => 'quotes',
        ], function () {

        Route::get('/', 'QuotesController@index')
            ->name('quotes.quote.index');

        Route::get('/create','QuotesController@create')
            ->name('quotes.quote.create');

        Route::get('/show/{quote}','QuotesController@show')
            ->name('quotes.quote.show')
            ->where('id', '[0-9]+');

        Route::get('/{quote}/edit','QuotesController@edit')
            ->name('quotes.quote.edit')
            ->where('id', '[0-9]+');

        Route::get('/{quote}/copy','QuotesController@copy')
            ->name('quotes.quote.copy')
            ->where('id', '[0-9]+');

        Route::post('/', 'QuotesController@store')
            ->name('quotes.quote.store');

        Route::put('quote/{quote}', 'QuotesController@update')
            ->name('quotes.quote.update')
            ->where('id', '[0-9]+');

        Route::delete('/quote/{quote}','QuotesController@destroy')
            ->name('quotes.quote.destroy')
            ->where('id', '[0-9]+');

        Route::get('/act-service','QuotesController@getActService')
            ->name('quotes.quote.actService')
            ->where('id', '[0-9]+');

        Route::get('/get-docs','QuotesController@getRequiredDocs')
            ->name('quotes.quote.getDocs');

        Route::get('/get-all-docs','QuotesController@getAllDocuments')
            ->name('quotes.quote.getAllDocs');

        Route::post('/save-docs','QuotesController@saveDocuments')
            ->name('quotes.quote.saveDocs');

        Route::post('{quote}/quote-mail','QuotesController@sendPdf')
            ->name('quotes.quote.sendQuoteMail');

        Route::get('{quote}/qoute-preview', 'QuotesController@generateQuotePDF')->name('quotes.quote.generatePdf');

        Route::get('{quote}/preview', 'QuotesController@preview')->name('quotes.quote.preview');
        
        Route::get('quote/excel', 'QuotesController@excel')->name('quotes.quote.excel');

        Route::get('{quote}/pdf', 'QuotesController@preview_pdf')
        ->name('quotes.quote.pdf');
    });

    Route::group(
        [
            'prefix' => 'users',
        ], function () {

        Route::get('/', 'UsersController@index')
            ->name('users.user.index');

        Route::get('/create','UsersController@create')
            ->name('users.user.create');

        Route::get('/show/{user}','UsersController@show')
            ->name('users.user.show')
            ->where('id', '[0-9]+');

        Route::get('/{user}/edit','UsersController@edit')
            ->name('users.user.edit')
            ->where('id', '[0-9]+');

        Route::post('/store', 'UsersController@store')
            ->name('users.user.store');

        Route::put('update/{user}', 'UsersController@update')
            ->name('users.user.update')
            ->where('id', '[0-9]+');

        Route::delete('/users/{user}','UsersController@destroy')
            ->name('users.user.destroy')
            ->where('id', '[0-9]+');
    });


});




