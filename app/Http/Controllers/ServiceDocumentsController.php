<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\FormationType;
use Illuminate\Http\Request;
use App\Models\ServiceDocument;
use App\Models\ServiceDocumentRef;
use App\Models\Service;
use App\Models\Document;
use App\Http\Controllers\Controller;
use Exception;
use DB;
class ServiceDocumentsController extends Controller
{

    /**
     * Display a listing of the service documents.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $serviceDocuments = ServiceDocument::getData();
        $serviceDocuments = $serviceDocuments->transform(function($sd){
           $sd->docs_count = ServiceDocument::getTotalRequiredDocuments($sd->id);
           return $sd;
       });
        return view('service_documents.index', compact('serviceDocuments'));
    }

    /**
     * Show the form for creating a new service document.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        $services = Service::OrderBy('title','ASC')->pluck('title','id');
        $formations = FormationType::OrderBy('name','ASC')->pluck('name','id');
        
        return view('service_documents.create', compact(['acts','services', 'formations']));
    }

    /**
     * Store a new service document in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            ServiceDocument::create($data);

            return redirect()->route('service_documents.service_document.index')
                             ->with('success_message', 'Service Document was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified service document.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $serviceDocument = ServiceDocument::findOrFail($id);

        return view('service_documents.show', compact('serviceDocument'));
    }

    /**
     * Show the form for editing the specified service document.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        $services = Service::OrderBy('title','ASC')->pluck('title','id');
        $formations = FormationType::OrderBy('name','ASC')->pluck('name','id');
        $serviceDocument = ServiceDocument::findOrFail($id);
        

        return view('service_documents.edit', compact(['serviceDocument','acts','services','formations']));
    }

    /**
     * Update the specified service document in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $serviceDocument = ServiceDocument::findOrFail($id);
            $serviceDocument->update($data);

            return redirect()->route('service_documents.service_document.index')
                             ->with('success_message', 'Service Document was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified service document from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            if($this->CanDelete()){
                $serviceDocument = ServiceDocument::findOrFail($id);
                $serviceDocument->delete();
                return redirect()->route('service_documents.service_document.index')
                    ->with('success_message', 'Service Document was successfully deleted!');
            }else{
                return redirect()->route('service_documents.service_document.index')
                    ->with('success_message', 'Permission denied!');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'act' => 'nullable',
            'service' => 'nullable',
            'fotmation' => 'nullable',
            'govt_fee' => 'string|min:1|nullable',
            'prof_fees' => 'string|min:1|nullable',
            'duration' => 'string|min:1|nullable',
            'unit' => 'required'
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

    public function servicedocs($serviceDocId)
    {
        $services = Service::orderBy('title','asc')->pluck('title', 'id');
        $associated_docs = ServiceDocumentRef::where('service_doc_id', $serviceDocId)->pluck('document_id');
        $docs  = Document::select('id', 'name')->orderBy('name', 'asc')->get();
        $selected_docs = $docs->whereIn('id', $associated_docs->toArray());
        $unselected_docs = $docs->whereNotIn('id', $associated_docs->toArray());

        return view('service_documents.edit_documents', compact('serviceDocId', 'services', 'selected_docs', 'unselected_docs'));
    }

    public function storeDocs(Request $request, $serviceDocId){
        ServiceDocumentRef::where('service_doc_id', $serviceDocId)->delete();
        if(is_array($request['docs_ids'])) {
            if (count($request['docs_ids'])) {
                foreach ($request['docs_ids'] as $doc) {
                    $obj = new ServiceDocumentRef();
                    $obj->service_doc_id = $serviceDocId;
                    $obj->document_id = $doc;
                    $obj->save();
                }
            }
        }
        return ['message' => 'Records updated successfully.'];
    }

    public function getActService(Request $request)
    {
        $act = $request->act_id;
        $services = Service::where('act',$act)->pluck('title','id')->toArray();
        return $services;
    }

    public function CanDelete()
    {
        $userRoleId = auth()->user()->role_id;
        if($userRoleId==1)
        {
            return true;
        }
        return false;
    }

    public function excel(Request $request)
    {
        $arrayval='';
      if($request->id!="")
       {
        $arrayval=explode(",",$request->id);
        $servicedocument=new ServiceDocument;
        $query=$servicedocument->excel($arrayval);
       }
       else
       {
         $servicedocument=new ServiceDocument;
         $query=$servicedocument->excel($arrayval);
       }
        
       $array = json_decode(json_encode($query), True);
       $flag = false;
          foreach($array  as $row) {
            if(!$flag) {
              // display field/column names as first row
              echo implode("\t", array_keys($row)) . "\r\n";
              $flag = true;
            }
            echo implode("\t", array_values($row)) . "\r\n";
          }
          header("Content-Type: text/plain");
          $filename = "website_data_" . date('Ymd') . ".xls";

       header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
         exit;
    }


}
