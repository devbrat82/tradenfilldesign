<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormationType;
use App\Http\Controllers\Controller;
use Exception;

class FormationTypesController extends Controller
{

    /**
     * Display a listing of the formation types.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $formationTypes = FormationType::get();

        return view('formation_types.index', compact('formationTypes'));
    }

    /**
     * Show the form for creating a new formation type.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('formation_types.create');
    }

    /**
     * Store a new formation type in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            FormationType::create($data);

            return redirect()->route('formation_types.formation_type.index')
                             ->with('success_message', 'Formation Type was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified formation type.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $formationType = FormationType::findOrFail($id);

        return view('formation_types.show', compact('formationType'));
    }

    /**
     * Show the form for editing the specified formation type.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $formationType = FormationType::findOrFail($id);
        

        return view('formation_types.edit', compact('formationType'));
    }

    /**
     * Update the specified formation type in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
        //try {
            
            $data = $this->getData($request,$id);
            
            $formationType = FormationType::findOrFail($id);
            $formationType->update($data);

        return redirect()->route('formation_types.formation_type.index')
                             ->with('success_message', 'Formation Type was successfully updated!');

        //} catch (Exception $exception) {
            //$error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             //if($exception->validator){
                  //  $error_messages = $exception->validator;
              //}
            //return back()->withInput()
                         //->withErrors($error_messages);
        //}        
    }

    /**
     * Remove the specified formation type from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $formationType = FormationType::findOrFail($id);
            $formationType->delete();

            return redirect()->route('formation_types.formation_type.index')
                             ->with('success_message', 'Formation Type was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request,$id=null)
    {
        $rules = [
            'name' => 'string|min:1|max:255|unique:formation_types,name,'.$id,
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
