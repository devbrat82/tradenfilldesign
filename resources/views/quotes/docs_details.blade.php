<div class="row ct">
<div class="col-md-2">
    <h3>Required Docs</h3>
</div>
    <div class="col-md-10">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label for="add_new" class="control-label add_doc"><img  src="{{asset('/images/plus.png')}}">&nbsp; Add More Documents</label>
        <div id="docs">


        </div>
    </div>
</div>
<hr>
<div class="row ct">
<div class="col-md-2">
    <h3>Fees</h3>
</div>
    <div class="col-md-5">
        <label for="govt_fee" class="control-label">Govt Fee</label>
        <input class="form-control" name="govt_fee" type="text" id="govt_fee" readonly required value="{{ old('govt_fee', optional($quote)->govt_fee) }}" minlength="1" placeholder="Enter govt fee here..."><span id='gvt' style=" position: absolute;top:23px;right: 14px;font-size:20px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
        {!! $errors->first('govt_fee', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-5">
        <label for="prof_fees" class="control-label">Professional Fees</label>
        <input class="form-control" name="prof_fees" type="text" id="prof_fees" readonly  value="{{ old('prof_fees', optional($quote)->prof_fees) }}" minlength="1" placeholder="Enter prof fees here..."><span id="prof" style="position: absolute;top:23px;right: 14px;font-size:20px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
        {!! $errors->first('prof_fees', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="row ct">
<div class="col-md-2"></div>
    <div class="col-md-5 custom-control custom-radio">
        <label for="" class=" control-label">Split Price</label><br>

        <div class="fees-radio">
            <div class="custom-control custom-radio">
                <input type="radio"class="custom-control-input" id="splitRadioYes" name = "split_price" value="1" {{ old('split_price', optional($quote)->split_price) == 1 ? 'checked' : '' }}>
                <label class="custom-control-label" for="splitRadioYes">Yes</label>&nbsp
            </div>

            <div class="custom-control custom-radio">&nbsp;
                <input type="radio" class="custom-control-input" id="splitRadioNo" name = "split_price" value="0" {{ old('split_price', optional($quote)->split_price) == 0 ? 'checked' : '' }}>
                <label class="custom-control-label" for="splitRadioNo">No</label>&nbsp;
            </div>
        </div>


        {{--<select class="form-control" id="split_price" name="split_price">--}}
            {{--<option value="0" {{ old('split_price', optional($quote)->split_price) == 0 ? 'selected' : '' }}>No</option>--}}
            {{--<option value="1" {{ old('split_price', optional($quote)->split_price) == 1 ? 'selected' : '' }}>Yes</option>--}}
        {{--</select>--}}
        {!! $errors->first('split_price', '<p class="help-block">:message</p>') !!}
    </div>


    <div class="col-md-5">

            <label for="duration" class="control-label">Duration</label>
        <div class="duration">
            <input class="form-control" name="duration" type="text" id="duration"  value="{{ old('duration', optional($quote)->duration) }}" minlength="1" >
            {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
            <label for="unit" class=" control-label"></label>
            <select class="form-control" id="unit" name="unit">
                <option value="Years" {{ old('unit', optional($quote)->unit) == 'Years' ? 'selected' : '' }}>Years</option>
                <option value="Months" {{ old('unit', optional($quote)->unit) == 'Months' ? 'selected' : '' }}>Months</option>
                <option value="Days" {{ old('unit', optional($quote)->unit) == 'Days' ? 'selected' : '' }}>Days</option>
            </select>
            {!! $errors->first('unit', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    {{--<div class="col-md-3">--}}
        {{--<label for="unit" class=" control-label"></label>--}}
        {{--<select class="form-control" id="unit" name="unit">--}}
            {{--<option value="Years" {{ old('unit', optional($quote)->unit) == 'Years' ? 'selected' : '' }}>Years</option>--}}
            {{--<option value="Months" {{ old('unit', optional($quote)->unit) == 'Months' ? 'selected' : '' }}>Months</option>--}}
            {{--<option value="Days" {{ old('unit', optional($quote)->unit) == 'Days' ? 'selected' : '' }}>Days</option>--}}
        {{--</select>--}}
        {{--{!! $errors->first('unit', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
</div>
<hr>
<div class="row ct">
    <div class='col-md-2'>

        <h3>Comment</h3>
    </div>
    <div class="col-md-10">

        <textarea rows="10" cols="10" name="comment" class="form-control">{{ old('comment', optional($quote)->comment) }}</textarea>
        {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
    </div>
</div>