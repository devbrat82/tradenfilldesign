<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_documents', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('act')->nullable();
            $table->integer('service')->nullable();
            $table->integer('fotmation')->nullable();
            $table->string('govt_fee')->nullable();
            $table->string('prof_fees')->nullable();
            $table->string('duration')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_documents');
    }
}
