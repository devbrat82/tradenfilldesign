<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ServiceDocumentRef;
use Illuminate\Database\Eloquent\SoftDeletes; 
class ServiceDocument extends Model
{
    
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_documents';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'act',
                  'service',
                  'fotmation',
                  'govt_fee',
                  'prof_fees',
                  'duration',
                    'unit'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    //protected $with =['Act','Service','FormationType'];


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return \DateTime::createFromFormat('j/n/Y g:i A', $value);

    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return \DateTime::createFromFormat('j/n/Y g:i A', $value);

    }

    public static function getTotalRequiredDocuments($sdId)
    {
        return ServiceDocumentRef::where('service_doc_id',$sdId)->get()->count();
    }

    public function Act()
    {
        return $this->hasOne(Act::Class, 'id', 'act');
    }

    public function Service()
    {
        return $this->hasOne(Service::Class, 'id', 'service');
    }

    public function FormationType()
    {
        return $this->hasOne(FormationType::Class, 'id', 'fotmation');
    }

    public static function getData()
    {
        return self::with('Act')->with('Service')->with('FormationType')->get();
    }
    public function excel($id)
    {
        $id = $id;
        $service_documents = ServiceDocument::select('service_documents.id as Id','acts.name as Main','services.title as Service','formation_types.name as Formation','service_documents.govt_fee as Goverment Fee','service_documents.prof_fees as Professional Fee')
        ->leftjoin('acts', 'acts.id', '=', 'service_documents.act')
        ->leftjoin('services', 'services.id', '=', 'service_documents.service')
        ->leftjoin('formation_types', 'formation_types.id', '=', 'service_documents.fotmation')
         ->when($id, function ($query) use ($id) {
         return $query->whereIn('service_documents.id',$id);
           })
        ->orderBy('service_documents.id')
        ->get();
        return $service_documents; 
    }
}
