<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DocumentCategory;
use App\Http\Controllers\Controller;
use Exception;

class DocumentCategoriesController extends Controller
{

    /**
     * Display a listing of the document categories.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $documentCategories = DocumentCategory::get();

        return view('document_categories.index', compact('documentCategories'));
    }

    /**
     * Show the form for creating a new document category.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('document_categories.create');
    }

    /**
     * Store a new document category in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            DocumentCategory::create($data);

            return redirect()->route('document_categories.document_category.index')
                             ->with('success_message', 'Document Category was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified document category.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $documentCategory = DocumentCategory::findOrFail($id);

        return view('document_categories.show', compact('documentCategory'));
    }

    /**
     * Show the form for editing the specified document category.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $documentCategory = DocumentCategory::findOrFail($id);
        

        return view('document_categories.edit', compact('documentCategory'));
    }

    /**
     * Update the specified document category in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
        try {
            
            $data = $this->getData($request,$id);
            
            $documentCategory = DocumentCategory::findOrFail($id);
            $documentCategory->update($data);

            return redirect()->route('document_categories.document_category.index')
                             ->with('success_message', 'Document Category was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified document category from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $documentCategory = DocumentCategory::findOrFail($id);
            $documentCategory->delete();

            return redirect()->route('document_categories.document_category.index')
                             ->with('success_message', 'Document Category was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request,$id=null)
    {
        $rules = [
            'name' => 'string|min:1|max:255|unique:document_categories,name,'.$id,
            'status' => 'nullable',
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
