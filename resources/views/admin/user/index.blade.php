@extends('layouts.app')
@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-check"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
@endif
    <div class="panel panel-default">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <div class="card form-card">
                    <div class="card-header">
                        <div class="card-title pull-left">Users List
                        <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    
    <li class="breadcrumb-item active" aria-current="page">Users</li>
  </ol>
</nav>
                </div>
                        </div>
                        <a href="{{ url('admin/users/create') }}">
                            <button class=" master-btn btn btn-labeled btn-green mb-2  pull-right customer-btn" type="button">
                           <span class="btn-label customer-btn-label"><i class="fa fa-plus"></i>
                           </span>New User</button>
                        </a>
                    </div>

                    {{--<form method="get" action="{{url('admin/users/index')}}" id="edit_countries_form"  accept-charset="UTF-8" class="">--}}
                        {{--<section class="card-header ">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}

                                        {{--<div class="form-group row">--}}
                                            {{--<div class="col-md-10">--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-lg-3">--}}
                                                        {{--<input class="form-control" id="search"--}}
                                                               {{--value="{{ request('search') }}"--}}
                                                               {{--placeholder="Enter Search Keyword" name="search"--}}
                                                               {{--type="text" id="search"/>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-lg-3">--}}
                                                        {{--<select class="form-control" id="published" name="status">--}}
                                                            {{--<option value="">Please select </option>--}}
                                                            {{--@foreach (\App\User::$status_lbl as $key => $text)--}}
                                                                {{--<option value="{{ $key }}"  {{request('status')==$key?'selected':''}}  >--}}
                                                                    {{--{{ $text }}--}}
                                                                {{--</option>--}}
                                                            {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    {{--</div>--}}

                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    {{--<div class="form-group row">--}}
                                        {{--<div class="col-md-10">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-lg-3">--}}
                                                    {{--<button type="submit" class="btn btn-warning" >  Search </button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<input type="hidden" value="{{request('field')}}" name="field"/>--}}
                        {{--<input type="hidden" value="{{request('sort')}}" name="sort"/>--}}
                    {{--</form>--}}



                    <div class="card-body">
                        <table class="table table-striped " id="datatable-countriesObjects" style='border-collapse: separate;
    border-spacing: 0px 15px;    margin-bottom: 50px;'>
                            <thead>
                            <tr>
                                {{--<th>ID</th>--}}
                                <th> Email </th>
                                <th>Name</th>
                                <th>Role </th>

                                <th>Registered Date</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($users))
                                @foreach($users as $user)
                                    <tr>
                                        {{--<td>{{ $user->id }}</td>--}}
                                        <td>{{ $user->email }}</td>
                                        <td>{{ ($user->name)}}</td>
                                        <td>{{ $user->role->title }}</td>
                                        <td>
                                            <a href="#" title='{{ date("F j, Y, g:i A", strtotime($user->created_at))}}'>{{ date("F j, Y", strtotime($user->created_at))}}</a>    
                                        </td>
                                        <td>

                                            <form method="POST" action="" accept-charset="UTF-8">
											   <!-- <input name="type" value="client" type="hidden">
                                                <input name="_method" value="DELETE" type="hidden">
                                                {{ csrf_field() }} -->
												<!-- <div class="btn-group btn-group-xs pull-right" role="group">
													{{--<button value="{{$user->id}}" class="btn  ajaxpopup btn-info  btn-xs" type="button" data-toggle="modal" data-target="#myModalLarge"><span class="fa fa-eye" aria-hidden="true"></span></button>--}}
                                                    <a href="{{ route('users.user.show', $user->id ) }}" class="btn  btn-xs" title="View Users">
                                                        <span class="fa fa-eye" aria-hidden="true"></span>
                                                    </a>
													<a href="{{ route('users.user.edit', $user->id ) }}" class="btn  btn-xs" title="Edit Users">
                                                        <span class="fa fa-pencil" aria-hidden="true"></span>
                                                    </a>

                                                    <button type="submit" class="btn  btn-xs" title="Delete User" onclick="return confirm(&quot;Are you sure? all associated record will be deleted permanently&quot;)">
                                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                                    </button>
                                                </div> -->
                                                <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon">
                                        <span class="dot dot1"></span>
                                        <span class="dot dot2"></span> 
                                        <span class="dot dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                    {{--<button value="{{$user->id}}" class="btn  ajaxpopup btn-info  btn-xs" type="button" data-toggle="modal" data-target="#myModalLarge"><span class="fa fa-eye" aria-hidden="true"></span></button>--}}
                                        <a href="{{ route('users.user.show', $user->id ) }}" class="dropdown-item btn  btn-xs" title="Show User">
                                            <span class="fa fa-eye" aria-hidden="true"></span> Show User
                                        </a>
                                        <a href="{{ route('users.user.edit', $user->id ) }}" class="dropdown-item btn  btn-xs" title="Edit User">
                                            <span class="fa fa-pencil" aria-hidden="true"></span> Edit User
                                        </a>

                                        <button type="button" class="dropdown-item btn btn-xs" title="Delete User" data-toggle="modal" data-target="#deleteModel{{$user->id}}">
                                            <span class="fa fa-trash" aria-hidden="true"></span> Delete User
                                        </button>
                                
                                    </div>
                                </div>
                                            </form>
                                            <form method="POST" action="{!! route('users.user.destroy', $user->id) !!}" accept-charset="UTF-8">
                                            <input name="type" value="client" type="hidden">
                                                <input name="_method" value="DELETE" type="hidden">
                                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'>All associated record of this user will be deleted permanently.</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
                                </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{--<ul class="pagination justify-content-end">--}}
                            {{--{{ $users->links() }}--}}
                        {{--</ul>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
	

@endsection


@section('popup')

<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
<div class="modal-dialog modal-lg" style="min-width:900px;">
 <div class="modal-content">
	<div class="modal-header">
	   <h4 class="modal-title" id="myModalLabelLarge">Client Detail</h4>
	   <button class="close" type="button" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
	   </button>
	</div>
	<div class="modal-body" id="displaycontent" >Please wait...</div>
	<div class="modal-footer">
	   <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
	</div>
 </div>
</div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
 $(function() {
//twitter bootstrap script
 $(".ajaxpopup").click(function(){
	 
	 $("#displaycontent").html('');
		id=  $(this).val();
		$.ajax({
			url: "{{url('admin/users/clientdetails')}}",
			type: "post",
			data: {id: id ,"_token": "{{ csrf_token() }}" },
			success: function(result) {
				$("#displaycontent").html(result);
			}
		});	
 });
 });
</script>

@endsection