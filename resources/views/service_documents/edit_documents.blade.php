@extends('layouts.app')

@section('content')



    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('service_documents.service_document.index') }}" class="btn btn-primary" title="Show All Services">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>
            </div>
        </div>


        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="jumbotron drag-drop-container">
                        <div class="row">
                            <div class="col-lg-6" style="border-right: 1px solid #eaeaea;">
                                <div class="text-center header-container">
                                    <h4 class="mt0">Available Documents</h4>

                                </div>
                                <ul id="sortable1" class="connectedSortable default">
                                    <?php foreach ($unselected_docs as $doc){ ?>
                                    <li class="ui-state-default"  data-id="{{$doc->id }}"><span class="item-lbl">{{$doc->name }}</span><span class="remove-wrapper"><a href="javascript:void(0)" class="remove"></a></span></li>
                                    <?php  } ?>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <div class="text-center header-container">
                                    <h4 class="mt0">Associated Documents</h4>

                                </div>
                                <ul id="sortable2" class="connectedSortable selected-items">
                                    <?php foreach ($selected_docs as $doc){ ?>
                                    <li class="ui-state-default"  data-id="{{$doc->id }}"><span class="item-lbl">{{$doc->name }}</span><span class="remove-wrapper"><a href="javascript:void(0)" class="remove"></a></span></li>
                                    <?php  } ?>
                                </ul>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6" style="border-right: 1px solid #eaeaea;">
                                <div class="text-center">
                                    <button class="btn btn-labeled btn-secondary js-add-all" type="button">Add all documents<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="text-center">
                                    <button class="btn btn-labeled btn-secondary js-remove-all" type="button"><span class="btn-label"><i class="fa fa-arrow-left"></i></span>Remove all documents</button>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 mt-5">
                                <div class="text-center">
                                    <button class="js-btn-cancel btn btn-labeled btn-green mb-2" type="button">
                           <span class="btn-label"><i class="fa fa-times"></i>
                           </span>Cancel</button>

                                    <button class="js-btn-save btn btn-labeled btn-green mb-2" type="button">
                           <span class="btn-label"><i class="fa fa-save"></i>
                           </span>Save</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#sortable1, #sortable2" ).sortable({
                connectWith: ".connectedSortable",
                cursor: 'move',

            }).disableSelection();

            $(".remove-wrapper").on('click', function() {
                const _this = $(this);
                const to_sortable = _this.closest('#sortable1').length? "#sortable2":"#sortable1";
                _this.closest('li').appendTo($(to_sortable));
            })


            $(".js-add-all").on('click', function() {
                let li_items  = $('#sortable1').find('li');
                if(li_items.length){
                    li_items.each(function(indx, item){
                        console.log(item);
                        $(item).appendTo($("#sortable2"));
                    })
                }
            })

            $(".js-remove-all").on('click', function() {
                let li_items  = $('#sortable2').find('li');
                if(li_items.length){
                    li_items.each(function(indx, item){
                        console.log(item);
                        $(item).appendTo($("#sortable1"));
                    })
                }
            })

            $(".js-btn-cancel").on('click', function() {
                location.reload();
            })

            $(".js-btn-save").on('click', function() {
                const data = $( "#sortable2").sortable( "toArray", { attribute: 'data-id'});
                let ajaxData = {"_token": "{{ csrf_token() }}",}
                ajaxData['docs_ids'] = data;
                var request = $.ajax({
                    url: "<?php echo route('service_documents.service_document.storedocs', $serviceDocId) ?>",
                    method: "PUT",
                    data: ajaxData,
                    dataType: "json"
                });

                request.done(function(data) {
                    let message = 'Record has been successfully updated.';
                    if(data.message){
                        message = data.message;
                    }
                    $.notify(message, {status: "success", pos: "bottom-right"});
                }).fail(function() {
                    $.notify("Unexpected error occurred, <br>please try again later.", {status: "danger", pos: "bottom-right"});
                });


            })

            $(".js-select-service").change(function(){
                const selectedValue = $(this).val();
                console.log(selectedValue);
                let locStr = '';
                window.location.href = locStr;
            })
        } );

        //@ sourceURL=edit_skills.js
    </script>
@endsection