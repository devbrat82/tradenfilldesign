<div class="form-group {{ $errors->has('act') ? 'has-error' : '' }}">
    <label for="act" class="col-md-2 control-label">Act</label>
    <div class="col-md-10">
        <select class="form-control" id="act" name="act" required>
            <option value="" style="display: none;" {{ old('act', optional($service)->act ?: '') == '' ? 'selected' : '' }} disabled selected>Select act</option>
            @foreach ($act as $key => $text)
                <option value="{{ $key }}" {{ old('act', optional($service)->act) == $key ? 'selected' : '' }}>
                    {{ $text }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('act', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Title</label>
    <div class="col-md-10">
        <input class="form-control" name="title" type="text" id="title" required value="{{ old('title', optional($service)->title) }}" minlength="1" maxlength="255" placeholder="Enter title here...">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>



{{--<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">--}}
    {{--<label for="description" class="col-md-2 control-label">Description</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<textarea class="form-control" name="description" cols="50" rows="10" id="description" minlength="1" maxlength="1000">{{ old('description', optional($service)->description) }}</textarea>--}}
        {{--{!! $errors->first('description', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="form-group {{ $errors->has('govt_fees') ? 'has-error' : '' }}">--}}
    {{--<label for="govt_fees" class="col-md-2 control-label">Govt Fees</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<input class="form-control" name="govt_fees" type="text" id="govt_fees" value="{{ old('govt_fees', optional($service)->govt_fees) }}" minlength="1" placeholder="Enter govt fees here...">--}}
        {{--{!! $errors->first('govt_fees', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="form-group {{ $errors->has('professional_charge') ? 'has-error' : '' }}">--}}
    {{--<label for="professional_charge" class="col-md-2 control-label">Professional Charge</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<input class="form-control" name="professional_charge" type="text" id="professional_charge" value="{{ old('professional_charge', optional($service)->professional_charge) }}" minlength="1" placeholder="Enter professional charge here...">--}}
        {{--{!! $errors->first('professional_charge', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
{{--</div>--}}

