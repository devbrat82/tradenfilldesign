

<input class="form-control" name="id" type="hidden" id="title" value="{{ old('title', optional($user)->id) }}" minlength="1" maxlength="255" placeholder="Enter title here...">

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="published" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input name="name"  class="form-control" value="{{old('name',optional($user)->name)}}" minlength="1" placeholder="Enter  Name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Email</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($user)->email) }}" minlength="1" placeholder="Enter Email here..." @if($user['email'] !="") readonly @endif >
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Password</label>
    <div class="col-md-10">
        <input class="form-control" name="password" type="password" id="password" value="" minlength="1" placeholder="Leave it if you do not want to change...">
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>




{{--<div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">--}}
    {{--<label for="lastname" class="col-md-2 control-label">Last Name</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<input class="form-control" name="lastname" type="text" id="lastname" value="{{ old('lastname', optional($user)->lastname) }}" minlength="1" placeholder="Enter Last Name here...">--}}
        {{--{!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}

    <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
        <label for="role_id" class="col-md-2 control-label">Role</label>
        <div class="col-md-10">
            <select class="form-control" id="published" name="role_id">
                <option value="" style="display: none;" {{ old('status', optional($user)->role_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select role</option>
                @foreach ($roles as $key => $text)
                    <option value="{{ $key }}" {{ old('role_id', optional($user)->role_id ?: '1') == $key ? 'selected' : '' }}>
                        {{ $text }}
                    </option>
                @endforeach
            </select>

            {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

