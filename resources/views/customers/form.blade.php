

    <div class="row ct">
        <div class="col-md-6">
            <label for="documenttype" class=" control-label">Customer type</label>
            <select class="form-control" id="customer_type" name="customer_type" required>
                <option value="" style="display: none;" {{ old('customer_type', optional($customer)->customer_type ?: '') == '' ? 'selected' : '' }} disabled selected>Select customer type</option>
                @foreach ($customerType as $key => $text)
                    <option value="{{ $key }}" {{ old('customer_type', optional($customer)->customer_type) == $key ? 'selected' : '' }}>
                        {{ $text }}
                    </option>
                @endforeach
            </select>

            {!! $errors->first('customer_type', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6" id="cmpn" style="display:none">
            <label for="company_name" class="control-label">Company Name</label>
            <input class="form-control" name="company_name" type="text" id="company_name" value="{{ old('company_name', optional($customer)->company_name) }}" minlength="1" placeholder="Enter company name here...">
            {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>





    <div class="row ct">

        <div class="col-md-2" >
            <label for="prefix" class=" control-label">Prefix</label>

            <select class="form-control" id="prefix" name="prefix">

                    <option value="Mr">Mr</option>
                    <option value="Mrs." {{ old('prefix', optional($customer)->prefix) == 'Mrs.' ? 'selected' : '' }}>Mrs.</option>
                    <option value="Ms." {{ old('prefix', optional($customer)->prefix) == 'Ms.' ? 'selected' : '' }}>Ms.</option>
                    <option value="Miss" {{ old('prefix', optional($customer)->prefix) == 'Miss' ? 'selected' : '' }}>Miss</option>
                </select>
                {!! $errors->first('prefix', '<p class="help-block">:message</p>') !!}
     
        </div>        
        
        <div class="col-md-4">
            <label for="firstname" class=" control-label">First Name</label>
            <input class="form-control" name="firstname" type="text" required id="firstname" value="{{ old('firstname', optional($customer)->first_name) }}" minlength="1" placeholder="Enter firstname here...">
            {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
        </div>
    
        <div class="col-md-6">
            <label for="lastname" class=" control-label">Last Name</label>
            <input class="form-control" name="lastname" type="text" id="lastname" value="{{ old('lastname', optional($customer)->last_name) }}" minlength="1" placeholder="Enter lastname here...">
            {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}

        </div>
    </div>



    <div class="row ct">

        <div class="col-md-6">
            <label for="email" class=" control-label">Email</label>
            <input class="form-control" name="email" type="email" required id="email" value="{{ old('email', optional($customer)->email) }}" placeholder="Enter email here...">
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6">
            <label for="contact1" class=" control-label">contact1</label>
            <input class="form-control" name="contact1" type="text" required id="contact1" value="{{ old('contact1', optional($customer)->phone) }}" minlength="1" placeholder="Enter contact1 here...">
            {!! $errors->first('contact1', '<p class="help-block">:message</p>') !!}

        </div>
    </div>


    <div class="row ct">

        <div class="col-md-6">
            <label for="email" class=" control-label">Contact2</label>
            <input class="form-control" name="contact2" type="text" id="contact2" value="{{ old('contact2', optional($customer)->contact2) }}" minlength="1" placeholder="Enter contact2 here...">
            {!! $errors->first('contact2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6">
            <label for="add1" class=" control-label">Address1</label>
            <input class="form-control" name="add1" type="text" required id="add1" value="{{ old('add1', optional($customer)->add1) }}" minlength="1" placeholder="Enter add1 here...">
            {!! $errors->first('add1', '<p class="help-block">:message</p>') !!}

        </div>
    </div>
<div class="row ct">

    <div class="col-md-6">
        <label for="add2" class=" control-label">Address2</label>
        <input class="form-control" name="add2" type="text" id="add2" value="{{ old('add2', optional($customer)->add2) }}" minlength="1" placeholder="Enter add2 here...">
        {!! $errors->first('add2', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6">
        <label for="city" class="control-label">City</label>
        <input class="form-control" name="city" type="text" required id="city" value="{{ old('city', optional($customer)->city) }}" minlength="1" placeholder="Enter city here...">
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="row ct">

    <div class="col-md-6">
        <label for="state" class=" control-label">State</label>
        <input class="form-control" name="state" type="text" required id="state" value="{{ old('state', optional($customer)->state) }}" minlength="1" placeholder="Enter state here...">
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-6">
        <label for="country" class=" control-label">Country</label>
        <input class="form-control" name="country" type="text" id="country" required value="{{ old('country', optional($customer)->country) }}" minlength="1" placeholder="Enter country here...">
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="row ct">

    <div class="col-md-6">
        <label for="pincode" class="control-label">Pincode</label>
        <input class="form-control" name="pincode" type="text" id="pincode" required value="{{ old('pincode', optional($customer)->pin_code) }}" minlength="1" placeholder="Enter pincode here...">
        {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>

{{--<div class="form-group {{ $errors->has('registration_date') ? 'has-error' : '' }}">--}}
    {{--<label for="registration_date" class="col-md-2 control-label">Registration Date</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<input class="form-control" name="registration_date" type="text" id="registration_date" value="{{ old('registration_date', optional($customer)->registration_date) }}" minlength="1" placeholder="Enter registration date here...">--}}
        {{--{!! $errors->first('registration_date', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
{{--</div>--}}


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>


    $('#customer_type').change(function(){
       let val = $(this).val();
       if(val == 2)
       {
           $('#cmpn').css('display','block');
       }
       else{
           $('#cmpn').css('display','none');
       }
    })

    $(document).ready(function(){
        let val = $('#customer_type').val();
        if(val == 2)
        {
            $('#cmpn').css('display','block');
        }
        else{
            $('#cmpn').css('display','none');
        }
    })



</script>