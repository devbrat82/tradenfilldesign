@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4>Create New Post</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
            <a href="{{ route('posts.post.index') }}" title="Show All Post">
                                <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show All Post</button>
                            </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('posts.post.store') }}" accept-charset="UTF-8" id="create_post_form" name="create_post_form" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include ('posts.form', [
                                        'post' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection
@section('styles')
    <!-- WYSIWYG-->
    <link rel="stylesheet" href="{{ asset('theme-angle')}}/vendor/bootstrap-wysiwyg/css/style.css">
@endsection
@section('javascript')
    <script src='{{ asset('theme-angle')}}/vendor/tinymce/tinymce.min.js'></script>
    <script>
        tinymce.init({
            selector: '.tinymce',
            menubar:false,
            theme: 'modern',
            plugins: 'code fullscreen image link media  hr pagebreak nonbreaking anchor  insertdatetime advlist lists textcolor wordcount   imagetools     colorpicker textpattern',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>
@endsection


