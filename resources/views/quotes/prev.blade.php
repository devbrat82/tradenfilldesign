@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading clearfix">

            <span class="pull-left">
                <h4>Quote Preview</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('quotes.quote.sendQuoteMail', $data['id']) }}" title="Send Quote on Mail">
                    <button class="btn btn-labeled btn-green mb-2" type="button" style="margin-right: 10px">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Send Mail</button>
                </a>
                <a href="{{ route('quotes.quote.index') }}" title="Show All Quote">
                    <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show All Quote</button>
                </a>
            </div>

        </div><br>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <center><img src="{{ $data['logo'] }}" class="img-responsive" ></center>
                </div>
                {{--<div class="col-md-9 col-sm-9">--}}
                    {{--{!! $data['title'] !!}--}}
                {{--</div>--}}
            </div><br><br><br>
            @if($data['customerType'] ==1)
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    Date : {{ $data['date'] }}
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                  {{ $data['salutation'] }}
                </div>
            </div><br>
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <center><b>{!! $data['heading']  !!}</b></center>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <ul>
                    @foreach($data['content'] as $k => $v)
                        <li style="list-style-type:decimal">
                            {!! $v !!}
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div><br>
            {{--{{dd($data['package'])}}--}}
{{--            @if(($data['package']!='')&&($data['package']!=null))--}}
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h3>Package Includes :</h3>
                        <ul>
                            {!! $data['package']  !!}
                        </ul>
                    </div>
                </div><br>
            {{--@endif--}}
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <ul><li> {!! $data['fee']  !!}</li></ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <ul><li>Duration : {!! $data['process_time']  !!}</li></ul>
                </div>
            </div><br><br><br>
            @if($data['comment']!='')
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <ul><li>Comment : {!! $data['comment']  !!}</li></ul>
                    </div>
                </div><br>
            @endif

            <div class="row">

                <div class="row">
                    <div class="col-md-12 col-sm-12" style="bottom: 0px; position:absolute; text-align: center; font-weight:normal;">
                        <div  style="background-color: #da251d; color: #ffff; padding: 10px 15px 10px 15px">
                            <center><small>{!! $data['footer']  !!}</small></center>
                        </div >
                        <div style="background-color: black; color: #ffff; padding: 10px 15px 10px 15px">
                            <small>{!! "GST | ITR | IEC | TRADEMARK | FSSAI | NGO | ESI | PF | TAN | PAN | DIGITAL SIGNATURE" !!}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection