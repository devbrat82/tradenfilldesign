<div class="aside-inner">
    <nav class="sidebar" data-sidebar-anyclick-close="">
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
            <!-- START user info-->
            <li class="has-user-block">
                <div class="collapse" id="user-block">
                    <div class="item user-block">
                        <!-- User picture-->
                        <div class="user-block-picture">
                            <div class="user-block-status">
                                <img class="img-thumbnail rounded-circle" src="{{asset('theme-angle/img/admin.png')}}" alt="Avatar" width="60" height="60">
                                <div class="circle bg-success circle-lg"></div>
                            </div>
                        </div>
                        <!-- Name and Job-->
                        <div class="user-block-info">
                            @php $first_name= (\Auth::user()!=null)?\Auth::user()->name:''; @endphp
                            <span class="user-block-name">Hello, {{@$first_name}}</span>
                            {{--<span class="user-block-role">Designer</span>--}}
                        </div>
                    </div>
                </div>
            </li>
            <!-- END user info-->
            <!-- Iterates over all sidebar items-->
            <li class="nav-heading ">
                <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
            </li>
            <li class="{{ request()->is('admin/dashboard') ? 'active' : '' }}">
                <a href="{{route('admin.dashboard')}}" title="Dashboard" >
                    {{-- <div class="float-right badge badge-success">3</div>--}}
                    <em class="icon-speedometer"></em>
                    <span>Dashboard</span>
                </a>

            </li>
            <li class="{{ request()->is('admin/customers') ? 'active' : '' }}">
                <a href="{{route('customers.customer.index')}}" title="Customers" >
                    {{-- <div class="float-right badge badge-success">3</div>--}}
                    <em class="fa fa-group"></em>
                    <span>Customers</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/service_documents') ? 'active' : '' }}">
                <a href="{{route('service_documents.service_document.index')}}" title="Service Docs" >
                    {{-- <div class="float-right badge badge-success">3</div>--}}
                    <em class="fa fa-file"></em>
                    <span>Service Docs</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/quotes') ? 'active' : '' }}">
                <a href="{{route('quotes.quote.index')}}" title="Quotes" >
                    <em class="fa fa-quote-left"></em>
                    <span>Quotes</span>
                </a>
            </li>
            {{--<li class=" ">--}}
                {{--<a href="#user-access" title="Masters" data-toggle="collapse">--}}
                    {{--<em class="icon-user"></em>--}}
                    {{--<span data-localize="sidebar.nav.DASHBOARD">User Access</span>--}}
                {{--</a>--}}
                {{--<ul class="nav sidebar-subnav collapse" id="user-access">--}}
                    {{--<li class="sidebar-subnav-header">User Access</li>--}}
                    {{--<li class="@if(Request::path() === 'admin/roles') active @endif">--}}
                        {{--<a href="{{route('roles.roles.index')}}" title="Roles">--}}
                            {{--<em class="fa fa-star"></em>--}}
                            {{--<span >Roles</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="@if(Request::path() === 'admin/permissions') active @endif">--}}
                        {{--<a href="{{route('permissions.permission.index')}}" title="Permission">--}}
                            {{--<em class="fa fa-asterisk"></em>--}}
                            {{--<span >Permissions</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
			 @php $roleid= (\Auth::user()!=null)?\Auth::user()->role->id:''; @endphp
             @if($roleid == 1)
            <li class=" @if(Request::path() === 'admin/users') active @endif">
                <a href="{{route('admin.setting')}}" title="Settings">
                    <em class="fa fa-gears"></em>
                    <span>Settings</span>
                </a>
            </li>
			 @endif
           @if($roleid != 1)

            <li class=" ">
                <a href="#settings" title="Masters" data-toggle="collapse">
                    <em class="fa fa-gears"></em>
                    <span data-localize="sidebar.nav.DASHBOARD">Settings</span>
                </a>
                <ul class="nav sidebar-subnav collapse" id="settings">
                    <li class="sidebar-subnav-header">Settings</li>
                    <li class="@if(Request::path() === 'admin/reset-password') active @endif">
                        @if($roleid != 1)
                        <a href="{{route('admin.resetpassword')}}" title="Posts">
                            <em class="fa fa-lock"></em>
                            <span >Reset Password</span>
                        </a>
                        @endif
                    </li>

                </ul>
            </li>
         @endif
            <li class=" ">
                <a href="#masters" title="Masters" data-toggle="collapse">
                    <em class="icon-star"></em>
                    <span data-localize="sidebar.nav.DASHBOARD">Masters</span>
                </a>
                <ul class="nav sidebar-subnav collapse" id="masters">
                    <li class="sidebar-subnav-header">Masters</li>
                    <li class="child-menu @if(Request::path() === 'admin/acts') active @endif">
                        <a href="{{route('acts.act.index')}}" title="Main">
                            <em class="fa fa-user"></em>
                            <span>Main</span>
                        </a>
                    </li>
                    <li class="child-menu @if(Request::path() === 'admin/services') active @endif">
                        <a href="{{route('services.service.index')}}" title="Services">
                            <em class="fa fa-server"></em>
                            <span>Services</span>
                        </a>
                    </li>
                    <li class="child-menu @if(Request::path() === 'admin/formation_types') active @endif">
                        <a href="{{route('formation_types.formation_type.index')}}" title="Formation Types">
                            <em class="fa fa-user"></em>
                            <span>Formation Types</span>
                        </a>
                    </li>
                    <li class="child-menu @if(Request::path() === 'admin/document') active @endif">
                        <a href="{{route('documents.document.index')}}" title="Documents">
                            <em class="fa fa-file"></em>
                            <span>Documents</span>
                        </a>
                    </li>
                    <li class="child-menu @if(Request::path() === 'admin/document-category') active @endif">
                        <a href="{{route('document_categories.document_category.index')}}" title="Document Categories">
                            <em class="fa fa-file-o"></em>
                            <span>Document Types</span>
                        </a>
                    </li>
                    <li class="child-menu @if(Request::path() === 'admin/customer-types') active @endif">
                        <a href="{{route('customer_types.customer_type.index')}}" title="Customer Type">
                            <em class="fa fa-user"></em>
                            <span>Customer Type</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class=" @if(Request::path() === 'admin/users') active @endif">
                <a href="{{route('users.user.index')}}" title="Users">
                    <em class="fa fa-user"></em>
                    <span>Users</span>
                </a>
            </li>
            <li class=" ">
                <a href="{{route('admin.logout')}}" title="Logout">
                    <em class="icon-logout"></em>
                    <span >Logout</span>
                </a>
            </li>
        </ul>
        <!-- END sidebar nav-->
    </nav>
</div>