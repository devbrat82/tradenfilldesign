<?php

namespace App\Http\Controllers;

use App\Models\Act;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use App\Role;

class ActsController extends Controller
{

    /**
     * Display a listing of the acts.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $acts = Act::get();
       // $roles = Role::pluck('title','id');

        return view('acts.index', compact('acts'));
    }

    /**
     * Show the form for creating a new act.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('acts.create');
    }

    /**
     * Store a new act in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Act::create($data);

            return redirect()->route('acts.act.index')
                             ->with('success_message', 'Act was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified act.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $act = Act::findOrFail($id);

        return view('acts.show', compact('act'));
    }

    /**
     * Show the form for editing the specified act.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $act = Act::findOrFail($id);
        

        return view('acts.edit', compact('act'));
    }

    /**
     * Update the specified act in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
        try {
            
            $data = $this->getDataupdate($request,$id);
            
            $act = Act::findOrFail($id);
            $act->update($data);

            return redirect()->route('acts.act.index')
                             ->with('success_message', 'Act was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified act from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $act = Act::findOrFail($id);
            $act->delete();

            return redirect()->route('acts.act.index')
                             ->with('success_message', 'Act was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
        'name' => 'required|string|min:1|max:255|nullable|unique:acts,name',
        ];
        $data = $request->validate($rules);
        return $data;
    }

     protected function getDataupdate(Request $request,$id)
    {
        $rules = [
        'name' => 'required|string|min:1|max:255|nullable|unique:acts,name,'.$id,
        ];
        $data = $request->validate($rules);
        return $data;
    }

}
