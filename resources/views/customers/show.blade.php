@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            {{--<h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Customer' }}</h4>--}}
            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('customers.customer.index')}}">Customer</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Customer</li>
  </ol>
</nav>
            </div>
        </span>

        <div class="pull-right">

            <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon show-icon">
                                        <span class="dot dot1 show-dot show-dot1"></span>
                                        <span class="dot dot2 show-dot show-dot2"></span> 
                                        <span class="dot dot3 show-dot show-dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                                        <a href="{{ route('customers.customer.index') }}" class=" dropdown-item btn " title="Show All Customer">
                                            <span class="fa fa-list" aria-hidden="true"></span> Show All Customer
                                        </a>
                                        <a href="{{ route('customers.customer.create') }}" class="btn dropdown-item" title="Create New Customer">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Customer
                    </a>
                                        <a href="{{ route('customers.customer.edit', $customer->id ) }}" class="dropdown-item btn  btn-xs" title="Edit Customer">
                                            <span class="fa fa-pencil" aria-hidden="true"></span> Edit Customer
                                        </a>

                                        <button type="button" class="dropdown-item btn btn-xs" title="Delete Customer" data-toggle="modal" data-target="#deleteModel{{$customer->id}}">
                                            <span class="fa fa-trash" aria-hidden="true"></span> Delete Customer
                                        </button>
                                
                                    </div>
                                </div>
                            </form>


                       

        </div>
        <form method="POST" action="{!! route('customers.customer.destroy', $customer->id) !!}" accept-charset="UTF-8">
<input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>
    </div>

    <div class="panel-body bak-white">
        {{--<div class="row">--}}
            {{--<div class="col-md-2">--}}
                {{--<h4><span></span>  Basic Details</h4>--}}
                {{--<hr style="border-bottom: 2px solid #5d9cec">--}}
                {{----}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
                <h4><span></span>  Basic Details</h4>
                {{--<hr style="border-bottom: 2px solid #5d9cec">--}}

            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Customer Code </dt><dd>{{ $customer->cust_code }}</dd>
                <dt>Customer Type</dt>
                <dd>{{ $customer->Type->name }}</dd>
            </div>

            <div class="col-md-4 col-sm-4 col-lg-4">
            <dt>Customer Name</dt>
                <dd>{{ $customer->prefix }} {{ $customer->first_name }} {{ $customer->last_name }}</dd>
                <dt>Company Name</dt>
                <dd>{{ $customer->company_name!=''?$customer->company_name:'NA' }}</dd>
            </div>

        </div>

        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
                <h4><span></span>  Contact Details</h4>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt><i class="fa fa-envelope"></i> Email</dt>
                <dd>{{ $customer->email }}</dd>
                <dt><i class="fa fa-phone"></i> Alternate Contact</dt>
                <dd>{{ $customer->contact2!=''?$customer->contact2:'NA' }}</dd> 
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt> Contact Number</dt>
                <dd>{{ $customer->phone }}</dd>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <h4><span></span>  Address Details</h4>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Address1</dt>
                <dd>{{ $customer->add1 }}</dd>
                <dt>City</dt>
                <dd>{{ $customer->city }}</dd>
                <dt>Country</dt>
                <dd>{{ $customer->country }}</dd>
            </div>

            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Address2</dt>
                <dd>{{ $customer->add2!=''?$customer->add2:'NA' }}</dd>
                <dt>State</dt>
                <dd>{{ $customer->state }}</dd>
                <dt>Pincode</dt>
                <dd>{{ $customer->pin_code }}</dd>
            </div>

        </div>
       
     
        
    </div>
</div>

@endsection