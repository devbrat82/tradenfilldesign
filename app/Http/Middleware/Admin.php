<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //print auth()->user()->role_id; die;
        if(auth()->user() && (auth()->user()->role_id == 1 || auth()->user()->role_id == 2) ){
            return $next($request);
        }
        return redirect('admin/login')->with('error','You have not admin access');
    }
}
