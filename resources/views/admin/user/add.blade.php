@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="">Hello {{ !empty($user->name) ? $user->name : 'Users' }}!</h4>
                <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('users.user.index')}}">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">New User</li>
  </ol>
</nav>
                </div>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('users.user.index') }}" class="btn btn-primary" title="Show All Users">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>

                {{--<a href="{{ route('countries.country.create') }}" class="btn btn-success" title="Create New Countries">--}}
                    {{--<span class="fa fa-plus" aria-hidden="true"></span>--}}
                {{--</a>--}}

            </div>
        </div>

        <div class="panel-body bak-white">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('users.user.store') }}" id="add_user_form" name="add_user_form" accept-charset="UTF-8" class="form-horizontal">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="POST">
                @include ('admin.user.form', [
                                            'user' => null,
                                          ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection