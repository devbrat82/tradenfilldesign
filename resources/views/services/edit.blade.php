@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class=" ">{{ !empty($service->title) ? $service->title : 'Service' }}</h4>
                <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('services.service.index')}}">Services</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Services</li>
  </ol>
</nav>
                </div>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">
                
                <div class="dropdown">
                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="dot-icon show-icon">
                        <span class="dot dot1 show-dot show-dot1"></span>
                        <span class="dot dot2 show-dot show-dot2"></span> 
                        <span class="dot dot3 show-dot show-dot3"></span>
                    </div>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                    <a href="{{ route('services.service.index') }}" class="btn dropdown-item" title="Show All Service">
                    <span class="fa fa-list" aria-hidden="true"></span> Show All Service
                </a>
                <a href="{{ route('services.service.create') }}" class="btn dropdown-item" title="Create New Service">
                    <span class="fa fa-plus" aria-hidden="true"></span> Create New Service
                </a>
                                
                    </div>
                </div>





            </div>
        </div>

        <div class="panel-body bak-white">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('services.service.update', $service->id) }}" id="edit_service_form" name="edit_service_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('services.form', [
                                        'service' => $service,
                                        'act' => $acts
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection