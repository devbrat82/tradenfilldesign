<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\Document;
use App\Models\Service;
use App\Models\ServiceDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ServicesController extends Controller
{

    /**
     * Display a listing of the services.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $services = Service::get();

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new service.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {

        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        return view('services.create', compact('acts'));
    }

    /**
     * Store a new service in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Service::create($data);

            return redirect()->route('services.service.index')
                             ->with('success_message', 'Service was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified service.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        $acts = Act::OrderBy('name','ASC')->pluck('name','id');

        return view('services.edit', compact(['service','acts']));
    }

    /**
     * Update the specified service in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
//        try {
            
            $data = $this->getData($request,$id);
            
            $service = Service::findOrFail($id);
            $service->update($data);

            return redirect()->route('services.service.index')
                             ->with('success_message', 'Service was successfully updated!');

//        } catch (Exception $exception) {
//            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
//             if($exception->validator){
//                    $error_messages = $exception->validator;
//              }
//            return back()->withInput()
//                         ->withErrors($error_messages);
//        }
    }

    /**
     * Remove the specified service from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $service = Service::findOrFail($id);
            $service->delete();

            return redirect()->route('services.service.index')
                             ->with('success_message', 'Service was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request,$id=null)
    {
        $rules = [
            'title' => 'required|string|min:1|max:255|unique:services,title,'.$id,
            'act' => 'integer|required',
            'package' => 'string|nullable',

        ];
        
        $data = $request->validate($rules);


        return $data;
    }

    public function servicedocs($serviceId)
    {

        $services = Service::orderBy('title','asc')->pluck('title', 'id');
        $associated_docs = ServiceDocument::where('service_id', $serviceId)->pluck('document_id');
        $docs  = Document::select('id', 'name')->orderBy('name', 'asc')->get();
        $selected_docs = $docs->whereIn('id', $associated_docs->toArray());
        $unselected_docs = $docs->whereNotIn('id', $associated_docs->toArray());

        return view('services.edit_documents', compact('serviceId', 'services', 'selected_docs', 'unselected_docs'));
    }

    public function storeDocs(Request $request, $serviceId){
        ServiceDocument::where('service_id', $serviceId)->delete();
        if(is_array($request['docs_ids'])) {
            if (count($request['docs_ids'])) {
                foreach ($request['docs_ids'] as $doc) {
                    $obj = new ServiceDocument();
                    $obj->service_id = $serviceId;
                    $obj->document_id = $doc;
                    $obj->save();
                }
            }
        }
        return ['message' => 'Records updated successfully.'];
    }

}
