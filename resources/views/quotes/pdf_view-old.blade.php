<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
    </head>
    <body style="font-family:'Arial Rounded MT Bold'; font-weight:700">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <center><img src="{{ $data['logo'] }}" class="img-responsive" ></center>
            </div>
            {{--<div class="col-md-9 col-sm-9">--}}
            {{--{!! $data['title'] !!}--}}
            {{--</div>--}}
        </div><br><br><br>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    Date : {{ $data['date'] }}
                </div>
            </div><br>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    {{ $data['salutation'] }}
                </div>
            </div><br>
        @if($data['customerType'] ==2)
            <div class="row">
                <div class="col-md-12 col-sm-12">
                   Company : {{ $data['company'] }}
                </div>
            </div><br>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <center><b>{!! $data['heading']  !!}</b></center>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <ul>
                    @foreach($data['content'] as $k => $v)
                        <li style="list-style-type:decimal">
                            {!! $v !!}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <ul><li> {!! $data['fee']  !!}</li></ul>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <ul><li>Duration : {!! $data['process_time']  !!}</li></ul>
            </div>
        </div><br>
        @if($data['comment']!='')
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <ul><li>Comment : {!! $data['comment']  !!}</li></ul>
            </div>
        </div><br>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12" style="bottom: 0px; position:absolute; text-align: center; font-weight:normal;">
                <div  style="background-color: #f34b2a; color: #ffff; padding: 10px 15px 10px 15px">
                    <center><small>{!! $data['footer']  !!}</small></center>
                </div >
            </div>
        </div>
    </body>
</html>