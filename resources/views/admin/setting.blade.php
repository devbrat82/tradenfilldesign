@extends('layouts.app')

@section('content')


<!-- CSS -->
<link href='{{asset("select2-develop/dist/css/select2.min.css")}}' rel='stylesheet' type='text/css'>
<style>

/* body .select2-container {
    max-width: 100%;
    display: block;
    width:100% !important;
}  */
.table .table-component__table__body tr:hover {
    box-shadow: 0 5px 15px 2px rgba(0,0,0,.03);
}
.table .table-component__table__body tr{
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
}
.table .table-component__table__body td {
    font-size: 15px;
    padding: 25px 15px;
    height: 80px;
    background-color: #f9fbff!important;
 ;
}
 .tax-table .compound-tax {
    display: inline;
    padding: 2px 8px;
    font-size: 10px;
    line-height: 15px;
    background: #fed7d7;
    color: #9b2c2c;
}
.select2-container {
    display:block;
}
#exampleModal{
    overflow:hidden;
}
#exampleModal .modal-header:before {
    content: "";
    position: absolute;
    width: 100%;
    top: 0;
    left: 0;
    height: 10px;
    background: #5851d8;
}
#exampleModal .overlay-wrap{
    position: fixed;
    left: -13px;
    right: 0;
    bottom: 0;
    background: #00000069;
    top: 0;
    width: 100%;
}
.col-form-label{
    text-align: right;
    color:#000;
}
.page-header {
    padding-bottom: 0rem;
    margin: 2.625rem 0 0.3125rem;
    border-bottom: none;
}
.page-footer{
    margin: 1.625rem 1px 2.3125rem;
}
.modal-title {
    margin-bottom: 0;
    line-height: 1.52857;
    font-size: 17.5px;
    font-weight: 500;
    color: #000;
    margin: 0;
}
#exampleModal .modal-footer .btn.btn-secondary{
    color:#5851d8
}
#exampleModal .modal-footer .btn.btn-secondary, #exampleModal .modal-footer .btn.btn-primary{
    border: 1px solid #5851d8;
    height: 40px;
    padding: 6px 20px;
    font-size: 14px;
    font-weight: 500;
    text-align: center;
    display: flex;
    align-items: center;
    white-space: nowrap;
    line-height: 14px;
}
#exampleModal .modal-footer .btn.btn-secondary:hover ,
#exampleModal .modal-footer  .btn.btn-primary:hover ,
#exampleModal .modal-footer  .btn.btn-secondary:active,
#exampleModal .modal-footer  .btn.btn-primary:active{
     color: #fff !important;
     background-color: #3a32d1 !important;
     border-color: #3a32d1 !important;
 }
.modal-content{
    border-radius: 0;
}
#exampleModal .modal-header {
    border-bottom: 1px solid #e9ecef;

}
#exampleModal .modal-footer{
    border-top: 1px solid #e9ecef;

}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #dddddd;
}
.base-text-area.text-area-field:focus,.base-input .input-field:focus{
    border: 1px solid #817ae3;
}
.card.setting-card .page-header .page-title {
    margin: 0;
    padding-bottom: 10px;
    font-weight: 500;
    font-size: 17.5px;
    line-height: 21px;
    letter-spacing: .1px;
    color: #000;
}
.card.setting-card .page-header .page-sub-title{
    max-width: 480px;
    font-size: 14px;
    line-height: 21px;
    color: #a5acc1;
    margin-bottom: 30px;
}
.base-button{
    height: 40px;
    padding: 6px 20px;
    font-size: 14px;
    font-weight: 500;
    text-align: center;
    display: flex;
    align-items: center;
    white-space: nowrap;
    line-height: 14px;
    color:#fff;
}
.add-new-tax{
    color: #5851d8;
    border-color: #5851d8;}
.add-new-tax:active:focus{
    box-shadow: 0 0 0 0.2rem rgba(88,81,216,.5) !important ;
}
.base-button:hover,.base-button:active{
    color: #fff !important;
    background-color: #3a32d1  !important;
    border-color:#3a32d1  !important;
}
.select2-selection.select2-selection--single{
    width: 100%;
    height: 40px;
    padding: 8px 13px;
    text-align: left;
    /* background: #fff; */
    border: 1px solid #ebf1fa;
    box-sizing: border-box;
    border-radius: 5px;
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
}
#select2-selUser-container{
    background: #fff;
    /* border: 1px solid #ebf1fa; */
    box-sizing: border-box;
    border-radius: 5px;
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 21px;
    width: 100%;
}
.select2-dropdown.select2-dropdown--below{
    border: 1px solid #ebf1fa;
    overflow:hidden;
}
.select2-dropdown.select2-dropdown--above{
    overflow:hidden;
    border: 1px solid #ebf1fa;
}
.switch-label {
	 position: relative;
	 display: block;
	 width: 40px;
	 height: 20px;
	 cursor: pointer;
	 -webkit-tap-highlight-color: transparent;
     transform: translate3d(0, 0, 0);
     top: -23px;
    left: -3px;
}
#_lnag3em22,#_6adoknhvl,#_yxoqjbvr2,#_idms2c9a1,#_eb4oarp3o{
    opacity:0;
}
 .switch-label:before {
	 content: "";
	 position: relative;
	 top: 0px;
	 left: 0px;
	 width: 40px;
     height: 14px;
     border: 1px solid #b9c1d1;
	 display: block;
     background: #a5acc1;
	 border-radius: 8px;
	 /* transition: background 0.2s ease; */
}
 .switch-label span {
	 position: absolute;
	 top: -2px !important;
	 left: 0;
	 width: 20px;
	 height: 20px;
	 display: block;
	 background: white;
	 border-radius: 10px;
	 box-shadow: 0 3px 8px rgba(154, 153, 153, .5);
	 transition: all 0.2s ease;
}
 .switch-label span:before {
	 content: "";
	 position: absolute;
	 display: block;
	 margin: -18px;
	 width: 56px;
	 height: 56px;
	 background: rgba(79, 46, 220, .5);
	 border-radius: 50%;
	 transform: scale(0);
	 opacity: 1;
	 pointer-events: none;
}
 #_lnag3em22:checked + .switch-label, #_eb4oarp3o:checked + .switch-label, #_6adoknhvl:checked + .switch-label , #_yxoqjbvr2:checked + .switch-label , #_idms2c9a1:checked + .switch-label {
     background: rgba(88,81,216,.2);
     height:14px;
     border-radius:10px;
}
 #_lnag3em22:checked + .switch-label span,#_eb4oarp3o:checked + .switch-label span, #_6adoknhvl:checked + .switch-label span, #_yxoqjbvr2:checked + .switch-label span, #_idms2c9a1:checked + .switch-label span {

	 background: #5851d8;
	 transform: translateX(20px);
	 transition: all 0.2s cubic-bezier(0.8, 0.4, 0.3, 1.25), background 0.15s ease;
	 box-shadow: 0 3px 8px rgba(79, 46, 220, .2);
}
 #_lnag3em22:checked + .switch-label:before,#_eb4oarp3o:checked + .switch-label:before , #_6adoknhvl:checked + .switch-label:before , #_yxoqjbvr2:checked + .switch-label:before , #_idms2c9a1:checked + .switch-label:before {
	 transform: scale(1);
	 opacity: 0;
	 transition: all 0.4s ease;
}
 
</style>
    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="">Settings</h4>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>

                        <li class="breadcrumb-item active" aria-current="page">Settings</li>
                    </ol>
                </nav>
            </div>
            {{--<div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('countries.country.index') }}" class="btn btn-primary" title="Show All Users">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('countries.country.create') }}" class="btn btn-success" title="Create New Countries">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>--}}
        </div>





        <div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="far fa-user"></i> Account Settings</a>
      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="far fa-building"></i> Company Information</a>
      <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fas fa-cog"></i> Preferences</a>
      {{--<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="far fa-check-circle"></i>   Tax Type</a>--}}
      <a class="nav-link" id="v-pills-mail-tab" data-toggle="pill" href="#v-pills-mail" role="tab" aria-controls="v-pills-mail" aria-selected="false"><i class="fas fa-envelope"></i> Mail Configuration</a>
      <a class="nav-link" id="v-pills-notifications-tab" data-toggle="pill" href="#v-pills-notifications" role="tab" aria-controls="v-pills-notifications" aria-selected="false"><i class="far fa-bell"></i> Notifications</a>
    </div>
  </div>
  <div class="col-9 ">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
      @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
      <!-- <form method="POST" action="{{ route('admin.password.update') }}" id="edit_user_form" name="edit_user_form" accept-charset="UTF-8" class="form-horizontal">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                <input name="user_id" type="hidden" value="{{$user->id}}">



                <input class="form-control" name="id" type="hidden" id="title" value="{{ old('title', optional($user)->id) }}" minlength="1" maxlength="255" placeholder="Enter title here...">

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">
                        <input class="form-control" name="password" type="password" id="password" value="" minlength="1" placeholder="Type new password">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>


                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Confirm Password</label>
                    <div class="col-md-10">
                        <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" value="" minlength="1" placeholder="Retype new password">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Reset password">
                    </div>
                </div>
            </form> -->


           <form method="POST" action="{{ route('admin.setting.account') }}" id="edit_user_form" name="edit_user_form" enctype="multipart/form-data">
                {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            <input name="user_id" type="hidden" value="{{$user->id}}">
                <div class="card setting-card">
                    <div class="page-header">
                        <h3 class="page-title">Account Settings</h3>
                        <p class="page-sub-title">You can update your name, email &amp; password using the form below.</p>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <label class="input-label">Profile Picture</label>
                            <div id="pick-avatar" class="image-upload-box avatar-upload">
                                <!-- <div class="overlay">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="camera" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="white-icon svg-inline--fa fa-camera fa-w-16"><path fill="currentColor" d="M512 144v288c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V144c0-26.5 21.5-48 48-48h88l12.3-32.9c7-18.7 24.9-31.1 44.9-31.1h125.5c20 0 37.9 12.4 44.9 31.1L376 96h88c26.5 0 48 21.5 48 48zM376 288c0-66.2-53.8-120-120-120s-120 53.8-120 120 53.8 120 120 120 120-53.8 120-120zm-32 0c0 48.5-39.5 88-88 88s-88-39.5-88-88 39.5-88 88-88 88 39.5 88 88z" class="">
                                    </path>
                                    </svg>
                                </div> -->
                                <img src="{{asset('/images/user.png')}}" class="preview-logo" id="preview-image">
                            </div>
                        </div>
                        <div class="avatar-cropper">
                         <input accept="image/png, image/gif, image/jpeg, image/bmp, image/x-icon" id="image-file" type="file" name="image" class="avatar-cropper-img-input">
                            </div>
                            </div> 
                        <div class="row">
                        <div class="col-md-6 mb-4 form-group">
                            <label class="input-label">Name</label> 
                            <div class="base-input">
                            <input name="name" tabindex="" placeholder="Name" autocomplete="on" type="text" class="input-field" value="{{ old('name', optional($user)->name) }}"> 
                            </div> 
                            </div> 
                            <div class="col-md-6 mb-4 form-group">
                            <label class="input-label">Email</label>
                             <div class="base-input">
                             <input name="email" tabindex="" placeholder="Email" autocomplete="on" type="text" class="input-field" value="{{ old('email', optional($user)->email) }}"> 
                             </div>
                             </div> <div class="col-md-6 mb-4 form-group">
                             <label class="input-label">Password</label> 
                             <div class="base-input"> 
                             <input name="password" tabindex="" placeholder="Password" autocomplete="new-password" type="password" class="input-field"> 
                             </div> 
                             </div> 
                             <div class="col-md-6 mb-4 form-group">
                             <label class="input-label">Confirm Password</label>
                              <div class="base-input">
                              <input name="password_confirmation" tabindex="" placeholder="Confirm Password" autocomplete="nope" type="password" class="input-field"> 
                              </div>
                              </div>
                              </div> 
                              <div class="row  mb-4">
                              <div class="col-md-12 input-group">
                              <button type="submit" class="base-button btn btn-primary ">

                              Save
                                </button></div></div></div></form>



      
      
      </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
      
      
      <form action="">
        <div class="card setting-card">
            <div class="page-header">
                <h3 class="page-title">Company info</h3>
                <p class="page-sub-title">  Information about your company that will be displayed on invoices, estimates and other documents created by Crater.
                </p>
            </div> 
            <div class="row mb-4">
                <div class="col-md-6">
                    <label class="input-label">Company Logo</label> 
                    <div id="pick-avatar" class="image-upload-box">
                        <div class="overlay">
                            <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="camera" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="white-icon svg-inline--fa fa-camera fa-w-16"><path fill="currentColor" d="M512 144v288c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V144c0-26.5 21.5-48 48-48h88l12.3-32.9c7-18.7 24.9-31.1 44.9-31.1h125.5c20 0 37.9 12.4 44.9 31.1L376 96h88c26.5 0 48 21.5 48 48zM376 288c0-66.2-53.8-120-120-120s-120 53.8-120 120 53.8 120 120 120 120-53.8 120-120zm-32 0c0 48.5-39.5 88-88 88s-88-39.5-88-88 39.5-88 88-88 88 39.5 88 88z" class="">
                                </path>
                            </svg> -->
                        </div> <img src="{{asset('/images/company.png')}}" class="preview-logo" id="company-preview-image">
                    </div>
                </div> 
                <div class="avatar-cropper">
                    <input accept="image/png, image/gif, image/jpeg, image/bmp, image/x-icon" type="file" class="avatar-cropper-img-input" id="company-image-file">
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6 mb-4">
                    <label class="input-label">Company Name</label> 
                    <span class="text-danger"> * </span> 
                    <div class="base-input">
                         <input name="" tabindex="" placeholder="Company Name" autocomplete="on" type="text" class="input-field"> 
                    </div> 
                </div> 
                <div class="col-md-6 mb-4">
                    <label class="input-label">Phone</label>
                    <div class="base-input">
                        <input name="" tabindex="" placeholder="Phone" autocomplete="on" type="text" class="input-field"> 
                    </div>
                </div> 
                <div class="col-md-6 mb-4">
                    <label class="input-label">Country</label>
                        <span class="text-danger"> * </span> 
                        <!-- <div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect">
                            <div class="multiselect__select">
                            </div> 
                            <div class="multiselect__tags">
                                <div class="multiselect__tags-wrap" style="display: none;">
                                </div>
                                <div class="multiselect__spinner" style="display: none;">
                                </div>
                                <input name="" placeholder="Select Country" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;">
                                <span class="multiselect__single">India</span>
                            </div> 
                            <div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;">
                                <ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;">
                                <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight">
                                    <span>Afghanistan</span></span>
                                </li>
                                <li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option">
                                    <span>Albania</span></span> 
                                </li>
                                <li id="null-2" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Algeria</span></span>
                                </li>
                                <li id="null-3" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>American Samoa</span></span> 
                                </li>
                                <li id="null-4" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Andorra</span></span> 
                                </li>
                                <li id="null-5" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Angola</span></span>
                                </li>
                                <li id="null-6" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Anguilla</span></span>
                                </li>
                                <li id="null-7" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Antarctica</span></span> 
                                </li>
                                <li id="null-8" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Antigua And Barbuda</span></span> 
                                </li>
                                <li id="null-9" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Argentina</span></span> 
                                </li>
                                <li id="null-10" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Armenia</span></span> 
                                </li><li id="null-11" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Aruba</span></span> 
                                </li>
                                <li id="null-12" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Australia</span></span> 
                                </li>
                                <li id="null-13" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Austria</span></span> 
                                </li><li id="null-14" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Azerbaijan</span></span> 
                                </li>
                                <li id="null-15" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bahamas The</span></span> 
                                </li>
                                <li id="null-16" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option">
                                    <span>Bahrain</span></span> 
                                </li>
                                <li id="null-17" role="option" class="multiselect__element">
                                    <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bangladesh</span></span> 
                                </li>
                                <li id="null-18" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Barbados</span></span> 
                                </li>
                                <li id="null-19" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Belarus</span></span> 
                                </li>
                                <li id="null-20" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Belgium</span></span> 
                                </li>
                                <li id="null-21" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Belize</span></span> 
                                </li>
                                <li id="null-22" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Benin</span></span> 
                                </li>
                                <li id="null-23" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bermuda</span></span> 
                                </li>
                                <li id="null-24" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bhutan</span></span>
                                </li>
                                <li id="null-25" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bolivia</span></span> </li><li id="null-26" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bosnia and Herzegovina</span></span> <</li><li id="null-27" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Botswana</span></span> <</li><li id="null-28" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bouvet Island</span></span> <</li><li id="null-29" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Brazil</span></span> </li><li id="null-30" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>British Indian Ocean Territory</span></span> </li><li id="null-31" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Brunei</span></span> </li><li id="null-32" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bulgaria</span></span> </li><li id="null-33" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Burkina Faso</span></span> </li><li id="null-34" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Burundi</span></span> </li><li id="null-35" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cambodia</span></span> </li><li id="null-36" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cameroon</span></span> </li><li id="null-37" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Canada</span></span> </li><li id="null-38" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cape Verde</span></span> </li><li id="null-39" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cayman Islands</span></span> </li><li id="null-40" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Central African Republic</span></span> </li><li id="null-41" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Chad</span></span> </li><li id="null-42" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Chile</span></span> </li><li id="null-43" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>China</span></span> </li><li id="null-44" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Christmas Island</span></span> </li><li id="null-45" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cocos (Keeling) Islands</span></span> </li><li id="null-46" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Colombia</span></span> </li><li id="null-47" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Comoros</span></span> </li><li id="null-48" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Congo</span></span> </li><li id="null-49" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Congo The Democratic Republic Of The</span></span> </li><li id="null-50" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cook Islands</span></span> </li><li id="null-51" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Costa Rica</span></span> </li><li id="null-52" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cote D Ivoire (Ivory Coast)</span></span> </li><li id="null-53" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Croatia (Hrvatska)</span></span> </li><li id="null-54" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cuba</span></span> </li><li id="null-55" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Cyprus</span></span> </li><li id="null-56" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Czech Republic</span></span> </li><li id="null-57" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Denmark</span></span> </li><li id="null-58" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Djibouti</span></span> </li><li id="null-59" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Dominica</span></span> </li><li id="null-60" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Dominican Republic</span></span> </li><li id="null-61" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>East Timor</span></span> </li><li id="null-62" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ecuador</span></span> </li><li id="null-63" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Egypt</span></span> </li><li id="null-64" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>El Salvador</span></span> </li><li id="null-65" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Equatorial Guinea</span></span> </li><li id="null-66" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Eritrea</span></span> </li><li id="null-67" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Estonia</span></span> </li><li id="null-68" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ethiopia</span></span> </li><li id="null-69" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>External Territories of Australia</span></span> </li><li id="null-70" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Falkland Islands</span></span> </li><li id="null-71" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Faroe Islands</span></span> </li><li id="null-72" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Fiji Islands</span></span> </li><li id="null-73" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Finland</span></span> </li><li id="null-74" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>France</span></span> </li><li id="null-75" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>French Guiana</span></span> </li><li id="null-76" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>French Polynesia</span></span> </li><li id="null-77" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>French Southern Territories</span></span> 
                               </li><li id="null-78" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Gabon</span></span> </li><li id="null-79" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Gambia The</span></span> </li><li id="null-80" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Georgia</span></span> </li><li id="null-81" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Germany</span></span> </li><li id="null-82" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ghana</span></span> </li><li id="null-83" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Gibraltar</span></span> </li><li id="null-84" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Greece</span></span> </li><li id="null-85" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Greenland</span></span> </li><li id="null-86" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Grenada</span></span> </li><li id="null-87" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guadeloupe</span></span> </li><li id="null-88" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guam</span></span> </li><li id="null-89" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guatemala</span></span> </li><li id="null-90" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guernsey and Alderney</span></span> </li><li id="null-91" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guinea</span></span> </li><li id="null-92" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guinea-Bissau</span></span> </li><li id="null-93" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guyana</span></span> </li><li id="null-94" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Haiti</span></span> </li><li id="null-95" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Heard and McDonald Islands</span></span> </li><li id="null-96" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Honduras</span></span> </li><li id="null-97" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Hong Kong S.A.R.</span></span> </li><li id="null-98" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Hungary</span></span> </li><li id="null-99" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Iceland</span></span> </li><li id="null-100" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--selected"><span>India</span></span> </li><li id="null-101" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Indonesia</span></span> </li><li id="null-102" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Iran</span></span> </li><li id="null-103" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Iraq</span></span> </li><li id="null-104" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ireland</span></span> </li><li id="null-105" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Israel</span></span> </li><li id="null-106" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Italy</span></span> </li><li id="null-107" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Jamaica</span></span> </li><li id="null-108" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Japan</span></span> </li><li id="null-109" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Jersey</span></span> </li><li id="null-110" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Jordan</span></span> </li><li id="null-111" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kazakhstan</span></span> </li><li id="null-112" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kenya</span></span> </li><li id="null-113" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kiribati</span></span> </li><li id="null-114" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Korea North</span></span> </li><li id="null-115" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Korea South</span></span> </li><li id="null-116" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kuwait</span></span> </li><li id="null-117" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kyrgyzstan</span></span> </li><li id="null-118" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Laos</span></span> </li><li id="null-119" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Latvia</span></span> </li><li id="null-120" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Lebanon</span></span> </li><li id="null-121" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Lesotho</span></span> </li><li id="null-122" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Liberia</span></span> </li><li id="null-123" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Libya</span></span> </li><li id="null-124" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Liechtenstein</span></span> </li><li id="null-125" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Lithuania</span></span> </li><li id="null-126" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Luxembourg</span></span> </li><li id="null-127" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Macau S.A.R.</span></span> </li><li id="null-128" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Macedonia</span></span> </li><li id="null-129" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Madagascar</span></span> </li><li id="null-130" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Malawi</span></span> </li><li id="null-131" role="option" class="multiselect__element">
                                <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Malaysia</span></span></li><li id="null-132" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Maldives</span></span> </li><li id="null-133" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mali</span></span> </li><li id="null-134" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Malta</span></span> </li><li id="null-135" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Man (Isle of)</span></span> </li><li id="null-136" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Marshall Islands</span></span> </li><li id="null-137" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Martinique</span></span> </li><li id="null-138" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mauritania</span></span> </li><li id="null-139" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mauritius</span></span> </li><li id="null-140" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mayotte</span></span> </li><li id="null-141" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mexico</span></span> </li><li id="null-142" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Micronesia</span></span> </li><li id="null-143" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Moldova</span></span> </li><li id="null-144" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Monaco</span></span> </li><li id="null-145" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mongolia</span></span> </li><li id="null-146" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Montserrat</span></span> </li><li id="null-147" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Morocco</span></span> </li><li id="null-148" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mozambique</span></span> </li><li id="null-149" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Myanmar</span></span> </li><li id="null-150" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Namibia</span></span> </li><li id="null-151" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Nauru</span></span> </li><li id="null-152" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Nepal</span></span> </li><li id="null-153" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Netherlands Antilles</span></span> </li><li id="null-154" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Netherlands The</span></span> </li><li id="null-155" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>New Caledonia</span></span> </li><li id="null-156" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>New Zealand</span></span> </li><li id="null-157" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Nicaragua</span></span> </li><li id="null-158" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Niger</span></span> </li><li id="null-159" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Nigeria</span></span> </li><li id="null-160" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Niue</span></span> </li><li id="null-161" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Norfolk Island</span></span> </li><li id="null-162" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Northern Mariana Islands</span></span> </li><li id="null-163" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Norway</span></span> </li><li id="null-164" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Oman</span></span> </li><li id="null-165" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Pakistan</span></span> </li><li id="null-166" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Palau</span></span> </li><li id="null-167" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Palestinian Territory Occupied</span></span> </li><li id="null-168" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Panama</span></span> </li><li id="null-169" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Papua new Guinea</span></span> </li><li id="null-170" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Paraguay</span></span> </li><li id="null-171" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Peru</span></span> </li><li id="null-172" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Philippines</span></span> </li><li id="null-173" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Pitcairn Island</span></span> </li><li id="null-174" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Poland</span></span> </li><li id="null-175" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Portugal</span></span> </li><li id="null-176" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Puerto Rico</span></span> </li><li id="null-177" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Qatar</span></span> </li><li id="null-178" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Reunion</span></span> </li><li id="null-179" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Romania</span></span> </li><li id="null-180" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Russia</span></span> </li><li id="null-181" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Rwanda</span></span> </li><li id="null-182" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saint Helena</span></span> </li><li id="null-183" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saint Kitts And Nevis</span></span> </li><li id="null-184" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option">
                                <span>Saint Lucia</span></span> </li><li id="null-185" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saint Pierre and Miquelon</span></span> </li><li id="null-186" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saint Vincent And The Grenadines</span></span> </li><li id="null-187" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Samoa</span></span> </li><li id="null-188" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>San Marino</span></span> </li><li id="null-189" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sao Tome and Principe</span></span> </li><li id="null-190" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saudi Arabia</span></span> </li><li id="null-191" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Senegal</span></span> </li><li id="null-192" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Serbia</span></span> </li><li id="null-193" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Seychelles</span></span> </li><li id="null-194" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sierra Leone</span></span> </li><li id="null-195" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Singapore</span></span> </li><li id="null-196" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Slovakia</span></span> </li><li id="null-197" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Slovenia</span></span> </li><li id="null-198" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Smaller Territories of the UK</span></span> </li><li id="null-199" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Solomon Islands</span></span> </li><li id="null-200" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Somalia</span></span> </li><li id="null-201" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>South Africa</span></span> </li><li id="null-202" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>South Georgia</span></span> </li><li id="null-203" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>South Sudan</span></span> </li><li id="null-204" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Spain</span></span> </li><li id="null-205" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sri Lanka</span></span> </li><li id="null-206" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sudan</span></span> </li><li id="null-207" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Suriname</span></span> </li><li id="null-208" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Svalbard And Jan Mayen Islands</span></span> </li><li id="null-209" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Swaziland</span></span> </li><li id="null-210" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sweden</span></span> </li><li id="null-211" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Switzerland</span></span> </li><li id="null-212" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Syria</span></span> </li><li id="null-213" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Taiwan</span></span> </li><li id="null-214" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tajikistan</span></span> </li><li id="null-215" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tanzania</span></span> </li><li id="null-216" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Thailand</span></span> </li><li id="null-217" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Togo</span></span> </li><li id="null-218" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tokelau</span></span> </li><li id="null-219" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tonga</span></span> </li><li id="null-220" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Trinidad And Tobago</span></span> </li><li id="null-221" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tunisia</span></span> </li><li id="null-222" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Turkey</span></span> </li><li id="null-223" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Turkmenistan</span></span> </li><li id="null-224" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Turks And Caicos Islands</span></span> </li><li id="null-225" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tuvalu</span></span> </li><li id="null-226" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Uganda</span></span> </li><li id="null-227" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ukraine</span></span> </li><li id="null-228" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>United Arab Emirates</span></span> </li><li id="null-229" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>United Kingdom</span></span> </li><li id="null-230" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>United States</span></span> </li><li id="null-231" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>United States Minor Outlying Islands</span></span> </li><li id="null-232" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Uruguay</span></span> </li><li id="null-233" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Uzbekistan</span></span> </li><li id="null-234" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Vanuatu</span></span> </li><li id="null-235" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Vatican City State (Holy See)</span></span> </li><li id="null-236" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Venezuela</span></span> </li><li id="null-237" role="option" class="multiselect__element">
                                <span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Vietnam</span></span></li><li id="null-238" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Virgin Islands (British)</span></span> </li><li id="null-239" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Virgin Islands (US)</span></span> </li><li id="null-240" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Wallis And Futuna Islands</span></span> </li><li id="null-241" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Western Sahara</span></span> </li><li id="null-242" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Yemen</span></span> </li><li id="null-243" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Yugoslavia</span></span> </li><li id="null-244" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Zambia</span></span> </li><li id="null-245" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Zimbabwe</span></span> </li> 
                                </ul> 
                            </div> -->
                        <!-- </div>  -->
                        
                        <select id='selUser' class='dropdown-search' style='width: 200px;'>
  <option selected disabled>Select Country</option> 
  <option value='1'>India</option> 
  <option value='2'>America</option> 
  <option value='3'>Algeria></option> 
  <option value='4'>Andorra</option> 
  <option value='5'>Angola</option> 
  <option value='6'>Anguilla</option> 
  <option value='7'>Antarctica</option> 
  <option value='8'>Argentina</option> 
</select>
                        
       
                    </div> 
                    <div class="col-md-6 mb-4">
                        <label class="input-label">State</label>
                        <div class="base-input"> 
                            <input name="state" tabindex="" placeholder="State" autocomplete="on" type="text" class="input-field"> <!----> <!---->
                        </div>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label class="input-label">City</label>
                        <div class="base-input"><!---->
                            <input name="city" tabindex="" placeholder="City" autocomplete="on" type="text" class="input-field"> <!----> <!---->
                        </div>
                    </div> 
                    <div class="col-md-6 mb-4">
                        <label class="input-label">Zip</label> 
                        <div class="base-input"><!---->
                            <input name="" tabindex="" placeholder="Zip" autocomplete="on" type="text" class="input-field"> <!----> <!---->
                        </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <label class="input-label">Address</label>
                            <textarea rows="2" cols="10" placeholder="Street 1" class="text-area-field base-text-area">
                            </textarea> <!----> 
                            <textarea rows="2" cols="10" placeholder="Street 2" class="text-area-field base-text-area">
                            </textarea> <!---->
                        </div>
                    </div> 
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="base-button btn btn-primary default-size " style="margin-bottom:20px;">
                             {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="vue-icon icon-left svg-inline--fa fa-save fa-w-14 mr-2">--}}
                                {{--<path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z" class="">--}}
                                {{--</path>--}}
                            {{--</svg>--}}
            Save
           <!----></button></div></div></div></form>
      
      
      </div>
      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
      
      <div class="setting-main-container"><div class="card setting-card"><div class="page-header"><h3 class="page-title">Preferences</h3> <p class="page-sub-title">
        Default preferences for the system.
      </p></div> <form action="">
        <div class="row">
            <div class="col-md-6 mb-4 form-group">
                <label class="input-label">Currency</label>
                <span class="text-danger"> * </span> 
                {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect">--}}
                    {{--<div class="multiselect__select"></div> --}}
                        {{--<div class="multiselect__tags">--}}
                            {{--<div class="multiselect__tags-wrap" style="display: none;"></div>--}}
                            {{--<div class="multiselect__spinner" style="display: none;"></div>--}}
                            {{--<input name="" placeholder="Select Currency" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;"> --}}
                            {{--<span class="multiselect__single">Indian Rupee</span>--}}
                        {{--</div> --}}
                            {{--<div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;">--}}
                                {{--<ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!----> <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight"><span>US Dollar</span></span> <!----></li><li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>British Pound</span></span> <!----></li><li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Euro</span></span> <!----></li><li id="null-3" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>South African Rand</span></span> <!----></li><li id="null-4" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Danish Krone</span></span> <!----></li><li id="null-5" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Israeli Shekel</span></span> <!----></li><li id="null-6" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Swedish Krona</span></span> <!----></li><li id="null-7" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kenyan Shilling</span></span> <!----></li><li id="null-8" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Kuwaiti Dinar</span></span> <!----></li><li id="null-9" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Canadian Dollar</span></span> <!----></li><li id="null-10" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Philippine Peso</span></span> <!----></li><li id="null-11" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--selected"><span>Indian Rupee</span></span> <!----></li><li id="null-12" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Australian Dollar</span></span> <!----></li><li id="null-13" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Singapore Dollar</span></span> <!----></li><li id="null-14" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Norske Kroner</span></span> <!----></li><li id="null-15" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>New Zealand Dollar</span></span> <!----></li><li id="null-16" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Vietnamese Dong</span></span> <!----></li><li id="null-17" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Swiss Franc</span></span> <!----></li><li id="null-18" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Guatemalan Quetzal</span></span> <!----></li><li id="null-19" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Malaysian Ringgit</span></span> <!----></li><li id="null-20" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Brazilian Real</span></span> <!----></li><li id="null-21" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Thai Baht</span></span> <!----></li><li id="null-22" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Nigerian Naira</span></span> <!----></li><li id="null-23" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Argentine Peso</span></span> <!----></li><li id="null-24" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bangladeshi Taka</span></span> <!----></li><li id="null-25" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>United Arab Emirates Dirham</span></span> <!----></li><li id="null-26" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Hong Kong Dollar</span></span> <!----></li><li id="null-27" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Indonesian Rupiah</span></span> <!----></li><li id="null-28" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mexican Peso</span></span> <!----></li><li id="null-29" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Egyptian Pound</span></span> <!----></li><li id="null-30" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Colombian Peso</span></span> <!----></li><li id="null-31" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>West African Franc</span></span> <!----></li><li id="null-32" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Chinese Renminbi</span></span> <!----></li><li id="null-33" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Rwandan Franc</span></span> <!----></li><li id="null-34" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tanzanian Shilling</span></span> <!----></li><li id="null-35" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Netherlands Antillean Guilder</span></span> <!----></li><li id="null-36" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Trinidad and Tobago Dollar</span></span> <!----></li><li id="null-37" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>East Caribbean Dollar</span></span> <!----></li><li id="null-38" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ghanaian Cedi</span></span> <!----></li><li id="null-39" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Bulgarian Lev</span></span> <!----></li><li id="null-40" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Aruban Florin</span></span> <!----></li><li id="null-41" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Turkish Lira</span></span> <!----></li><li id="null-42" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Romanian New Leu</span></span> <!----></li><li id="null-43" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Croatian Kuna</span></span> <!----></li><li id="null-44" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Saudi Riyal</span></span> <!----></li><li id="null-45" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Japanese Yen</span></span> <!----></li><li id="null-46" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Maldivian Rufiyaa</span></span> <!----></li>--}}
                                    {{--<li id="null-47" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Costa Rican Colón</span></span> <!----></li><li id="null-48" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Pakistani Rupee</span></span> <!----></li><li id="null-49" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Polish Zloty</span></span> <!----></li><li id="null-50" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Sri Lankan Rupee</span></span> <!----></li><li id="null-51" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Czech Koruna</span></span> <!----></li><li id="null-52" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Uruguayan Peso</span></span> <!----></li><li id="null-53" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Namibian Dollar</span></span> <!----></li><li id="null-54" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Tunisian Dinar</span></span> <!----></li><li id="null-55" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Russian Ruble</span></span> <!----></li><li id="null-56" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Mozambican Metical</span></span> <!----></li><li id="null-57" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Omani Rial</span></span> <!----></li><li id="null-58" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Ukrainian Hryvnia</span></span> <!----></li><li id="null-59" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Macanese Pataca</span></span> <!----></li><li id="null-60" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Taiwan New Dollar</span></span> <!----></li><li id="null-61" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Dominican Peso</span></span> <!----></li><li id="null-62" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Chilean Peso</span></span> <!----></li> <!----></ul>--}}
                        {{--</div>--}}
                {{--</div> --}}


                <select id='selCurrency' class='dropdown-search' style='width: 200px;'>
                    <option selected disabled>Select Currency</option>
                    <option value='1'>Us Dollar</option>
                    <option value='2'>British Pound</option>
                    <option value='3'>Euro</option>
                    <option value='4'>South African Rand</option>
                    <option value='5'>Danish Krone</option>
                    <option value='6'>Indian Rupee</option>
                    <option value='7'>Thai Baht</option>
                    <option value='8'>Yen</option>
                </select>


       <!----></div>
            <div class="col-md-6 mb-4 form-group"><label class="input-label">Language</label><span class="text-danger"> * </span>
                {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect">--}}
                    {{--<div class="multiselect__select"></div>--}}
                    {{--<div class="multiselect__tags">--}}
                        {{--<div class="multiselect__tags-wrap" style="display: none;">--}}
                            {{----}}
                        {{--</div> <!----> --}}
                        {{--<div class="multiselect__spinner" style="display: none;"></div> --}}
                        {{--<input name="" placeholder="select language" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;"> --}}
                        {{--<span class="multiselect__single">English</span>--}}
                    {{--</div> --}}
                    {{--<div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;">--}}
                        {{--<ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;">--}}
                            {{--<li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight multiselect__option--selected"><span>English</span></span> <!----></li>--}}
                            {{--<li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>French</span></span> <!----></li>--}}
                            {{--<li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>Spanish</span></span> <!----></li> <!---->--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <select id='selLanguge' class='dropdown-search' style='width: 200px;'>
                    <option selected disabled>Select Language</option>
                    <option value='1'>English</option>
                    <option value='2'>French</option>
                    <option value='3'>Spanish</option>
                </select>
            </div>

            <div class="col-md-6 mb-4 form-group">
                <label class="input-label">Time Zone</label>
                <span class="text-danger"> * </span>
                {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect">--}}
                    {{--<div class="multiselect__select"></div> <div class="multiselect__tags">--}}
                        {{--<div class="multiselect__tags-wrap" style="display: none;"></div>--}}
                        {{--<div class="multiselect__spinner" style="display: none;"></div>--}}
                        {{--<input name="" placeholder="select Time Zone" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;">--}}
                        {{--<span class="multiselect__single">(UTC+00:00) UTC</span>--}}
                    {{--</div>--}}
                    {{--<div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;">--}}
                        {{--<ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!---->--}}
                            {{--<li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight"><span>(UTC-11:00) Midway</span></span> <!----></li>--}}
                            {{--<li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-11:00) Niue</span></span> <!----></li>--}}
                            {{--<li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-11:00) Pago Pago</span></span> <!----></li>--}}
                            {{--<li id="null-3" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-10:00) Adak</span></span> <!----></li>--}}
                            {{--<li id="null-4" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-10:00) Honolulu</span></span> <!----></li>--}}
                            {{--<li id="null-5" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-10:00) Johnston</span></span> <!----></li>--}}
                            {{--<li id="null-6" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-10:00) Rarotonga</span></span> <!----></li>--}}
                            {{--<li id="null-7" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-10:00) Tahiti</span></span> <!----></li>--}}
                            {{--<li id="null-8" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:30) Marquesas</span></span> <!----></li>--}}
                            {{--<li id="null-9" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Anchorage</span></span> <!----></li>--}}
                            {{--<li id="null-10" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Gambier</span></span> <!----></li>--}}
                            {{--<li id="null-11" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Juneau</span></span> <!----></li>--}}
                            {{--<li id="null-12" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Nome</span></span> <!----></li>--}}
                            {{--<li id="null-13" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Sitka</span></span> <!----></li>--}}
                            {{--<li id="null-14" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-09:00) Yakutat</span></span> <!----></li>--}}
                            {{--<li id="null-15" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Dawson</span></span> <!----></li>--}}
                            {{--<li id="null-16" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Los Angeles</span></span> <!----></li>--}}
                            {{--<li id="null-17" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Metlakatla</span></span> <!----></li>--}}
                            {{--<li id="null-18" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Pitcairn</span></span> <!----></li>--}}
                            {{--<li id="null-19" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Santa Isabel</span></span> <!----></li>--}}
                            {{--<li id="null-20" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Tijuana</span></span> <!----></li>--}}
                            {{--<li id="null-21" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Vancouver</span></span> <!----></li>--}}
                            {{--<li id="null-22" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-08:00) Whitehorse</span></span> <!----></li>--}}
                            {{--<li id="null-23" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Boise</span></span> <!----></li>--}}
                            {{--<li id="null-24" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Cambridge Bay</span></span> <!----></li>--}}
                            {{--<li id="null-25" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Chihuahua</span></span> <!----></li>--}}
                            {{--<li id="null-26" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Creston</span></span> <!----></li>--}}
                            {{--<li id="null-27" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Dawson Creek</span></span> <!----></li>--}}
                            {{--<li id="null-28" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Denver</span></span> <!----></li>--}}
                            {{--<li id="null-29" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Edmonton</span></span> <!----></li><li id="null-30" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Hermosillo</span></span> <!----></li><li id="null-31" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Inuvik</span></span> <!----></li><li id="null-32" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Mazatlan</span></span> <!----></li><li id="null-33" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Ojinaga</span></span> <!----></li><li id="null-34" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Phoenix</span></span> <!----></li><li id="null-35" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Shiprock</span></span> <!----></li><li id="null-36" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-07:00) Yellowknife</span></span> <!----></li><li id="null-37" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Bahia Banderas</span></span> <!----></li><li id="null-38" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Belize</span></span> <!----></li><li id="null-39" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Beulah</span></span> <!----></li><li id="null-40" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Cancun</span></span> <!----></li><li id="null-41" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Center</span></span> <!----></li><li id="null-42" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Chicago</span></span> <!----></li><li id="null-43" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Costa Rica</span></span> <!----></li><li id="null-44" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Easter</span></span> <!----></li><li id="null-45" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) El Salvador</span></span> <!----></li><li id="null-46" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Galapagos</span></span> <!----></li><li id="null-47" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Guatemala</span></span> <!----></li><li id="null-48" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Knox</span></span> <!----></li><li id="null-49" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Managua</span></span> <!----></li><li id="null-50" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Matamoros</span></span> <!----></li><li id="null-51" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Menominee</span></span> <!----></li><li id="null-52" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Merida</span></span> <!----></li><li id="null-53" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Mexico City</span></span> <!----></li><li id="null-54" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Monterrey</span></span> <!----></li><li id="null-55" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) New Salem</span></span> <!----></li><li id="null-56" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Rainy River</span></span> <!----></li><li id="null-57" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Rankin Inlet</span></span> <!----></li><li id="null-58" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Regina</span></span> <!----></li><li id="null-59" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Resolute</span></span> <!----></li><li id="null-60" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Swift Current</span></span> <!----></li><li id="null-61" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Tegucigalpa</span></span> <!----></li><li id="null-62" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Tell City</span></span> <!----></li><li id="null-63" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-06:00) Winnipeg</span></span> <!----></li><li id="null-64" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Atikokan</span></span> <!----></li><li id="null-65" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Bogota</span></span> <!----></li><li id="null-66" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Cayman</span></span> <!----></li><li id="null-67" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Detroit</span></span> <!----></li><li id="null-68" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Grand Turk</span></span> <!----></li><li id="null-69" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Guayaquil</span></span> <!----></li><li id="null-70" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Havana</span></span> <!----></li><li id="null-71" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Indianapolis</span></span> <!----></li><li id="null-72" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Iqaluit</span></span> <!----></li><li id="null-73" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Jamaica</span></span> <!----></li>--}}
                            {{--<li id="null-74" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Lima</span></span> <!----></li><li id="null-75" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Louisville</span></span> <!----></li><li id="null-76" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Marengo</span></span> <!----></li><li id="null-77" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Monticello</span></span> <!----></li><li id="null-78" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Montreal</span></span> <!----></li><li id="null-79" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Nassau</span></span> <!----></li><li id="null-80" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) New York</span></span> <!----></li><li id="null-81" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Nipigon</span></span> <!----></li><li id="null-82" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Panama</span></span> <!----></li><li id="null-83" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Pangnirtung</span></span> <!----></li><li id="null-84" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Petersburg</span></span> <!----></li><li id="null-85" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Port-au-Prince</span></span> <!----></li><li id="null-86" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Thunder Bay</span></span> <!----></li><li id="null-87" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Toronto</span></span> <!----></li><li id="null-88" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Vevay</span></span> <!----></li><li id="null-89" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Vincennes</span></span> <!----></li><li id="null-90" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-05:00) Winamac</span></span> <!----></li><li id="null-91" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:30) Caracas</span></span> <!----></li><li id="null-92" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Anguilla</span></span> <!----></li><li id="null-93" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Antigua</span></span> <!----></li><li id="null-94" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Aruba</span></span> <!----></li><li id="null-95" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Asuncion</span></span> <!----></li><li id="null-96" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Barbados</span></span> <!----></li><li id="null-97" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Bermuda</span></span> <!----></li><li id="null-98" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Blanc-Sablon</span></span> <!----></li><li id="null-99" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Boa Vista</span></span> <!----></li><li id="null-100" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Campo Grande</span></span> <!----></li><li id="null-101" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Cuiaba</span></span> <!----></li><li id="null-102" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Curacao</span></span> <!----></li><li id="null-103" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Dominica</span></span> <!----></li><li id="null-104" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Eirunepe</span></span> <!----></li><li id="null-105" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Glace Bay</span></span> <!----></li><li id="null-106" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Goose Bay</span></span> <!----></li><li id="null-107" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Grenada</span></span> <!----></li><li id="null-108" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Guadeloupe</span></span> <!----></li><li id="null-109" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Guyana</span></span> <!----></li><li id="null-110" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Halifax</span></span> <!----></li><li id="null-111" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Kralendijk</span></span> <!----></li><li id="null-112" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) La Paz</span></span> <!----></li><li id="null-113" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Lower Princes</span></span> <!----></li><li id="null-114" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Manaus</span></span> <!----></li><li id="null-115" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Marigot</span></span> <!----></li><li id="null-116" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Martinique</span></span> <!----></li><li id="null-117" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Moncton</span></span> <!----></li><li id="null-118" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Montserrat</span></span> <!----></li><li id="null-119" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Palmer</span></span> <!----></li><li id="null-120" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Port of Spain</span></span> <!----></li><li id="null-121" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Porto Velho</span></span> <!----></li><li id="null-122" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Puerto Rico</span></span> <!----></li><li id="null-123" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option">--}}
                                    {{--<span>(UTC-04:00) Rio Branco</span></span> <!----></li><li id="null-124" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Santiago</span></span> <!----></li><li id="null-125" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Santo Domingo</span></span> <!----></li><li id="null-126" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) St. Barthelemy</span></span> <!----></li><li id="null-127" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) St. Kitts</span></span> <!----></li><li id="null-128" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) St. Lucia</span></span> <!----></li><li id="null-129" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) St. Thomas</span></span> <!----></li><li id="null-130" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) St. Vincent</span></span> <!----></li><li id="null-131" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Thule</span></span> <!----></li><li id="null-132" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-04:00) Tortola</span></span> <!----></li><li id="null-133" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:30) St. Johns</span></span> <!----></li><li id="null-134" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Araguaina</span></span> <!----></li><li id="null-135" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Bahia</span></span> <!----></li><li id="null-136" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Belem</span></span> <!----></li><li id="null-137" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Buenos Aires</span></span> <!----></li><li id="null-138" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Catamarca</span></span> <!----></li><li id="null-139" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Cayenne</span></span> <!----></li><li id="null-140" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Cordoba</span></span> <!----></li><li id="null-141" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Fortaleza</span></span> <!----></li><li id="null-142" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Godthab</span></span> <!----></li><li id="null-143" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Jujuy</span></span> <!----></li><li id="null-144" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) La Rioja</span></span> <!----></li><li id="null-145" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Maceio</span></span> <!----></li><li id="null-146" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Mendoza</span></span> <!----></li><li id="null-147" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Miquelon</span></span> <!----></li><li id="null-148" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Montevideo</span></span> <!----></li><li id="null-149" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Paramaribo</span></span> <!----></li><li id="null-150" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Recife</span></span> <!----></li><li id="null-151" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Rio Gallegos</span></span> <!----></li><li id="null-152" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Rothera</span></span> <!----></li><li id="null-153" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Salta</span></span> <!----></li><li id="null-154" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) San Juan</span></span> <!----></li><li id="null-155" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) San Luis</span></span> <!----></li><li id="null-156" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Santarem</span></span> <!----></li><li id="null-157" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Sao Paulo</span></span> <!----></li><li id="null-158" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Stanley</span></span> <!----></li><li id="null-159" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Tucuman</span></span> <!----></li><li id="null-160" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-03:00) Ushuaia</span></span> <!----></li><li id="null-161" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-02:00) Noronha</span></span> <!----></li><li id="null-162" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-02:00) South Georgia</span></span> <!----></li><li id="null-163" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-01:00) Azores</span></span> <!----></li><li id="null-164" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-01:00) Cape Verde</span></span> <!----></li><li id="null-165" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC-01:00) Scoresbysund</span></span> <!----></li><li id="null-166" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Abidjan</span></span> <!----></li><li id="null-167" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Accra</span></span> <!----></li><li id="null-168" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Bamako</span></span> <!----></li><li id="null-169" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Banjul</span></span> <!----></li><li id="null-170" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Bissau</span></span> <!----></li><li id="null-171" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Canary</span></span> <!----></li><li id="null-172" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Casablanca</span></span> <!----></li><li id="null-173" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Conakry</span></span> <!----></li><li id="null-174" role="option" class="multiselect__element">--}}
                                    {{--<span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Dakar</span></span> <!----></li><li id="null-175" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Danmarkshavn</span></span> <!----></li><li id="null-176" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Dublin</span></span> <!----></li><li id="null-177" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) El Aaiun</span></span> <!----></li><li id="null-178" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Faroe</span></span> <!----></li><li id="null-179" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Freetown</span></span> <!----></li><li id="null-180" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Guernsey</span></span> <!----></li><li id="null-181" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Isle of Man</span></span> <!----></li><li id="null-182" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Jersey</span></span> <!----></li><li id="null-183" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Lisbon</span></span> <!----></li><li id="null-184" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Lome</span></span> <!----></li><li id="null-185" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) London</span></span> <!----></li><li id="null-186" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Madeira</span></span> <!----></li><li id="null-187" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Monrovia</span></span> <!----></li><li id="null-188" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Nouakchott</span></span> <!----></li><li id="null-189" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Ouagadougou</span></span> <!----></li><li id="null-190" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Reykjavik</span></span> <!----></li><li id="null-191" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) Sao Tome</span></span> <!----></li><li id="null-192" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+00:00) St. Helena</span></span> <!----></li><li id="null-193" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--selected"><span>(UTC+00:00) UTC</span></span> <!----></li><li id="null-194" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Algiers</span></span> <!----></li><li id="null-195" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Amsterdam</span></span> <!----></li><li id="null-196" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Andorra</span></span> <!----></li><li id="null-197" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Bangui</span></span> <!----></li><li id="null-198" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Belgrade</span></span> <!----></li><li id="null-199" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Berlin</span></span> <!----></li><li id="null-200" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Bratislava</span></span> <!----></li><li id="null-201" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Brazzaville</span></span> <!----></li><li id="null-202" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Brussels</span></span> <!----></li><li id="null-203" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Budapest</span></span> <!----></li><li id="null-204" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Busingen</span></span> <!----></li><li id="null-205" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Ceuta</span></span> <!----></li><li id="null-206" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Copenhagen</span></span> <!----></li><li id="null-207" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Douala</span></span> <!----></li><li id="null-208" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Gibraltar</span></span> <!----></li><li id="null-209" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Kinshasa</span></span> <!----></li><li id="null-210" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Lagos</span></span> <!----></li><li id="null-211" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Libreville</span></span> <!----></li><li id="null-212" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Ljubljana</span></span> <!----></li><li id="null-213" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Longyearbyen</span></span> <!----></li><li id="null-214" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Luanda</span></span> <!----></li><li id="null-215" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Luxembourg</span></span> <!----></li><li id="null-216" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Madrid</span></span> <!----></li><li id="null-217" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Malabo</span></span> <!----></li><li id="null-218" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Malta</span></span> <!----></li><li id="null-219" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Monaco</span></span> <!----></li><li id="null-220" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Ndjamena</span></span> <!----></li><li id="null-221" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Niamey</span></span> <!----></li><li id="null-222" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Oslo</span></span> <!----></li><li id="null-223" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Paris</span></span> <!----></li><li id="null-224" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Podgorica</span></span> --}}
                                             {{--<!----></li><li id="null-225" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Porto-Novo</span></span> <!----></li><li id="null-226" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Prague</span></span> <!----></li><li id="null-227" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Rome</span></span> <!----></li><li id="null-228" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) San Marino</span></span> <!----></li><li id="null-229" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Sarajevo</span></span> <!----></li><li id="null-230" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Skopje</span></span> <!----></li><li id="null-231" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Stockholm</span></span> <!----></li><li id="null-232" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Tirane</span></span> <!----></li><li id="null-233" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Tripoli</span></span> <!----></li><li id="null-234" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Tunis</span></span> <!----></li><li id="null-235" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Vaduz</span></span> <!----></li><li id="null-236" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Vatican</span></span> <!----></li><li id="null-237" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Vienna</span></span> <!----></li><li id="null-238" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Warsaw</span></span> <!----></li><li id="null-239" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Windhoek</span></span> <!----></li><li id="null-240" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Zagreb</span></span> <!----></li><li id="null-241" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+01:00) Zurich</span></span> <!----></li><li id="null-242" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Athens</span></span> <!----></li><li id="null-243" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Beirut</span></span> <!----></li><li id="null-244" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Blantyre</span></span> <!----></li><li id="null-245" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Bucharest</span></span> <!----></li><li id="null-246" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Bujumbura</span></span> <!----></li><li id="null-247" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Cairo</span></span> <!----></li><li id="null-248" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Chisinau</span></span> <!----></li><li id="null-249" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Damascus</span></span> <!----></li><li id="null-250" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Gaborone</span></span> <!----></li><li id="null-251" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Gaza</span></span> <!----></li><li id="null-252" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Harare</span></span> <!----></li><li id="null-253" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Hebron</span></span> <!----></li><li id="null-254" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Helsinki</span></span> <!----></li><li id="null-255" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Istanbul</span></span> <!----></li><li id="null-256" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Jerusalem</span></span> <!----></li><li id="null-257" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Johannesburg</span></span> <!----></li><li id="null-258" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Kiev</span></span> <!----></li><li id="null-259" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Kigali</span></span> <!----></li><li id="null-260" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Lubumbashi</span></span> <!----></li><li id="null-261" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Lusaka</span></span> <!----></li><li id="null-262" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Maputo</span></span> <!----></li><li id="null-263" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Mariehamn</span></span> <!----></li><li id="null-264" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Maseru</span></span> <!----></li><li id="null-265" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Mbabane</span></span> <!----></li><li id="null-266" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Nicosia</span></span> <!----></li><li id="null-267" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Riga</span></span> <!----></li><li id="null-268" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Simferopol</span></span> <!----></li><li id="null-269" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Sofia</span></span> <!----></li><li id="null-270" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Tallinn</span></span> <!----></li><li id="null-271" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Uzhgorod</span></span> <!----></li><li id="null-272" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Vilnius</span></span> <!----></li><li id="null-273" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+02:00) Zaporozhye</span></span> <!----></li><li id="null-274" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Addis Ababa</span></span> <!----></li><li id="null-275" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Aden--}}
                                    {{--</span></span> <!----></li><li id="null-276" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Amman</span></span> <!----></li><li id="null-277" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Antananarivo</span></span> <!----></li><li id="null-278" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Asmara</span></span> <!----></li><li id="null-279" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Baghdad</span></span> <!----></li><li id="null-280" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Bahrain</span></span> <!----></li><li id="null-281" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Comoro</span></span> <!----></li><li id="null-282" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Dar es Salaam</span></span> <!----></li><li id="null-283" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Djibouti</span></span> <!----></li><li id="null-284" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Juba</span></span> <!----></li><li id="null-285" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Kaliningrad</span></span> <!----></li><li id="null-286" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Kampala</span></span> <!----></li><li id="null-287" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Khartoum</span></span> <!----></li><li id="null-288" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Kuwait</span></span> <!----></li><li id="null-289" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Mayotte</span></span> <!----></li><li id="null-290" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Minsk</span></span> <!----></li><li id="null-291" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Mogadishu</span></span> <!----></li><li id="null-292" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Nairobi</span></span> <!----></li><li id="null-293" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Qatar</span></span> <!----></li><li id="null-294" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Riyadh</span></span> <!----></li><li id="null-295" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:00) Syowa</span></span> <!----></li><li id="null-296" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+03:30) Tehran</span></span> <!----></li><li id="null-297" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Baku</span></span> <!----></li><li id="null-298" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Dubai</span></span> <!----></li><li id="null-299" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Mahe</span></span> <!----></li><li id="null-300" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Mauritius</span></span> <!----></li><li id="null-301" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Moscow</span></span> <!----></li><li id="null-302" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Muscat</span></span> <!----></li><li id="null-303" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Reunion</span></span> <!----></li><li id="null-304" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Samara</span></span> <!----></li><li id="null-305" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Tbilisi</span></span> <!----></li><li id="null-306" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Volgograd</span></span> <!----></li><li id="null-307" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:00) Yerevan</span></span> <!----></li><li id="null-308" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+04:30) Kabul</span></span> <!----></li><li id="null-309" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Aqtau</span></span> <!----></li><li id="null-310" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Aqtobe</span></span> <!----></li><li id="null-311" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Ashgabat</span></span> <!----></li><li id="null-312" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Dushanbe</span></span> <!----></li><li id="null-313" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Karachi</span></span> <!----></li><li id="null-314" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Kerguelen</span></span> <!----></li><li id="null-315" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Maldives</span></span> <!----></li><li id="null-316" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Mawson</span></span> <!----></li><li id="null-317" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Oral</span></span> <!----></li><li id="null-318" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Samarkand</span></span> <!----></li><li id="null-319" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:00) Tashkent</span></span> <!----></li><li id="null-320" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:30) Colombo</span></span> <!----></li><li id="null-321" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:30) Kolkata</span></span> <!----></li><li id="null-322" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+05:45) Kathmandu</span></span> <!----></li><li id="null-323" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Almaty</span></span> <!----></li><li id="null-324" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Bishkek</span></span> <!----></li><li id="null-325" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Chagos</span></span> <!----></li><li id="null-326" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Dhaka--}}
                                     {{--</span></span> <!----></li><li id="null-327" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Qyzylorda</span></span> <!----></li><li id="null-328" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Thimphu</span></span> <!----></li><li id="null-329" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Vostok</span></span> <!----></li><li id="null-330" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:00) Yekaterinburg</span></span> <!----></li><li id="null-331" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:30) Cocos</span></span> <!----></li><li id="null-332" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+06:30) Rangoon</span></span> <!----></li><li id="null-333" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Bangkok</span></span> <!----></li><li id="null-334" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Christmas</span></span> <!----></li><li id="null-335" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Davis</span></span> <!----></li><li id="null-336" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Ho Chi Minh</span></span> <!----></li><li id="null-337" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Hovd</span></span> <!----></li><li id="null-338" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Jakarta</span></span> <!----></li><li id="null-339" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Novokuznetsk</span></span> <!----></li><li id="null-340" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Novosibirsk</span></span> <!----></li><li id="null-341" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Omsk</span></span> <!----></li><li id="null-342" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Phnom Penh</span></span> <!----></li><li id="null-343" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Pontianak</span></span> <!----></li><li id="null-344" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+07:00) Vientiane</span></span> <!----></li><li id="null-345" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Brunei</span></span> <!----></li><li id="null-346" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Casey</span></span> <!----></li><li id="null-347" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Choibalsan</span></span> <!----></li><li id="null-348" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Chongqing</span></span> <!----></li><li id="null-349" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Harbin</span></span> <!----></li><li id="null-350" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Hong Kong</span></span> <!----></li><li id="null-351" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Kashgar</span></span> <!----></li><li id="null-352" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Krasnoyarsk</span></span> <!----></li><li id="null-353" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Kuala Lumpur</span></span> <!----></li><li id="null-354" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Kuching</span></span> <!----></li><li id="null-355" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Macau</span></span> <!----></li><li id="null-356" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Makassar</span></span> <!----></li><li id="null-357" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Manila</span></span> <!----></li><li id="null-358" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Perth</span></span> <!----></li><li id="null-359" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Shanghai</span></span> <!----></li><li id="null-360" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Singapore</span></span> <!----></li><li id="null-361" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Taipei</span></span> <!----></li><li id="null-362" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Ulaanbaatar</span></span> <!----></li><li id="null-363" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:00) Urumqi</span></span> <!----></li><li id="null-364" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+08:45) Eucla</span></span> <!----></li><li id="null-365" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Dili</span></span> <!----></li><li id="null-366" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Irkutsk</span></span> <!----></li><li id="null-367" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Jayapura</span></span> <!----></li><li id="null-368" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Palau</span></span> <!----></li><li id="null-369" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Pyongyang</span></span> <!----></li><li id="null-370" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Seoul</span></span> <!----></li><li id="null-371" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:00) Tokyo</span></span> <!----></li><li id="null-372" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:30) Adelaide</span></span> <!----></li><li id="null-373" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:30) Broken Hill</span></span> <!----></li><li id="null-374" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+09:30) Darwin</span></span> <!----></li><li id="null-375" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Brisbane</span></span> <!----></li><li id="null-376" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Chuuk</span></span> <!----></li><li id="null-377" role="option" class="multiselect__element">--}}
                                        {{--<span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Currie</span></span> <!----></li><li id="null-378" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) DumontDUrville</span></span> <!----></li><li id="null-379" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Guam</span></span> <!----></li><li id="null-380" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Hobart</span></span> <!----></li><li id="null-381" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Khandyga</span></span> <!----></li><li id="null-382" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Lindeman</span></span> <!----></li><li id="null-383" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Melbourne</span></span> <!----></li><li id="null-384" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Port Moresby</span></span> <!----></li><li id="null-385" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Saipan</span></span> <!----></li><li id="null-386" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Sydney</span></span> <!----></li><li id="null-387" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:00) Yakutsk</span></span> <!----></li><li id="null-388" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+10:30) Lord Howe</span></span> <!----></li><li id="null-389" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Efate</span></span> <!----></li><li id="null-390" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Guadalcanal</span></span> <!----></li><li id="null-391" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Kosrae</span></span> <!----></li><li id="null-392" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Macquarie</span></span> <!----></li><li id="null-393" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Noumea</span></span> <!----></li><li id="null-394" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Pohnpei</span></span> <!----></li><li id="null-395" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Sakhalin</span></span> <!----></li><li id="null-396" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Ust-Nera</span></span> <!----></li><li id="null-397" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:00) Vladivostok</span></span> <!----></li><li id="null-398" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+11:30) Norfolk</span></span> <!----></li><li id="null-399" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Anadyr</span></span> <!----></li><li id="null-400" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Auckland</span></span> <!----></li><li id="null-401" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Fiji</span></span> <!----></li><li id="null-402" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Funafuti</span></span> <!----></li><li id="null-403" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Kamchatka</span></span> <!----></li><li id="null-404" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Kwajalein</span></span> <!----></li><li id="null-405" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Magadan</span></span> <!----></li><li id="null-406" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Majuro</span></span> <!----></li><li id="null-407" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) McMurdo</span></span> <!----></li><li id="null-408" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Nauru</span></span> <!----></li><li id="null-409" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) South Pole</span></span> <!----></li><li id="null-410" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Tarawa</span></span> <!----></li><li id="null-411" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Wake</span></span> <!----></li><li id="null-412" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:00) Wallis</span></span> <!----></li><li id="null-413" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+12:45) Chatham</span></span> <!----></li><li id="null-414" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+13:00) Apia</span></span> <!----></li><li id="null-415" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+13:00) Enderbury</span></span> <!----></li><li id="null-416" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+13:00) Fakaofo</span></span> <!----></li><li id="null-417" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+13:00) Tongatapu</span></span> <!----></li><li id="null-418" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>(UTC+14:00) Kiritimati</span></span> <!----></li> <!----></ul> --}}
                    {{--</div>--}}
                {{--</div> <!---->--}}

                <select id='selTimeZone' class='dropdown-search' style='width: 200px;'>
                    <option selected disabled>Select Time Zone</option>
                    <option value='1'>(UTC-11:00) Midway</option>
                    <option value='2'>(UTC-11:00) Niue</option>
                    <option value='3'>(UTC-11:00) Pago Pago</option>
                    <option value='4'>(UTC-10:00) Adak</option>
                    <option value='5'>(UTC-10:00) Honolulu</option>
                    <option value='6'>(UTC-10:00) Johnston</option>
                    <option value='7'>(UTC-10:00) Rarotonga</option>

                </select>
            </div>

            <div class="col-md-6 mb-4 form-group"><label class="input-label">Date Format</label><span class="text-danger"> * </span>

                {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect"><div class="multiselect__select"></div> <div class="multiselect__tags"><div class="multiselect__tags-wrap" style="display: none;"></div> <!----> <div class="multiselect__spinner" style="display: none;"></div> <input name="" placeholder="select Date Formate" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;"> <span class="multiselect__single">23 Dec 2019</span></div> <div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;"><ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!----> <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight"><span>2019 Dec 23</span></span> <!----></li><li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--selected"><span>23 Dec 2019</span></span> <!----></li><li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>23/12/2019</span></span> <!----></li><li id="null-3" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>23.12.2019</span></span> <!----></li>--}}
      {{--<li id="null-4" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>23-12-2019</span></span> <!----></li><li id="null-5" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>12/23/2019</span></span> <!----></li><li id="null-6" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>2019/12/23</span></span> <!----></li><li id="null-7" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>2019-12-23</span></span> <!----></li> <!----></ul> </div></div>--}}
                {{----}}

                <select id='selDateFormat' class='dropdown-search' style='width: 200px;'>
                    <option selected disabled>Select Date Format</option>
                    <option value='1'>2019 Dec 24</option>
                    <option value='2'>24 Dec 2019</option>
                    <option value='3'>24/12/2019</option>
                    <option value='4'>24.12.2019 </option>
                    <option value='5'>24-12-2019</option>
                    <option value='6'>12/24/2019</option>
                </select>

                </div>


            <div class="col-md-6 mb-4 form-group"><label class="input-label">Financial Year</label><span class="text-danger"> * </span>

                {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect"><div class="multiselect__select"></div> <div class="multiselect__tags"><div class="multiselect__tags-wrap" style="display: none;"></div> <!----> <div class="multiselect__spinner" style="display: none;"></div> <input name="" placeholder="select financial year" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;"> <span class="multiselect__single">january-december</span></div> <div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;"><ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!----> <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight multiselect__option--selected"><span>january-december</span></span> <!----></li><li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>february-january</span></span> <!----></li><li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>march-february</span></span> <!----></li><li id="null-3" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>april-march</span></span> <!----></li><li id="null-4" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>may-april</span></span> <!----></li><li id="null-5" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>june-may</span></span> <!----></li><li id="null-6" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>july-june</span></span> <!----></li><li id="null-7" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>august-july</span></span> <!----></li><li id="null-8" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>september-august</span></span> <!----></li><li id="null-9" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>october-september</span></span> <!----></li><li id="null-10" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>november-october</span></span> <!----></li><li id="null-11" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>december-november</span></span> <!----></li> <!----></ul> </div></div> <!---->--}}
            {{----}}

                <select id='selFinancialYear' class='dropdown-search' style='width: 200px;'>
                    <option selected disabled>Select Financial Year</option>
                    <option value='1'>january-december</option>
                    <option value='2'>february-january</option>
                    <option value='3'>march-february</option>
                    <option value='4'>april-march</option>
                    <option value='5'>may-april</option>
                    <option value='6'>june-may</option>
                    <option value='7'>july-june</option>
                    <option value='8'>august-july</option>
                    <option value='9'>september-august</option>
                    <option value='10'>october-september</option>
                    <option value='11'>november-october</option>
                    <option value='12'>december-november</option>
                </select>
            </div>

        </div>
                  <div class="row mb-3"><div class="col-md-12 input-group"><button type="submit" class="base-button btn btn-primary default-size ">
        
            <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="vue-icon icon-left svg-inline--fa fa-save fa-w-14 mr-2">
                <path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z" class="">
                </path>
                </svg>  -->
                <!----> 
            Save
           <!----></button></div></div></form> <hr>
           <div class="page-footer">

                <h3 class="page-title">Discount Setting</h3> 
                <div class="flex-box"><div class="left" style='height: 28px;'>
                    <div data-v-9ab91546="" class="base-switch btn-switch">
                        <input data-v-9ab91546="" id="_lnag3em22" type="checkbox">
                        <label data-v-9ab91546="" for="_lnag3em22" class="switch-label">
                            <span></span>
                        </label>
                    </div>
                </div>

            <div class="right ml-15"><p class="box-title">  Discount Per Item  </p> <p class="box-desc">  Enable this if you want to add Discount to individual invoice items. By default, Discount are added directly to the invoice. </p></div></div></div></div></div>
      
      </div>

        <!-- TAX TYPE-->

      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
      
      <div class="setting-main-container"><div class="card setting-card"><div class="page-header d-flex justify-content-between"><div><h3 class="page-title">
          Tax Types
        </h3> <p class="page-sub-title">
          You can add or Remove Taxes as you please. Crater supports Taxes on Individual Items as well as on the invoice.
        </p></div>
                  <button type="button" class="add-new-tax base-button btn btn-outline-primary default-size " data-toggle="modal" data-target="#exampleModal">
        Add New Tax
       </button>


                  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="overlay-wrap"></div>
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Add Tax</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>

                              <div class="modal-body">
                                  <form action="">

                                      {{--<div class="card-body">--}}
                                          <div class="form-group row"><label class="col-sm-4 col-form-label input-label">Name <span class="required"> *</span></label> <div class="col-sm-7"><div class="base-input"><!----> <input name="" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div></div> <div class="form-group row"><label class="col-sm-4 col-form-label input-label">Percent <span class="required"> *</span></label> <div class="col-sm-7"><div class="base-input"><input type="tel" class="v-money input-field"></div> <!----></div></div> <div class="form-group row"><label class="col-sm-4 col-form-label input-label">Description</label> <div class="col-sm-7"><textarea rows="4" cols="50" placeholder="" class="text-area-field base-text-area"></textarea> <!----></div></div> <div class="form-group row"><label class="col-sm-4 col-form-label input-label">Compound Tax</label> <div class="col-sm-7 mr-4"><div data-v-9ab91546="" class="base-switch btn-switch compound-tax-toggle"><input data-v-9ab91546="" id="_eb4oarp3o" type="checkbox"> <label data-v-9ab91546="" for="_eb4oarp3o" class="switch-label">
                                                      <span></span>

                                                  </label></div></div></div>
                              {{--</div>--}}
                                      {{--<div class="card-footer">--}}
                                          {{--<button type="button" class="mr-3 base-button btn btn-outline-primary default-size "><!----> <!---->--}}
                                              {{--Cancel--}}
                                              {{--<!----></button> --}}

                                          {{--<button type="submit" class="base-button btn btn-primary default-size "><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="vue-icon icon-left svg-inline--fa fa-save fa-w-14 mr-2"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z" class=""></path></svg> <!---->--}}
                                              {{--Save--}}
                                              {{--<!----></button>--}}
                                      {{--</div>--}}
                              </form>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button>
                              </div>
                          </div>
                      </div>
                  </div>









              </div> <div class="table-component mb-3"><div class="table-component__table-wrapper">
                      <table class="table-component__table table tax-table"><thead class="table-component__table__head"><tr><th aria-sort="none" role="columnheader" class="table-component__th table-component__th--sort">
  Tax Name
</th><th aria-sort="none" role="columnheader" class="table-component__th table-component__th--sort">
  Compound Tax
</th><th aria-sort="none" role="columnheader" class="table-component__th table-component__th--sort">
  Percent
</th><th aria-disabled="true" role="columnheader" class="table-component__th">
  Action
</th></tr></thead> <tbody class="table-component__table__body">

                          <tr>
                              <td>cvb</td>
                              <td>
                                  <div class="compound-tax">
                                      No
                                  </div>
                              </td>
                              <td>
                                  10 %
                              </td>
                              <td class="action-dropdown">
                                  <form method="POST" action="" accept-charset="UTF-8">
                                      <div class="dropdown">
                                          <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <div class="dot-icon ">
                                                  <span class="dot dot1 "></span>
                                                  <span class="dot dot2 "></span>
                                                  <span class="dot dot3 "></span>
                                              </div>
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >


                                              <a href="" class="dropdown-item btn  btn-xs" title="Edit Tax Type" data-toggle="modal" data-target="#exampleModal">
                                                  <span class="fa fa-pencil" aria-hidden="true"></span> Edit Tax Type
                                              </a>

                                              <button type="button" class="dropdown-item btn btn-xs" title="Delete Tax Type" data-toggle="modal" data-target="#deleteModel">
                                                  <span class="fa fa-trash" aria-hidden="true"></span> Delete Tax Type
                                              </button>

                                          </div>
                                      </div>
                                  </form>

                                  <form method="POST" action="" accept-charset="UTF-8">
                                      <input name="_method" value="DELETE" type="hidden">
                                      {{ csrf_field() }}
                                      <div class="modal fade deleteModel" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="overlay-wrap" ></div>
                                          <div class="modal-dialog" role="document">

                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>
                                                  <div class="modal-body">
                                                      <i class="fas fa-trash" ></i>
                                                      <div class='modelHeading'>
                                                          <div class='modelMainHeading'>
                                                              Are you sure?
                                                          </div>
                                                          <div class='modelSubHeading'> You will not be able to recover this data</div>
                                                      </div>

                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                                                      <button type="submit" class="btn btn-danger">Delete</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </td>
                          </tr>
                          </tbody>
                          <tfoot></tfoot></table></div>  <div style="display: none;"></div> <!----></div> <hr> <div class="page-footer"><h3 class="page-title">
        Tax Settings
      </h3> <div class="flex-box"><div class="left" style='height: 28px;'><div data-v-9ab91546="" class="base-switch btn-switch"><input data-v-9ab91546="" id="_6adoknhvl" type="checkbox"> <label data-v-9ab91546="" for="_6adoknhvl" class="switch-label">
      <span></span>
      </label></div></div> <div class="right ml-15"><p class="box-title">  Tax Per Item </p> <p class="box-desc">  Enable this if you want to add taxes to individual invoice items. By default, taxes are added directly to the invoice. </p></div></div></div></div></div>

      
      
      
      </div>

        <!-- TAX TYPE ENDS -->

        <!--   MAIL CONFIGURATIONS  STARTS   -->

      <div class="tab-pane fade" id="v-pills-mail" role="tabpanel" aria-labelledby="v-pills-settings-tab">
      
      
      <div class="setting-main-container"><div class="card setting-card"><div class="page-header"><h3 class="page-title">Mail Configuration</h3> <p class="page-sub-title">
        Below is the form for Configuring Email driver for sending emails from the app. You can also configure third party providers like Sendgrid, SES etc.
      </p></div> <div><form><div class="row">
                          <div class="col-md-6 my-2"><label class="form-label">Mail Driver</label> <span class="text-danger"> *</span>

                              {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect"><div class="multiselect__select"></div> <div class="multiselect__tags"><div class="multiselect__tags-wrap" style="display: none;"></div> <!----> <div class="multiselect__spinner" style="display: none;"></div> <input name="" placeholder="Select option" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" style="width: 0px; position: absolute; padding: 0px;"> <span class="multiselect__single">smtp</span></div> <div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 261px; display: none;"><ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!----> <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight multiselect__option--selected"><span>smtp</span></span> <!----></li><li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>mail</span></span> <!----></li><li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>sendmail</span></span> <!----></li><li id="null-3" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>mailgun</span></span> <!----></li><li id="null-4" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>ses</span></span> <!----></li> <!----></ul> </div></div> <!---->--}}
                          {{----}}

                              <select id='selMailDriver' class='dropdown-search' style='width: 200px;'>
                                  <option selected disabled>Select Mail Driver</option>
                                  <option value='1'>smtp</option>
                                  <option value='2'>mail</option>
                                  <option value='3'>sendmail</option>
                                  <option value='4'>mailgun</option>
                                  <option value='5'>ses</option>

                              </select>

                          </div> <div class="col-md-6 my-2"><label class="form-label">Mail Host</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="mail_host" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div></div> <div class="row my-2"><div class="col-md-6 my-2"><label class="form-label">Mail Username</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="db_name" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div> <div class="col-md-6 my-2"><label class="form-label">Mail Password</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="name" tabindex="" placeholder="" autocomplete="on" type="password" class="input-field"> <div style="cursor: pointer;">
      
      <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="eye" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="right-icon svg-inline--fa fa-eye fa-w-18"><path fill="currentColor" d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z" class=""></path></svg>
       -->
      </div> <!----></div> <!----></div></div> <div class="row my-2"><div class="col-md-6 my-2"><label class="form-label">Mail Port</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="mail_port" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div>
                          <div class="col-md-6 my-2"><label class="form-label">Mail Encryption</label> <span class="text-danger"> *</span>

                              {{--<div tabindex="-1" aria-owns="listbox-null" role="combobox" class="base-select multiselect multiselect--above"><div class="multiselect__select"></div> <div class="multiselect__tags"><div class="multiselect__tags-wrap" style="display: none;"></div> <!----> <div class="multiselect__spinner" style="display: none;"></div> <input name="" placeholder="Select option" tabindex="0" aria-controls="listbox-null" type="text" autocomplete="off" spellcheck="false" class="multiselect__input" aria-activedescendant="null-2" style=""> <!----></div> <div tabindex="-1" class="multiselect__content-wrapper" style="max-height: 300px; display: none;"><ul id="listbox-null" role="listbox" class="multiselect__content" style="display: inline-block;"> <!----> <li id="null-0" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>tls</span></span> <!----></li><li id="null-1" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option"><span>ssl</span></span> <!----></li><li id="null-2" role="option" class="multiselect__element"><span data-select="" data-selected="" data-deselect="" class="multiselect__option multiselect__option--highlight"><span>starttls</span></span> <!----></li> <!----></ul> </div></div> <!---->--}}
                          {{----}}
                              <select id='selMailEncryption' class='dropdown-search' style='width: 200px;'>
                                  <option selected disabled>Select Mail Encryption</option>
                                  <option value='1'>TLS</option>
                                  <option value='2'>SSl</option>
                                  <option value='3'>StartTLS</option>


                              </select>
                          </div></div> <div class="row my-2"><div class="col-md-6 my-2"><label class="form-label">From Mail Address</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="from_mail" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div> <div class="col-md-6 my-2"><label class="form-label">From Mail Name</label> <span class="text-danger"> *</span> <div class="base-input"><!----> <input name="from_name" tabindex="" placeholder="" autocomplete="on" type="text" class="input-field"> <!----> <!----></div> <!----></div></div> <button type="submit" class="pull-right mt-4 base-button btn btn-primary default-size ">
      
      <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="vue-icon icon-left svg-inline--fa fa-save fa-w-14 mr-2"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z" class=""></path></svg>   -->
    Save
   <!----></button></form></div></div></div>
      </div>

      <div class="tab-pane fade" id="v-pills-notifications" role="tabpanel" aria-labelledby="v-pills-notifications-tab">
      <div class="setting-main-container"><div class="card setting-card"><div class="page-header"><h3 class="page-title">Notification</h3> <p class="page-sub-title">
        Which email notifications would you like to receive when something changes?
      </p></div> <form action=""><div class="form-group"><label class="form-label">Send Notifications to</label><span class="text-danger"> *</span> <div class="base-input">
      <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="left-icon svg-inline--fa fa-envelope fa-w-16"><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z" class=""></path></svg>  -->
      <input name="notification_email" tabindex="" placeholder="Please Enter Email" autocomplete="on" type="text" class="input-field input-field-left-icon col-md-6"> <!----> <!----></div> <!----> <button type="submit" class="mt-4 base-button btn btn-primary default-size ">
      
      <!-- <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="vue-icon icon-left svg-inline--fa fa-save fa-w-14 mr-2"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z" class=""></path></svg> 
       -->
        Save  </button></div></form> <hr> <div class="flex-box mt-3 mb-4"><div class="left" style='height: 28px;'><div data-v-9ab91546="" class="base-switch btn-switch"><input data-v-9ab91546="" id="_yxoqjbvr2" type="checkbox"> <label data-v-9ab91546="" for="_yxoqjbvr2" class="switch-label">
        <span></span>
        </label></div></div> <div class="right ml-15"><p class="box-title">  Invoice viewed </p> <p class="box-desc">  When your customer views the invoice sent via crater dashboard. </p></div></div> <div class="flex-box mb-2"><div class="left" style='height: 28px;'><div data-v-9ab91546="" class="base-switch btn-switch"><input data-v-9ab91546="" id="_idms2c9a1" type="checkbox"> <label data-v-9ab91546="" for="_idms2c9a1" class="switch-label">
        <span></span>
        </label></div></div> <div class="right ml-15"><p class="box-title">  Estimate viewed </p> <p class="box-desc">  When your customer views the estimate sent via crater dashboard. </p></div></div></div></div>
      
      </div>
      <!-- <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div> -->
    </div>
  </div>
</div>




<!-- 
        <div class="panel-body bak-white">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error) 
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('admin.password.update') }}" id="edit_user_form" name="edit_user_form" accept-charset="UTF-8" class="form-horizontal">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PUT">
                <input name="user_id" type="hidden" value="{{$user->id}}">



                <input class="form-control" name="id" type="hidden" id="title" value="{{ old('title', optional($user)->id) }}" minlength="1" maxlength="255" placeholder="Enter title here...">

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">
                        <input class="form-control" name="password" type="password" id="password" value="" minlength="1" placeholder="Type new password">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>


                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Confirm Password</label>
                    <div class="col-md-10">
                        <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" value="" minlength="1" placeholder="Retype new password">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Reset password">
                    </div>
                </div>
            </form>

        </div> -->
    </div>

@endsection
@section('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<script src='{{asset("select2-develop/dist/js/select2.min.js")}}' type='text/javascript'></script>
    <script>
$(document).ready(function(){

  // Initialize select2
  $("#selUser").select2();
  $(".select2-container").removeAttr("style");
});

$(document).ready(function(){

    // Initialize select2
    $("#selCurrency").select2();
    $(".select2-container").removeAttr("style");
});
$(document).ready(function(){

    // Initialize select2
    $("#selTimeZone").select2();
    $(".select2-container").removeAttr("style");
});
$(document).ready(function(){

    // Initialize select2
    $("#selLanguge").select2();
    $(".select2-container").removeAttr("style");
});

$(document).ready(function(){

    // Initialize select2
    $("#selDateFormat").select2();
    $(".select2-container").removeAttr("style");
});

$(document).ready(function(){

    // Initialize select2
    $("#selFinancialYear").select2();
    $(".select2-container").removeAttr("style");
});
$(document).ready(function(){

    // Initialize select2
    $("#selMailDriver").select2();
    $(".select2-container").removeAttr("style");
});
$(document).ready(function(){

    // Initialize select2
    $("#selMailEncryption").select2();
    $(".select2-container").removeAttr("style");
});





// image preview before upload

document.querySelector("#image-file").addEventListener('change', function() {
    // user selected file
    var file = this.files[0];

    // allowed MIME types
    var mime_types = [ 'image/jpeg', 'image/png' ];

    // validate MIME
    if(mime_types.indexOf(file.type) == -1) {
        alert('Error : Incorrect file type');
        return;
    }

    // validate file size
    if(file.size > 2*1024*1024) {
        alert('Error : Exceeded size 2MB');
        return;
    }

    // validation is successful

    // hide upload dialog button
    //document.querySelector("#upload-dialog").style.display = 'none';

    // object url
    _PREVIEW_URL = URL.createObjectURL(file);

    // set src of image and show
    document.querySelector("#preview-image").setAttribute('src', _PREVIEW_URL);
    document.querySelector("#preview-image").style.display = 'inline-block';
});




//company image preview before uopload

document.querySelector("#company-image-file").addEventListener('change', function() {
    // user selected file
    var file = this.files[0];

    // allowed MIME types
    var mime_types = [ 'image/jpeg', 'image/png' ];

    // validate MIME
    if(mime_types.indexOf(file.type) == -1) {
        alert('Error : Incorrect file type');
        return;
    }

    // validate file size
    if(file.size > 2*1024*1024) {
        alert('Error : Exceeded size 2MB');
        return;
    }

    // validation is successful

    // hide upload dialog button
    //document.querySelector("#upload-dialog").style.display = 'none';

    // object url
    _PREVIEW_URL = URL.createObjectURL(file);

    // set src of image and show
    document.querySelector("#company-preview-image").setAttribute('src', _PREVIEW_URL);
    document.querySelector("#company-preview-image").style.display = 'inline-block';
});


$('#exampleModal').on('shown.bs.modal', function () {
    $('#exampleModal').trigger('focus')
})
    </script>

@endsection