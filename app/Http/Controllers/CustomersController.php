<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use DB;

class CustomersController extends Controller
{

    /**
     * Display a listing of the customers.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $customers = Customer::get();
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new customer.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {

        $customerTypes = CustomerType::OrderBy('name','ASC')->pluck('name','id');
        return view('customers.create', compact('customerTypes'));
    }

    /**
     * Store a new customer in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // try {
            
            $data = $this->getData($request);
            Customer::saveRecord($request);

            if($request->has('from_quote'))
            {
                return redirect()->route('quotes.quote.create')->with('success_message', 'Customer was successfully added!');
            }

            return redirect()->route('customers.customer.index')
                             ->with('success_message', 'Customer was successfully added!');

        // } catch (Exception $exception) {
        //     $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
        //      if(@$exception->validator){
        //             $error_messages = $exception->validator;
        //       }
        //     return back()->withInput()
        //                  ->withErrors($error_messages);
        // }
    }

    /**
     * Display the specified customer.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);


        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified customer.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $customerTypes = CustomerType::OrderBy('name','ASC')->pluck('name','id');
        $customer = Customer::findOrFail($id);
        

        return view('customers.edit', compact(['customer','customerTypes']));
    }

    /**
     * Update the specified customer in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
//            $customer = Customer::findOrFail($id);
//            $customer->update($data);
            Customer::saveRecord($request, $id);

            return redirect()->route('customers.customer.index')
                             ->with('success_message', 'Customer was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified customer from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {

            if($this->CanDelete()){
                $customer = Customer::findOrFail($id);
                $customer->delete();
                
                return redirect()->route('customers.customer.index')
                    ->with('success_message', 'Customer was successfully deleted!');
                
            }else{
                return redirect()->route('customers.customer.index')
                    ->with('success_message', 'Permission denied!');
            }


            return redirect()->route('customers.customer.index')
                             ->with('success_message', 'Customer was successfully deleted!');

        } catch (Exception $exception) {

             return redirect()->route('customers.customer.index')
                    ->with('fail_message', 'Customer already exist in quote section .Kindly delete quotes first'); 

            //return back()->withInput()
                        // ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'firstname' => 'string|required|min:1',
            'lastname' => 'string|min:1|nullable',
            'email' => 'required',
            'contact1' => 'string|required|min:1',
            'contact2' => 'string|min:1|nullable',
            'add1' => 'string|required|min:1',
            'add2' => 'string|min:1|nullable',
            'city' => 'string|required|min:1',
            'state' => 'string|required|min:1',
            'country' => 'string|required|min:1',
            'pincode' => 'string|min:1|nullable',
            'customer_type' => 'integer|required|min:1',
     
        ];

        if(isset($request->customer_type) && $request->customer_type==2)
        {
            $rules['company_name'] = 'string|required|min:1';
        }
        
        $data = $request->validate($rules);


        return $data;
    }

    public function CanDelete()
    {
        $userRoleId = auth()->user()->role_id;
        if($userRoleId==1)
        {
            return true;
        }
        return false;
    }

    public function excel(Request $request)
    {
       if($request->id!="")
       {
        $arrayval=explode(",",$request->id);
       $query = DB::table('customers')->select(['id','first_name','last_name','email','phone','contact2','add1','add2','city','state','country'])->whereIn('id',$arrayval)->get()->toArray();
       }
       else
       {
         $query = DB::table('customers')->select(['id','first_name','last_name','email','phone','contact2','add1 as address','add2','city','state','country'])->get()->toArray();
       }
        
       $array = json_decode(json_encode($query), True);
       $flag = false;
          foreach($array  as $row) {
            if(!$flag) {
              // display field/column names as first row
              echo implode("\t", array_keys($row)) . "\r\n";
              $flag = true;
            }
            echo implode("\t", array_values($row)) . "\r\n";
          }
          header("Content-Type: text/plain");
          $filename = "website_data_" . date('Ymd') . ".xls";

       header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
         exit;
    }

}
