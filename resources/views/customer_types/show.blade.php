@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="">{{ isset($customerType->name) ? $customerType->name : 'Customer Type' }}</h4>
            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('customer_types.customer_type.index')}}">Customer Types</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Customer Types</li>
  </ol>
</nav>
            </div>
        </span>

        <div class="pull-right">

            <!-- <form method="POST" action="{!! route('customer_types.customer_type.destroy', $customerType->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('customer_types.customer_type.index') }}" class="btn dropdown-item" title="Show All Customer Type">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Customer Type
                    </a>

                    <a href="{{ route('customer_types.customer_type.create') }}" class="btn dropdown-item" title="Create New Customer Type">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Customer Type
                    </a>
                    
                    <a href="{{ route('customer_types.customer_type.edit', $customerType->id ) }}" class="btn dropdown-item" title="Edit Customer Type">
                        <span class="fa fa-pencil" aria-hidden="true"></span> Edit Customer Type
                    </a>

                    <button type="button" class="btn dropdown-item" title="Delete Customer Type" data-toggle="modal" data-target="#deleteModel">
                        <span class="fa fa-trash" aria-hidden="true"></span> Delete Customer Type
                    </button>
                </div>
            </form> -->





            <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon show-icon">
                                        <span class="dot dot1 show-dot show-dot1"></span>
                                        <span class="dot dot2 show-dot show-dot2"></span> 
                                        <span class="dot dot3 show-dot show-dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                                    <a href="{{ route('customer_types.customer_type.index') }}" class="btn dropdown-item" title="Show All Customer Type">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Customer Type
                    </a>

                    <a href="{{ route('customer_types.customer_type.create') }}" class="btn dropdown-item" title="Create New Customer Type">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Customer Type
                    </a>
                    
                    <a href="{{ route('customer_types.customer_type.edit', $customerType->id ) }}" class="btn dropdown-item" title="Edit Customer Type">
                        <span class="fa fa-pencil" aria-hidden="true"></span> Edit Customer Type
                    </a>

                    <button type="button" class="btn dropdown-item" title="Delete Customer Type" data-toggle="modal" data-target="#deleteModel{{$customerType->id }}">
                        <span class="fa fa-trash" aria-hidden="true"></span> Delete Customer Type
                    </button>
                                
                                    </div>
                                </div>
                            </form>


                       

       
        <form method="POST" action="{!! route('customer_types.customer_type.destroy', $customerType->id) !!}" accept-charset="UTF-8">
        <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$customerType->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>

        </div>

    </div>

    <div class="panel-body bak-white">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <dt>Name</dt>
                <dd>{{ $customerType->name }}</dd>
            </div>
            <div class="col-md-6 col-sm-6">
                <dt>Status</dt>
                <dd>{{ $customerType->status }}</dd>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <dt>Created At</dt>
                <dd>{{ $customerType->created_at }}</dd>
            </div>
            <div class="col-md-6 col-sm-6">
                <dt>Updated At</dt>
                <dd>{{ $customerType->updated_at }}</dd>
            </div>
        </div>
    </div>
</div>

@endsection