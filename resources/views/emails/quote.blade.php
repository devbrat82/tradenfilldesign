<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
<head>
    <title>TradeNFill.com</title>
</head>
<br>
<div class="row">
    <div class="col-md-12">
        <h4>Dear {{ $name.',' }}</h4>
    </div>
</div><br>
<div class="row">
    <div class="col-md-12">
        {!! $msg !!}
    </div>
</div><br>
<div class="row">
    <div class="col-md-12">
        Best Regards,<br>
       <p><font color="red"><b>Swati Singh</b> <br>
                            <b>Business Consultant</b><br>
                            <b>Trade N Fill</b>
                        </font></p>
    </div>
</div>
<div class="row" style="display: flex;">
    <div class="col-md-6" style="border-right: 2px solid black;padding-top:50px;padding-right:100px;" >
       <img src="https://www.tradenfill.com/wp-content/uploads/2018/08/Trade-N-Fill-Logo-e1559649850106.png" alt="logo.jpg" >
    </div>
    <div class="col-md-1 col-sm-1" style="height: 133px; position: absolute;left: 50%; argin-left: -3px;top: 0; margin: 0 20px;"> </div>
    <div class="col-md-6 col-sm-6">
        107, 1st Floor, Plot H-6<br>
        Aggarwal Tower, NSP Pitampura, New Delhi-34<br>
        (India)<br>
        M: +91 9711427808<br>
        O: +91 1140454931<br>
        <img src="http://3.16.37.7/tradenfill-crm/public/images/whatsapp.png">: +91 9654 533433
        <br>
        <a href="www.tradenfill.com" target="_blank">www.tradenfill.com</a><br>
        Follow us on: <a href="https://www.facebook.com/tradenfill/"><img src="http://3.16.37.7/tradenfill-crm/public/images/facebook.png"></a> 
        <a href="https://www.instagram.com/tradenfill/"><img src="http://3.16.37.7/tradenfill-crm/public/images/instagram.png"></a>
       <a href="https://plus.google.com/u/1/105079579028534316445/"><img src="http://3.16.37.7/tradenfill-crm/public/images/googleplus.png"></a>
       <a href="https://in.linkedin.com/company/tradenfill/"><img src="http://3.16.37.7/tradenfill-crm/public/images/linkedin.png"></a> 
        <a href="https://twitter.com/tradenfill/"><img src="http://3.16.37.7/tradenfill-crm/public/images/twitter.png"></a>
        <a href="https://www.youtube.com/channel/UCXPTAGTAz7eLDm_7kCz4mnQ?view_as=subscriber">
        <img src="http://3.16.37.7/tradenfill-crm/public/images/youtube.png"></a>
    </div>
</div>
</body>
</html>