@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4>Create New Quote</h4>
                <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('quotes.quote.index')}}">Quotes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add Quotes</li>
  </ol>
</nav>
                </div>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
            <a href="{{ route('quotes.quote.index') }}" title="Show All Quote">
                                <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show All Quote</button>
                            </a>
            </div>

        </div>

        <div class="panel-body bak-white">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('quotes.quote.store') }}" accept-charset="UTF-8" id="create_quote_form" name="create_quote_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('quotes.form', [
                                        'quote' => null,
                                        'customer' => $customers,
                                        'act' => $acts,
                                        'service' => $services,
                                        'formation' => $formations,
                                      ])

                <div class="row">
                    <div class="col-md-10">
                        <button name="button" class="btn btn-primary" type="submit" value="Save" name="button">Save</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


