@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">{{ !empty($post->title) ? $post->title : 'Post' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('posts.post.index') }}" class="btn btn-primary" title="Show All Post">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('posts.post.create') }}" class="btn btn-success" title="Create New Post">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('posts.post.update', $post->id) }}" id="edit_post_form" name="edit_post_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('posts.form', [
                                        'post' => $post,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('styles')
    <!-- WYSIWYG-->
    <link rel="stylesheet" href="{{ asset('theme-angle')}}/vendor/bootstrap-wysiwyg/css/style.css">
@endsection
@section('javascript')
    <script src='{{ asset('theme-angle')}}/vendor/tinymce/tinymce.min.js'></script>
    <script>
        tinymce.init({
            selector: '.tinymce',
            menubar:false,
            theme: 'modern',
            plugins: 'code fullscreen image link media  hr pagebreak nonbreaking anchor  insertdatetime advlist lists textcolor wordcount   imagetools     colorpicker textpattern',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>
@endsection
