@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-check"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    @if(Session::has('fail_message'))
        <div class="alert alert-danger">
            <span class="fa fa-check"></span>
            {!! session('fail_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <div class="card form-card">
                    <div class="card-header">

                     

                        <div class="card-title pull-left">
                            Customers

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
   
    <li class="breadcrumb-item active" aria-current="page">Customer</li>
  </ol>
</nav>

                        </div>

                      

                        <a href="{{ route('customers.customer.create') }}">
                        <button class="btn btn-labeled btn-green mb-2  pull-right customer-btn" type="button">
                           <span class="btn-label customer-btn-label"><i class="fa fa-plus"></i></span>
                           New Customer
                        </button>
                        </a>
                        <button title='Download Excel' class="btn btn-green customer-excel buttons-excel" type="button" onclick="excelgen('{{ route('customers.customer.excel') }}')">

                        </button>
                                   
                    </div>

</div>
                      
                    
       
<!-- </div>

                       

                    </div> -->

                    <div class="card-body customer-body">
                <table class="table table-striped full-width" id="datatable-customers">
                    <thead>
                        <tr class='checkbox-row'>
                           <th style='padding-right:0px;' >
                           <span class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="1" onclick="check(this);">
                                    <label class="custom-control-label" for="customCheck"></label>
                                </span>
                           </th>
                            <th>  
                            First Name
                            </th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Add</th>
                            <th>Customer Type</th>


                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($customers))
                    @foreach($customers as $customer)
                        <tr>
                            
                        <td>
                            <span class="custom-control custom-checkbox mb-4">
                                <input type="checkbox" class="custom-control-input custom-check" id="customCheck{{ $customer->id }}" value="{{ $customer->id }}" name="customername[]">
                                <label class="custom-control-label" for="customCheck{{ $customer->id }}"> </label>
                            </span>    
                        
                        </td>
                        <td>{{$customer->prefix }} {{ $customer->first_name }}</td>
                            <td>{{ $customer->last_name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->phone }}</td>
                            <td>{{ $customer->add1 }}</td>
                            <td>{{ $customer->Type->name }}</td>

                            <td>

                                <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon ">
                                        <span class="dot dot1 "></span>
                                        <span class="dot dot2 "></span> 
                                        <span class="dot dot3 "></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                    
                                        <a href="{{ route('customers.customer.show', $customer->id ) }}" class="dropdown-item btn  btn-xs" title="Show Customer">
                                            <span class="fa fa-eye" aria-hidden="true"></span> Show Customer
                                        </a>
                                        <a href="{{ route('customers.customer.edit', $customer->id ) }}" class="dropdown-item btn  btn-xs" title="Edit Customer">
                                            <span class="fa fa-pencil" aria-hidden="true"></span> Edit Customer
                                        </a>

                                        <button type="button" class="dropdown-item btn btn-xs" title="Delete Customer" data-toggle="modal" data-target="#deleteModel{{$customer->id}}">
                                            <span class="fa fa-trash" aria-hidden="true"></span> Delete Customer
                                        </button>
                                
                                    </div>
                                </div>
                            </form>


<form method="POST" action="{!! route('customers.customer.destroy', $customer->id) !!}" accept-charset="UTF-8">
<input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>
                                
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>

@endsection

@section('styles')
    <!-- Datatables-->
    <link rel="stylesheet" href="{{url('theme-angle')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">

@endsection
@section('javascript')
    <script src="{{url('theme-angle')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/jszip/dist/jszip.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/pdfmake.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/vfs_fonts.js"></script>
    

<script>

    $('#datatable-customers').DataTable({
        'paging': true, // Table pagination
        'ordering': true, // Column ordering
        'info': true, // Bottom left status text
        responsive: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch: 'Search all columns:',
            sLengthMenu: '_MENU_ records per page',
            info: 'Showing page _PAGE_ of _PAGES_',
            zeroRecords: 'Nothing found - sorry',
            infoEmpty: 'No records available',
            infoFiltered: '(filtered from _MAX_ total records)',
            oPaginate: {
                sNext: '<em class="fa fa-caret-right"></em>',
                sPrevious: '<em class="fa fa-caret-left"></em>'
            }
        },
        // Datatable Buttons setup
        dom: 'Bfrtip',
        buttons: [
            //{ extend: 'copy', className: 'btn-green' },
           // { extend: 'csv', className: 'btn-green' },
              

            //{ extend: 'excel', className: 'btn-green', title: 'XLS-File' },
            //{ extend: 'pdf', className: 'btn-green', title: $('title').text() },
            //{ extend: 'print', className: 'btn-green' }
        ]
    });

</script>
<!-- 
<script type="text/javascript">
    $(function () {
        $("#btnPassport").click(function () {
            if ($(this).val() == "Yes") {
                $("#dvPassport").show();
                $(this).val("No");
            } else {
                $("#dvPassport").hide();
                $(this).val("Yes");
            }
        });
    });
</script> -->

<script language="JavaScript">
function check(source) {
  checkboxes = $('[id^="customCheck"]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}


function excelgen(var1)
{
    var selected = [];
    $('[name^="customername"]').each(function() {
        if ($(this).is(":checked")) {
        selected.push($(this).attr('value'));
        }
    });
    selected=selected.toString()
    window.location.href = var1+"?id="+selected;
}



$('.custom-check').change(function(){ //".checkbox" change
    if($('.custom-check:checked').length == $('.custom-check').length){
        $('#customCheck').prop('checked',true);
    }else{
        $('#customCheck').prop('checked',false);
    }
});



$('input[type="search"]').attr('placeholder','Search here');
</script>
@endsection