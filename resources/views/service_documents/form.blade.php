
<div class="form-group {{ $errors->has('act') ? 'has-error' : '' }}">
    <label for="act" class="col-md-2 control-label">Main</label>
    <div class="col-md-10">
        <select class="form-control" id="act" name="act" required>
        	    <option value="" style="display: none;" {{ old('act', optional($serviceDocument)->act ?: '') == '' ? 'selected' : '' }} disabled selected>Select act</option>
            @foreach ($act as $key => $text)
			    <option value="{{ $key }}" {{ old('act', optional($serviceDocument)->act) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('act', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('service') ? 'has-error' : '' }}">
    <label for="service" class="col-md-2 control-label">Service</label>
    <div class="col-md-10">
        <select class="form-control" id="service" name="service" required>
        	    <option value="" style="display: none;" {{ old('service', optional($serviceDocument)->service ?: '') == '' ? 'selected' : '' }} disabled selected>Select service</option>
        	@foreach ($service as $key => $text)
			    <option value="{{ $key }}" {{ old('service', optional($serviceDocument)->service) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('service', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fotmation') ? 'has-error' : '' }}">
    <label for="fotmation" class="col-md-2 control-label">Formation</label>
    <div class="col-md-10">
        <select class="form-control" id="fotmation" name="fotmation" required>
        	    <option value="" style="display: none;" {{ old('fotmation', optional($serviceDocument)->fotmation ?: '') == '' ? 'selected' : '' }} disabled selected>Select fotmation</option>
        	@foreach ($formation as $key => $text)
			    <option value="{{ $key }}" {{ old('fotmation', optional($serviceDocument)->fotmation) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('fotmation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('govt_fee') ? 'has-error' : '' }}">
    <label for="govt_fee" class="col-md-2 control-label">Govt Fee</label>
    <div class="col-md-10">
        <input class="form-control" name="govt_fee" type="text" id="govt_fee" value="{{ old('govt_fee', optional($serviceDocument)->govt_fee) }}" minlength="1" placeholder="Enter govt fee here...">
        {!! $errors->first('govt_fee', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('prof_fees') ? 'has-error' : '' }}">
    <label for="prof_fees" class="col-md-2 control-label">Prof Fees</label>
    <div class="col-md-10">
        <input class="form-control" name="prof_fees" type="text" id="prof_fees" value="{{ old('prof_fees', optional($serviceDocument)->prof_fees) }}" minlength="1" placeholder="Enter prof fees here...">
        {!! $errors->first('prof_fees', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="{{ $errors->has('duration') ? 'has-error' : '' }}" style="margin-bottom: 15px">
    <label for="duration" class="col-md-2 control-label">Duration</label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6" style="float: left;">
                <input class="form-control" name="duration" type="number" min="1" max="29" id="duration" value="{{ old('duration', optional($serviceDocument)->duration) }}" minlength="1" placeholder="Enter duration here...">
                {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="col-md-4" style="float: left;">
                <select class="form-control" id="unit" name="unit">
                    <option value="Days" {{ old('unit', optional($serviceDocument)->unit) == 'Days' ? 'selected' : '' }} >Days</option>
                    <option value="Months" {{ old('unit', optional($serviceDocument)->unit) == 'Months' ? 'selected' : '' }}>Months</option>
                    <option value="Years" {{ old('unit', optional($serviceDocument)->unit) == 'Years' ? 'selected' : '' }}>Years</option>
                </select>
                {!! $errors->first('unit', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {


            $("#act").on('change', function() {
                const val = $( this).val();
                let ajaxData = {"_token": "{{ csrf_token() }}",}
                ajaxData['act_id'] = val;

                var request = $.ajax({
                    url: "<?php echo route('service_documents.service_document.actService'); ?>",
                    method: "GET",
                    data: ajaxData,
                    dataType: "json"
                });

                request.done(function(data) {
                    var trHTML =[];
                    $('#service').html('');
                    $.each(data, function (k, v)
                    {

                        trHTML += '<option value="' + k + '">' + v + ' </option>';

                    })
                    $('#service').append(trHTML);
                    // $.notify(message, {status: "success", pos: "bottom-right"});
                })
            })


        } );

        //@ sourceURL=edit_skills.js
    </script>
@endsection

