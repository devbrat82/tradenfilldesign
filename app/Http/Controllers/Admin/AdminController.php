<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Document;
use App\Models\Customer;
use App\Models\Quote;
use App\Models\Service;
use App\Models\Post;
use App\Rules\ValidateAdminExist;
use App\Rules\ValidateEmailExist;
use App\User;
use Redirect;
use Illuminate\Support\Facades\DB;
use Schema;
use Illuminate\Http\Request;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller {

	/**
	 * Display a listing of tmpcountries
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {

	}

	/**
	 * Show the form for creating a new tmpcountries
	 *
     * @return \Illuminate\View\View
	 */
    public function showLogin()
    {
        return view('admin.login');
    }

    public function showDashboard()
    {
        $serviceCount = Service::count();
        $customerCount = Customer::count();
        $this_month_customers = Customer::where(DB::raw('EXTRACT( Year from customers.created_at)'), '>=',   date('Y') )->where(DB::raw('EXTRACT( Month from customers.created_at)'), '>=',   date('m') )->count();
        $this_year_customers = Customer::where(DB::raw('EXTRACT( Year from created_at)'), '>=',   date('Y') )
            ->select(DB::raw('DATE_FORMAT(created_at, \'%m\') as month,  DATE_FORMAT(created_at, \'%b\') as month_name, count(id) as total_customer'))
            ->groupBy('month','month_name')->orderBy('month','asc')->get();
//        dd($this_year_customers->toArray()); die;
        $this_year_customers = $this->convertTomonth($this_year_customers);

        $quoteCount = Quote::count();
        $total_quote_this_month = Quote::where(DB::raw('EXTRACT( Year from quotes.created_at)'), '>=',   date('Y') )
            ->where(DB::raw('EXTRACT( Month from quotes.created_at)'), '>=',   date('m') )->count();

        $this_year_quotes_data = Quote::where(DB::raw('EXTRACT( Year from created_at)'), '=',   date('Y') )
            ->select(DB::raw('DATE_FORMAT(created_at, \'%b\') as month,  DATE_FORMAT(created_at, \'%b\') as month_name, count(id) as total_quote'))
            ->groupBy('month','month_name')->orderBy('month','asc')->get();
            $arrayyear=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            $monthpresent[]='';
            foreach($this_year_quotes_data as $thisyear)
             {
                $monthpresent[]=$thisyear->month_name;
               $thisyearq[$thisyear->month_name]= $thisyear->total_quote; 
             }
             foreach($arrayyear as $newdata)
             {
                if(in_array($newdata,$monthpresent))
                {
                  $this_year_quotes[$newdata]=$thisyearq[$newdata];  
                }
                else
                {
                  $this_year_quotes[$newdata]=0;  
                }
                
             }
        $this_previous_quotes_data = Quote::where(DB::raw('EXTRACT( Year from created_at)'), '=',   date('Y',strtotime("-1 year")) )
            ->select(DB::raw('DATE_FORMAT(created_at, \'%b\') as month,  DATE_FORMAT(created_at, \'%b\') as month_name, count(id) as total_quote'))
            ->groupBy('month','month_name')->orderBy('month','asc')->get();
            $monthprevious[]='';
        foreach($this_previous_quotes_data as $thispreyear)
             {
                $monthprevious[]=$thispreyear->month_name;
               $thisyearpreq[$thisyear->month_name]= $thispreyear->total_quote; 
             }
             foreach($arrayyear as $newdata)
             {
                if(in_array($newdata,$monthprevious))
                {
                  $this_previous_quotes[$newdata]=$thisyearpreq[$newdata];  
                }
                else
                {
                  $this_previous_quotes[$newdata]=0;  
                }
                
             }
        $docs = Document::count();
        return view('admin.dashboard', compact('serviceCount', 'docs','customerCount','quoteCount','this_month_customers','this_year_customers','total_quote_this_month','this_year_quotes','this_previous_quotes'));
       // return view('layouts.app');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', new ValidateAdminExist()],
            'password' => 'required',
        ]);

        $user = User::where('email', $request->get('email'))->first();
        
        $passwordChecked = \Hash::check($request->get('password'), $user->password);
        if(!$passwordChecked){
            return redirect()->back()->withInput()->withErrors(['password' => "Invalid login credential."]);
        }

        $auth = \Auth::loginUsingId($user->id, $request->get('remember_me'));
        return redirect('admin/dashboard');

    }

    public function logout(){
       \Auth::logout();
        return redirect('admin/login');
    }

    public function showResetPassword()
    {
        $user = \Auth::user();
        return view('admin.reset_password', compact('user'));
    }

    public function resetPassword(Request $request)
    {

        $user = User::findOrFail($request->user_id);

        $request->validate([
            'password' => 'nullable|required_with:password_confirmation|string|confirmed',
        ]);

        $user->password =  \Hash::make($request->password);
        $user->save();
        return redirect()->route('admin.resetpassword')->with('message', 'Password updated successfully');
    }

    public function convertTomonth($data)
    {

        $data= $data->toArray();
        $new_arr=array();
        for($i=0; $i<date('m')+1; $i++)
        {
            foreach($data as $key=>$val)
            {

                if( $val['month']!= ($i+1) )
                {
                    $new_arr[$i]['month']=$i+1;
                    $new_arr[$i]['month_name']=date("F", strtotime(date("Y") ."-". ($i+1) ."-01"));
                    $new_arr[$i]['total_customer']=0;
                }
                else
                {
                    $new_arr[$i]['month']=$val['month'];
                    $new_arr[$i]['month_name']=$val['month_name'];
                    $new_arr[$i]['total_customer']=$val['total_customer'];
                }
            }

        }
        //print_r($data); die;
        return  $new_arr;
    }

    public function setting(Request $request)
    {
       $user = \Auth::user();
        return view('admin.setting', compact('user')); 
    }

    public function account(Request $request)
    {
        $user = User::find($request->user_id);
         $validator = Validator::make($request->all(), [
            'password' => 'nullable|required_with:password_confirmation|string|confirmed|min:4|max:20',
            'name'=>'required',
            'email' => ['required', 'email', new ValidateAdminExist()],
        ]);

        if ($validator->fails()) {
            return redirect('admin/setting')->withErrors($validator);
        }
        if($request->hasFile('image')) {
        $image       = $request->file('image');
        $filename    = microtime().$image->getClientOriginalName();

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(300, 300);
        $image_resize->save(public_path('storage/uploads/' .$filename));
         }

        $name =$request->input('name');
        $password =$request->input('password');
        $email =$request->input('email');
        $user->name=$name;
        $user->email=$email;
        if($password!="")
        {
          $passwordnew = Hash::make($password);
          $user->password=$passwordnew;  
        }
        $user->save();

        

        return redirect()->route('admin.setting')->withMessage(trans('Account details updated successfully'));
        
    }


}
