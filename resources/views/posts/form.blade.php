
<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
    <label for="category_id" class="col-md-2 control-label">Category</label>
    <div class="col-md-10">
        <select class="form-control" id="category_id" name="category_id">
        	    <option value="" style="display: none;" {{ old('category_id', optional($post)->category_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select category</option>
        	@foreach ($categories as $key => $category)
			    <option value="{{ $key }}" {{ old('category_id', optional($post)->category_id) == $key ? 'selected' : '' }}>
			    	{{ $category }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Title</label>
    <div class="col-md-10">
        <input class="form-control" name="title" type="text" id="title" value="{{ old('title', optional($post)->title) }}" minlength="1" maxlength="255" placeholder="Enter title here...">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="col-md-2 control-label">Description</label>
    <div class="col-md-10">
        <textarea class="form-control tinymce" name="description" cols="50" rows="10" id="description" minlength="1">{{ old('description', optional($post)->description) }}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('img_src') ? 'has-error' : '' }}">
    <label for="img_src" class="col-md-2 control-label">Img Src</label>
    <div class="col-md-10">
        <div class="input-group uploaded-file-group">
            <label class="input-group-btn">
                <span class="btn btn-default">
                    Browse <input type="file" name="img_src" id="img_src" class="hidden">
                </span>
            </label>
            <input type="text" class="form-control uploaded-file-name" readonly>
        </div>

        @if (isset($post->img_src) && !empty($post->img_src))
            <?php
            if(is_array($post->img_src)){
                $img_src = @$post->img_src['thumbs']['100W_SQUARECROP']['path'];
            }else{
                $img_src =  asset('storage/'.$post->img_src);
            }
            ?>
                <div class="input-group input-width-input">
                <span class="input-group-addon">
                    <input type="checkbox" name="custom_delete_img_src" class="custom-delete-file" value="1" {{ old('custom_delete_img_src', '0') == '1' ? 'checked' : '' }}> Delete
                </span>

                    <span class="input-group-addon custom-delete-file-name" style="margin-left: 20px">
                  <img src="{{ $img_src }}" style="max-height: 100px; max-width: 100px " >
                </span>
                </div>
        @endif
        {!! $errors->first('img_src', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('published') ? 'has-error' : '' }}">
    <label for="published" class="col-md-2 control-label">Published</label>
    <div class="col-md-10">
        <select class="form-control" id="published" name="published">
            <option value="" style="display: none;" {{ old('published', optional($post)->published ?: '') == '' ? 'selected' : '' }} disabled selected>Select published</option>
            @foreach (['1' => 'True',
'0' => 'False'] as $key => $text)
                <option value="{{ $key }}" {{ old('published', optional($post)->published) == $key ? 'selected' : '' }}>
                    {{ $text }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('display_type') ? 'has-error' : '' }}">
    <label for="display_type" class="col-md-2 control-label">Display Type</label>
    <div class="col-md-10">
        <select class="form-control" id="display_type" name="display_type">
        	    <option value="" style="display: none;" {{ old('display_type', optional($post)->display_type ?: '') == '' ? 'selected' : '' }} disabled selected>Select display type</option>
        	@foreach (['main' => 'Main',
'brief' => 'Brief',
'unspecified' => 'Unspecified'] as $key => $text)
			    <option value="{{ $key }}" {{ old('display_type', optional($post)->display_type) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('display_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('published_at') ? 'has-error' : '' }}">
    <label for="published_at" class="col-md-2 control-label">Published At</label>
    <div class="col-md-10">
        <input class="form-control" name="published_at" type="text" id="published_at" value="{{ old('published_at', (optional($post)->published_at?optional($post)->published_at:date("Y-m-d H:i:s"))) }}" placeholder="Enter published at here...">
        {!! $errors->first('published_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('created_by') ? 'has-error' : '' }}">
    <label for="created_by" class="col-md-2 control-label">Created By</label>
    <div class="col-md-10">
        <select class="form-control" id="created_by" name="created_by">
        	    <option value="" style="display: none;" {{ old('created_by', optional($post)->created_by ?: '') == '' ? 'selected' : '' }} disabled selected>Select created by</option>
        	@foreach ($creators as $key => $creator)
			    <option value="{{ $key }}" {{ old('created_by', optional($post)->created_by) == $key ? 'selected' : '' }}>
			    	{{ $creator }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('created_by', '<p class="help-block">:message</p>') !!}
    </div>
</div>

