<?php

namespace App\Http\Controllers;

use App\MailToken;
use App\Models\Customer;
use App\Models\CustomerType;
use App\Models\Quote;
use App\Models\Act;
use App\Models\Service;
use App\Models\Document;
use App\Models\FormationType;
use App\Models\ServiceDocument;
use App\Models\ServiceDocumentRef;
use http\Env\Response;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Exception;
use \DB;
use PDF;
use \Auth;


class old extends Controller
{

    /***
     * Display a listing of the quotes.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $quotes = Quote::getData();
        return view('quotes.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new quote.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $customers = Customer::select(DB::raw("CONCAT(first_name,' ',last_name) AS name, id"))->OrderBy('name','ASC')->pluck('name','id');
        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        $services = Service::OrderBy('title','ASC')->pluck('title','id');
        $formations = FormationType::OrderBy('name','ASC')->pluck('name','id');
        $customerTypes = CustomerType::OrderBy('name','ASC')->pluck('name','id');
        return view('quotes.create', compact(['customers','acts','services','formations','customerTypes']));
    }

    /**
     * Store a new quote in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $this->getData($request);
        if(!$request->has('docs') && $request->docs=='')
        {
            return redirect()->back()->withErrors(['Documents Required'])->withInput();
        }
        else{
            $quoteId =  Quote::saveRecord($request);
            $this->generateQuotePDF($quoteId);
            return redirect()->route('quotes.quote.index')
                ->with('success_message', 'Quote was successfully added!');
        }


    }

    /**
     * Display the specified quote.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $quote = Quote::findOrFail($id);



        return view('quotes.show', compact('quote'));

    }
    /**
     * Show the form for editing the specified quote.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {

        $quote = Quote::findOrFail($id);

        $customers = Customer::select(DB::raw("CONCAT(first_name,' ',last_name) AS name, id"))->OrderBy('name','ASC')->pluck('name','id');

        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        $services = Service::OrderBy('title','ASC')->pluck('title','id');
        $formations = FormationType::OrderBy('name','ASC')->pluck('name','id');

        return view('quotes.edit', compact(['quote','customers','acts','services','formations']));
    }

    public function copy($id)
    {

        $quote = Quote::findOrFail($id);

        $customers = Customer::select(DB::raw("CONCAT(first_name,' ',last_name) AS name, id"))->OrderBy('name','ASC')->pluck('name','id');

        $acts = Act::OrderBy('name','ASC')->pluck('name','id');
        $services = Service::OrderBy('title','ASC')->pluck('title','id');
        $formations = FormationType::OrderBy('name','ASC')->pluck('name','id');

        return view('quotes.copy', compact(['quote','customers','acts','services','formations']));
    }

    /**
     * Update the specified quote in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
//        try {
            
            $data = $this->getData($request);
            //dd($data);
            $quote = Quote::findOrFail($id);
//            $quote->update($data);
            Quote::saveRecord($request,$id);
            $this->generateQuotePDF($quote->id);

            return redirect()->route('quotes.quote.index')
                             ->with('success_message', 'Quote was successfully updated!');

//        } catch (Exception $exception) {
//            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
//             if($exception->validator){
//                    $error_messages = $exception->validator;
//              }
//            return back()->withInput()
//                         ->withErrors($error_messages);
//        }
    }

    /**
     * Remove the specified quote from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $quote = Quote::findOrFail($id);
            if($this->CanDelete()){
                $quote->delete();
                return redirect()->route('quotes.quote.index')
                    ->with('success_message', 'Quote was successfully deleted!');
            }else{
                return redirect()->route('quotes.quote.index')
                    ->with('success_message', 'Permission denied!');
            }


        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'customer' => 'nullable',
            'act' => 'nullable',
            'service' => 'nullable',
            'fotmation' => 'nullable',
            'govt_fee' => 'string|min:1|nullable',
            'prof_fees' => 'string|min:1|nullable',
            'split_price' => 'string|min:1|nullable',
            'duration' => 'integer|min:1|nullable',
            'unit' => 'string|min:1|nullable',
            'comment' => 'string|min:1|nullable',

        ];
        
        $data = $request->validate($rules);


        return $data;
    }

    public function getActService(Request $request)
    {
       $act = $request->act_id;
        $services = Service::where('act',$act)->OrderBy('title','ASC')->pluck('title','id')->toArray();
        return $services;
    }

    public function getRequiredDocs(Request $request)
    {
        $dcmnt= []; $docs=''; $documents='';

        if($request->quoteId!=0)
        {
            $docs = Quote::find($request->quoteId)->toArray();
            $dcmnt['docs'] = $docs;

            $documents = Quote::where('id',$request->quoteId)->pluck('docs')->toArray();

            if(is_array($documents)) {
                $docid = json_decode($documents[0]);
                $documents = Document::whereIn('id', $docid)->pluck('name', 'id')->toArray();
                $dcmnt['reqd'] = $documents;
            }

            return $dcmnt;
        }

        $act = $request->act;
        $serv = $request->serv;
        $formtn = $request->formtn;


        $docs = ServiceDocument::where('act',$act)->where('service',$serv)->where('fotmation',$formtn)->first();
       //print_r($docs); die;
        if(!empty($docs) && $docs!=null && $docs!='')
        {
            $docs = $docs->toArray();
            $dcmnt['docs'] = $docs;
        }

        if(!empty($docs) && $docs!=null && $docs!='')
        {
            $documents = DB::table('service_document_ref')->join('documents','documents.id','=','service_document_ref.document_id')->where('service_document_ref.service_doc_id',$docs['id'])->select('documents.name' ,'documents.id')->pluck('name','id')->toArray();
            $dcmnt['reqd'] = $documents;
        }




        //print_r($documents); die;

        return $dcmnt;

    }

    public function generateQuotePDF($id, $mail=0)
    {
        $fee ='';
        $docs = Quote::with('Act')->with('Service')->with('FormationType')->with('Customer')->find($id);
        if($docs->split_price)
        {
            $fee = 'Government Fee : Rs. '.$docs->govt_fee.'<br> Professional Fee : Rs. '.$docs->prof_fees;
        }else{
            $fee = 'Fee : Rs. '. ((integer)$docs->govt_fee+(integer)$docs->prof_fees);
        }

        $documents = Quote::where('id',$id)->pluck('docs')->toArray();

        if(is_array($documents)) {
            $docid = json_decode($documents[0]);
            $documents = Document::whereIn('id', $docid)->pluck('name', 'id')->toArray();
        }
        $data = [
            'logo' => env('APP_URL').'images/t&flogo.png',
            'title' => '<u>TRADE N FILL CONSULTING SERVICES</u>',
            'heading' => '<u>LIST OF REQUIRED DOCUMENTS OF '.$docs->Service->title.'</u>',
            'date' => date('d/m/Y'),
            'salutation' => 'Dear'.' '.ucfirst($docs->Customer->prefix.' '.$docs->Customer->first_name.' '.$docs->Customer->last_name.','),
            'content' => $documents,
            'comment' => $docs->comment,
            'customerType' =>$docs->Customer->customer_type,
            'company'=> $docs->Customer->customer_type==2?$docs->Customer->company_name:'',
            'footer' => 'Parv Filling Services Private Limited : 107, 1st Floor, Plot H-6, Aggrawal Tower, NSP, Pitampura, Delhi-34 <br>Tel.: 011-42058185, Mob.:9899868680, info@tradenfill.com, www.tradenfill.com | CIN: U74999DL2018PTC337498',
            'fee' => $fee,
            'process_time' =>$docs->duration.' '.$docs->unit,
            'id' =>$id
            ];


        //$str=str_replace(' ','-',$docs->Service->title);dd($str_);
    $name = $docs->code.'-'.$docs->Customer->first_name.'-'.str_replace(array(' ','/','\'','@','$','_','&','*','(',')'),'-',$docs->Service->title).'.pdf';

        $output = PDF::loadView('quotes.pdf_view', compact('data'))->output();
        $file = base_path().'/public/storage/'.$name;
        if(\File::exists($file)){

            \File::delete($file);

        }
        file_put_contents($file, $output);

        Quote::where('id',$id)->update(['file_path' => $name]);


        if($mail) {

            $this->sendMail($output, $name,$docs);
        }

        return view('quotes.prev', compact('data'));

        //return $pdf->download('quotes.pdf');
    }

    public function sendMail($output,$name,$docs)
    {
        $to = $docs->Customer->email;
        $html = 'Dear '.ucfirst($docs->Customer->first_name.' '.$docs->Customer->last_name).', Thank you for your Quotation Request. Please find attached quotation file.';
       \Mail::send(array(), array(), function ($message) use($output, $name, $to, $html) {
           $message->to($to)
                ->subject('Quotation')
                ->from('tradenfill@gmail.com')->attachData($output, $name,['mime' => 'application/pdf'])
                ->setBody($html, 'text/html');
        });
    }

    public function sendPdf(Request $request, $id)
    {
        $docs = Quote::with('Act')->with('Service')->with('FormationType')->with('Customer')->find($id);
        $user = Customer::find($docs->Customer->id);
        $template = 'emails.quote_mail';
        $subject = $request->subj;
        $msg = $request->message;
        $email = $request->email;
        $prefix=$request->prefix;
        //$time = date()->date_format('H')->timezone_open('Asia/Kolkata');
        //print $time; die;
        $otherAttachment =null;
        $name = $docs->file_path;
        if($request->hasFile('otherfile')){
            $otherAttachment = $request->file('otherfile');

        }

        $file = base_path().'/public/storage/'.$name;

        try{
            Quote::sendWelcomeConfirmEmail($email, $user, $subject, $msg,$file,$otherAttachment);
        }catch (Exception $exception) {
            $error_messages = $exception->getMessage();
            return redirect()->route('quotes.quote.index')
                         ->withErrors($error_messages);
        }

//
//        $to = $docs->Customer->email;
//
//        $sub = $request->subj;
//        $html = $request->message;
//            \Mail::send(array(), array(), function ($message) use($file, $to,$name, $html,$sub) {
//            $message->to($to)
//                ->subject($sub)
//                ->from('tradenfill@gmail.com')->attach($file, [], ['mime' => 'application/pdf'])
//                ->setBody($html, 'text/html');
//        });

        $data = [
          'sent_by' => Auth::user()->id, 'sent_date' => date('Y-m-d H:i:s'), 'status'=>'sent'
        ];
        Quote::where('id',$id)->update($data);

        return redirect()->route('quotes.quote.index')
            ->with('success_message', 'Quote mail send to customer successfully');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function preview($id)
    {

        $document = Quote::find($id);


        $filePath = env('APP_URL').'storage/'.$document->file_path;

        $pdfContent = file_get_contents($filePath);

        // for pdf, it will be 'application/pdf'
        $type       = 'application/pdf';
        $fileName   = $document->file_path;

       return  \Response::make($pdfContent, 200, [
            'Content-Type'        => $type,
            'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);

    }

    public static function getAllDocuments(Request $request)
    {
        $act = $request->act_id;
        $service = $request->service_id;
        $formation = $request->formation_id;

        $docs = Document::All()->pluck('name','id');
        return $docs;
    }



    public static function saveDocuments(Request $request)
    {

        $act = $request->act;
        $service = $request->serv;
        $formation = $request->fmt;
        $list =  $request->list;
        $id='';



        $docs = ServiceDocument::where('act',$act)->where('service',$service)->where('fotmation',$formation)->pluck('id');

        if(!empty($docs) && count($docs)>0)
        {

           $id = $docs[0];
            $documents = ServiceDocumentRef::where('service_doc_id',$id)->pluck('document_id')->toArray();

            foreach ($list as $val)
            {
                if(!in_array($val,$documents))
                {
                    $docRef = new ServiceDocumentRef();
                    $docRef->service_doc_id = $id;
                    $docRef->document_id = $val;
                    $docRef->save();
                }
            }

        }
        else{

            $servDocs = new ServiceDocument();
            $servDocs->act = $act;
            $servDocs->service = $service;
            $servDocs->fotmation = $formation;
            $servDocs->save();
            $id = $servDocs->id;

            foreach ($list as $val)
            {
                $docRef = new ServiceDocumentRef();
                $docRef->service_doc_id = $id;
                $docRef->document_id = $val;
                $docRef->save();
            }
        }

        $servicDocs = ServiceDocumentRef::where('service_doc_id', $id)->pluck('document_id')->toArray();
        $docs = Document::whereIn('id', $servicDocs)->pluck('name','id');
        return $docs;
        //return redirect()->back()->withInput();
    }

    public function CanDelete()
    {
        $userRoleId = auth()->user()->role_id;
        if($userRoleId==1)
        {
            return true;
        }
        return false;
    }

}
