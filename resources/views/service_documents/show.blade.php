@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            {{--<h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Service Document' }}</h4>--}}

            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('service_documents.service_document.index')}}">Service Documents</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Service Documents</li>
  </ol>
</nav>
            </div>
        </span>

        <div class="pull-right">

            <!-- <form method="POST" action="{!! route('service_documents.service_document.destroy', $serviceDocument->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('service_documents.service_document.index') }}" class="btn btn-primary" title="Show All Service Document">
                        <span class="fa fa-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('service_documents.service_document.create') }}" class="btn btn-success" title="Create New Service Document">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('service_documents.service_document.edit', $serviceDocument->id ) }}" class="btn btn-primary" title="Edit Service Document">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Service Document" data-toggle="modal" data-target="#deleteModel">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form> -->




            <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon show-icon">
                                        <span class="dot dot1 show-dot show-dot1"></span>
                                        <span class="dot dot2 show-dot show-dot2"></span> 
                                        <span class="dot dot3 show-dot show-dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                                    <a href="{{ route('service_documents.service_document.index') }}" class="btn dropdown-item" title="Show All Service Document">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Service Document
                    </a>

                    <a href="{{ route('service_documents.service_document.create') }}" class="btn dropdown-item" title="Create New Service Document">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Service Document
                    </a>
                    
                    <a href="{{ route('service_documents.service_document.edit', $serviceDocument->id ) }}" class="btn dropdown-item" title="Edit Service Document">
                        <span class="fa fa-pencil" aria-hidden="true"></span> Edit Service Document
                    </a>

                    <button type="button" class="btn dropdown-item" title="Delete Service Document" data-toggle="modal" data-target="#deleteModel{{$serviceDocument->id}}">
                        <span class="fa fa-trash" aria-hidden="true"></span> Delete Service Document
                    </button>
                                
                                    </div>
                                </div>
                            </form>


                       

        </div>
        <form method="POST" action="{!! route('service_documents.service_document.destroy', $serviceDocument->id) !!}" accept-charset="UTF-8">
        <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$serviceDocument->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>

        </div>

    </div>

    <div class="panel-body bak-white">
        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-3">
                <h4><span><li class="fa fa-file"></li></span>  Service Docs Detail</h4>

            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Act</dt>
                <dd>{{ $serviceDocument->Act->name }}</dd>
            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Service</dt>
                <dd>{{ $serviceDocument->Service->title }}</dd>
            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Formation</dt>
                <dd>{{ $serviceDocument->FormationType->name }}</dd>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <h4><span><li class="fa fa-money"></li></span>  Service Charges</h4>

            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Govt Fee</dt>
                <dd>{{'₹'. ($serviceDocument->govt_fee!=''?$serviceDocument->govt_fee:0) }}</dd>
            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Prof Fees</dt>
                <dd>{{'₹'. ($serviceDocument->prof_fees!=''?$serviceDocument->prof_fees:0) }}</dd>
            </div>
            <div class="col-md-3 col-sm-3">
                <dt>Duration</dt>
                <dd>{{ ($serviceDocument->duration!=''?$serviceDocument->duration:0) }} {{  $serviceDocument->unit }}</dd>
            </div>
        </div>
        {{--<div class="row">--}}
            {{--<div class="col-md-3 col-sm-3">--}}
                {{--<dt>Created At</dt>--}}
                {{--<dd>{{ $serviceDocument->created_at }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-3 col-sm-3">--}}
                {{--<dt>Updated At</dt>--}}
                {{--<dd>{{ $serviceDocument->updated_at }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
</div>

@endsection