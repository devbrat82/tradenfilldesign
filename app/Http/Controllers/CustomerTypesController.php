<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class CustomerTypesController extends Controller
{

    /**
     * Display a listing of the customer types.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $customerTypes = CustomerType::get();

        return view('customer_types.index', compact('customerTypes'));
    }

    /**
     * Show the form for creating a new customer type.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('customer_types.create');
    }

    /**
     * Store a new customer type in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            CustomerType::create($data);

            return redirect()->route('customer_types.customer_type.index')
                             ->with('success_message', 'Customer Type was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified customer type.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $customerType = CustomerType::findOrFail($id);

        return view('customer_types.show', compact('customerType'));
    }

    /**
     * Show the form for editing the specified customer type.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $customerType = CustomerType::findOrFail($id);
        

        return view('customer_types.edit', compact('customerType'));
    }

    /**
     * Update the specified customer type in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
        try {
            
            $data = $this->getData($request,$id);
            
            $customerType = CustomerType::findOrFail($id);
            $customerType->update($data);

            return redirect()->route('customer_types.customer_type.index')
                             ->with('success_message', 'Customer Type was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified customer type from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $customerType = CustomerType::findOrFail($id);
            $customerType->delete();

            return redirect()->route('customer_types.customer_type.index')
                             ->with('success_message', 'Customer Type was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request,$id=null)
    {
        $rules = [
            'name' => 'string|min:1|max:255|unique:customer_types,name,'.$id,
            'status' => 'nullable',
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
