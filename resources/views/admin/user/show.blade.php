@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="">{{ isset($users->name) ? $users->name : 'Users' }}</h4>
            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('users.user.index')}}">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Users</li>
  </ol>
</nav>
                </div>
        </span>

            <div class="pull-right">

                <form method="POST" action="{!! route('users.user.destroy', $users->id) !!}" accept-charset="UTF-8">
                    <input name="_method" value="DELETE" type="hidden">
                    {{ csrf_field() }}
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('users.user.index') }}" class="btn btn-primary" title="Show All Users">
                            <span class="fa fa-list" aria-hidden="true"></span> Show All Users
                        </a>

                    </div>
                </form>

            </div>

        </div>
 
        <div class="panel-body bak-white">
            <dl class="dl-horizontal">
                {{--<dt>Id</dt>--}}
                {{--<dd>{{ $users->id }}</dd>--}}
                <dt>Email</dt>
                <dd>{{ $users->email }}</dd>
                <dt>Name</dt>
                <dd>{{ ($users->name)}}</dd>
                <dt>Role</dt>
                <dd>{{ ($users->role->title)}}</dd>
                <dt>Created At</dt>
                <dd>{{ $users->created_at }}</dd>

            </dl>

        </div>
    </div>

@endsection