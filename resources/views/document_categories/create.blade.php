@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4>Create New Document Category</h4>
                <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('document_categories.document_category.index')}}">Documents Category</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add Document Category</li>
  </ol>
</nav>
                </div>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
            <a href="{{ route('document_categories.document_category.index') }}" title="Show All Document Category">
                                <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show All Document Category</button>
                            </a>
            </div>

        </div>

        <div class="panel-body bak-white">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('document_categories.document_category.store') }}" accept-charset="UTF-8" id="create_document_category_form" name="create_document_category_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('document_categories.form', [
                                        'documentCategory' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


