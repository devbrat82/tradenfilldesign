<?php

namespace App\Mail;

use App\MailToken;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeAccountConfirm extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    public $subject;
    protected $msg;
    protected $file;
    protected $otherAttachment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$subject=null,$msg=null, $file=null, $otherAttachment=null)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->msg = $msg;
        $this->file = $file;
        $this->otherAttachment = $otherAttachment;

        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $msg = $this->msg;
       $response =  $this->subject($subject)
            ->view('emails.quote', [
                'type' => MailToken::TYPE_MAIL_QUOTE,
                'user_id' => $this->user->id,
                'msg' => $msg,
                'name' => ucfirst($this->user->prefix.' '.$this->user->first_name),
                'user'=> $this->user
            ]);
            if($this->file !=null)
            {
                $response->attach($this->file,
                    [
                        'mime' => 'application/pdf',
                    ]);
            }
            if($this->otherAttachment!=null) {

                $response->attach($this->otherAttachment->getRealPath(),
                    [
                        'as' => $this->otherAttachment->getClientOriginalName(),
                        'mime' => $this->otherAttachment->getClientMimeType(),
                    ]);
            }

        return $response;
    }
}
