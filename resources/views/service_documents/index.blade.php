@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-check"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <div class="card form-card">
                    <div class="card-header">
                     <div class="card-title pull-left">Service Documents
                     <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    
    <li class="breadcrumb-item active" aria-current="page">Service Dcuments</li>
  </ol>
</nav>
                </div>
                     </div>
                
                        <a href="{{ route('service_documents.service_document.create') }}">
                        <button class="btn btn-labeled btn-green mb-2  pull-right customer-btn" type="button">
                           <span class="btn-label customer-btn-label"><i class="fa fa-plus"></i>
                           </span>New Service Documents
                        </button>
                        </a>

                 
                    
                        
                        <button class="btn btn-green  buttons-excel" type="button" onclick="excelgen('{{ route('service_documents.service_document.excel') }}')">
                           {{--<span class="btn-label"><i class="fas fa-file-excel"></i></span>--}}
                           {{--Excel--}}
                        </button>

                    </div>
                    <div class="card-body">
                <table class="table table-striped full-width" id="datatable-serviceDocuments">
                    <thead>
                        <tr class='checkbox-row'>
                        <th style='padding-right:0px;'>
                           <span class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="1" onclick="check(this);">
                                    <label class="custom-control-label" for="customCheck1"></label>
                                </span>
                           </th>
                            <th>Main</th>
                            <th>Service</th>
                            <th>Formation</th>
                            <th style="width:60px">Govt Fee</th>
                            <th style="width:60px">Prof Fees</th>
                            <th style="width:75px">Assoc. Docs</th>
                            <th>Duration</th>

                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($serviceDocuments))
                    @foreach($serviceDocuments as $serviceDocument)
                        <tr>
                        <td>
                           <span class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input custom-check" id="customCheck{{ $serviceDocument->id }}" name="customername" value="{{ $serviceDocument->id }}">
                                    <label class="custom-control-label" for="customCheck{{ $serviceDocument->id }}"></label>
                                </span>
                           </td>
                            
                            <td>{{ $serviceDocument->Act['name'] }}</td>
                            <td>{{ $serviceDocument->Service['title'] }}</td>
                            <td>{{ $serviceDocument->FormationType['name'] }}</td>
                            <td>{{'₹'. ($serviceDocument->govt_fee!=''?$serviceDocument->govt_fee:0) }}</td>
                            <td>{{'₹'. ($serviceDocument->prof_fees!=''?$serviceDocument->prof_fees:0) }}</td>
                            <td><a href="{{ route('service_documents.service_document.documents',$serviceDocument->id) }}">{{ $serviceDocument->docs_count }}</a></td>
                            <td>{{ ($serviceDocument->duration!=''?$serviceDocument->duration:0) }} {{ $serviceDocument->unit }}</td>

                            <td>

                                <form method="POST" action="" accept-charset="UTF-8">
                              














                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon">
                                        <span class="dot dot1"></span>
                                        <span class="dot dot2"></span> 
                                        <span class="dot dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                    
                                        <a href="{{ route('service_documents.service_document.show', $serviceDocument->id ) }}" class="dropdown-item btn  btn-xs" title="Show Service Document">
                                            <span class="fa fa-eye" aria-hidden="true"></span> Show Service Document
                                        </a>
                                        <a href="{{ route('service_documents.service_document.edit', $serviceDocument->id ) }}" class="dropdown-item btn  btn-xs" title="Edit Service Document">
                                            <span class="fa fa-pencil" aria-hidden="true"></span> Edit Service Document
                                        </a>

                                        <button type="button" class="dropdown-item btn btn-xs" title="Delete Service Document" data-toggle="modal" data-target="#deleteModel{{$serviceDocument->id}}">
                                            <span class="fa fa-trash" aria-hidden="true"></span> Delete Service Document
                                        </button>
                                
                                    </div>
                                </div>
                                </form>

                                <form method="POST" action="{!! route('service_documents.service_document.destroy', $serviceDocument->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$serviceDocument->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading' > You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- Datatables-->
    <link rel="stylesheet" href="{{url('theme-angle')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">

@endsection
@section('javascript')
    <script src="{{url('theme-angle')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/jszip/dist/jszip.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/pdfmake.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/vfs_fonts.js"></script>

<script>

    $('#datatable-serviceDocuments').DataTable({
        'paging': true, // Table pagination
        'ordering': true, // Column ordering
        'info': true, // Bottom left status text
        responsive: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            //sSearch: 'Search all columns:',
            sLengthMenu: '_MENU_ records per page',
            info: 'Showing page _PAGE_ of _PAGES_',
            zeroRecords: 'Nothing found - sorry',
            infoEmpty: 'No records available',
            infoFiltered: '(filtered from _MAX_ total records)',
            oPaginate: {
                sNext: '<em class="fa fa-caret-right"></em>',
                sPrevious: '<em class="fa fa-caret-left"></em>'
            }
        },
        // Datatable Buttons setup
        dom: 'Bfrtip',
        buttons: [
           // { extend: 'copy', className: 'btn-green' },
           // { extend: 'csv', className: 'btn-green' },
            //{ extend: 'excel', className: 'btn-green', title: 'XLS-File' },
           // { extend: 'pdf', className: 'btn-green', title: $('title').text() },
//{ extend: 'print', className: 'btn-green' }
        ]
    });

</script>
<script language="JavaScript">
function check(source) {
  checkboxes = $('[id^="customCheck"]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}


function excelgen(var1)
{
    var selected = [];
    $('[name^="customername"]').each(function() {
        if ($(this).is(":checked")) {
        selected.push($(this).attr('value'));
        }
    });
    selected=selected.toString()
    window.location.href = var1+"?id="+selected;
}
$('.custom-check').change(function(){ //".checkbox" change
    if($('.custom-check:checked').length == $('.custom-check').length){
        $('#customCheck1').prop('checked',true);
    }else{
        $('#customCheck1').prop('checked',false);
    }
});
$('input[type="search"]').attr('placeholder','Search here');
</script>
@endsection