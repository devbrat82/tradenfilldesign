<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'prefix',
                  'firstname',
                  'lastname',
                  'email',
                  'contact1',
                  'contact2',
                  'add1',
                  'add2',
                  'city',
                  'state',
                  'country',
                  'pincode',
                  'registration_date',
                  'customer_type'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'registration_date'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    


    /**
     * Get created_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getCreatedAtAttribute($value)
    {
        return \DateTime::createFromFormat('j/n/Y g:i A', $value);

    }

    /**
     * Get updated_at in array format
     *
     * @param  string  $value
     * @return array
     */
    public function getUpdatedAtAttribute($value)
    {
        return \DateTime::createFromFormat('j/n/Y g:i A', $value);

    }

    public function Type()
    {
        return $this->hasOne(CustomerType::Class, 'id', 'customer_type');
    }

    public static function saveRecord($request, $id=0){

        $customer = ($id==0)?new Customer():Customer::FindOrFail($id);
        if($id ==0){
            $customer->cust_code = (string) self::customerCode();
        }
        $customer->prefix = (string) $request->prefix;

        $customer->first_name = (string) $request->firstname;

        $customer->last_name = (string) $request->lastname;

        $customer->email = (string) $request->email;

        $customer->phone = (string) $request->contact1;

        $customer->contact2 = (string) $request->contact2;

        $customer->add1 = (string) $request->add1;

        $customer->add2 = (string) $request->add2;

        $customer->city = (string) $request->city;

        $customer->state = (string) $request->state;

        $customer->country = (string) $request->country;


        $customer->pin_code =  $request->pincode;

//        $customer->registration_date =  date('Y-m-d', strtotime($request->registration_date));

        $customer->customer_type =  $request->customer_type;
        if($request->customer_type ==2)
            $customer->company_name =  $request->company_name;


        $customer->save();
    }

    public static function customerCode()
    {
        $id = self::latest()->first()->id;
        $id +=1;
        return 'CU'.rand(0,999999).$id;
    }

}
