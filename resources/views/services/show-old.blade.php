@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($service->title) ? $service->title : 'Service' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('services.service.destroy', $service->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('services.service.index') }}" class="btn btn-primary" title="Show All Service">
                        <span class="fa fa-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('services.service.create') }}" class="btn btn-success" title="Create New Service">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('services.service.edit', $service->id ) }}" class="btn btn-primary" title="Edit Service">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Service" onclick="return confirm(&quot;Delete Service??&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <dt>Title</dt>
                <dd>{{ $service->title }}</dd>
            </div>
            <div class="col-md-6 col-sm-6">
                <dt>Act</dt>
                <dd>{{ $service->Act->name }}</dd>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <dt>Created At</dt>
                <dd>{{ $service->created_at }}</dd>
            </div>
            <div class="col-md-6 col-sm-6">
                <dt>Updated At</dt>
                <dd>{{ $service->updated_at }}</dd>
            </div>
        </div>
    </div>
</div>

@endsection