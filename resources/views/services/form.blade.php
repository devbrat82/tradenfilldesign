<div class="form-group {{ $errors->has('act') ? 'has-error' : '' }}">
    <label for="act" class="col-md-2 control-label">Act</label>
    <div class="col-md-10">
        <select class="form-control" id="act" name="act" required>
            <option value="" style="display: none;" {{ old('act', optional($service)->act ?: '') == '' ? 'selected' : '' }} disabled selected>Select act</option>
            @foreach ($act as $key => $text)
                <option value="{{ $key }}" {{ old('act', optional($service)->act) == $key ? 'selected' : '' }}>
                    {{ $text }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('act', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Title</label>
    <div class="col-md-10">
        <input class="form-control" name="title" type="text" id="title" required value="{{ old('title', optional($service)->title) }}" minlength="1" maxlength="255" placeholder="Enter title here...">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('package') ? 'has-error' : '' }}">
{{--<label for="act" class="col-md-2 control-label">Package</label>--}}

{{--<form   name="edit_sys_setting_form" accept-charset="UTF-8" class="form-horizontal">--}}
    {{--{{ csrf_field() }}--}}
    {{--<input name="_method" type="hidden" value="PUT">--}}
    {{--<input name="id" type="hidden" value="{{ @$service->id }}">--}}
    {{--<input name="constant_key" type="hidden" value="{{ @$key }}">--}}
    {{--<input name="constant_type" type="hidden" value="html">--}}
    {{--<input name="section" type="hidden" value="front">--}}

    <label for="act" class="col-md-2 control-label">Package</label>

    <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
        <div class="col-md-10">
            <textarea  style="height: 400px"  class="form-control tinymce" name="package" cols="50" rows="10" id="value" minlength="1" placeholder="Enter terms here...">{{ old('package', optional($service)->package) }}</textarea>
            {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


</div>
@section('javascript')
    <script src='{{ asset('theme-angle')}}/vendor/tinymce/tinymce.min.js'></script>
    <script>
        tinymce.init({
            selector: '.tinymce',
            menubar:false,
            theme: 'modern',
            plugins: 'code fullscreen image link media  hr pagebreak nonbreaking anchor  insertdatetime advlist lists textcolor wordcount   imagetools     colorpicker textpattern',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>
@endsection