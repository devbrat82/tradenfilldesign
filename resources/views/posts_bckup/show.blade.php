@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($post->title) ? $post->title : 'Post' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('posts.post.destroy', $post->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('posts.post.index') }}" class="btn btn-primary" title="Show All Post">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('posts.post.create') }}" class="btn btn-success" title="Create New Post">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('posts.post.edit', $post->id ) }}" class="btn btn-primary" title="Edit Post">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Post" onclick="return confirm(&quot;Delete Post??&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Category</dt>
            <dd>{{ optional($post->category)->title }}</dd>
            <dt>Title</dt>
            <dd>{{ $post->title }}</dd>
            <dt>Description</dt>
            <dd>{{ $post->description }}</dd>
            <dt>Img Src</dt>
            <dd>{{ asset('storage/' . $post->img_src) }}</dd>
            <dt>Published</dt>
            <dd>{{ $post->published }}</dd>
            <dt>Is Main</dt>
            <dd>{{ ($post->is_main) ? 'False' : 'True' }}</dd>
            <dt>Published At</dt>
            <dd>{{ $post->published_at }}</dd>
            <dt>Created By</dt>
            <dd>{{ optional($post->creator)->name }}</dd>
            <dt>Created At</dt>
            <dd>{{ $post->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $post->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection