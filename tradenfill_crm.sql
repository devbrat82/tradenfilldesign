-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 12, 2019 at 06:32 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tradenfill_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `acts`
--

DROP TABLE IF EXISTS `acts`;
CREATE TABLE IF NOT EXISTS `acts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acts`
--

INSERT INTO `acts` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ACCOUNTS', '2019-08-05 00:07:37', '2019-08-05 00:07:37'),
(2, 'PAN (PERMANENT ACCOUNT NUMBER)', '2019-08-05 00:08:00', '2019-08-20 17:40:11'),
(3, 'INCOME TAX', '2019-08-20 12:39:45', '2019-08-20 17:36:46'),
(4, 'TAN', '2019-08-20 17:37:20', '2019-08-20 17:37:20'),
(5, 'TDS (TAX DEDUCT AT SOURCE)', '2019-08-20 17:37:29', '2019-08-20 17:39:26'),
(6, 'GST (GOODS AND SERVICE TAX)', '2019-08-20 17:38:30', '2019-08-20 17:39:43'),
(7, 'MCA / ROC', '2019-08-20 17:49:47', '2019-08-20 17:49:47'),
(8, 'NGO (NON-GOVERNMENT ORGANISATION)', '2019-08-20 17:50:58', '2019-08-20 17:50:58'),
(9, 'MSME', '2019-08-20 17:52:06', '2019-08-20 17:52:06'),
(10, 'IEC (IMPORT & EXPORT CODE)', '2019-08-20 17:52:42', '2019-08-20 17:52:42'),
(11, 'FSSAI (FOOD LICENSE)', '2019-08-20 17:52:51', '2019-08-20 17:54:41'),
(12, 'DSC (DIGITAL SIGNATURE CERTIFICATE)', '2019-08-20 17:53:53', '2019-08-20 17:53:53'),
(13, 'ISO (INTERNATIONAL STANDARD ORGANISATION)', '2019-08-20 17:54:21', '2019-08-20 17:54:21'),
(14, 'PROFESSIONAL TAX', '2019-08-20 17:55:04', '2019-08-20 17:55:04'),
(15, 'TRADEMARK', '2019-08-20 17:55:49', '2019-08-20 17:55:49'),
(16, 'COPYRIGHT', '2019-08-20 17:55:59', '2019-08-20 17:55:59'),
(17, 'PATENT', '2019-08-20 17:56:06', '2019-08-20 17:56:06'),
(18, 'DESIGN REGISTRATION', '2019-08-20 17:56:22', '2019-08-20 17:56:22'),
(19, 'CONTRACT LABOUR LICENSE', '2019-08-20 17:56:58', '2019-08-20 17:56:58'),
(20, 'FACTORY LICENSE', '2019-08-20 17:57:07', '2019-08-20 17:57:07'),
(21, 'PSARA (PRIVATE SECURITY LICENSE)', '2019-08-20 17:57:23', '2019-08-20 17:57:23'),
(22, 'SHOP & ESTABLISHMENT ACT', '2019-08-20 17:57:36', '2019-08-20 17:57:36'),
(23, 'TRADE / BUSINESS LICENSE', '2019-08-20 17:57:58', '2019-08-20 17:57:58'),
(24, 'ESI (EMPLOYEES STATE\'S INSURANCE)', '2019-08-20 17:58:37', '2019-08-20 17:58:37'),
(25, 'EPF (EMPLOYEES PROVIDENT FUND)', '2019-08-20 17:59:05', '2019-08-20 17:59:05'),
(26, 'PAYROLL', '2019-08-20 17:59:42', '2019-08-20 17:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cust_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_date` date DEFAULT NULL,
  `customer_type` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_contact1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_contact2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_add1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_add2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `cust_code`, `first_name`, `last_name`, `email`, `phone`, `contact2`, `add1`, `add2`, `city`, `state`, `country`, `pin_code`, `registration_date`, `customer_type`, `company_name`, `contact_person`, `person_contact1`, `person_contact2`, `person_email`, `person_add1`, `person_add2`, `person_country`, `person_city`, `person_state`, `person_pincode`, `created_at`, `updated_at`) VALUES
(5, 'CU8670115', 'Awadhesh', 'Singh', 'awadhesh.glocalview@gmail.com', '1234567890', '12345678934', 'Glocalview', 'A-37, B-block', 'Noida', 'Utter Pradesh', 'India', '201301', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-07 23:49:54', '2019-08-07 23:49:54'),
(7, 'CU2915277', 'Pravesh', 'Rathore', 'tax.rathore@gmail.com', '9899868680', '', '107', 'NSP', 'Delhi', 'Delhi', 'India', '110034', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-20 17:04:04', '2019-08-20 17:04:04'),
(8, 'CU7562868', 'Harsh', 'Yadav', 'harsh@gmail.com', '723367238', '847248238', 'Agra', 'Agra', 'Agra', 'Utter pradesh', 'India', '201301', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-22 13:39:54', '2019-08-22 13:39:54'),
(9, 'CU1060779', 'Kapli', 'Sharma', 'kapil.sharma@gmail.com', '283768736723', '27838782323', 'Noida', 'Noida', 'Noida', 'Uttar Pradesh', 'India', '201302', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-13 07:26:54', '2019-09-13 07:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `customer_service`
--

DROP TABLE IF EXISTS `customer_service`;
CREATE TABLE IF NOT EXISTS `customer_service` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_types`
--

DROP TABLE IF EXISTS `customer_types`;
CREATE TABLE IF NOT EXISTS `customer_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_types`
--

INSERT INTO `customer_types` (`id`, `created_at`, `updated_at`, `name`, `status`) VALUES
(1, '2019-07-31 05:16:31', '2019-07-31 05:16:31', 'Individual', '1'),
(2, '2019-07-31 05:16:44', '2019-07-31 05:16:44', 'Company', '1');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documenttype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `alias`, `description`, `documenttype`, `created_at`, `updated_at`) VALUES
(1, 'Pancard', 'Pan', 'Pan', '1', '2019-08-01 02:04:01', '2019-08-01 05:14:25'),
(2, 'VoterCard', 'VoterCard', 'VoterCard', '1', '2019-08-01 02:04:30', '2019-08-01 05:18:46'),
(4, 'Driving Licence', 'DL', 'Driving Licence', '2', '2019-08-08 03:07:56', '2019-08-08 03:07:56'),
(5, 'Passport', 'Passport', 'Passport', '1', '2019-08-08 03:08:30', '2019-08-08 03:08:30'),
(6, 'Declaration form', 'Declaration form', 'Declaration form', '1', '2019-08-08 03:09:31', '2019-08-08 03:09:31'),
(7, 'Photo', 'Passport Size Photo', NULL, '1', '2019-08-20 17:08:36', '2019-08-20 17:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `document_categories`
--

DROP TABLE IF EXISTS `document_categories`;
CREATE TABLE IF NOT EXISTS `document_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_categories`
--

INSERT INTO `document_categories` (`id`, `created_at`, `updated_at`, `name`, `status`) VALUES
(1, '2019-07-31 05:12:46', '2019-07-31 05:12:46', 'Id proof', '1'),
(2, '2019-07-31 05:13:04', '2019-07-31 05:13:04', 'Address proof', '1');

-- --------------------------------------------------------

--
-- Table structure for table `formation_types`
--

DROP TABLE IF EXISTS `formation_types`;
CREATE TABLE IF NOT EXISTS `formation_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `formation_types`
--

INSERT INTO `formation_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'INDIVIDUAL', '2019-08-05 00:14:14', '2019-08-20 18:15:29'),
(2, 'Individual / Partnership / Limited Liability Partnership / Company', '2019-08-05 00:14:31', '2019-08-05 00:14:31'),
(3, 'BUSINESS / PROFESSION', '2019-08-05 00:14:54', '2019-08-20 18:15:43'),
(4, 'PROPRIETORSHIP', '2019-08-05 00:15:08', '2019-08-20 18:13:24'),
(5, 'PARTNERSHIP', '2019-08-20 18:13:07', '2019-08-20 18:13:07'),
(6, 'LIMITED LIABILITY PARTNERSHIP', '2019-08-20 18:13:50', '2019-08-20 18:13:50'),
(7, 'PRIVATE LIMITED COMPANY', '2019-08-20 18:14:04', '2019-08-20 18:14:04'),
(8, 'PUBLIC LIMITED COMPANY', '2019-08-20 18:14:18', '2019-08-20 18:14:18'),
(9, 'ONE PERSON COMPANY', '2019-08-20 18:14:28', '2019-08-20 18:14:28'),
(10, 'TRUST', '2019-08-20 18:15:53', '2019-08-20 18:15:53'),
(11, 'GOVERNMENT ORGANISATION', '2019-08-20 18:16:06', '2019-08-20 18:16:06'),
(12, 'RETAILERS', '2019-08-20 18:17:02', '2019-08-20 18:17:02'),
(13, 'WHOLESALER', '2019-08-20 18:17:15', '2019-08-20 18:17:58'),
(14, 'DISTRIBUTOR', '2019-08-20 18:18:21', '2019-08-20 18:18:21'),
(15, 'MANUFACTURER', '2019-08-20 18:18:31', '2019-08-20 18:18:31'),
(16, 'IMPORTER', '2019-08-20 18:18:39', '2019-08-20 18:18:39'),
(17, 'EXPORTER', '2019-08-20 18:18:48', '2019-08-20 18:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_10_10_000000_create_menus_table', 1),
(4, '2015_10_10_000000_create_roles_table', 1),
(5, '2015_10_10_000000_update_users_table', 1),
(6, '2015_12_11_000000_create_users_logs_table', 1),
(7, '2016_03_14_000000_update_menus_table', 1),
(8, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(9, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(10, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(11, '2016_06_01_000004_create_oauth_clients_table', 1),
(12, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(13, '2018_08_26_062933_create_todos_table', 1),
(14, '2018_09_23_182354_create_tmpcountries_table', 1),
(23, '2019_06_25_143952_create_categories_table', 2),
(27, '2019_06_25_144040_create_posts_table', 3),
(29, '2019_07_07_035707_add_display_type_to_posts_table', 4),
(31, '2019_07_14_060415_create_contactus_queries_table', 5),
(32, '2019_07_31_065840_create_services_table', 6),
(33, '2019_07_31_071804_create_permissions_table', 7),
(34, '2019_07_31_073215_create_document_categories_table', 8),
(35, '2019_07_31_074104_create_customer_types_table', 9),
(36, '2019_07_31_091025_create_documents_table', 10),
(37, '2019_07_31_105755_service_document', 11),
(38, '2019_07_31_110230_customer_service', 12),
(39, '2019_07_31_110529_customers', 13),
(40, '2019_08_05_053303_create_acts_table', 14),
(41, '2019_08_05_053957_create_formation_types_table', 15),
(42, '2019_08_05_065736_create_service_documents_table', 16),
(43, '2019_08_05_112035_create_quotes_table', 17);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('02786963343cb9210521868d0b4632655c9491e26b6d70754ef58d7f6220e3fa104566ed144c778a', 56, 2, NULL, '[]', 0, '2018-09-27 06:14:36', '2018-09-27 06:14:36', '2019-09-27 11:44:36'),
('02e49462454cdce8562480792feaa34969149c359903320bb6d8873f4ea4e5bea61cef7b1c1b8d58', 56, 2, NULL, '[]', 0, '2019-03-09 02:01:10', '2019-03-09 02:01:10', '2020-03-09 07:31:10'),
('03f792f6dfb3a7b830bb18700efde1236b987c47f2e6ab3af96a1e5c356301243bad78f22ed7a2ce', 56, 2, NULL, '[]', 0, '2019-03-12 05:43:08', '2019-03-12 05:43:08', '2020-03-12 11:13:08'),
('061ce521351f692528f78614844b5ee5b5eadfe4d2a13e79cf665fca517d06aa93e3722ff2101415', 56, 1, 'Personal Token', '[]', 0, '2018-10-07 13:40:15', '2018-10-07 13:40:15', '2019-10-07 19:10:15'),
('07e000e01b9cc8a185999cad9ca1f260ff39bd14352449c5fc3722d10f7600c3e280e6d86769afb7', 59, 2, NULL, '[]', 0, '2018-10-30 05:09:12', '2018-10-30 05:09:12', '2019-10-30 10:39:12'),
('08422f948c5edeba6f563d22d62d770e2677b491d50a921c5828f78889a9f0bd3e704e9fb81b3a4b', 54, 1, 'Personal Token', '[]', 0, '2018-10-07 13:43:27', '2018-10-07 13:43:27', '2019-10-07 19:13:27'),
('08f64123c481002297e0e4133e4aafd06d2f8f695a5f3566498e5301616501d182b0b9a194fbd32c', 56, 2, NULL, '[]', 0, '2019-02-27 03:55:29', '2019-02-27 03:55:29', '2020-02-27 09:25:29'),
('09202450aa62e8348f6395bf5b5411ee3baa87883ce31023bdddb6f61e68f2ed19728185e1578679', 55, 2, NULL, '[]', 0, '2019-03-13 23:46:41', '2019-03-13 23:46:41', '2020-03-14 05:16:41'),
('0a36b2285a4a560bf3c5c65426656c1a039eeb7991375a68e256aa080b9881551fde75cd02cc414d', 1, 2, NULL, '[]', 0, '2018-09-16 13:02:05', '2018-09-16 13:02:05', '2019-09-16 18:32:05'),
('0a9df511228414ac76b6a2fef071bdd4935a256ea055b43356ec46df0c86b7538e1025c80d2e8598', 55, 2, NULL, '[]', 0, '2019-03-20 04:14:00', '2019-03-20 04:14:00', '2020-03-20 09:44:00'),
('0af16c0a312955f105ac52c1cc83521130b8c4ee0ddbcfedf8106070563eee29f05a60fbd8992623', 1, 2, NULL, '[]', 0, '2018-09-16 13:02:46', '2018-09-16 13:02:46', '2019-09-16 18:32:46'),
('0b87848b489ab397bdc788f6c2e1e7291acfa27639ce34e0ad8b29b9a01b1324f79b8163cff23baf', 18, 2, NULL, '[\"*\"]', 0, '2018-09-16 06:07:00', '2018-09-16 06:07:00', '2019-09-16 11:37:00'),
('104835c58006eefdf8cb9358de85d352bad73b1bd1399fd1bd4c3f32ef90a4bab1667b48675a7781', 56, 2, NULL, '[]', 0, '2018-10-02 23:55:00', '2018-10-02 23:55:00', '2019-10-03 05:25:00'),
('1100709d81cdf432cec1808942789331929110fd9fbe0dc3495f7efd65ed011a3002b7c5f8776758', 56, 2, NULL, '[]', 0, '2018-09-28 00:44:32', '2018-09-28 00:44:32', '2019-09-28 06:14:32'),
('110fd7e5c34f81d556227cfc5deb29c751da4134b5efc631e327ee8b84b9d63bfa1d1ada27294df7', 1, 2, NULL, '[\"*\"]', 0, '2018-09-16 12:48:06', '2018-09-16 12:48:06', '2019-09-16 18:18:06'),
('11993a61909159d2332e9b5d2b2aeebc2efc2de43189b0344ce21c09c248c8e769b3d29126d16049', 59, 2, NULL, '[]', 0, '2018-10-30 05:18:32', '2018-10-30 05:18:32', '2019-10-30 10:48:32'),
('120ca34fb9409b0d7caa45e68b5ee263c77c684a4da3625d1d40fb627e81bbc520f511c2c81d5ed1', 43, 2, NULL, '[]', 0, '2018-09-18 23:25:50', '2018-09-18 23:25:50', '2019-09-19 04:55:50'),
('12a4ad22d7c062859ecc8c577f8fab902508efe66525a56c08aa80fbd12c0af65d885dfd6b349506', 18, 2, NULL, '[]', 0, '2018-09-16 23:11:53', '2018-09-16 23:11:53', '2019-09-17 04:41:53'),
('17038250a56742bcf3ccdaec24fb3f607cca47c03cb249d0e31678cb82edbc82ef606493e10555a2', 43, 2, NULL, '[]', 0, '2018-09-18 23:27:29', '2018-09-18 23:27:29', '2019-09-19 04:57:29'),
('194545cf7343822fcc0b918127fb5dcc5b869b72c12779f2ccc431fb3e55fad3912077164eed85bc', 56, 2, NULL, '[]', 0, '2019-03-12 13:30:51', '2019-03-12 13:30:51', '2020-03-12 19:00:51'),
('199c59c6a0ca1799d20c4fd517b4d3ce293d018e5561b3385db6fef664956054453b7d96c1c9d264', 1, 2, NULL, '[]', 0, '2019-06-11 00:00:36', '2019-06-11 00:00:36', '2020-06-11 05:30:36'),
('1f5e53e3adce0be26fdff326fc31aa8c5a8093fe1a3240dfd5e7a5bc487d4283b2d0bf77215d88b4', 1, 2, NULL, '[]', 0, '2018-09-16 12:59:36', '2018-09-16 12:59:36', '2019-09-16 18:29:36'),
('228c0609c030f22238f4f96accd0ba4f6f631b5c0f0ef95f876c5aca0468f8ad4d5d89c33f12c9ae', 56, 2, NULL, '[]', 0, '2018-10-08 23:01:14', '2018-10-08 23:01:14', '2019-10-09 04:31:14'),
('22ec0062d42deaac277bb48b3832712838c4aa92aa7f0669774e9473f9a377d7218f818fc5034430', 56, 2, NULL, '[]', 0, '2019-03-25 01:17:04', '2019-03-25 01:17:04', '2020-03-25 06:47:04'),
('2348570eae60ce4191b28e6f247dd3faa733785cea7af30f249f83a8f4cc14b230358c138bce624d', 1, 2, NULL, '[\"*\"]', 0, '2018-09-23 01:30:08', '2018-09-23 01:30:08', '2019-09-23 07:00:08'),
('25031176e6bde1aed18dcf0c2579c0626d6572d3353c6f5d7ddd7b462cb0d52b8abbbf2951c7ebd0', 56, 2, NULL, '[]', 0, '2018-11-21 05:20:31', '2018-11-21 05:20:31', '2019-11-21 10:50:31'),
('257840298226ca2f9921c76b9de00c474461020c9dc69f2a540a1a58d590ec97f41010fd555b71b7', 56, 2, NULL, '[]', 0, '2018-09-26 22:21:39', '2018-09-26 22:21:39', '2019-09-27 03:51:39'),
('25e0ef7226235068fedb0388a00bf1beae3bc0cd36f470bee616a3776ae0627fbaa7b1329a1f6d0e', 56, 1, 'Token Name', '[]', 0, '2018-10-07 13:36:51', '2018-10-07 13:36:51', '2019-10-07 19:06:51'),
('2610fc47d294f4c44ca1f70c6cf16dc129cb769ae80620e2806fd1102ac5fcbabc289859597ba86d', 1, 2, NULL, '[]', 0, '2019-06-10 23:59:35', '2019-06-10 23:59:35', '2020-06-11 05:29:35'),
('26155c3f596c5b6b1e33de982be2217f2365764ee82e91cfa7dbd6152b1e9d3383a86329c804a02a', 56, 2, NULL, '[]', 0, '2019-02-28 13:26:16', '2019-02-28 13:26:16', '2020-02-28 18:56:16'),
('26d9300b73c5b0db0075eb984b8e397b7d5c02c6a19886229670feafdf4412fa46616c259e6bcdc5', 56, 2, NULL, '[]', 0, '2019-03-12 04:54:49', '2019-03-12 04:54:49', '2020-03-12 10:24:49'),
('287bcae13d62829407e9c7eaf711e1f7c1ca33805a59fa962834d685eb5bb7d288c2c7720ca5ef20', 56, 2, NULL, '[]', 0, '2019-02-28 13:23:54', '2019-02-28 13:23:54', '2020-02-28 18:53:54'),
('293d3c09eb65904e5d73e494c836802ba0205934fe8cbd9287007c26d128a34a87099546159ab0eb', 56, 2, NULL, '[]', 0, '2018-11-06 04:53:00', '2018-11-06 04:53:00', '2019-11-06 10:23:00'),
('2afb9381d849e3a3861606f010b6fa89fd72ece11df27bb5d102770cec2bd327f3110942468b5003', 56, 2, NULL, '[]', 0, '2018-10-15 07:03:28', '2018-10-15 07:03:28', '2019-10-15 12:33:28'),
('2dd98d9191709873a75255371ab783fcce09299f25610d620d169c673fe2458a1c0a5bddfcef9e0b', 43, 2, NULL, '[]', 0, '2018-09-18 23:30:37', '2018-09-18 23:30:37', '2019-09-19 05:00:37'),
('2e53d90ddcfff8c7efc0b2db2b0a107d5b9a02e9afd3c3e146390c311c9ce1bc0ae86f93e5a718ac', 59, 2, NULL, '[]', 0, '2018-10-28 00:11:35', '2018-10-28 00:11:35', '2019-10-28 05:41:35'),
('303ca4a6a7a3d83a4a330c1953c0defd259ec747e43de208c13017a896d0f5b1c31a1ffb3c1611e3', 18, 2, NULL, '[]', 0, '2018-09-16 13:11:44', '2018-09-16 13:11:44', '2019-09-16 18:41:44'),
('324bf93256ed57cbdb4b0a54c62b812e753433679080a6e82963d817d429aa0e6e6e3ee4d7e5e8e3', 43, 2, NULL, '[]', 0, '2018-09-18 06:36:27', '2018-09-18 06:36:27', '2019-09-18 12:06:27'),
('32d289cce138b04f04e2c4158f9db32652a6d8d0765830628177490f811127f09fee81e9a330c9fb', 1, 2, NULL, '[]', 0, '2018-09-16 13:02:24', '2018-09-16 13:02:24', '2019-09-16 18:32:24'),
('398a3b9eca968a59869e13b87497094ad11de24879629b8eeffd23e3fd427a00d16ee7081c3e991d', 57, 2, NULL, '[]', 0, '2019-04-03 00:47:08', '2019-04-03 00:47:08', '2020-04-03 06:17:08'),
('39b1f4c6723cddd7c65158206ab8bb009fad4ab102cf8ab5be166d8194554f5a21b064c9c2845961', 56, 2, NULL, '[]', 0, '2019-02-28 13:25:28', '2019-02-28 13:25:28', '2020-02-28 18:55:28'),
('3aa4a56c06aaa12be9070eb250cb3ba88f4434cc4428ee36a0c315154aef7ec611201edd907b7246', 56, 2, NULL, '[]', 0, '2019-03-12 05:35:44', '2019-03-12 05:35:44', '2020-03-12 11:05:44'),
('3e386614ec0755b224d2dce0b42c6a442c452004a2fb0c15ec940947f5d57ea40ba5519b0eb0a6cd', 1, 2, NULL, '[\"*\"]', 0, '2018-08-26 06:28:29', '2018-08-26 06:28:29', '2019-08-26 11:58:29'),
('3f0807b817ceb14963e1653628675eb3a95c4f3de43822f36a0001962d910bac6863656e03d9718f', 48, 2, NULL, '[]', 0, '2018-09-23 08:35:41', '2018-09-23 08:35:41', '2019-09-23 14:05:41'),
('40a1de3e14e39c852b2ac979656c0c41ce56de59643e08045b9a653b6f80f1ece9a8db124700c68c', 48, 2, NULL, '[]', 0, '2018-09-23 01:31:52', '2018-09-23 01:31:52', '2019-09-23 07:01:52'),
('41513836a751d6a5010454048aa7b3a92d987249c459c40a3bc3f6f87750ccf5bc96e84aba316483', 1, 2, NULL, '[\"*\"]', 0, '2019-06-11 00:03:09', '2019-06-11 00:03:09', '2020-06-11 05:33:09'),
('4154b1886085aae8463a7167fdcca1316c47d365eaffaff645aa15ebe34cc497daa75ed419066ad2', 48, 2, NULL, '[]', 0, '2018-09-23 02:00:23', '2018-09-23 02:00:23', '2019-09-23 07:30:23'),
('424d428461449a72f29aa4535855209404ed238b809e63dd6e9393058499086a7515b2c8eb138ebd', 56, 2, NULL, '[]', 0, '2019-03-12 04:56:06', '2019-03-12 04:56:06', '2020-03-12 10:26:06'),
('43485df7e9243392b3492b62b4cfce7f40663954c96ea3216aae931574ed4641b1fbc4f9bae66812', 56, 2, NULL, '[]', 0, '2019-03-12 05:00:22', '2019-03-12 05:00:22', '2020-03-12 10:30:22'),
('443bee6cf3bb0abb3e3ff23cc54975f251d44566953e2f97051675a73e6069779a1c0ee00a6aced2', 55, 2, NULL, '[]', 0, '2019-03-13 23:48:04', '2019-03-13 23:48:04', '2020-03-14 05:18:04'),
('449e6fbea07c06ecd5653939a2b3891366f9fead09360ebb30d6cf0afb9760d96b5e4abbfe1805bd', 55, 2, NULL, '[]', 0, '2019-03-20 04:04:40', '2019-03-20 04:04:40', '2020-03-20 09:34:40'),
('457ba9fe8a024eb12df7ef2a1d61df3ebc89c616162f0cbeb317a303b016f1f401911b0a7336c956', 56, 2, NULL, '[]', 0, '2018-11-21 05:37:50', '2018-11-21 05:37:50', '2019-11-21 11:07:50'),
('47a9f5276df01cc222ba0fd5bb56cfb3bdc4e37ac4cc0bb64161ccc6a64df67010593fd1cddd54fa', 1, 2, NULL, '[]', 0, '2018-09-16 12:58:47', '2018-09-16 12:58:47', '2019-09-16 18:28:47'),
('4848160cd70ab2dd9a0790d23d26308c4a4a48bcbfe163ca8497e054e2183f2857f15b9142f2c8bc', 56, 2, NULL, '[]', 0, '2019-02-18 01:16:52', '2019-02-18 01:16:52', '2020-02-18 06:46:52'),
('488de8a9b7aab2fbb1603bda8a5415833d26e3e0674e6149191320d1b4e05de8932e75c8847086fb', 56, 2, NULL, '[]', 0, '2018-11-04 10:17:48', '2018-11-04 10:17:48', '2019-11-04 15:47:48'),
('4a15e300dcba67052b848bb7b97d8695dfbb012841b69363d2871256695dae0d2d82de4ff612c06a', 56, 2, NULL, '[]', 0, '2018-11-21 05:00:32', '2018-11-21 05:00:32', '2019-11-21 10:30:32'),
('4b1bc21f0824ecab46c99a40e66aa0c29c46200becd1a0fff957bc03c268a4d69b40c05b0a4afa57', 18, 2, NULL, '[]', 0, '2018-09-18 04:59:21', '2018-09-18 04:59:21', '2019-09-18 10:29:21'),
('4bc1088f03457ffb3f1ddd95e964b13087fee7975fff33ec52a01f6d895dbfae5f6ff41335e31208', 1, 2, NULL, '[]', 0, '2018-09-16 13:00:33', '2018-09-16 13:00:33', '2019-09-16 18:30:33'),
('4bef1c25e3904650bc473a757edcc2e655a337a9e1d71a0c9fd0064fee28b08f58dd91a845b702a2', 56, 2, NULL, '[]', 0, '2018-11-21 04:43:07', '2018-11-21 04:43:07', '2019-11-21 10:13:07'),
('4d0e8719958f7a8f6f29b0b7598dcdf0613b881fd4f03488006e569328e5f531664e506ae0908129', 43, 2, NULL, '[]', 0, '2018-09-18 06:32:38', '2018-09-18 06:32:38', '2019-09-18 12:02:38'),
('4dc0d3531a8341fae8ef414176617810eb285f452badc19a57793540c4c4e5a3edab36203a27d2b5', 56, 2, NULL, '[]', 0, '2018-10-10 02:47:02', '2018-10-10 02:47:02', '2019-10-10 08:17:02'),
('4f9d59e1720dde3487826cef20e1db507ecf6fa5acbfd0f7019705e40078f5a3ae7f86750c580418', 56, 2, NULL, '[]', 0, '2019-02-18 01:51:30', '2019-02-18 01:51:30', '2020-02-18 07:21:30'),
('4faa71b0a277adb8e1c32e996b3ac399352f2b8cd2b3d4ab7996209993be5ade9cd8398569b89a23', 1, 2, NULL, '[\"*\"]', 0, '2018-08-31 05:17:04', '2018-08-31 05:17:04', '2019-08-31 10:47:04'),
('57aabcb213af4fa286b676b840a29505eb7229c0e4e03eb230444a9d49fc248706d1e0191a3f3752', 1, 2, NULL, '[]', 0, '2018-09-16 13:03:17', '2018-09-16 13:03:17', '2019-09-16 18:33:17'),
('586143891815a1c099a2bffea3990632982929f8493c7ba49b4a7fc2a7a922404aed1f3a9489fc28', 48, 2, NULL, '[]', 0, '2018-09-23 01:03:34', '2018-09-23 01:03:34', '2019-09-23 06:33:34'),
('5874b49cc4d03b9dbd4a2c0c750b22963982f71147def55eb896da7dd494d7f05f9b4af166777a56', 57, 2, NULL, '[]', 0, '2018-09-27 05:34:02', '2018-09-27 05:34:02', '2019-09-27 11:04:02'),
('58dda1223f08944b0de57a845de1c3d21dfb7748dbe27556caa0553e971b955e9bf69f1497c00e69', 56, 2, NULL, '[]', 0, '2018-11-21 05:15:00', '2018-11-21 05:15:00', '2019-11-21 10:45:00'),
('5923c20fd08281673d97f494e6f0bfe9b7840851ff282e256a4bb45973a6cfa854861d8cae150947', 55, 2, NULL, '[]', 0, '2019-03-14 02:00:35', '2019-03-14 02:00:35', '2020-03-14 07:30:35'),
('5a19b1723d86bb99199549a150730f6df94164505fe1943e55bf63efa24d192ee55bdf1b19a7a8f6', 1, 2, NULL, '[]', 0, '2018-09-16 12:58:56', '2018-09-16 12:58:56', '2019-09-16 18:28:56'),
('5a4d0cb1b9d28ffcb0e62d033ca1551a96eb2660aae0284a31d59ae04bdd95e0a3fa964ee07c5758', 59, 2, NULL, '[]', 0, '2019-02-27 04:38:47', '2019-02-27 04:38:47', '2020-02-27 10:08:47'),
('5aa985488e7ef6540466e7123d8d43a385303e4cb5a7238cd331217cb8239e3f5a86f2dc8ed7c0b8', 56, 2, NULL, '[]', 0, '2019-03-06 05:33:13', '2019-03-06 05:33:13', '2020-03-06 11:03:13'),
('5b1acc8a926185784ef6cc88d43f64699f2f391c3b44a8d6b4baea513a4ece5f481e0386d5e2c8f9', 1, 2, NULL, '[]', 0, '2018-09-16 12:57:48', '2018-09-16 12:57:48', '2019-09-16 18:27:48'),
('5b269d0a70d125009f833862f3bd168c9f57fdbebed05a099564400241ea42c655eb7a42da41a019', 57, 2, NULL, '[]', 0, '2018-09-27 23:03:57', '2018-09-27 23:03:57', '2019-09-28 04:33:57'),
('5bc2cbeb018aa192fd79d8630094819c376bda2aff11694580f86b267f881bb9b0521dc288629d63', 18, 2, NULL, '[]', 0, '2018-09-16 13:43:51', '2018-09-16 13:43:51', '2019-09-16 19:13:51'),
('5ce34abdc3b20fac9613190594116fb333cf776bf5fe47719da0cb84705bbcbe09b71feb8938105a', 56, 2, NULL, '[]', 0, '2018-09-23 09:46:35', '2018-09-23 09:46:35', '2019-09-23 15:16:35'),
('5fdb26bb256bbd7b56f10192f3b7e31bb3b1bfb675a9b72c5bc2e7331b183bf1592238d65551fd32', 43, 2, NULL, '[]', 0, '2018-09-18 05:05:58', '2018-09-18 05:05:58', '2019-09-18 10:35:58'),
('61748943ca5547ab10810abe35ee7abd8c5dd3175658a215d8653d4d70e1b20bbdc62ba92d0e2b0a', 55, 2, NULL, '[]', 0, '2019-03-20 04:53:12', '2019-03-20 04:53:12', '2020-03-20 10:23:12'),
('61da349bb6e7d544ffbca318d68f5e965183da31de68600ad967117da31efb24f78c82dff540d71b', 48, 2, NULL, '[]', 0, '2018-09-23 02:18:06', '2018-09-23 02:18:06', '2019-09-23 07:48:06'),
('62b20f4e583e822c2cdbe9f24182f0dd8584a524cfbd1b47c5b9f9c5ac425c7422142972c9564d9f', 55, 2, NULL, '[]', 0, '2018-10-23 03:56:23', '2018-10-23 03:56:23', '2019-10-23 09:26:23'),
('6372431df9b7d518b921fe3ad04037a5e50406b6961a0dad12897f598fd613470b4fedcce88efff9', 56, 2, NULL, '[]', 0, '2018-10-10 00:11:30', '2018-10-10 00:11:30', '2019-10-10 05:41:30'),
('6423b1970adddd1cb3736d7856863832d2dd5fc126d4bde93b57cae8d6710771e15a30e1042765ed', 56, 2, NULL, '[]', 0, '2018-10-22 06:12:58', '2018-10-22 06:12:58', '2019-10-22 11:42:58'),
('647037f33d34e9f2a58df1c3771378a4b1aaf4d572dad8403b949cb4f9861a793ab6a70ee512b636', 56, 2, NULL, '[]', 0, '2019-02-27 03:54:23', '2019-02-27 03:54:23', '2020-02-27 09:24:23'),
('662934b1ddd77672731305174e8376fa25eb31da89e05cdd3579452585e8b6a50a2f1c7c5f6bbe11', 54, 1, 'Personal Token', '[]', 0, '2018-10-28 00:02:42', '2018-10-28 00:02:42', '2019-10-28 05:32:42'),
('66de80acde779c57a90fdf7abe96f405e61bfbca2da6a7637e6703cb6ef1a803331b6f45dca5561b', 56, 2, NULL, '[]', 0, '2018-09-27 05:53:40', '2018-09-27 05:53:40', '2019-09-27 11:23:40'),
('6781ae0aaa1f021a8dea73f14d6e4c832f84e4806a22f51edab80096950c067d7c1baee8921e1092', 18, 2, NULL, '[]', 0, '2018-09-16 13:03:35', '2018-09-16 13:03:35', '2019-09-16 18:33:35'),
('68016db0680ad095af425061faeb9993f186c13b37956168f0d32e001cdae3bbc8f1d11d9e136634', 56, 2, NULL, '[]', 0, '2019-04-03 00:46:21', '2019-04-03 00:46:21', '2020-04-03 06:16:21'),
('68854c35737a861df59d3de3be6c626fd7f86e95d26eeb86e3bf7d60e308faceab006659db521ba7', 1, 2, NULL, '[]', 0, '2019-06-11 00:11:29', '2019-06-11 00:11:29', '2020-06-11 05:41:29'),
('6ae663a8f13b1a962ec660510b784974201687816c9efe51d85a637aed5b34533c40e622ef432d56', 56, 2, NULL, '[]', 0, '2018-09-30 22:41:50', '2018-09-30 22:41:50', '2019-10-01 04:11:50'),
('6c1e745ff24c52e47c7b3f431038fcc92e8046a73d039ae2c0a2f15666fa8848616ff52dc511258b', 55, 2, NULL, '[]', 0, '2019-03-14 02:25:34', '2019-03-14 02:25:34', '2020-03-14 07:55:34'),
('6c29a0908d9c995e9ea2c115e8b62a97ab655f90e8e37751037a382cdf4fea4463064b9e66dc6e31', 56, 2, NULL, '[]', 0, '2019-03-14 02:11:20', '2019-03-14 02:11:20', '2020-03-14 07:41:20'),
('6ce624250a8fc2af558d3601ba840b0543bba2d2898c836313449f9b44203012112499bb4be9bd81', 1, 2, NULL, '[]', 0, '2019-06-10 23:58:31', '2019-06-10 23:58:31', '2020-06-11 05:28:31'),
('6e14cb302ea33faec9d7e189a1710c69dc834d9d57599123cac4f7e6c96beb81906798357a6f0ee4', 56, 2, NULL, '[]', 0, '2019-04-02 23:38:16', '2019-04-02 23:38:16', '2020-04-03 05:08:16'),
('6f1af1eaeef02f393f257dc7d7acb0b0965d7b1ba507692c136736a2e4dad3a248b3e653c6f1f010', 56, 2, NULL, '[]', 0, '2018-11-21 05:29:05', '2018-11-21 05:29:05', '2019-11-21 10:59:05'),
('6f8e7cc91aa1fa0bbc7baee6f41e36869f3761e71e8c25d5de557d9f00a1832744b879cb0d13b3b3', 56, 2, NULL, '[]', 0, '2019-03-12 04:17:01', '2019-03-12 04:17:01', '2020-03-12 09:47:01'),
('705033fd11625266988a56ce7c7cc3b55cf89efe3dc2690877f3a7c7a02ccef7221565b093ea13ab', 48, 2, NULL, '[]', 0, '2018-09-23 07:33:37', '2018-09-23 07:33:37', '2019-09-23 13:03:37'),
('734c48cb9fd50503bc605d569557c40908aeb553259f73a1f5b93c644eff1f84260547c3d437b0d8', 55, 2, NULL, '[]', 0, '2019-03-12 12:11:39', '2019-03-12 12:11:39', '2020-03-12 17:41:39'),
('752091c1b21837cfeb18aa53a8a513281d719ecd57f41d41511a4275b6e579a93665ade88969ce73', 56, 2, NULL, '[]', 0, '2018-09-24 00:39:50', '2018-09-24 00:39:50', '2019-09-24 06:09:50'),
('766a5e555c2e2f22eeb250e662d9594319fd7fbf3f7d2f0bccbd1b1cdcf1eda3f0d68db998eb7836', 1, 2, NULL, '[]', 0, '2018-09-16 12:49:21', '2018-09-16 12:49:21', '2019-09-16 18:19:21'),
('78938ce0bae52b02021d943bac9d60ca904f28ced570c58b51123f88f9098ab0e97b4aa535fb6325', 48, 2, NULL, '[]', 0, '2018-09-23 01:30:18', '2018-09-23 01:30:18', '2019-09-23 07:00:18'),
('789cfecf4cc18d7b92e7309cfa846433ac8a0673fbfda563aaa66f63f0d2c4c832fcf1e9071fd869', 56, 2, NULL, '[]', 0, '2019-03-12 04:42:24', '2019-03-12 04:42:24', '2020-03-12 10:12:24'),
('79b5c8122efe0cc46e69e45138093c3d3c5971b7be5e990f1de975c4083066609075d2ce97987999', 56, 2, NULL, '[]', 0, '2018-10-09 12:43:03', '2018-10-09 12:43:03', '2019-10-09 18:13:03'),
('7cd0756573d4df2d0ef81c6889075575766cee40f4734272dc05d4d124289105d731fed8f7d96c9b', 56, 2, NULL, '[]', 0, '2018-10-10 02:54:37', '2018-10-10 02:54:37', '2019-10-10 08:24:37'),
('7dc8ae6624360d9474b05000b1f7a79b17e536c7e79a4f613d6e0cb66cdb66b925850e24c1f2817b', 48, 2, NULL, '[]', 0, '2018-09-23 01:05:17', '2018-09-23 01:05:17', '2019-09-23 06:35:17'),
('80f8272d7a6016244979302a2c23b7997d1343f113521f8f74bbadebe147fc2f3dfefb37601c2e7f', 56, 2, NULL, '[]', 0, '2019-03-06 05:16:14', '2019-03-06 05:16:14', '2020-03-06 10:46:14'),
('81316147dacdd440f5a5511d8cd046875cd36b405573b7ac85e228a6e0f030c5d61f63dadcd67c93', 55, 2, NULL, '[]', 0, '2018-10-09 12:34:06', '2018-10-09 12:34:06', '2019-10-09 18:04:06'),
('8166739ccf6d6d8587eaaf4669d80f58c65645a21d398cbb1f4a2cd1bb0a9d40a0c9a62b25b30cbd', 56, 2, NULL, '[]', 0, '2019-03-28 04:24:10', '2019-03-28 04:24:10', '2020-03-28 09:54:10'),
('81e335d7caea1678fbd681cb614887bc1f99cfb363501dfa5cd3dd0eca12b6867567cb4a5571241e', 1, 2, NULL, '[\"*\"]', 0, '2019-02-06 06:20:09', '2019-02-06 06:20:09', '2020-02-06 11:50:09'),
('83ebb45b6b549da4563cc18ead53b9d4df2659acedd80c57ef76133856505aae895af5da384b09f9', 56, 2, NULL, '[]', 0, '2019-03-06 01:29:51', '2019-03-06 01:29:51', '2020-03-06 06:59:51'),
('84a99ef9096768cbff5d8807f841e3e9c4bf530e3aec61ed751a96af0c1fbbdea90e98869ba5f29e', 55, 2, NULL, '[]', 0, '2019-03-20 03:45:35', '2019-03-20 03:45:35', '2020-03-20 09:15:35'),
('8559d9220faa68794caec06b47e98c2befd36d76c3df229d2fc675e434e7621caff520bdcce3b7f7', 56, 2, NULL, '[]', 0, '2019-02-18 00:40:29', '2019-02-18 00:40:29', '2020-02-18 06:10:29'),
('85964cd1eee90d06c97addbf44f4e6aea6df08ab847a5dbe26a1401f716de3ca702ae9163db612c2', 56, 2, NULL, '[]', 0, '2018-09-28 00:58:31', '2018-09-28 00:58:31', '2019-09-28 06:28:31'),
('85dc272d450d0f6d16dc340d55879034aad3fb1405513be4b5d4a1a0579f72cc9bcc10a402016e2d', 56, 2, NULL, '[]', 0, '2019-02-17 10:32:33', '2019-02-17 10:32:33', '2020-02-17 16:02:33'),
('863900a30567661a40659575361a3e7c2688064ee33161bf4ecda0ff4128663d201b4400d993c2f5', 56, 2, NULL, '[]', 0, '2018-11-06 05:04:36', '2018-11-06 05:04:36', '2019-11-06 10:34:36'),
('87cdf6548dac719a13d399a0b715c087e426f19e0f192db4aff666074225f240ab6afa5154565d3e', 56, 2, NULL, '[]', 0, '2018-11-22 03:16:26', '2018-11-22 03:16:26', '2019-11-22 08:46:26'),
('89292babe185186f4311c5e790771dc30feb4b3de7db5cd7498be4561f0b425f30e057b3f64cf877', 59, 2, NULL, '[]', 0, '2018-10-30 04:32:55', '2018-10-30 04:32:55', '2019-10-30 10:02:55'),
('89c66de97049165d536fa3e9376642ebcf365cf7f3338fcee78991f2689e901cbed3a7a672b85e04', 56, 2, NULL, '[]', 0, '2018-11-12 01:47:02', '2018-11-12 01:47:02', '2019-11-12 07:17:02'),
('8a77a49ab59e4636ab52a5e558aa23d6510e750394c6cad66713e2161ae58820884b35f8871689f7', 56, 2, NULL, '[]', 0, '2019-02-28 12:34:12', '2019-02-28 12:34:12', '2020-02-28 18:04:12'),
('8be6ba77d457f39d27852102636cf557002100c5aa4915561b728567ceed824359360ee0b4efa895', 56, 2, NULL, '[]', 0, '2018-10-27 12:35:26', '2018-10-27 12:35:26', '2019-10-27 18:05:26'),
('8bebb1697ae68b9630843d7707418ffefeefcf5899432ee641e2a54e57e62e7cab8837585b89f4cf', 56, 2, NULL, '[]', 0, '2018-10-09 12:26:28', '2018-10-09 12:26:28', '2019-10-09 17:56:28'),
('8c12289df71756b3a0f68a0d8cc2dac214b8d4d792b732809b2665a35b914f4b799e87a47546b953', 1, 2, NULL, '[]', 0, '2019-06-11 00:01:57', '2019-06-11 00:01:57', '2020-06-11 05:31:57'),
('8c3413df885aa5c9ac01730bb721fcd667337b027fe62dfdb1503337e0a59b30e2318ef7d69c1c05', 56, 2, NULL, '[]', 0, '2019-03-06 03:53:22', '2019-03-06 03:53:22', '2020-03-06 09:23:22'),
('8d2d4411503b56a5795af5b2e0d0d5d8b3a403b56badb28470c1947f23a0715738810ea0b5ae1d5d', 56, 2, NULL, '[]', 0, '2019-03-08 12:18:46', '2019-03-08 12:18:46', '2020-03-08 17:48:46'),
('8dabc529ff9f1115f400b5582d9255f05a7dc4ba3cb6e179558aa5d380337f860004835f2efda8bf', 56, 2, NULL, '[]', 0, '2019-03-12 04:54:04', '2019-03-12 04:54:04', '2020-03-12 10:24:04'),
('8f134a43ccb7ff941145549a803bdbb2a7e69943b7ed59088a626a76c07b5b859f0d0c16c7717785', 56, 2, NULL, '[]', 0, '2019-01-21 07:33:36', '2019-01-21 07:33:36', '2020-01-21 13:03:36'),
('9068929aba336ea4b41e68cd222933d918b2d0350ec254e4077f1b74b6b854112c2d41c68b40010d', 56, 2, NULL, '[]', 0, '2019-02-18 00:59:59', '2019-02-18 00:59:59', '2020-02-18 06:29:59'),
('92a37a2b07e7941155845e28de176ec062aa876182f111f3b6db973b26bdf4b0f2d489e4a40ee5c4', 18, 2, NULL, '[]', 0, '2018-09-16 13:08:50', '2018-09-16 13:08:50', '2019-09-16 18:38:50'),
('92f0d847f28e03156d6fac034aac54b8959d55bf479eb07846c4f99c6acbc6c59351cfb15bda7588', 56, 2, NULL, '[]', 0, '2019-02-18 01:30:35', '2019-02-18 01:30:35', '2020-02-18 07:00:35'),
('94a18ab07f61f12de4b93d54a5304f5024c2d33fdeb612b7d1f19df83c644a72087c23e83bd15c83', 55, 2, NULL, '[]', 0, '2019-03-12 12:08:37', '2019-03-12 12:08:37', '2020-03-12 17:38:37'),
('963c308057de92793c83eeb0658a4c437289b75cd2cf5547d43f55803079bf8ccd33c0ab651c0bd5', 56, 2, NULL, '[]', 0, '2018-10-15 07:18:23', '2018-10-15 07:18:23', '2019-10-15 12:48:23'),
('96a1ae49d5a3cee5dc8675c7a2c12a4c09723a9f216e7e5a423c1df2603c19f9eca73dfadd70254c', 56, 2, NULL, '[]', 0, '2019-03-06 05:39:48', '2019-03-06 05:39:48', '2020-03-06 11:09:48'),
('96cebf79310e0a7f8b97e9270837183cf03c80c76e0f05aab800cf75f342b375f958cedbff8c5435', 1, 2, NULL, '[]', 0, '2019-06-10 23:58:38', '2019-06-10 23:58:38', '2020-06-11 05:28:38'),
('97e965f193aba5bbf29885a3e4647d5c7d6de7d2b9ff3163f1dc0053b21e00e143990b7cd55c970c', 56, 2, NULL, '[]', 0, '2018-11-06 04:32:06', '2018-11-06 04:32:06', '2019-11-06 10:02:06'),
('99e7ad1785443cd283c8cab4f415086525bc99f9686724c5c9246765c9b605de1a789e3d109370a9', 59, 2, NULL, '[]', 0, '2018-10-30 04:52:53', '2018-10-30 04:52:53', '2019-10-30 10:22:53'),
('9b241c01419f3fd6087174c03b59e7429d97862227883a0fcc027ddcc7d7cfd9cad7f060e9c90ab4', 1, 2, NULL, '[]', 0, '2018-09-16 12:59:54', '2018-09-16 12:59:54', '2019-09-16 18:29:54'),
('9bfa57427f814755c17bc579800a32435db98a977f3c5596da86ec0f9f73c1e9cd1fee401422ae33', 56, 2, NULL, '[]', 0, '2019-03-20 03:06:52', '2019-03-20 03:06:52', '2020-03-20 08:36:52'),
('9cca870176d533bf7ef0e6e4c3716a973355e3a8a85fa221a61df170d3d97aacc3cad30a4302612c', 56, 2, NULL, '[]', 0, '2019-03-12 05:27:16', '2019-03-12 05:27:16', '2020-03-12 10:57:16'),
('9d9744443b2136c222f913df72536326b2a859bbf89a1eb090cd806f5822bdd3a017c3ba8c08c97e', 56, 2, NULL, '[]', 0, '2018-11-21 05:25:15', '2018-11-21 05:25:15', '2019-11-21 10:55:15'),
('a18faccf8ab0f4680c8c837dfaa7b4d34234b92e977837344e9ddbc26e8e064858f79f7deefa4c99', 49, 2, NULL, '[]', 0, '2018-09-23 07:36:35', '2018-09-23 07:36:35', '2019-09-23 13:06:35'),
('a2fd770306da562ab67729e25456eb5d8863722693be3e6b4ba79bb853d03c3d7b98018bd0a808e8', 56, 2, NULL, '[]', 0, '2019-04-03 01:50:31', '2019-04-03 01:50:31', '2020-04-03 07:20:31'),
('a3b4c4eb5e3704c495e667dac337a234b0926734a2d23e83b6e831f433d5fd19a4d52148e0b38097', 49, 2, NULL, '[]', 0, '2018-09-23 09:19:35', '2018-09-23 09:19:35', '2019-09-23 14:49:35'),
('a408ccef1745aabd949acf42eb891d0594de4642f1cf67b58d337e4edf1add9f000c8a31bd76c837', 56, 2, NULL, '[]', 0, '2019-02-18 01:00:35', '2019-02-18 01:00:35', '2020-02-18 06:30:35'),
('a5d78bdf5ba798a379a4292737cec06e3f2b6f5d6fb5a163adb015fe4f9abebc2db44421ec12ed3f', 56, 2, NULL, '[]', 0, '2018-09-26 00:12:23', '2018-09-26 00:12:23', '2019-09-26 05:42:23'),
('a68ce16edf855c037201b94f9b98ed8a11a9d2bed77b0cb8d53ba19fe897cebee521bcc021bad1f4', 56, 2, NULL, '[]', 0, '2019-03-06 05:38:02', '2019-03-06 05:38:02', '2020-03-06 11:08:02'),
('a818bc14d0fd85454b62398d4218a66ceca8f8b07e5974c5b4ab7df254f72f305c4568b86e113a18', 56, 2, NULL, '[]', 0, '2018-11-21 05:43:40', '2018-11-21 05:43:40', '2019-11-21 11:13:40'),
('a84ac0c65274cf3b7fb53723ebd91115ee788b5fdda2955a9a6fe7f289fad80e98d7fbebd9400ed3', 56, 2, NULL, '[]', 0, '2018-09-23 09:45:51', '2018-09-23 09:45:51', '2019-09-23 15:15:51'),
('a8b73a653afd94253284b6b3eb5b585a7b8953659602364111e69525ed709cc02d34bbe67fc07126', 56, 2, NULL, '[]', 0, '2019-03-04 03:12:08', '2019-03-04 03:12:08', '2020-03-04 08:42:08'),
('ab07c4ddf6a3921466005a67a1c00d0bb93257331034c3e64eb1664d21fb1e656d256ed9bf7ebc98', 56, 2, NULL, '[]', 0, '2019-03-12 03:36:50', '2019-03-12 03:36:50', '2020-03-12 09:06:50'),
('ad9d2953f150f4ca7704699466490f82c65c1f91cea8001d144a1cfda4215b2b5e3c19435cb8058a', 56, 2, NULL, '[]', 0, '2018-09-30 23:54:57', '2018-09-30 23:54:57', '2019-10-01 05:24:57'),
('b166e6b90bb78c96a4762d77e8e433b21b07e1cf2434c412ba46b7e01a6c9bb8b3bfa0411f6d2f3c', 1, 2, NULL, '[]', 0, '2018-09-16 13:00:57', '2018-09-16 13:00:57', '2019-09-16 18:30:57'),
('b8877ae919b5ff488012df02e538468f3ae74f66d82174403c20c5c4867de76ad4bee50505d67723', 56, 2, NULL, '[]', 0, '2019-02-13 03:00:50', '2019-02-13 03:00:50', '2020-02-13 08:30:50'),
('b92f3cf092307d063b3b17c2f3eedc294828c1126f8557dfb51a8d3fc8d350c8e1ea233a9f5deadb', 56, 2, NULL, '[]', 0, '2019-03-12 13:17:29', '2019-03-12 13:17:29', '2020-03-12 18:47:29'),
('bd058605081d21a5b6038188861c77bb66276a7ef142e5789926ee472abe2c35e2c76f85313be642', 43, 2, NULL, '[]', 0, '2018-09-18 23:30:03', '2018-09-18 23:30:03', '2019-09-19 05:00:03'),
('c03cc26882a802ec40a4e4f0ea04e8d30d09c37ee4523f5d84bda905028bad0daff8f21418b41f26', 55, 2, NULL, '[]', 0, '2019-03-12 13:28:13', '2019-03-12 13:28:13', '2020-03-12 18:58:13'),
('c1841c1977a0c0737762141ce9e90c12d2781a17408e06f4bd323c99d04c90656ecccb08cd662a31', 43, 2, NULL, '[]', 0, '2018-09-18 23:20:49', '2018-09-18 23:20:49', '2019-09-19 04:50:49'),
('c47db0c4eb9added2e9f8da1d57aeb8e43cbf21c734faa2974271f0511f551f9e338f9623acbbb1f', 55, 2, NULL, '[]', 0, '2019-03-12 13:31:42', '2019-03-12 13:31:42', '2020-03-12 19:01:42'),
('c84461e6b1435a4629a84be4bcb315fd1be578f819dc728768569a118e508a5eb4341e4c82494a16', 56, 2, NULL, '[]', 0, '2018-09-25 03:35:18', '2018-09-25 03:35:18', '2019-09-25 09:05:18'),
('c87cbfaa3ad7bbc3e8380023d8c8d4a29fd22023630020e09d59bd478298e53b2e6b4a02feab68bd', 56, 2, NULL, '[]', 0, '2019-03-06 01:27:06', '2019-03-06 01:27:06', '2020-03-06 06:57:06'),
('c8d65c3e461e5b49ac9e482434081c1dd8f4f88242efbe07bbb54aa2c7d5165ddd4a14b8ac0f82f0', 56, 2, NULL, '[]', 0, '2018-09-28 00:58:36', '2018-09-28 00:58:36', '2019-09-28 06:28:36'),
('c9fc2fad03e61b5bd593cbd5dced59bfda75d86058442e99c75fca14c0c14891dc59670f52a7ced4', 1, 2, NULL, '[]', 0, '2018-09-16 13:00:08', '2018-09-16 13:00:08', '2019-09-16 18:30:08'),
('ca297c4417c72f19a501b3e4f5f733bcfb05f744ce97a6e50f2e231cd48aa59d7fda0973f1f9ecb3', 56, 2, NULL, '[]', 0, '2018-11-04 11:17:33', '2018-11-04 11:17:33', '2019-11-04 16:47:33'),
('cb4d4b623b1bae93ef1541effc60579d49998e8890a6ad0b22af35f676cdc7ca9d1de24489cfc91b', 56, 2, NULL, '[]', 0, '2019-03-08 03:11:05', '2019-03-08 03:11:05', '2020-03-08 08:41:05'),
('cc38ee136120e125995dd5529b7db1286604589b42f04e889e4d353a6213780fd95b7974208da609', 56, 2, NULL, '[]', 0, '2018-09-27 23:04:32', '2018-09-27 23:04:32', '2019-09-28 04:34:32'),
('ce3b7b25e7307d9f7f75c325e1262d3c3716faf770d10fd12db4f82058f641cf5ffde66ac2c02e80', 56, 2, NULL, '[]', 0, '2019-04-03 00:43:51', '2019-04-03 00:43:51', '2020-04-03 06:13:51'),
('cede75bdea5d39133140430c4801e54b25421e962d22173386d76944dc03b66fd34ceec136078711', 56, 2, NULL, '[]', 0, '2019-02-18 01:28:02', '2019-02-18 01:28:02', '2020-02-18 06:58:02'),
('cef0d1e1bd23539683bae4d649fb73c082887f497210909c6d403c422051e72d53085032d931d024', 60, 2, NULL, '[]', 0, '2019-02-28 12:53:03', '2019-02-28 12:53:03', '2020-02-28 18:23:03'),
('d172ea499bbed6412cba25b9d66a1f333e7735761aa8f108df4830fa0b23d4b5708b59e6ec06121b', 56, 2, NULL, '[]', 0, '2019-03-11 03:39:24', '2019-03-11 03:39:24', '2020-03-11 09:09:24'),
('d1b839754bef7f820dc974cc2a3c680928961f503fd00ddd1a1e48a37c05800d22d854703510db13', 56, 2, NULL, '[]', 0, '2019-02-28 23:25:53', '2019-02-28 23:25:53', '2020-03-01 04:55:53'),
('d4e31d892677b3714ec6abe3d40c445e10fd1398e10e36164376cef2a4f0e431ba284877615ada94', 56, 2, NULL, '[]', 0, '2018-09-28 00:58:50', '2018-09-28 00:58:50', '2019-09-28 06:28:50'),
('d62929c5ecd7092abe7cf4e22648d768fa2d29493a374f3b5f85e208748a0bebf8abaa5a3a2a2e98', 1, 2, NULL, '[]', 0, '2018-09-16 13:01:28', '2018-09-16 13:01:28', '2019-09-16 18:31:28'),
('d70152d79efedb670d69d53dfcb768f697d5cd0548007fe59dc3d93e61c8b4afee777ca760876033', 56, 2, NULL, '[]', 0, '2018-09-25 03:13:33', '2018-09-25 03:13:33', '2019-09-25 08:43:33'),
('d7333c96b2e90770675f58693b6388ffeb7ac4069a076e08e56216748f6d2f188931f7d48f440d16', 58, 2, NULL, '[]', 0, '2018-09-27 23:02:18', '2018-09-27 23:02:18', '2019-09-28 04:32:18'),
('d77211f03813e26d22e001d97b3921e56ba11f4bcb478a25707ae7536219df5f53a73a7e621cd8ea', 55, 2, NULL, '[]', 0, '2019-03-20 03:41:20', '2019-03-20 03:41:20', '2020-03-20 09:11:20'),
('d7f375fa98155c03426a6010a3d87e33755222e3293047a243b57c1ea7a1dfffb34a5bd3ab25d609', 55, 2, NULL, '[]', 0, '2019-04-03 00:45:42', '2019-04-03 00:45:42', '2020-04-03 06:15:42'),
('d967126424ccbcb5979518dbd9e9d435e0e65c2842d2f15f47c022c9684bb49edb3c297077455d23', 18, 2, NULL, '[]', 0, '2018-09-16 13:15:58', '2018-09-16 13:15:58', '2019-09-16 18:45:58'),
('d9ddb92369f848d1710d255614e806b959cf7b14332aa653383f79a75919d892577b9a6c321d35d4', 56, 2, NULL, '[\"*\"]', 0, '2018-11-06 03:29:04', '2018-11-06 03:29:04', '2019-11-06 08:59:04'),
('da8312f833d899af0aaee20aebe8cc2de131aaed0c9855184dce8d3da82874d2a2b0418e67fed4b1', 56, 2, NULL, '[]', 0, '2019-03-04 03:11:52', '2019-03-04 03:11:52', '2020-03-04 08:41:52'),
('dbc5d448b1f84a4e4f51ece12a2a2a3f4143c42c8682ee089a38add009b25113afe5f8ce2c4ffb44', 43, 2, NULL, '[]', 0, '2018-09-18 05:03:59', '2018-09-18 05:03:59', '2019-09-18 10:33:59'),
('dc3ed3a146e71a25f7586521e8691566adf046d9da6ded69742874a142b0f823e7951990e68075f5', 56, 2, NULL, '[]', 0, '2019-02-13 01:26:22', '2019-02-13 01:26:22', '2020-02-13 06:56:22'),
('dc91958948591f0cf8c8a90dc71a4f37481c51c17cbcd5a5cf472d2f23e43af05b33b9a4c84bd070', 54, 1, 'Personal Token', '[]', 0, '2018-10-28 00:02:39', '2018-10-28 00:02:39', '2019-10-28 05:32:39'),
('dc98859b2096fc07e3c3c016a99e07b869adc7d59f40f6742f4c10413cbd741e1887312c5b67b057', 56, 2, NULL, '[]', 0, '2019-02-18 01:03:55', '2019-02-18 01:03:55', '2020-02-18 06:33:55'),
('dd9728c844ff371b82c9dad4d50f4c3eea901918a201c186ca24569b271f987465d538dc14f5fb01', 55, 2, NULL, '[]', 0, '2019-03-20 03:29:25', '2019-03-20 03:29:25', '2020-03-20 08:59:25'),
('dff2cc7ff87faf712697caa5625874a3d0ff74747cbc2fea917c38fa3bb0479eb24805661711186a', 56, 2, NULL, '[]', 0, '2019-02-28 23:06:35', '2019-02-28 23:06:35', '2020-03-01 04:36:35'),
('e188cb02987de6ecfedf022d14169b11d6292630f6794d4d1577c9ad1fc5983e16bba93235c8625b', 56, 2, NULL, '[]', 0, '2018-10-09 23:27:06', '2018-10-09 23:27:06', '2019-10-10 04:57:06'),
('e2291ac4e834341b060a66686908676b32ac6d4e68c18b58947efc9498ebd5462705e80230fa200c', 56, 2, NULL, '[]', 0, '2019-02-18 06:58:00', '2019-02-18 06:58:00', '2020-02-18 12:28:00'),
('e4d470c8258dd493736c8270c89eff1c0865ceaf20d1a07a7439a24b3c0623a5f5cd8f6275a7416d', 56, 2, NULL, '[]', 0, '2018-09-27 04:20:41', '2018-09-27 04:20:41', '2019-09-27 09:50:41'),
('e51d7db1b8623791592702d962a16e8a4683e9a2bb1f487ae20f84332447422ccbc5fdb4fc6040e1', 18, 2, NULL, '[]', 0, '2018-09-17 01:21:23', '2018-09-17 01:21:23', '2019-09-17 06:51:23'),
('e642e7e89eef528811ae6ce7b49a140b549491d509c405c2979bf3c9c3afc3eee82cd97f245c8d83', 18, 2, NULL, '[]', 0, '2018-09-17 23:08:41', '2018-09-17 23:08:41', '2019-09-18 04:38:41'),
('e754813acaca04d079d48aaaac67838ec40cdf3a0b610b4a1eb6e4e792363edb5e6bb4bb4782a909', 54, 1, 'Personal Token', '[]', 0, '2018-10-28 00:02:42', '2018-10-28 00:02:42', '2019-10-28 05:32:42'),
('e858c18679ca7e936f3d8ae7ebd31b98e80b373f00b7cfb10c6ad91e0217a482f466766bd51ec379', 56, 2, NULL, '[]', 0, '2019-03-14 03:22:23', '2019-03-14 03:22:23', '2020-03-14 08:52:23'),
('e9c376bf0bb3b01873fc2f6cb75540dd86a6b9741ec6cea4c5343cc61880810eb22f5b1c2d56c11f', 56, 2, NULL, '[]', 0, '2019-03-06 01:35:29', '2019-03-06 01:35:29', '2020-03-06 07:05:29'),
('e9d2fdc022aa73f8fd114b5dff086535a9ad8bcb96ddc06bad89cb40b0a57c851e971a848f54d9b1', 60, 2, NULL, '[]', 0, '2019-02-28 12:51:42', '2019-02-28 12:51:42', '2020-02-28 18:21:42'),
('ec39316671cf0965e8b74ce9d97fbef5d0b9f50cc80ff3eb66b974c766d830ab0f3f551951e30644', 56, 2, NULL, '[]', 0, '2019-03-06 06:32:48', '2019-03-06 06:32:48', '2020-03-06 12:02:48'),
('ecbe9ad21daeb2fc48f89cae547194b77a95c762a45ffa8f4faf321bce25caae180eac4bffd15db2', 1, 2, NULL, '[]', 0, '2018-09-16 12:50:28', '2018-09-16 12:50:28', '2019-09-16 18:20:28'),
('ed34fb24b0bbec96609d13546f0f02ef452434a73e9f24e19871c4a0ef5beed9fd8ce80fe0f4b8ce', 56, 2, NULL, '[]', 0, '2019-02-13 04:18:08', '2019-02-13 04:18:08', '2020-02-13 09:48:08'),
('ee88c5eaf0f60104a963888d52540a97538068b8a677a4c92c1de5659565ed20787018d50e99c07e', 56, 2, NULL, '[]', 0, '2018-11-21 05:34:34', '2018-11-21 05:34:34', '2019-11-21 11:04:34'),
('f071705cc3b31a6f92a7c16034eef8c3aeaad0472151c7fbfb963e4cd3a0f9a60a80b5f6d1da561f', 55, 2, NULL, '[]', 0, '2019-03-12 13:17:54', '2019-03-12 13:17:54', '2020-03-12 18:47:54'),
('f0d3d3056c2f1d1ecd07c022ddcbbd3663451e88771fe227fa9e3ed886ca1973c56012cc8e50e73f', 56, 2, NULL, '[]', 0, '2018-09-28 00:43:55', '2018-09-28 00:43:55', '2019-09-28 06:13:55'),
('f275a1721efb0e58558071b0f5ccaa9cfe479361cad800e81d8c60596debfcae0f7629f0d884a4bf', 1, 2, NULL, '[]', 0, '2018-09-16 13:01:32', '2018-09-16 13:01:32', '2019-09-16 18:31:32'),
('f395d0d791a164bd9a8a84dd0d296ac441168624d34199a7c652487642cd80c5e83e001072981ccf', 1, 2, NULL, '[\"*\"]', 0, '2018-08-31 06:25:40', '2018-08-31 06:25:40', '2019-08-31 11:55:40'),
('f3b562ef630a7af3d4920bb2c08873b9c63d1fca32f97660e7ee91df09323be26d417f97a5d08be9', 48, 2, NULL, '[]', 0, '2018-09-23 06:27:50', '2018-09-23 06:27:50', '2019-09-23 11:57:50'),
('f47bcf8c242c0c212f4e5bcb149a07b868b691929350283ee3ee0531d8c358750c4326123f2f5d27', 57, 2, NULL, '[]', 0, '2018-09-27 05:54:23', '2018-09-27 05:54:23', '2019-09-27 11:24:23'),
('f55ac8cf82eaa028a11f428976a2b992a5d819cbd7d4816818f2ee76445a0bf5e98a393970ec44f6', 56, 2, NULL, '[]', 0, '2018-11-21 04:34:28', '2018-11-21 04:34:28', '2019-11-21 10:04:28'),
('f5d4efbc87cb15350ad8506327e551f389e6b62b1a5de9967e785961beb5f12153faff97f4dc6f66', 55, 2, NULL, '[]', 0, '2019-03-14 02:12:52', '2019-03-14 02:12:52', '2020-03-14 07:42:52'),
('f627dc4434d014793ce149e944c1fa6a747fc088ccea50461c0fd8a8db0a4fa6979028f4a4660a81', 56, 2, NULL, '[]', 0, '2018-09-25 03:37:10', '2018-09-25 03:37:10', '2019-09-25 09:07:10'),
('f6baa7950afdebddaf79d824a171e5dc5fa6eed0ef0ce8804fd01edb178c0f1b6afb8e2b4b950648', 56, 2, NULL, '[]', 0, '2019-02-17 10:27:21', '2019-02-17 10:27:21', '2020-02-17 15:57:21'),
('f7c6dcbc739914bc3c8dbe915c4fdf0c0b172badc2ccc54af547942b94b18e6864215bd71d25c427', 1, 2, NULL, '[]', 0, '2018-09-16 12:59:09', '2018-09-16 12:59:09', '2019-09-16 18:29:09'),
('fe96626b551ec8639af25475120cf4681879c005c9ff879a497b018fab1521bc145c0618bb7a93f0', 55, 2, NULL, '[]', 0, '2019-03-28 04:25:07', '2019-03-28 04:25:07', '2020-03-28 09:55:07'),
('fefe111c72280268dcee7c1716d9e7fc8003c6234b068d68c18bb0be70316f12cf4a840a23ef091f', 56, 2, NULL, '[]', 0, '2019-02-18 23:05:41', '2019-02-18 23:05:41', '2020-02-19 04:35:41'),
('ff6e95b6073998aeec44e73627f4c8750077bdda044f2bb61ba9e21703970e8e759f0ce54e37c41f', 1, 2, NULL, '[]', 0, '2018-09-16 13:02:40', '2018-09-16 13:02:40', '2019-09-16 18:32:40'),
('ff8b99e8b968bc43baadffa435e20683db885c78ad6aea99f1deb3cb0d34a9fc0d4549bf0d042072', 49, 2, NULL, '[]', 0, '2018-09-23 08:51:34', '2018-09-23 08:51:34', '2019-09-23 14:21:34'),
('ffa0d84e28a645dade4fefe5ad6544846585619a9e3221039356920d87f023584c3465a37448aae5', 43, 2, NULL, '[]', 0, '2018-09-18 05:50:41', '2018-09-18 05:50:41', '2019-09-18 11:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'uyIp4eksuoBOH8azTxsvAd52wTjZ06HrF0PmO8Br', 'http://localhost', 1, 0, 0, '2018-08-26 06:12:52', '2018-08-26 06:12:52'),
(2, NULL, 'Laravel Password Grant Client', '4vmvsdgSWOtpzmsHUE1Br1qQ3vqdMHqby7PIvHyO', 'http://localhost', 0, 1, 0, '2018-08-26 06:12:52', '2018-08-26 06:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-08-26 06:12:52', '2018-08-26 06:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('002c768cb0f277aaba45776565d1d892d8786fc55f669444d4aff3db1b64408438939fe0af32c905', '9bfa57427f814755c17bc579800a32435db98a977f3c5596da86ec0f9f73c1e9cd1fee401422ae33', 0, '2020-03-20 08:36:53'),
('009129b3fe3cbbf5a6b09957eda241d12b267c24d6a6a4ba87fdeb8712e04d281b299790f09c19b3', '03f792f6dfb3a7b830bb18700efde1236b987c47f2e6ab3af96a1e5c356301243bad78f22ed7a2ce', 0, '2020-03-12 11:13:08'),
('053042848e9e30ea31e3aff9c96e5b184849dd26919d7ebdb94cf6f27ba1e5869d12473ac5d0a795', 'c84461e6b1435a4629a84be4bcb315fd1be578f819dc728768569a118e508a5eb4341e4c82494a16', 0, '2019-09-25 09:05:18'),
('06956857bcd8aeaba9b5e648070578d120acc7f50dff809a09d66302cf6f44e406ef1f1c8c2e91ce', '80f8272d7a6016244979302a2c23b7997d1343f113521f8f74bbadebe147fc2f3dfefb37601c2e7f', 0, '2020-03-06 10:46:14'),
('08607fb771b8a8cb53f0d16c683f0b75051f5b3cea632cbd508bb983cad65899a0e3a0adab3c9799', '3e386614ec0755b224d2dce0b42c6a442c452004a2fb0c15ec940947f5d57ea40ba5519b0eb0a6cd', 0, '2019-08-26 11:58:29'),
('09edf5f25ea5dea82223c2a9e34c286ebcc217bca2a47cc77cc0cb0de6a314aa4ad690aeb6ea17c4', '6e14cb302ea33faec9d7e189a1710c69dc834d9d57599123cac4f7e6c96beb81906798357a6f0ee4', 0, '2020-04-03 05:08:16'),
('0a0963f27a1157474f08bffc0948de15024a9b1d43114082bf102039b3216bb457c0933771f34b30', 'a18faccf8ab0f4680c8c837dfaa7b4d34234b92e977837344e9ddbc26e8e064858f79f7deefa4c99', 0, '2019-09-23 13:06:35'),
('0a1c6fbb819cecd88484380e1d9c9dc6af9be1a75efdd44e514570923cb83d507fc9f9a7d74efec6', '752091c1b21837cfeb18aa53a8a513281d719ecd57f41d41511a4275b6e579a93665ade88969ce73', 0, '2019-09-24 06:09:50'),
('0a2d8c85d4a0cfa7cbee03d10bd2201c668ec75224a6b347af915f1d2e4352f792fa6931672a77af', '8dabc529ff9f1115f400b5582d9255f05a7dc4ba3cb6e179558aa5d380337f860004835f2efda8bf', 0, '2020-03-12 10:24:04'),
('0a39345dbc0d29757dfe174698a461aeb19a2a48aadc588ea18583b0d4628cfc5ccdce3c1d2fef1a', 'd9ddb92369f848d1710d255614e806b959cf7b14332aa653383f79a75919d892577b9a6c321d35d4', 0, '2019-11-06 08:59:04'),
('0bf361212df03b66b8cb217d91e66e9e8ea9fb255e4c58638bded87bbedac2f49542fb5074d41172', '02e49462454cdce8562480792feaa34969149c359903320bb6d8873f4ea4e5bea61cef7b1c1b8d58', 0, '2020-03-09 07:31:10'),
('0d5d9d0e476f71fe7ece16fbcfea41664032aee18aa0169df64685e3ac083123b4e733f055a15a31', '5b1acc8a926185784ef6cc88d43f64699f2f391c3b44a8d6b4baea513a4ece5f481e0386d5e2c8f9', 0, '2019-09-16 18:27:48'),
('0d9dd4afbd2d21973d4c5cf0af5a7a0ee2a4ccf241211b4eb82d5db23137fb47c67029addfded623', '4154b1886085aae8463a7167fdcca1316c47d365eaffaff645aa15ebe34cc497daa75ed419066ad2', 0, '2019-09-23 07:30:23'),
('0dceae0fe76ce1fd0addc815c6dbe53a2ae13c4f94a4108dbe15ca51300390a67120f7f12161f3d5', '26155c3f596c5b6b1e33de982be2217f2365764ee82e91cfa7dbd6152b1e9d3383a86329c804a02a', 0, '2020-02-28 18:56:16'),
('0df67c39c39e43081daf7345572eb22a2f26b861af3e0009caa5489cb5721fa98772b9304f1edc06', '0a36b2285a4a560bf3c5c65426656c1a039eeb7991375a68e256aa080b9881551fde75cd02cc414d', 0, '2019-09-16 18:32:05'),
('0ef8577c6f92cd6f584ee1cb27a7ace67aa4608809ce8a15348bb38bb400b20ab869422908c8bcf5', 'c8d65c3e461e5b49ac9e482434081c1dd8f4f88242efbe07bbb54aa2c7d5165ddd4a14b8ac0f82f0', 0, '2019-09-28 06:28:36'),
('12156512b03135d44d13a383aa1e93c40201f7ca53844c5b5e647e96277f72e7804112975c9479f3', '257840298226ca2f9921c76b9de00c474461020c9dc69f2a540a1a58d590ec97f41010fd555b71b7', 0, '2019-09-27 03:51:40'),
('1387e383a846e5b33d8c3b046d17c92f3bbe359f30cc0e2ef6777770dce1d685e558ff15a16af9cb', 'ff8b99e8b968bc43baadffa435e20683db885c78ad6aea99f1deb3cb0d34a9fc0d4549bf0d042072', 0, '2019-09-23 14:21:34'),
('147d51a60803ffd42e66c2ea16924baf22366e3776d481fd4c164272bb3d1aaa674f3b62c3d6322c', 'a3b4c4eb5e3704c495e667dac337a234b0926734a2d23e83b6e831f433d5fd19a4d52148e0b38097', 0, '2019-09-23 14:49:35'),
('18941e1baf903161aa8b0f75dbde3f8ae094edb078736e266d83db1a48cd6c45b3f72ab73c330cf8', 'e9d2fdc022aa73f8fd114b5dff086535a9ad8bcb96ddc06bad89cb40b0a57c851e971a848f54d9b1', 0, '2020-02-28 18:21:42'),
('1ca8416452dcbdea27a4d019e33fb5e4841e6f6c6940af86304bb9b5f0111e410973d24d905be0ba', '11993a61909159d2332e9b5d2b2aeebc2efc2de43189b0344ce21c09c248c8e769b3d29126d16049', 0, '2019-10-30 10:48:32'),
('1def6ab5531848685414d76499ee09109e76da9c316ba89d915d272a1fbf0570cafc8748643189ce', '68016db0680ad095af425061faeb9993f186c13b37956168f0d32e001cdae3bbc8f1d11d9e136634', 0, '2020-04-03 06:16:21'),
('1e2f2d0da7ae300f87382607af2900b57164fc4577d520786b6a671dd65fd3e54fd601742a951378', '85964cd1eee90d06c97addbf44f4e6aea6df08ab847a5dbe26a1401f716de3ca702ae9163db612c2', 0, '2019-09-28 06:28:31'),
('1ed7275968989ca287c9c344cbb2de1988f8c31d9ce9a4b54b5d33d4e80389adad6328a505d90d83', 'c9fc2fad03e61b5bd593cbd5dced59bfda75d86058442e99c75fca14c0c14891dc59670f52a7ced4', 0, '2019-09-16 18:30:08'),
('1f0728bd98eb2f70fc86e7fdd14f68f0055774462c9363b9b2d4194158c6924aabd32ce508a45e37', '5874b49cc4d03b9dbd4a2c0c750b22963982f71147def55eb896da7dd494d7f05f9b4af166777a56', 0, '2019-09-27 11:04:02'),
('1f2a6aeb13ac7e68bc4722a5e2a082b1b83637a83855b8bee0b6a430ac025e6485f681aaf188fb0d', '6423b1970adddd1cb3736d7856863832d2dd5fc126d4bde93b57cae8d6710771e15a30e1042765ed', 0, '2019-10-22 11:42:59'),
('21646fbe2f9e577acb4651da755f10eae3803cd6dbfcf39199a321e776d251db8b9c2d5ce4b63506', 'f55ac8cf82eaa028a11f428976a2b992a5d819cbd7d4816818f2ee76445a0bf5e98a393970ec44f6', 0, '2019-11-21 10:04:29'),
('2388c3b3c26f0ca5e50a279b82135f75f8ce99ee6ba5f3a3d9308ebcce6495a8435b2e9b26858448', '6ce624250a8fc2af558d3601ba840b0543bba2d2898c836313449f9b44203012112499bb4be9bd81', 0, '2020-06-11 05:28:31'),
('2494c7bc4ba15352faf3f17c2d36a5330c5b4f56d044702028fe05df33e95b3e3a9a76fdfde4bd8e', '96cebf79310e0a7f8b97e9270837183cf03c80c76e0f05aab800cf75f342b375f958cedbff8c5435', 0, '2020-06-11 05:28:38'),
('2518c7b96b138d7d78b67db3f9e44016f9337054e94851498ac2c617c3a959cdc0b68b542440776e', '8d2d4411503b56a5795af5b2e0d0d5d8b3a403b56badb28470c1947f23a0715738810ea0b5ae1d5d', 0, '2020-03-08 17:48:46'),
('2558fd023e38607f7fa2079becaa2907dc4e8b9e64d3ba2b2f8525d36487d3972d2e69ecc495bf05', '4faa71b0a277adb8e1c32e996b3ac399352f2b8cd2b3d4ab7996209993be5ade9cd8398569b89a23', 0, '2019-08-31 10:47:04'),
('25c4d552c4ab239dc60fd9a502ffaf6f3964af2d5ceac494d1e8585468e7c5d43177b6e5a85f65f1', 'e4d470c8258dd493736c8270c89eff1c0865ceaf20d1a07a7439a24b3c0623a5f5cd8f6275a7416d', 0, '2019-09-27 09:50:41'),
('26044fa1eaf20b151c217f42d921971074993413eb925f3ba2b94be1653a8d5e418027ce2415d35f', '12a4ad22d7c062859ecc8c577f8fab902508efe66525a56c08aa80fbd12c0af65d885dfd6b349506', 0, '2019-09-17 04:41:53'),
('2774de117e9692203c9fafecd7ddf0369ac8acc2d90e9261a1dca7af285358c3ef7266cf9e970e2e', '81e335d7caea1678fbd681cb614887bc1f99cfb363501dfa5cd3dd0eca12b6867567cb4a5571241e', 0, '2020-02-06 11:50:09'),
('282a10e9da8c470b114765e373fd39ffd3c483c60243715b8c5e87fe5b89dbf1f47324220e8a6c41', '734c48cb9fd50503bc605d569557c40908aeb553259f73a1f5b93c644eff1f84260547c3d437b0d8', 0, '2020-03-12 17:41:39'),
('285ca32bf52476a883d5901bf6afb4a6cb099a1c0e3c41189b1f171373849ccc2c0e4bdc1ac253e3', '4bc1088f03457ffb3f1ddd95e964b13087fee7975fff33ec52a01f6d895dbfae5f6ff41335e31208', 0, '2019-09-16 18:30:33'),
('28a58753175b4fd8bc7f71ac1d606133605a7e640fde00217c9852f3e5a821192085786319815bcf', 'dff2cc7ff87faf712697caa5625874a3d0ff74747cbc2fea917c38fa3bb0479eb24805661711186a', 0, '2020-03-01 04:36:36'),
('2cd1c6970c3aef08de3d78d8f4f6d3e9ca72a4f7bd17e34307d2c913a8011308c2e8d4dc0281bfbc', 'd7f375fa98155c03426a6010a3d87e33755222e3293047a243b57c1ea7a1dfffb34a5bd3ab25d609', 0, '2020-04-03 06:15:42'),
('30976132f7a16dc7dd194834842ec543974287352349aeaddbf84ae0b21ac4ffe74aaa706d0865ac', 'c1841c1977a0c0737762141ce9e90c12d2781a17408e06f4bd323c99d04c90656ecccb08cd662a31', 0, '2019-09-19 04:50:49'),
('3191d27ae7536849cfff6d7e24815d2a7e9f799944d6d9709495e24beeee3df045af57adff7b3840', '89c66de97049165d536fa3e9376642ebcf365cf7f3338fcee78991f2689e901cbed3a7a672b85e04', 0, '2019-11-12 07:17:03'),
('3422c8cc66b550187ce2fab0578d8784d4319514588195573387ba7d21e6b2757a453663dfa96dd8', '0af16c0a312955f105ac52c1cc83521130b8c4ee0ddbcfedf8106070563eee29f05a60fbd8992623', 0, '2019-09-16 18:32:46'),
('3490f7cdc753dcf71415f0efa5e89596d4b43c3f37b4e1168e6f59017b1cd8fce7325e9fb2a817bb', '96a1ae49d5a3cee5dc8675c7a2c12a4c09723a9f216e7e5a423c1df2603c19f9eca73dfadd70254c', 0, '2020-03-06 11:09:48'),
('34a46349f49e8f18efcddd652d0c29644ae6f4b841e739ec23d48e2807e4f6be2164ddab4d2a5327', '449e6fbea07c06ecd5653939a2b3891366f9fead09360ebb30d6cf0afb9760d96b5e4abbfe1805bd', 0, '2020-03-20 09:34:40'),
('3517bc98191598f0e676a1d8119ccad5c220f8c65de438ccfb963371be6061e3cc4f7ad768c1f04d', '61da349bb6e7d544ffbca318d68f5e965183da31de68600ad967117da31efb24f78c82dff540d71b', 0, '2019-09-23 07:48:06'),
('35260e7b13002d49038e3d8eb0affbab88a1f7be397a35e3c6376cf237805b8b54479bd82fc6bd3c', '87cdf6548dac719a13d399a0b715c087e426f19e0f192db4aff666074225f240ab6afa5154565d3e', 0, '2019-11-22 08:46:27'),
('360942adf369da418b17e9d46ba0efbe29a146179f5e6a03c61397a83171bad904591c081a69af26', '9cca870176d533bf7ef0e6e4c3716a973355e3a8a85fa221a61df170d3d97aacc3cad30a4302612c', 0, '2020-03-12 10:57:16'),
('36971b0227274baa55d30037f8cf36f519a804b618f5cf567453c6c30fd3538a853d1244ff95cc39', 'a68ce16edf855c037201b94f9b98ed8a11a9d2bed77b0cb8d53ba19fe897cebee521bcc021bad1f4', 0, '2020-03-06 11:08:02'),
('381e19d07877adb1305cfe297eb9767fdd26476a5ae43f7d73bc05551694b3e90461f46d058c0dd9', '5bc2cbeb018aa192fd79d8630094819c376bda2aff11694580f86b267f881bb9b0521dc288629d63', 0, '2019-09-16 19:13:51'),
('3a3197af845a3aba7b47caa2e7afce3749d98b50418b51c4d9bc150e4659461888ee1c1794910552', '110fd7e5c34f81d556227cfc5deb29c751da4134b5efc631e327ee8b84b9d63bfa1d1ada27294df7', 0, '2019-09-16 18:18:06'),
('3a86f616c0f48d5cab8a88b6fadacf7f2d46575cadb5bca6c4b8deb9ae3ac160416a8c5fe89ff225', '4a15e300dcba67052b848bb7b97d8695dfbb012841b69363d2871256695dae0d2d82de4ff612c06a', 0, '2019-11-21 10:30:32'),
('3bf1d96791b23ea4c7809d80bfe25de899a1c17c9f73448044d08dc3736399ce3247f45d9e87472f', 'ce3b7b25e7307d9f7f75c325e1262d3c3716faf770d10fd12db4f82058f641cf5ffde66ac2c02e80', 0, '2020-04-03 06:13:51'),
('3d15b6ddf48758719de8b6c8bfd0545067bdf12abdb695dd143d0893efcef379ffbdb78574c1aa14', '8f134a43ccb7ff941145549a803bdbb2a7e69943b7ed59088a626a76c07b5b859f0d0c16c7717785', 0, '2020-01-21 13:03:36'),
('3dfeaf73e005c75149a3a36050a27fbd32fcbf7b6f4a1febc25166b3e474c7cb4d5400d9eb226860', '789cfecf4cc18d7b92e7309cfa846433ac8a0673fbfda563aaa66f63f0d2c4c832fcf1e9071fd869', 0, '2020-03-12 10:12:24'),
('3f4a55551e44a15d82f0d79f16555860b52e636cef24a03f43f8b1945f52af8305b5ae6af886f2b1', '85dc272d450d0f6d16dc340d55879034aad3fb1405513be4b5d4a1a0579f72cc9bcc10a402016e2d', 0, '2020-02-17 16:02:33'),
('4057ef587ecfcca9aeaf5af996b961693e446b0d0cc23106877b93486b54564b03c3f4c18f9e11a5', 'ffa0d84e28a645dade4fefe5ad6544846585619a9e3221039356920d87f023584c3465a37448aae5', 0, '2019-09-18 11:20:41'),
('44b7a47709391ec653ddd3ddb3b9e597830cc68f84bedf69d3e172fe22103da4a8af7b470088bfaa', 'fe96626b551ec8639af25475120cf4681879c005c9ff879a497b018fab1521bc145c0618bb7a93f0', 0, '2020-03-28 09:55:07'),
('454c0c2fe3876aaa2a546ba66e0e3c005d00ede862c89d5f844a6e6deb874e291e77b9b116d1e515', '8c3413df885aa5c9ac01730bb721fcd667337b027fe62dfdb1503337e0a59b30e2318ef7d69c1c05', 0, '2020-03-06 09:23:22'),
('456cd60575213bd8445b037956c3b5f9c3263333bf3efafb63826e31637bd5ad8964f5e68cbd26f9', '8559d9220faa68794caec06b47e98c2befd36d76c3df229d2fc675e434e7621caff520bdcce3b7f7', 0, '2020-02-18 06:10:29'),
('45881fb0b3f309b8b98b7e9b55ec68b020a51e8d633e9df718d26950927c2d63d85b6d00c2780809', 'ecbe9ad21daeb2fc48f89cae547194b77a95c762a45ffa8f4faf321bce25caae180eac4bffd15db2', 0, '2019-09-16 18:20:28'),
('459c5c9cabdf9724f10c9f3f332097ace382e9a30f3ef58b8075a944bf6afa6ac93c6e0acfb01175', '4d0e8719958f7a8f6f29b0b7598dcdf0613b881fd4f03488006e569328e5f531664e506ae0908129', 0, '2019-09-18 12:02:38'),
('47b04f3ff482c36640a6c5de4e580da9a4b317c9151d7de8a967841e99dfc4ea9a90bdbb450a4ba5', '81316147dacdd440f5a5511d8cd046875cd36b405573b7ac85e228a6e0f030c5d61f63dadcd67c93', 0, '2019-10-09 18:04:06'),
('47daaedd6e27688f5b23d3feccd482a6ba859765a6a81474f19be597d0a395194a4b997a703b69c1', '8bebb1697ae68b9630843d7707418ffefeefcf5899432ee641e2a54e57e62e7cab8837585b89f4cf', 0, '2019-10-09 17:56:28'),
('48dbde32fc684f36dee18863554550c2ebc2ab6af9cd332351a69558d5486e0df07fe1d3aec1648e', 'ab07c4ddf6a3921466005a67a1c00d0bb93257331034c3e64eb1664d21fb1e656d256ed9bf7ebc98', 0, '2020-03-12 09:06:50'),
('49fa56032566d21664c3d4f1a932b2d8aa3db2e3288b7a98ef5c1bf4641dd1c6b36d8f226f18f664', '287bcae13d62829407e9c7eaf711e1f7c1ca33805a59fa962834d685eb5bb7d288c2c7720ca5ef20', 0, '2020-02-28 18:53:54'),
('49fccee03fd0c784f755f2b1f9930752b7ee476869fe7e266bca88e9f5425dfde989aae7fc234429', '61748943ca5547ab10810abe35ee7abd8c5dd3175658a215d8653d4d70e1b20bbdc62ba92d0e2b0a', 0, '2020-03-20 10:23:12'),
('4bb8d10a1713e0d8afdc1514abed9fafd4ce00d8610094c75a0af0ffda875cb31eb28205570030cd', '9b241c01419f3fd6087174c03b59e7429d97862227883a0fcc027ddcc7d7cfd9cad7f060e9c90ab4', 0, '2019-09-16 18:29:55'),
('4bf3ad53a3bf8b3162d09a366db49ef075561d5ba1d823fbb24a7b719b0b4ab1a1c67e432fc17094', 'f627dc4434d014793ce149e944c1fa6a747fc088ccea50461c0fd8a8db0a4fa6979028f4a4660a81', 0, '2019-09-25 09:07:10'),
('4c2aecbc5358ab69f88032cd847e6f2bd2ef9b49915a5accbbb723db08437dad25fed6a45d8fedc8', 'b92f3cf092307d063b3b17c2f3eedc294828c1126f8557dfb51a8d3fc8d350c8e1ea233a9f5deadb', 0, '2020-03-12 18:47:29'),
('4c2d9978adc17d3a21100b609d43e666c0fc314ad44ced276905818d3156dede84b9189327cee065', '3aa4a56c06aaa12be9070eb250cb3ba88f4434cc4428ee36a0c315154aef7ec611201edd907b7246', 0, '2020-03-12 11:05:44'),
('4d054ea5b8e79ecc351eb359321671760f97d8ebe2164f96bfcb00afa7c31f427ee16dd0b1e446f3', '79b5c8122efe0cc46e69e45138093c3d3c5971b7be5e990f1de975c4083066609075d2ce97987999', 0, '2019-10-09 18:13:03'),
('4d30619924e877f3c45f092069935d236540bddea987962ce35c59fd07f9be0f3cf08a0a61b2c215', '7dc8ae6624360d9474b05000b1f7a79b17e536c7e79a4f613d6e0cb66cdb66b925850e24c1f2817b', 0, '2019-09-23 06:35:17'),
('5003d21a538957670d0c3e10dce91b1edf8361b518ad446a94453822b385a24997e7a011f30a0533', '766a5e555c2e2f22eeb250e662d9594319fd7fbf3f7d2f0bccbd1b1cdcf1eda3f0d68db998eb7836', 0, '2019-09-16 18:19:21'),
('50ac517ef36084bc3f52ed534209ffd7cc12b1dbdd6107c2dda27bbba05cbe5a2ec2e5434a18b036', '78938ce0bae52b02021d943bac9d60ca904f28ced570c58b51123f88f9098ab0e97b4aa535fb6325', 0, '2019-09-23 07:00:18'),
('519934fdd4214029da4216bf0f3792e3f5cd433b6c0828de634b7aefeae3edd5958584fc1a385443', '6781ae0aaa1f021a8dea73f14d6e4c832f84e4806a22f51edab80096950c067d7c1baee8921e1092', 0, '2019-09-16 18:33:35'),
('54ce10039a5399597dc4c74a0f7151c2928b44b154b75c34eda8f106640e3b505a911353d82241bc', '647037f33d34e9f2a58df1c3771378a4b1aaf4d572dad8403b949cb4f9861a793ab6a70ee512b636', 0, '2020-02-27 09:24:23'),
('56e1bfcf2e9040c4170a3ca40137292ebe98ea7fc3f885cbeda81e9ad202f050c285ae67015f8803', 'dc98859b2096fc07e3c3c016a99e07b869adc7d59f40f6742f4c10413cbd741e1887312c5b67b057', 0, '2020-02-18 06:33:55'),
('5720b93d8f642e665248707302ac8ed3787f835afcfa845eb271cd855332d22bf4043f563c4f0260', 'e2291ac4e834341b060a66686908676b32ac6d4e68c18b58947efc9498ebd5462705e80230fa200c', 0, '2020-02-18 12:28:00'),
('57e783cee12fb91a4285e45b440839d4f44fde1f098743aaae064c41c4609d89dfa4115817c30e0c', '07e000e01b9cc8a185999cad9ca1f260ff39bd14352449c5fc3722d10f7600c3e280e6d86769afb7', 0, '2019-10-30 10:39:12'),
('598fd44ecca2ed92bd9cf428e68e332a184980b94c5064a9851baf46bfd22a7516e81d49795057a6', '2610fc47d294f4c44ca1f70c6cf16dc129cb769ae80620e2806fd1102ac5fcbabc289859597ba86d', 0, '2020-06-11 05:29:36'),
('5a73565b9b4816f9421f4c8b801bea35736ada6c800185e302a3ee0c34bf6d29bd52c5b4a195592c', 'dc3ed3a146e71a25f7586521e8691566adf046d9da6ded69742874a142b0f823e7951990e68075f5', 0, '2020-02-13 06:56:22'),
('5bbb50ab82477a27f38448cc41d27fa85180c2303cfad8c2653c0bc6823215d6d6549ed3d966dc94', '0a9df511228414ac76b6a2fef071bdd4935a256ea055b43356ec46df0c86b7538e1025c80d2e8598', 0, '2020-03-20 09:44:00'),
('5c57a28b4a3dcfe8090c33215d08e44021d9f0b2f947bc90682ab59531938623aa3f3acbed88c6a0', '488de8a9b7aab2fbb1603bda8a5415833d26e3e0674e6149191320d1b4e05de8932e75c8847086fb', 0, '2019-11-04 15:47:49'),
('5c9d0b4767434b446363006e55decdf236c34677d86773d0bcf9aaa0804332fdb46ade8318377d81', '66de80acde779c57a90fdf7abe96f405e61bfbca2da6a7637e6703cb6ef1a803331b6f45dca5561b', 0, '2019-09-27 11:23:40'),
('5d7e4d696b61e60f807d14c4735df875802dd383c91c5ab47e6271d40e88e31d5cffcd0ecbdd5944', '4dc0d3531a8341fae8ef414176617810eb285f452badc19a57793540c4c4e5a3edab36203a27d2b5', 0, '2019-10-10 08:17:02'),
('5e7b3dc25f5c3ae642e8113c7ea838f9f6939a21cc45b7c4660d804c0eba54f5c4b480a1c5d18cab', 'dbc5d448b1f84a4e4f51ece12a2a2a3f4143c42c8682ee089a38add009b25113afe5f8ce2c4ffb44', 0, '2019-09-18 10:33:59'),
('6276b0c2b5c0f41cfb39b8e13cece192a11bff3d7f2481181cbf4e6852e067b0dec5cbbc8a9e5b5a', '194545cf7343822fcc0b918127fb5dcc5b869b72c12779f2ccc431fb3e55fad3912077164eed85bc', 0, '2020-03-12 19:00:51'),
('64aba060b7a78b695d878cd05ee0be318946389afca303d0087f65b47808c358b4e00f394c98b6cd', 'b8877ae919b5ff488012df02e538468f3ae74f66d82174403c20c5c4867de76ad4bee50505d67723', 0, '2020-02-13 08:30:50'),
('655b3a5f0d90bcb96776d47f6593ec0d3c6c94b52af02af9d0314dc73786eaf8b35502b6dbbe33ab', '9068929aba336ea4b41e68cd222933d918b2d0350ec254e4077f1b74b6b854112c2d41c68b40010d', 0, '2020-02-18 06:29:59'),
('662fb91e6e12f8fcd83448310825904e30fb86466b877de07c36757494755a4fdd3287c58ba09637', 'f071705cc3b31a6f92a7c16034eef8c3aeaad0472151c7fbfb963e4cd3a0f9a60a80b5f6d1da561f', 0, '2020-03-12 18:47:54'),
('66664c140ce626b0f00badf91a64ce08a4cf1d7115c37d535a5d9692ba09c4566588f4b46b0c9fd0', 'b166e6b90bb78c96a4762d77e8e433b21b07e1cf2434c412ba46b7e01a6c9bb8b3bfa0411f6d2f3c', 0, '2019-09-16 18:30:57'),
('683718f677788b98faac8633ee9ec62a9935c51f4fa157320a8984d349a8e09019fb44d890d43f48', '62b20f4e583e822c2cdbe9f24182f0dd8584a524cfbd1b47c5b9f9c5ac425c7422142972c9564d9f', 0, '2019-10-23 09:26:23'),
('6a5e4935d12a8530852bc5b071a17aaffdc5bed964109352854d7e4cff46fc4aabd9c8ec9936389e', '92a37a2b07e7941155845e28de176ec062aa876182f111f3b6db973b26bdf4b0f2d489e4a40ee5c4', 0, '2019-09-16 18:38:50'),
('6b897e7f5489fdbf0136246594cae3b9e747e250d44d414ca6150fa56196b96dd616690d3d6903cc', '5aa985488e7ef6540466e7123d8d43a385303e4cb5a7238cd331217cb8239e3f5a86f2dc8ed7c0b8', 0, '2020-03-06 11:03:13'),
('6d6b5ad72c4121d502d465b82af50645075f0c26c6a2677c20d03bee28447bbd638c7db71b126cb9', '586143891815a1c099a2bffea3990632982929f8493c7ba49b4a7fc2a7a922404aed1f3a9489fc28', 0, '2019-09-23 06:33:34'),
('6ef8a0db377c29fc5902527d60c126534f66d14090aac80526e38bba1c82cdef5faa728fa5509343', '83ebb45b6b549da4563cc18ead53b9d4df2659acedd80c57ef76133856505aae895af5da384b09f9', 0, '2020-03-06 06:59:51'),
('6fde3f4a9ee75700de351ea8d81f59899c75e1e4f9697747d65e31c4616ffb49964558d669fcdb76', '1f5e53e3adce0be26fdff326fc31aa8c5a8093fe1a3240dfd5e7a5bc487d4283b2d0bf77215d88b4', 0, '2019-09-16 18:29:36'),
('70cb604b385be50a5c66a66108ccd2058cc0908ce31fde3c8c2c4a15dc3328cfe5512d04e9ceaef4', '9d9744443b2136c222f913df72536326b2a859bbf89a1eb090cd806f5822bdd3a017c3ba8c08c97e', 0, '2019-11-21 10:55:15'),
('71fceedd9be6577e49fb02fa3200bc10063f7191189b3fbcddd0549f3f8eb04e64ffba384cc186e8', '68854c35737a861df59d3de3be6c626fd7f86e95d26eeb86e3bf7d60e308faceab006659db521ba7', 0, '2020-06-11 05:41:29'),
('7230a522ed8e1138b82d55217f675bb78e42118601f6b1c852153cd4e6cb3da3f5a574b5906b7791', 'f3b562ef630a7af3d4920bb2c08873b9c63d1fca32f97660e7ee91df09323be26d417f97a5d08be9', 0, '2019-09-23 11:57:50'),
('73b3cad4e9ad655e0addf592c4218bd6cbaa36a385b29dfafbd7626dbf19310552fc2293b544b4fa', 'd77211f03813e26d22e001d97b3921e56ba11f4bcb478a25707ae7536219df5f53a73a7e621cd8ea', 0, '2020-03-20 09:11:20'),
('79aa4c74431905d34d41a1209d482bff9e4a211e7933c4f0ea684e6205b67b093d23bdabb8acbfe7', '08f64123c481002297e0e4133e4aafd06d2f8f695a5f3566498e5301616501d182b0b9a194fbd32c', 0, '2020-02-27 09:25:29'),
('7a5a3829b38e8aae561293b7489e7754163a3857d72188566b7ab9dcad9dc423d32a614641d52efa', '398a3b9eca968a59869e13b87497094ad11de24879629b8eeffd23e3fd427a00d16ee7081c3e991d', 0, '2020-04-03 06:17:08'),
('7b11cba51b033485e7edc17c39be1c338272b2459c8bf699c76d22e83b9fc23c3e1657afdeff555f', '963c308057de92793c83eeb0658a4c437289b75cd2cf5547d43f55803079bf8ccd33c0ab651c0bd5', 0, '2019-10-15 12:48:23'),
('7ede32ff558b4aa2080e3193ef0264507b0cb678608208a222e003a7a0113446fe56a778e5235067', 'cb4d4b623b1bae93ef1541effc60579d49998e8890a6ad0b22af35f676cdc7ca9d1de24489cfc91b', 0, '2020-03-08 08:41:05'),
('7f8654d06d28f48e2a5c0728df9bfc8ee55449255d2d6ece69e45a31ea766f98f0071fb093eecb0d', '57aabcb213af4fa286b676b840a29505eb7229c0e4e03eb230444a9d49fc248706d1e0191a3f3752', 0, '2019-09-16 18:33:17'),
('8170086e4fddf55e39590ba6fb3ba0c7f73f838247a4ca9e391330b4af1d8a4dfa7ad30e5fe8b519', 'f7c6dcbc739914bc3c8dbe915c4fdf0c0b172badc2ccc54af547942b94b18e6864215bd71d25c427', 0, '2019-09-16 18:29:09'),
('818310d5eaa80c269e5317bc7779ba52226cd8a9e91011dba3f28c50f5b34dfb3ebf7b5771bb0fcb', 'e188cb02987de6ecfedf022d14169b11d6292630f6794d4d1577c9ad1fc5983e16bba93235c8625b', 0, '2019-10-10 04:57:06'),
('81c4b0709ecc4454420cec3a256c650a53b916e033ea0efa398bf583e0215a03b9a5787b2cf85ffd', '6c1e745ff24c52e47c7b3f431038fcc92e8046a73d039ae2c0a2f15666fa8848616ff52dc511258b', 0, '2020-03-14 07:55:34'),
('82938bbd043a75a23a11ab3c0bbfbfc28539b212c5d7d0b4b98e30d305d15564f4b0b56962f28f01', '22ec0062d42deaac277bb48b3832712838c4aa92aa7f0669774e9473f9a377d7218f818fc5034430', 0, '2020-03-25 06:47:05'),
('832bb5402b03e98ed9c5a0cda7ce2954de669117d953fbaa31870e970f5c2af7c627b2cd6fe5ad25', '5a4d0cb1b9d28ffcb0e62d033ca1551a96eb2660aae0284a31d59ae04bdd95e0a3fa964ee07c5758', 0, '2020-02-27 10:08:47'),
('848259d3ee7811e627a8e71299ca55c61e44b245ccadfe8ac80ad181ee984c9330991618a0463afa', '97e965f193aba5bbf29885a3e4647d5c7d6de7d2b9ff3163f1dc0053b21e00e143990b7cd55c970c', 0, '2019-11-06 10:02:06'),
('86f86370a7c5b0a789c5c79a6bf39f11338a107fd6b9e6755d50b46f34f513db330364923a9a253f', 'ee88c5eaf0f60104a963888d52540a97538068b8a677a4c92c1de5659565ed20787018d50e99c07e', 0, '2019-11-21 11:04:35'),
('8762a5ed3bf92b46879f1c268710ee03e1920a057e4b21f53df783a217ca4a6c4311ad144c552d33', '443bee6cf3bb0abb3e3ff23cc54975f251d44566953e2f97051675a73e6069779a1c0ee00a6aced2', 0, '2020-03-14 05:18:04'),
('87d4255fbb4cd558372b338ba1599e8e78b3da018ea4c63681a9cd4fe615c5137a273faf28c71efa', 'e858c18679ca7e936f3d8ae7ebd31b98e80b373f00b7cfb10c6ad91e0217a482f466766bd51ec379', 0, '2020-03-14 08:52:23'),
('88e5ddaa2ad853667cfd1cb70e3e75438c6a347d88cfce3d45c68da66baecf23529859a2a9837e7c', '4f9d59e1720dde3487826cef20e1db507ecf6fa5acbfd0f7019705e40078f5a3ae7f86750c580418', 0, '2020-02-18 07:21:30'),
('8cf34025455b5a7f6ec524b327872556064b1b6d72997c1d78aa15010a2b21d06eae027856d148aa', '8a77a49ab59e4636ab52a5e558aa23d6510e750394c6cad66713e2161ae58820884b35f8871689f7', 0, '2020-02-28 18:04:12'),
('8eedee308a5060dbc9883fd250b70d4e3b4ffdbb58a7be44df14ddf9edd22346c18f8c2d2c14f3a9', 'a84ac0c65274cf3b7fb53723ebd91115ee788b5fdda2955a9a6fe7f289fad80e98d7fbebd9400ed3', 0, '2019-09-23 15:15:51'),
('8f000c6ccdc0ac027e7a274fcbf6c82d3c19087f778a6cd765ca07a24c62fb3ce0228f872d9db692', '32d289cce138b04f04e2c4158f9db32652a6d8d0765830628177490f811127f09fee81e9a330c9fb', 0, '2019-09-16 18:32:24'),
('8f659eb681e6f17e4f4ade94fa30b67b447b5e072805c53dd9b8f69b191b1b14ca5cdeba1942fd9d', '6372431df9b7d518b921fe3ad04037a5e50406b6961a0dad12897f598fd613470b4fedcce88efff9', 0, '2019-10-10 05:41:30'),
('8fe4b6e6c6059f4aece63514d7b8384f58cd37c28da3a5870317cd139c66ed9e3ead76ee4beb33c5', '120ca34fb9409b0d7caa45e68b5ee263c77c684a4da3625d1d40fb627e81bbc520f511c2c81d5ed1', 0, '2019-09-19 04:55:50'),
('934a477fc0adf2d15b21d4b8225f1b515bd7d146b65ef46ff5b3eba25ca3780be48ca9b512634c0e', '40a1de3e14e39c852b2ac979656c0c41ce56de59643e08045b9a653b6f80f1ece9a8db124700c68c', 0, '2019-09-23 07:01:52'),
('948445ef5f3c9df81c0c032a1e0dc2a9f27c104b9f9eb9509242e944aecdd2582feb10220d37a41b', '39b1f4c6723cddd7c65158206ab8bb009fad4ab102cf8ab5be166d8194554f5a21b064c9c2845961', 0, '2020-02-28 18:55:28'),
('94cd3f1af500bc08ae7fa80476140503733265e63c6a0a3bed8b9ff76828949853829e918ef48809', '43485df7e9243392b3492b62b4cfce7f40663954c96ea3216aae931574ed4641b1fbc4f9bae66812', 0, '2020-03-12 10:30:22'),
('94e9a853c9a6b501bcec88f9197f29e8e20724455949eb9adb147a47d8110987d8f27572a2e59e35', '5a19b1723d86bb99199549a150730f6df94164505fe1943e55bf63efa24d192ee55bdf1b19a7a8f6', 0, '2019-09-16 18:28:56'),
('951a62365040a1138f19d0d0e9f18d32e4870219f8e5aa85d664f8cac79fa3893fb2b25ec7637f1f', '0b87848b489ab397bdc788f6c2e1e7291acfa27639ce34e0ad8b29b9a01b1324f79b8163cff23baf', 0, '2019-09-16 11:37:00'),
('961a67c3c1bc6d5dbfbef554feb2395f9209dceeee7e819578d2ccbc05cd254fe2b01b8135686e18', '94a18ab07f61f12de4b93d54a5304f5024c2d33fdeb612b7d1f19df83c644a72087c23e83bd15c83', 0, '2020-03-12 17:38:37'),
('964aca24b19495caf4751427cbca5f581be22e5bc0af6da8bc42b71b08197cb85c3a2c3c6483ef5f', 'a2fd770306da562ab67729e25456eb5d8863722693be3e6b4ba79bb853d03c3d7b98018bd0a808e8', 0, '2020-04-03 07:20:31'),
('96ae4e359948b062e39e76aa8547ceb9f3fa9fe5cf753c267b97c25359adc9243ba4cd826f3254d5', '4848160cd70ab2dd9a0790d23d26308c4a4a48bcbfe163ca8497e054e2183f2857f15b9142f2c8bc', 0, '2020-02-18 06:46:52'),
('98ddca5038a48f1328159085d1738782a98db0c9b993619bc2ffe40f5e955782f8af31da61f46893', 'cede75bdea5d39133140430c4801e54b25421e962d22173386d76944dc03b66fd34ceec136078711', 0, '2020-02-18 06:58:02'),
('9a42dac8376b97c608e4c5ffe5fb2d7d0a8ecac0cb6caf24ed2831df926913d048674e9882ed5ae5', 'a818bc14d0fd85454b62398d4218a66ceca8f8b07e5974c5b4ab7df254f72f305c4568b86e113a18', 0, '2019-11-21 11:13:40'),
('9afd3499e4839c1e581b253ccb2afa2fc0c6d8898280433867eabc47941f4dce7b3e080bec54b10b', '17038250a56742bcf3ccdaec24fb3f607cca47c03cb249d0e31678cb82edbc82ef606493e10555a2', 0, '2019-09-19 04:57:29'),
('9bc98bd8f19d36931981ada7b2dc501e8916a82b21a23b5c95b5e1d0b5027c7f4ed2acfb0264addc', 'f395d0d791a164bd9a8a84dd0d296ac441168624d34199a7c652487642cd80c5e83e001072981ccf', 0, '2019-08-31 11:55:40'),
('9ca6dfa51a9967d4623fecfb4cc7793431e4c567613ae21a7bfeb4fd1aa24caaf77fdf28e10a190b', 'a8b73a653afd94253284b6b3eb5b585a7b8953659602364111e69525ed709cc02d34bbe67fc07126', 0, '2020-03-04 08:42:08'),
('9d6787067995fd68ebc0f522522818b1f61095aef300ab78d171cef8d5ceea0d16f428f7d8b5a435', '293d3c09eb65904e5d73e494c836802ba0205934fe8cbd9287007c26d128a34a87099546159ab0eb', 0, '2019-11-06 10:23:00'),
('9d8dcc4deb02b6425b28877e5337b39cd4a093332b1aa2fb20356ff51f4213a1a264ea7ea82a89ec', 'c87cbfaa3ad7bbc3e8380023d8c8d4a29fd22023630020e09d59bd478298e53b2e6b4a02feab68bd', 0, '2020-03-06 06:57:06'),
('9f1f7474bbf873ace9268da57e850cde59d900bd53d2bdecd5b803eb10669cba1486254dae95b31c', '25031176e6bde1aed18dcf0c2579c0626d6572d3353c6f5d7ddd7b462cb0d52b8abbbf2951c7ebd0', 0, '2019-11-21 10:50:31'),
('9f37e49515d3d745dcbe29f0b0ba18baf8087601ed2e6dc31dd6c5ecaf77105e7a2ec2d8903eae85', 'c47db0c4eb9added2e9f8da1d57aeb8e43cbf21c734faa2974271f0511f551f9e338f9623acbbb1f', 0, '2020-03-12 19:01:42'),
('9f6bad11189d0a9eb062a490a36c995007775d4f156c94198173db07effb8919fc2bac98840c4878', '104835c58006eefdf8cb9358de85d352bad73b1bd1399fd1bd4c3f32ef90a4bab1667b48675a7781', 0, '2019-10-03 05:25:00'),
('a0aa11057cccfd1e6e0e1cad88368dd94760c9a75ae2e8455ec32bb7ba2bdf30b9b76ba1b0e34021', '863900a30567661a40659575361a3e7c2688064ee33161bf4ecda0ff4128663d201b4400d993c2f5', 0, '2019-11-06 10:34:36'),
('a16dbdcff061a6354c69c54bf6f0948d85baab92438f58c77a16ba8145290f859440912b29a968ee', '705033fd11625266988a56ce7c7cc3b55cf89efe3dc2690877f3a7c7a02ccef7221565b093ea13ab', 0, '2019-09-23 13:03:37'),
('a20d9adabfb82d5372b3943ea777d5b75bf57b98c4224b68fce077792d5ae6dcd5d2f5b4ab7fe8b6', 'd172ea499bbed6412cba25b9d66a1f333e7735761aa8f108df4830fa0b23d4b5708b59e6ec06121b', 0, '2020-03-11 09:09:25'),
('a455c5a7bc6f437ba0a50b920e60ebaae45844156676ad4347adfbe72af607c0fa39ef95a8093ddc', '4bef1c25e3904650bc473a757edcc2e655a337a9e1d71a0c9fd0064fee28b08f58dd91a845b702a2', 0, '2019-11-21 10:13:07'),
('a45bf12a7d94090b968c133d448fb2f2f0c1cf7be94f54839e85cd20e1193b480e51af160e9e7ffa', '5fdb26bb256bbd7b56f10192f3b7e31bb3b1bfb675a9b72c5bc2e7331b183bf1592238d65551fd32', 0, '2019-09-18 10:35:58'),
('a5a57ed232161aa9f41c18a8c1267ee591d87a34f7023555c62c8891baf37b17c2c99f9c52b8cd2e', 'c03cc26882a802ec40a4e4f0ea04e8d30d09c37ee4523f5d84bda905028bad0daff8f21418b41f26', 0, '2020-03-12 18:58:13'),
('a850839524ff5e7c94b30a06509c55a790c6b43256d63f08d6b200269bbab237e67c269a4d872be6', '3f0807b817ceb14963e1653628675eb3a95c4f3de43822f36a0001962d910bac6863656e03d9718f', 0, '2019-09-23 14:05:41'),
('a86521fe5570aedf4a20b6371026e351eaf97d3836a7e2d664b20534bcf8e42af6471b63a8482ebd', 'd70152d79efedb670d69d53dfcb768f697d5cd0548007fe59dc3d93e61c8b4afee777ca760876033', 0, '2019-09-25 08:43:33'),
('a8cc7f2cfb00aaafc17a240b97992eb68bff7203d2ff4fad2d18fdd63fa55eeb94e34218cfe5a5a5', '199c59c6a0ca1799d20c4fd517b4d3ce293d018e5561b3385db6fef664956054453b7d96c1c9d264', 0, '2020-06-11 05:30:36'),
('aa121ec918b9f4cd7cb46219e01ddd02b44810f468b804e5cb68c51d6e97c1248eb0bb75475e20bc', 'ec39316671cf0965e8b74ce9d97fbef5d0b9f50cc80ff3eb66b974c766d830ab0f3f551951e30644', 0, '2020-03-06 12:02:48'),
('ab46e0b6892c8a7266a641ac28df9d1e881b34119ec3d4afe6bc3a9013dbc63f5a908fc04209e22a', '41513836a751d6a5010454048aa7b3a92d987249c459c40a3bc3f6f87750ccf5bc96e84aba316483', 0, '2020-06-11 05:33:09'),
('ac34f26b3523a2012c6957a143f8d1937a0fb55b8cd8f36546cfd280006e6a383aaec94bd2275286', '457ba9fe8a024eb12df7ef2a1d61df3ebc89c616162f0cbeb317a303b016f1f401911b0a7336c956', 0, '2019-11-21 11:07:50'),
('b22cbdebf202d7609e1eae7b0056713e981223b8b3b2506f271a5377c9ab368ad927f16d03e51733', 'da8312f833d899af0aaee20aebe8cc2de131aaed0c9855184dce8d3da82874d2a2b0418e67fed4b1', 0, '2020-03-04 08:41:52'),
('b2af80ee2d85c2450cfc61009fa6bfc3e76264dc768858974def9ece0a870a2abbd04452575badab', 'cef0d1e1bd23539683bae4d649fb73c082887f497210909c6d403c422051e72d53085032d931d024', 0, '2020-02-28 18:23:03'),
('b32875dab2d7034bc00b5c091a8a3ec58b82a7b79579410e1b3e4092ddbe5eff240412ff2f7fdbe5', '228c0609c030f22238f4f96accd0ba4f6f631b5c0f0ef95f876c5aca0468f8ad4d5d89c33f12c9ae', 0, '2019-10-09 04:31:14'),
('b4a37fc7feb6b338f68c187a7e4f1f4af09650990bf56d8d421818169a53089e6d879e4c6d181742', '6f8e7cc91aa1fa0bbc7baee6f41e36869f3761e71e8c25d5de557d9f00a1832744b879cb0d13b3b3', 0, '2020-03-12 09:47:01'),
('b603fa5098715bdf1fa774df071c9837f686e1f30ccf514d649e65278722cb7698a49b254a08ba63', '5ce34abdc3b20fac9613190594116fb333cf776bf5fe47719da0cb84705bbcbe09b71feb8938105a', 0, '2019-09-23 15:16:35'),
('b64d93c01a79ef05f4519328723459bc95f5a90d89b7c04029de51f56fb84c4f9a3b68cd1cbf5829', 'ff6e95b6073998aeec44e73627f4c8750077bdda044f2bb61ba9e21703970e8e759f0ce54e37c41f', 0, '2019-09-16 18:32:40'),
('b817525612051d3559e1d0fd740eaf4e00dc023fdf544a87a1872fde078bf794bf802d44876a8d6e', '2348570eae60ce4191b28e6f247dd3faa733785cea7af30f249f83a8f4cc14b230358c138bce624d', 0, '2019-09-23 07:00:08'),
('b9d1aeed6f43eb4350a6ae65843f037239ec348a714c2b0b176cf529584a76a1db9124eb7c3a5ce8', '92f0d847f28e03156d6fac034aac54b8959d55bf479eb07846c4f99c6acbc6c59351cfb15bda7588', 0, '2020-02-18 07:00:35'),
('ba80a609f005bd3b9159968e3b48db245fd2b1da3d600c5628ba8d193793568eed6699e3547cb5c8', '47a9f5276df01cc222ba0fd5bb56cfb3bdc4e37ac4cc0bb64161ccc6a64df67010593fd1cddd54fa', 0, '2019-09-16 18:28:47'),
('bba808aa967983304c2e7e35c3727fcd1c16372a405ddd0d5a716e75da8d65aab3e801d9a9673430', '1100709d81cdf432cec1808942789331929110fd9fbe0dc3495f7efd65ed011a3002b7c5f8776758', 0, '2019-09-28 06:14:32'),
('bff7394d07829ee6363288d725f2aaaed2a09739cc68d2c57be8d272a6c58b7ee150f010d480fa02', 'd967126424ccbcb5979518dbd9e9d435e0e65c2842d2f15f47c022c9684bb49edb3c297077455d23', 0, '2019-09-16 18:45:58'),
('c18b0bebba8d774caf17c6b08d5899bf429ade710b34f6c709a06291be0a876f9f377a8d57ead2d9', '2afb9381d849e3a3861606f010b6fa89fd72ece11df27bb5d102770cec2bd327f3110942468b5003', 0, '2019-10-15 12:33:28'),
('c3014b621af9f4475ec13b2ce65a6111bf4f3158af4c6509f0c7e43f83d22ac2030b9174855ace3a', 'bd058605081d21a5b6038188861c77bb66276a7ef142e5789926ee472abe2c35e2c76f85313be642', 0, '2019-09-19 05:00:03'),
('c320ed9a3d1682c9f8672e5dad6cb15e7c5d0b46456f13f3e7b96bf4db40fe8287c85d8321beee1a', '7cd0756573d4df2d0ef81c6889075575766cee40f4734272dc05d4d124289105d731fed8f7d96c9b', 0, '2019-10-10 08:24:37'),
('c4d62b2bd705ff665738bd9127482257692ce8e4dfefae2813627df18f6baa1d8035c34ed2295b91', 'fefe111c72280268dcee7c1716d9e7fc8003c6234b068d68c18bb0be70316f12cf4a840a23ef091f', 0, '2020-02-19 04:35:41'),
('c672900fdb3c90bf21079aa3867c8c44aa2fcde05c8d39872629f35951b4a440117550dc7d39cb63', '89292babe185186f4311c5e790771dc30feb4b3de7db5cd7498be4561f0b425f30e057b3f64cf877', 0, '2019-10-30 10:02:55'),
('c6e91efaf74b37c6e811e860758726b1de3cc4637a36eb9e0be80e7b4763bcde4b8d39e34c440521', 'ad9d2953f150f4ca7704699466490f82c65c1f91cea8001d144a1cfda4215b2b5e3c19435cb8058a', 0, '2019-10-01 05:24:57'),
('c6fb944f50f9230a85a5fcfae3aa234fd9b694f1131039d7d7915871e7c671c92c95e098bdabb64c', '2dd98d9191709873a75255371ab783fcce09299f25610d620d169c673fe2458a1c0a5bddfcef9e0b', 0, '2019-09-19 05:00:37'),
('c7e97635fd9ae77b2e3dc2ffd853b51c74aeda74b1bbd6ec9f2e140c7fb3b76b3593ad4c69eb7d5f', 'f6baa7950afdebddaf79d824a171e5dc5fa6eed0ef0ce8804fd01edb178c0f1b6afb8e2b4b950648', 0, '2020-02-17 15:57:21'),
('ca961fe45c80d6ad0cfd9fc4bd6c064ad28dc179bbeb54e88250dbece3994b5d81f3e66d970d6411', 'dd9728c844ff371b82c9dad4d50f4c3eea901918a201c186ca24569b271f987465d538dc14f5fb01', 0, '2020-03-20 08:59:25'),
('cb5d558758d87b8157e3d0cfd7372a7ceb3d7cbedf127ccf3bc6824fc8ed29d00e76f15b068e8190', '8c12289df71756b3a0f68a0d8cc2dac214b8d4d792b732809b2665a35b914f4b799e87a47546b953', 0, '2020-06-11 05:31:58'),
('cfe2b0ec2aedcd437d10c524d7d00f675bf2608f214e3d34a6307927a129e56c3b2eefdc058cdc34', '8166739ccf6d6d8587eaaf4669d80f58c65645a21d398cbb1f4a2cd1bb0a9d40a0c9a62b25b30cbd', 0, '2020-03-28 09:54:10'),
('d1daca979e63d1cd4fd8b20ffec8592683dc74878251946ba363c4d3e2200cf7602e1597a62e9ed9', 'f47bcf8c242c0c212f4e5bcb149a07b868b691929350283ee3ee0531d8c358750c4326123f2f5d27', 0, '2019-09-27 11:24:23'),
('d2c80c0054c2af5fb1825928e317feb23a135e3e3fa340b93f89fc2c4b1393d0faf27ff59deb3559', 'e642e7e89eef528811ae6ce7b49a140b549491d509c405c2979bf3c9c3afc3eee82cd97f245c8d83', 0, '2019-09-18 04:38:41'),
('d54345f4908e26ee64349b8ec8b3159111abc8e5658e2d95efa20d80c2b020c816814e4a49b89625', '5923c20fd08281673d97f494e6f0bfe9b7840851ff282e256a4bb45973a6cfa854861d8cae150947', 0, '2020-03-14 07:30:35'),
('d611be8e26bea20ce893871c636c5206f429e143bb6570ac2d652e4c33ca3cdd3c7d5b8b0994fadb', '2e53d90ddcfff8c7efc0b2db2b0a107d5b9a02e9afd3c3e146390c311c9ce1bc0ae86f93e5a718ac', 0, '2019-10-28 05:41:35'),
('d6e6b1dacc4d71e7b8794dc1a45dd00e13d0540ec2a87242dca67a6e027b96b19319a9edf3d98160', 'a408ccef1745aabd949acf42eb891d0594de4642f1cf67b58d337e4edf1add9f000c8a31bd76c837', 0, '2020-02-18 06:30:35'),
('d874607af5afc3b06206ced64bc564b8f004335138832d122728231ffdffd0d48d9a7a0d090fa106', 'd4e31d892677b3714ec6abe3d40c445e10fd1398e10e36164376cef2a4f0e431ba284877615ada94', 0, '2019-09-28 06:28:50'),
('d8dc5a5bb40c86871a9cc96196759e7fb1a2ed91b23f06f799a9509c3837cee043c61826f33190c9', '424d428461449a72f29aa4535855209404ed238b809e63dd6e9393058499086a7515b2c8eb138ebd', 0, '2020-03-12 10:26:06'),
('dbb58d6f11fefd701703639d28fb1000a06757f55fecfe7cc212f91849faf31758b8247c59aa8684', 'e9c376bf0bb3b01873fc2f6cb75540dd86a6b9741ec6cea4c5343cc61880810eb22f5b1c2d56c11f', 0, '2020-03-06 07:05:29'),
('de3bb04b44f59ee269563bee00a1e1c1a9952abc4f25bee21f28703fda79584f49f0fd2699bc8fc6', 'a5d78bdf5ba798a379a4292737cec06e3f2b6f5d6fb5a163adb015fe4f9abebc2db44421ec12ed3f', 0, '2019-09-26 05:42:23'),
('e0162d9bfca41fb4cc37e5bf9bb79995b9cec3e9f7819e6c4d9636053335336ba7f4bb4f9bb2b916', '84a99ef9096768cbff5d8807f841e3e9c4bf530e3aec61ed751a96af0c1fbbdea90e98869ba5f29e', 0, '2020-03-20 09:15:35'),
('e03cf9052bddb7565e71c501a6b57a1d971555380dd011733b407c3e69caf73987b8b098ebdbe4fa', '8be6ba77d457f39d27852102636cf557002100c5aa4915561b728567ceed824359360ee0b4efa895', 0, '2019-10-27 18:05:27'),
('e05dd1e36ea31a2d826ff6f30d2dc435cda9e9457ef7a63c649c072913e723d62965ea94d9421041', 'd1b839754bef7f820dc974cc2a3c680928961f503fd00ddd1a1e48a37c05800d22d854703510db13', 0, '2020-03-01 04:55:53'),
('e21c84d395a9c43874a42780ac83041c14c83fb2f84b5f2feb420adbc457d5482df57fe604462ffc', '4b1bc21f0824ecab46c99a40e66aa0c29c46200becd1a0fff957bc03c268a4d69b40c05b0a4afa57', 0, '2019-09-18 10:29:21'),
('e2544780e1bb4e548ecda81befd479465e21c1d58df3c345fa14427083507182a346d74dfdf8867e', 'e51d7db1b8623791592702d962a16e8a4683e9a2bb1f487ae20f84332447422ccbc5fdb4fc6040e1', 0, '2019-09-17 06:51:23'),
('e4a894d5487f107ba03aaf04b46f13bf8e54f4a751ecafe8ef1a2fe7a4720413ebb4292c9118e24b', '6c29a0908d9c995e9ea2c115e8b62a97ab655f90e8e37751037a382cdf4fea4463064b9e66dc6e31', 0, '2020-03-14 07:41:20'),
('e78f35d5ebb8747daa2043f459448eeb2797bf15472fd370ead5a7989569a3d58010cfe9485656c9', 'f5d4efbc87cb15350ad8506327e551f389e6b62b1a5de9967e785961beb5f12153faff97f4dc6f66', 0, '2020-03-14 07:42:52'),
('e82e96c77c1798d95b29b2608ac2112ce35b02fdc5832faa5c2c04983ca087280a45b37f4cda4bb1', 'cc38ee136120e125995dd5529b7db1286604589b42f04e889e4d353a6213780fd95b7974208da609', 0, '2019-09-28 04:34:32'),
('e980ca12265ef0d8dc542844887e2101a083d37964774e5489fa6a57583f54451ee071f69fe25a3f', '58dda1223f08944b0de57a845de1c3d21dfb7748dbe27556caa0553e971b955e9bf69f1497c00e69', 0, '2019-11-21 10:45:00'),
('ea90e5e7590e6b5b15eb9d92fa25f0bace4b2ec23e52aa9d8d00527a11272334a2f1bb98ae2fc3b5', '5b269d0a70d125009f833862f3bd168c9f57fdbebed05a099564400241ea42c655eb7a42da41a019', 0, '2019-09-28 04:33:57'),
('eab870ddc34d816b58f9621e9e65e033cd88afb8942a617656d914e63bb9b6ed5bf89f0d1d8cad65', '26d9300b73c5b0db0075eb984b8e397b7d5c02c6a19886229670feafdf4412fa46616c259e6bcdc5', 0, '2020-03-12 10:24:49'),
('eb89258d3d4a5e2e011ba59120f58a1e6cd86a11e5c6217465ff72a56eec67908e3c19c85cbd3e49', 'f275a1721efb0e58558071b0f5ccaa9cfe479361cad800e81d8c60596debfcae0f7629f0d884a4bf', 0, '2019-09-16 18:31:32'),
('ece68d86f4644be73c50b8d933957cc955af9b0bf80da719064fbf7de2a26f9165bec10b1fbeee47', 'd7333c96b2e90770675f58693b6388ffeb7ac4069a076e08e56216748f6d2f188931f7d48f440d16', 0, '2019-09-28 04:32:18'),
('edb2c424531a5f89897134e77cd759abca97ce0f9bf2fd7d65b6466a927a8142d6298a9db7b77129', '02786963343cb9210521868d0b4632655c9491e26b6d70754ef58d7f6220e3fa104566ed144c778a', 0, '2019-09-27 11:44:36'),
('ef570390f2ff636a6285fc14da2e0b921021e6ce4b594ec21234a10d3c91bda0b4f1180ff73e8cc4', 'ed34fb24b0bbec96609d13546f0f02ef452434a73e9f24e19871c4a0ef5beed9fd8ce80fe0f4b8ce', 0, '2020-02-13 09:48:08'),
('ef93243fb1891233cfe5fcefd4c6bb723f3e6b636cfed42d4bd4529ee5c9d98bb70ca6951d68f263', 'd62929c5ecd7092abe7cf4e22648d768fa2d29493a374f3b5f85e208748a0bebf8abaa5a3a2a2e98', 0, '2019-09-16 18:31:28'),
('f1758dcc2cf00e28f57cfbf08bf310cc40e9a8bc82101dfa40c9b7b7d478377baee511cefc3595a4', 'f0d3d3056c2f1d1ecd07c022ddcbbd3663451e88771fe227fa9e3ed886ca1973c56012cc8e50e73f', 0, '2019-09-28 06:13:55'),
('f24660a3ee920324ba1662d878f10629212e29fee7ea15ec6496dd9ac815a5f64a0b0c4eecc70816', 'ca297c4417c72f19a501b3e4f5f733bcfb05f744ce97a6e50f2e231cd48aa59d7fda0973f1f9ecb3', 0, '2019-11-04 16:47:33'),
('f3d1136f3c64049464689675b19a3a604b0b63ef39a874acbb9b81a633c04b25f047e460d693b146', '6ae663a8f13b1a962ec660510b784974201687816c9efe51d85a637aed5b34533c40e622ef432d56', 0, '2019-10-01 04:11:50'),
('f748ad42d007f201f33170cdaf1394ae311d1167db3dda783aab5cc8ca546f4faa9d525266789760', '324bf93256ed57cbdb4b0a54c62b812e753433679080a6e82963d817d429aa0e6e6e3ee4d7e5e8e3', 0, '2019-09-18 12:06:27'),
('f7764898ba3f1757f1be9936dce57f08d060e8228e79460bb0e405fb70c1aa5152387067381de4db', '303ca4a6a7a3d83a4a330c1953c0defd259ec747e43de208c13017a896d0f5b1c31a1ffb3c1611e3', 0, '2019-09-16 18:41:44'),
('f796c6a2a8ead3b1b87debe2b1ecca2c4dc9c21a3862b0987cc2dbae50113756131a14ac25f263b4', '99e7ad1785443cd283c8cab4f415086525bc99f9686724c5c9246765c9b605de1a789e3d109370a9', 0, '2019-10-30 10:22:53'),
('fb9ad9de5a63dc55759adecbde0d669653db740cb70d88799f980527bae649298388a96eca404427', '09202450aa62e8348f6395bf5b5411ee3baa87883ce31023bdddb6f61e68f2ed19728185e1578679', 0, '2020-03-14 05:16:42'),
('fe0268f6da36b73cfb01e809cb30cbefda18838718ec3e9b8c1acc3c2323feffda957c92ca46e354', '6f1af1eaeef02f393f257dc7d7acb0b0965d7b1ba507692c136736a2e4dad3a248b3e653c6f1f010', 0, '2019-11-21 10:59:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `created_at`, `updated_at`, `name`, `status`) VALUES
(1, '2019-07-31 01:51:02', '2019-07-31 01:51:02', 'create role', '1'),
(2, '2019-07-31 01:51:23', '2019-07-31 01:51:23', 'edit_role', '1');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
CREATE TABLE IF NOT EXISTS `quotes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` int(11) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `service` int(11) DEFAULT NULL,
  `fotmation` int(11) DEFAULT NULL,
  `govt_fee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prof_fees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8mb4_unicode_ci,
  `split_price` tinyint(1) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Draft','Sent') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Draft',
  `created_by` int(11) DEFAULT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `code`, `customer`, `act`, `service`, `fotmation`, `govt_fee`, `prof_fees`, `duration`, `unit`, `docs`, `split_price`, `comment`, `file_path`, `status`, `created_by`, `sent_by`, `sent_date`, `created_at`, `updated_at`) VALUES
(58, 'Q00002', 5, 1, 3, 1, '200', '100', 2, 'Days', '[\"1\",\"2\",\"4\",\"5\",\"6\"]', 0, '', 'Q00002-Awadhesh-ACCOUNTING.pdf', 'Sent', 1, 1, '2019-11-12 16:31:02', '2019-11-12 10:56:25', '2019-11-12 11:01:02'),
(59, 'Q000059', 5, 1, 3, 2, '400', '200', 4, 'Days', '[\"1\",\"2\",\"4\"]', 0, '', 'Q000059-Awadhesh-ACCOUNTING.pdf', 'Sent', 1, 1, '2019-11-29 12:01:46', '2019-11-29 05:29:04', '2019-11-29 06:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super admin', NULL, NULL),
(2, 'Sub Admin', 'sub admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `act` int(11) NOT NULL,
  `package` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `act`, `package`, `created_at`, `updated_at`) VALUES
(3, 'ACCOUNTING', 1, '<ol>\r\n<li>DSC (Digital Signature Certificate)</li>\r\n<li>DIN (Directors Identification Number</li>\r\n<li>PAN of Company</li>\r\n<li>TAN of Company</li>\r\n<li>COI, MOA and AOA</li>\r\n</ol>', '2019-08-05 00:41:54', '2019-11-29 05:27:46'),
(4, 'PAN APPLICATION NEW', 2, NULL, '2019-08-05 00:43:37', '2019-08-20 18:01:05'),
(5, 'PAN APPLICATION CORRECTION / AMENDMENT', 2, NULL, '2019-08-20 12:41:12', '2019-08-20 18:01:29'),
(6, 'GST REGISTRATION', 6, NULL, '2019-08-20 17:41:13', '2019-08-20 17:41:13'),
(7, 'TAN APPLICATION NEW', 4, NULL, '2019-08-20 18:02:02', '2019-08-20 18:02:02'),
(8, 'INCOME TAX RETURN-1', 3, NULL, '2019-08-20 18:02:39', '2019-08-20 18:03:28'),
(9, 'INCOME TAX RETURN-2', 3, NULL, '2019-08-20 18:02:55', '2019-08-20 18:03:40'),
(10, 'INCOME TAX RETURN-3', 3, NULL, '2019-08-20 18:03:58', '2019-08-20 18:03:58'),
(11, 'INCOME TAX RETURN-4', 3, NULL, '2019-08-20 18:04:12', '2019-08-20 18:04:12'),
(12, 'INCOME TAX RETURN-5', 3, NULL, '2019-08-20 18:04:29', '2019-08-20 18:04:29'),
(13, 'TDS CHALLAN PAYMENT', 5, NULL, '2019-08-20 18:05:55', '2019-08-20 18:05:55'),
(14, 'TDS RETURN FILING', 5, NULL, '2019-08-20 18:06:10', '2019-08-20 18:06:10'),
(15, 'GST REGISTRATION FOR FOREIGNERS', 6, NULL, '2019-08-20 18:07:03', '2019-08-20 18:07:03'),
(16, 'FOOD REGISTRATION (BASIC)', 11, NULL, '2019-08-20 18:08:21', '2019-08-20 18:08:21'),
(17, 'FOOD LICENSE-STATE', 11, NULL, '2019-08-20 18:08:45', '2019-08-20 18:08:45'),
(18, 'FOOD LICENSE-CENTRAL', 11, NULL, '2019-08-20 18:09:00', '2019-08-20 18:09:00'),
(19, 'DSC CLASS-2', 12, NULL, '2019-08-20 18:09:54', '2019-08-20 18:09:54'),
(20, 'DSC CLASS-2 WITH ENCRYPTION', 12, NULL, '2019-08-20 18:10:13', '2019-08-20 18:10:13'),
(21, 'DSC CLASS-3', 12, NULL, '2019-08-20 18:10:24', '2019-08-20 18:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `service_documents`
--

DROP TABLE IF EXISTS `service_documents`;
CREATE TABLE IF NOT EXISTS `service_documents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act` int(11) DEFAULT NULL,
  `service` int(11) DEFAULT NULL,
  `fotmation` int(11) DEFAULT NULL,
  `govt_fee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prof_fees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` enum('Days','Months','Years','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_documents`
--

INSERT INTO `service_documents` (`id`, `act`, `service`, `fotmation`, `govt_fee`, `prof_fees`, `duration`, `unit`, `created_at`, `updated_at`) VALUES
(9, 12, 20, 1, NULL, NULL, NULL, 'Days', '2019-09-17 03:55:22', '2019-09-17 03:55:22'),
(8, 12, 19, 1, '500', '300', '4', 'Days', '2019-09-17 01:33:10', '2019-09-17 01:43:38'),
(7, 1, 3, 1, '200', '100', '2', 'Days', '2019-09-17 01:04:44', '2019-09-17 01:38:47'),
(6, 6, 6, 1, NULL, NULL, NULL, 'Days', '2019-09-16 08:18:31', '2019-09-16 08:18:31'),
(10, 11, 16, 1, NULL, NULL, NULL, 'Days', '2019-09-18 01:53:27', '2019-09-18 01:53:27'),
(11, 1, 3, 2, NULL, NULL, NULL, 'Days', '2019-11-29 05:28:42', '2019-11-29 05:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `service_document_ref`
--

DROP TABLE IF EXISTS `service_document_ref`;
CREATE TABLE IF NOT EXISTS `service_document_ref` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_doc_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_document_ref`
--

INSERT INTO `service_document_ref` (`id`, `service_doc_id`, `document_id`, `created_at`, `updated_at`) VALUES
(100, 10, 1, '2019-09-18 01:53:27', '2019-09-18 01:53:27'),
(99, 9, 5, '2019-09-17 03:58:00', '2019-09-17 03:58:00'),
(98, 9, 1, '2019-09-17 03:58:00', '2019-09-17 03:58:00'),
(97, 8, 7, '2019-09-17 03:46:05', '2019-09-17 03:46:05'),
(96, 8, 5, '2019-09-17 01:47:58', '2019-09-17 01:47:58'),
(95, 8, 4, '2019-09-17 01:33:10', '2019-09-17 01:33:10'),
(94, 8, 2, '2019-09-17 01:33:10', '2019-09-17 01:33:10'),
(93, 8, 1, '2019-09-17 01:33:10', '2019-09-17 01:33:10'),
(92, 7, 6, '2019-09-17 01:15:00', '2019-09-17 01:15:00'),
(91, 7, 4, '2019-09-17 01:15:00', '2019-09-17 01:15:00'),
(90, 7, 5, '2019-09-17 01:14:38', '2019-09-17 01:14:38'),
(89, 7, 2, '2019-09-17 01:04:44', '2019-09-17 01:04:44'),
(88, 7, 1, '2019-09-17 01:04:44', '2019-09-17 01:04:44'),
(87, 6, 7, '2019-09-16 08:22:21', '2019-09-16 08:22:21'),
(86, 6, 5, '2019-09-16 08:20:21', '2019-09-16 08:20:21'),
(85, 6, 4, '2019-09-16 08:18:31', '2019-09-16 08:18:31'),
(84, 6, 2, '2019-09-16 08:18:31', '2019-09-16 08:18:31'),
(83, 6, 1, '2019-09-16 08:18:31', '2019-09-16 08:18:31'),
(101, 10, 2, '2019-09-18 01:53:27', '2019-09-18 01:53:27'),
(102, 10, 4, '2019-09-18 01:53:27', '2019-09-18 01:53:27'),
(103, 11, 1, '2019-11-29 05:28:42', '2019-11-29 05:28:42'),
(104, 11, 2, '2019-11-29 05:28:42', '2019-11-29 05:28:42'),
(105, 11, 4, '2019-11-29 05:28:42', '2019-11-29 05:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@tradenfillcrm.com', '$2y$10$yp3nea7PsVCoAjbrMtiOYujyVdN3nflxlKTHVaN3WHR3BxS37UhY.', 'KOg8rxRusH6eVJtb6iUxCaW44dkCCs306qAD9rPzEnYaYshOEuG7VklIYNWO', '2018-08-24 06:57:23', '2019-07-08 01:36:37'),
(4, 2, 'Awadhesh Singh', 'awadhesh.glocalview@gmail.com', '$2y$10$n2OndUcSNFKIdswALYii9uuwtUuhUnDai.BJ4NaJdZVFYOzclVdSS', 'lJv4Jp7SvtcfqTv1PvQhhMgeZUIGY3D6yi0IE06fsEzFxDMye1sWik12akpK', '2019-10-14 21:45:16', '2019-10-14 21:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

DROP TABLE IF EXISTS `users_logs`;
CREATE TABLE IF NOT EXISTS `users_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
