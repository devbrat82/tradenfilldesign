@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="">{{ !empty($title) ? $title : 'Customer' }}</h4>
                <div>
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('customers.customer.index')}}">Customer</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Customer</li>
  </ol>
</nav>
        </div>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <!-- <a href="{{ route('customers.customer.index') }}" class="btn btn-primary" title="Show All Customer">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('customers.customer.create') }}" class="btn btn-success" title="Create New Customer">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a> -->
                <div class="dropdown">
                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="dot-icon show-icon">
                        <span class="dot dot1 show-dot show-dot1"></span>
                        <span class="dot dot2 show-dot show-dot2"></span> 
                        <span class="dot dot3 show-dot show-dot3"></span>
                    </div>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                        <a href="{{ route('customers.customer.index') }}" class=" dropdown-item btn " title="Show All Customer">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Customer
                        </a>
                        <a href="{{ route('customers.customer.create') }}" class="dropdown-item btn " title="Create New Customer">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Customer
                        </a>
                                
                    </div>
                </div>
            </div>


           
        </div>

        <div class="panel-body bak-white">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('customers.customer.update', $customer->id) }}" id="edit_customer_form" name="edit_customer_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('customers.form', [
                                        'customer' => $customer,
                                        'customerType' => $customerTypes
                                      ])

                <div class="row">
                    <div class="col-md-6">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection