@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-check"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <div class="card form-card">
                    <div class="card-header">
                     <div class="card-title pull-left">Quotes
                     <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
   
    <li class="breadcrumb-item active" aria-current="page">Quotes</li>
  </ol>
</nav>
                </div>
                     </div>
                        <a href="{{ route('quotes.quote.create') }}">
                        <button class="btn btn-labeled btn-green mb-2  pull-right customer-btn" type="button">
                           <span class="btn-label customer-btn-label"><i class="fa fa-plus"></i>
                           </span>New Quote</button>
                        </a>
                        
                        <button class="btn btn-green  buttons-excel" type="button" onclick="excelgen('{{ route('quotes.quote.excel') }}')">
                           {{--<span class="btn-label"><i class="fas fa-file-excel"></i></span>--}}
                           {{--Excel--}}
                        </button>

                    </div>
                    <div class="card-body">
                <table class="table table-striped full-width" id="datatable-quotes">
                    <thead>
                        <tr class='checkbox-row'>
                        <th style='padding-right:0px;'>
                           <span class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="1" onclick="check(this);">
                                    <label class="custom-control-label" for="customCheck1"></label>
                                </span>
                           </th>
                            <th style="width:60px">Ref. No</th>
                            {{--<th>Customer</th>--}}
                            <th style="width:66px">Cust. Code</th>
                            <th>Main</th>
                            <th>Service</th>
                            <th>Formation</th>
                            <th>Status</th>
                            <th>Created Date</th>

                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($quotes))
                    @foreach($quotes as $quote)
                        <tr>
                        <td>
                           <span class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input custom-check" id="customCheck{{ $quote->id }}" name="customername" value="{{ $quote->id }}">
                                    <label class="custom-control-label" for="customCheck{{$quote->id}}"></label>
                                </span>
                           </td>
                            <td>{{ $quote->code }}</td>
                            {{--<td>{{ ucfirst($quote->Customer->first_name.' '.$quote->Customer->last_name) }}</td>--}}
                            <td><a href="{{ route('customers.customer.show', $quote->Customer->id ) }}">{{ ucfirst($quote->Customer->cust_code) }}</a></td>
                            <td>{{ $quote->Act['name'] or '' }}</td>
                            <td>{{ $quote->Service['title'] or '' }}</td>
                            <td>{{ $quote->FormationType['name'] or '' }}</td>
                            <td>{{ $quote->status }}</td>
                            <td>{{ $quote->created_at}} </td>
                            <td>
                                <div class="btn-group open quote-list">
                                    <!-- <button class="btn btn-default btn-blue" type="button">Action</button>
                                    <button class="btn btn-blue dropdown-toggle btn-default" type="button" data-toggle="dropdown" aria-expanded="true">
                                        <span class="caret"></span>
                                        <span class="sr-only">default</span>
                                    </button> -->
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon">
                                        <span class="dot dot1"></span>
                                        <span class="dot dot2"></span> 
                                        <span class="dot dot3"></span>
                                    </div>
                                     </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('quotes.quote.show', $quote->id ) }}"><span class="fa fa-eye" aria-hidden="true"></span>  Show Quote</a>
                                        </li>
                                        <li>
                                            <a id="quote-mail" data-toggle="modal" data-target="#myModal{{$quote->id}}"><span class="fa fa-envelope" aria-hidden="true"></span>  Send Mail</a>
                                        </li>
                                        @if($quote->status != 'Sent')
                                            <li>
                                                <a href="{{ route('quotes.quote.edit', $quote->id ) }}"><span class="fa fa-pencil" aria-hidden="true"></span>  Edit Quote</a>
                                            </li>
                                        @endif
                                        {{--<li><a href="{{ env('APP_URL').'storage/'.$quote->file_path }}"><span class="fa fa-eye-slash" aria-hidden="true"></span>  Preview Quote</a></li>--}}
                                        <li>
                                            <a href="{{ route('quotes.quote.pdf',$quote->id) }} "><span class="fa fa-eye-slash" aria-hidden="true"></span> Preview Quote</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('quotes.quote.copy', $quote->id ) }}"><span class="fa fa-copy" aria-hidden="true"></span>  Copy Quote</a>
                                        </li>
                                        <li>
                                            <form method="POST" action="" accept-charset="UTF-8">
                                              
                                                <button type="button" class="delete_quote btn-xs" title="Delete Quote" data-toggle="modal" data-target="#deleteModel{{$quote->id}}">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>  Delete Quote
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                                <form method="POST" action="{!! route('quotes.quote.destroy', $quote->id) !!}" accept-charset="UTF-8">
                        <input name="_method" value="DELETE" type="hidden">
                                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
                                </form>
                            </td>
                        </tr>

                        




                        <div id="myModal{{ $quote->id }}" class="modal fade" role="dialog">
                            <div class="overlay-wrap" ></div>
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title">Qoute Form</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" method="post" id="reused_form{{$quote->id}}" action="{{ route('quotes.quote.sendQuoteMail', $quote->id ) }}" enctype="multipart/form-data">
                                            <p></p>
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="name">Subject:</label>
                                                <input type="text" class="form-control" id="subj" name="subj" value="{{ 'Quotation to  '.$quote->Customer->prefix.' '.$quote->Customer->first_name .' '.$quote->Customer->last_name.' for '.$quote->Service['title'] }}"  required maxlength="100">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Email:</label>
                                                <input type="email" class="form-control" id="email" name="email" value="{{$quote->Customer->email}}"  required maxlength="100">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Message:</label>
                                                <textarea class="form-control" type="textarea" name="message"
                                                          id="message" placeholder="Your Message Here"
                                                          maxlength="6000" rows="7">
                                                    Greeting from Tradenfill !

Hope you are doing well!!!

Thank you for your quotation request, as per discuss with you, please find enclosed the list of required documents for requested quotation.

If you need any further clarification, please feel free to contact us.

Waiting for your positive response.

                                                </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Attach File</label>
                                                <input type="file" class="form-control" id="otherfile" name="otherfile"  maxlength="100">
                                            </div>
                                            <button type="submit" class="btn btn-lg btn-blue btn-block" id="sendMail">Send Quote</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('styles')
    <!-- Datatables-->
    <link rel="stylesheet" href="{{url('theme-angle')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">

@endsection
@section('javascript')
    <script src="{{url('theme-angle')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/jszip/dist/jszip.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/pdfmake.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/vfs_fonts.js"></script>

<script>

    // $(document).ready(function() {
    //     // Setup - add a text input to each footer cell
    //     $('#datatable-quotes thead tr').clone(true).appendTo( '#datatable-quotes thead' );
    //     $('#datatable-quotes thead tr:eq(1) th').each( function (i) {
    //         var title = $(this).text();
    //         $(this).removeClass('sorting');
    //         if(title == 'Reference No'|| title == 'Customer' || title == 'Customer Code') {
    //             $(this).html('<input type="text" placeholder="Search ' + title + '" style="width:100%"/>');
    //             $(this).removeClass('sorting');
    //         }
    //         else {
    //             $(this).html('');
    //         }
    //         var table = $('#datatable-quotes').DataTable();
    //         $( 'input', this ).on( 'keyup change', function () {
    //             if ( table.column(i).search() !== this.value ) {
    //                 table
    //                     .column(i)
    //                     .search( this.value )
    //                     .draw();
    //             }
    //         } );
    //     } );
    //
    //
    // } );

    $('#datatable-quotes').DataTable({
        'paging': true, // Table pagination
        'ordering': true, // Column ordering
        'info': true, // Bottom left status text
        responsive: true,
        orderCellsTop: true,
        fixedHeader: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],

        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch: 'Search all columns:',
            sLengthMenu: '_MENU_ records per page',
            info: 'Showing page _PAGE_ of _PAGES_',
            zeroRecords: 'Nothing found - sorry',
            infoEmpty: 'No records available',
            infoFiltered: '(filtered from _MAX_ total records)',
            oPaginate: {
                sNext: '<em class="fa fa-caret-right"></em>',
                sPrevious: '<em class="fa fa-caret-left"></em>'
            }
        },
        // Datatable Buttons setup
        dom: 'Bfrtip',
        buttons: [
            //{ extend: 'copy', className: 'btn-green' },
            //{ extend: 'csv', className: 'btn-green' },
            //{ extend: 'excel', className: 'btn-green', title: 'XLS-File' },
            //{ extend: 'pdf', className: 'btn-green', title: $('title').text() },
            //{ extend: 'print', className: 'btn-green' }
        ]
    });

</script>

<script language="JavaScript">
function check(source) {
  checkboxes = $('[id^="customCheck"]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}


function excelgen(var1)
{
    var selected = [];
    $('[name^="customername"]').each(function() {
        if ($(this).is(":checked")) {
        selected.push($(this).attr('value'));
        }
    });
    selected=selected.toString()
    window.location.href = var1+"?id="+selected;
}
$('.custom-check').change(function(){ //".checkbox" change
    if($('.custom-check:checked').length == $('.custom-check').length){
        $('#customCheck1').prop('checked',true);
    }else{
        $('#customCheck1').prop('checked',false);
    }
});

$('input[type="search"]').attr('placeholder','Search here');
</script>
@endsection