@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="">{{ isset($service->title) ? $service->title : 'Service' }}</h4>
            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('services.service.index')}}">Services</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Services</li>
  </ol>
</nav>
                </div>
        </span>

        <div class="pull-right">

            <!-- <form method="POST" action="" accept-charset="UTF-8">
           
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('services.service.index') }}" class="btn btn-primary" title="Show All Service">
                        <span class="fa fa-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('services.service.create') }}" class="btn btn-success" title="Create New Service">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('services.service.edit', $service->id ) }}" class="btn btn-primary" title="Edit Service">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Service" data-toggle="modal" data-target="#deleteModel">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form> -->





            <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon show-icon">
                                        <span class="dot dot1 show-dot show-dot1"></span>
                                        <span class="dot dot2 show-dot show-dot2"></span> 
                                        <span class="dot dot3 show-dot show-dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                                    <a href="{{ route('services.service.index') }}" class="btn dropdown-item " title="Show All Service">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Service
                    </a>
                    <a href="{{ route('services.service.create') }}" class="btn dropdown-item " title="Create New Service">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Service
                    </a>
                    <a href="{{ route('services.service.edit', $service->id ) }}" class="btn dropdown-item " title="Edit Service">
                        <span class="fa fa-pencil" aria-hidden="true"></span> Edit Service
                    </a>

                    <button type="button" class="btn dropdown-item" title="Delete Service" data-toggle="modal" data-target="#deleteModel{{$service->id }}">
                        <span class="fa fa-trash" aria-hidden="true"></span> Delete Service
                    </button>
                                
                                    </div>
                                </div>
                            </form>


                       

       
        <form method="POST" action="{!! route('services.service.destroy', $service->id) !!}" accept-charset="UTF-8">
        <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$service->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>

        </div>

    </div>

    <div class="panel-body bak-white">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <dt>Title</dt>
                <dd>{{ $service->title }}</dd>
            </div>
            <div class="col-md-6 col-sm-6">
                <dt>Act</dt>
                <dd>{{ $service->Act->name }}</dd>
            </div>
        </div>

        @if($service->package!=null && $service->package!='')
        <div class="row">
            <div class="col-md-6 col-sm-6">
            <dt>Package</dt>
            <dd>{!!$service->package !!}</dd>
            </div>
        </div>
        @endif


<div class="row">
    <div class="col-md-6 col-sm-6">
        <dt>Created At</dt>
        <dd>{{ $service->created_at }}</dd>
    </div>
    <div class="col-md-6 col-sm-6">
        <dt>Updated At</dt>
        <dd>{{ $service->updated_at }}</dd>
    </div>
</div>
</div>
</div>

@endsection