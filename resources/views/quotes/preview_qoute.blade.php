@extends('layouts.app')
<style>
    .dataTables_filter input{
        padding: 1.25rem 2rem !important;
        border: 1px solid #ebf1fa !important;
    }
    /*div.dataTables_wrapper div.dataTables_filter input[type='search']::placeholder{*/
        /*padding-left:10px;*/
    /*}*/
    .layout-default .main-content, .layout-icon-sidebar .main-content {
        padding: 90px 30px 10px 270px;
        min-height: calc(100% - 39px);
    }
    .skin-crater .main-content {
        background: #f9fbff;
    }
    .estimate-view-page {
        padding-left: 334px!important;
    }
    .page-header {
        margin-bottom: 30px !important;
        position: relative !important;
        padding-bottom: 0 !important;

     border-bottom: none !important;
    }
    .page-header .page-actions {
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        margin: auto;
    }
    .estimate-view-page .filter-container {
        margin-left: 12px;
    }.skin-crater .dropdown-group .dropdown-container {
          color: #040405;
          background-color: #fff;
      }.estimate-view-page .estimate-sidebar {
           height: 100vh;
           height: 100%;
           left: 220px;
           padding: 60px 0 10px;
           position: fixed;
           top: 0;
           width: 300px;
           z-index: 30;
           background: #fff;

       }.estimate-view-page .side-header {
            height: 100px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 30px 15px;
            border-bottom: 1px solid rgba(185,193,209,.41);
        }.base-input {
             width: 100%;
             position: relative;
         }.estimate-view-page .inv-search {
              background: #f9fbff!important;
          }
    .base-input .input-field-right-icon {
        padding-right: 35px;
    }
    .base-input .right-icon {
        min-width: 18px;
        right: 0;
    }
    .base-input .left-icon, .base-input .right-icon {
        position: absolute;
        width: 13px;
        height: 18px;
        color: #b9c1d1;
        font-style: normal;
        font-weight: 900;
        font-size: 14px;
        line-height: 16px;
        top: 50%;
        z-index: 1;
        transform: translate(-50%,-50%);
    }.estimate-view-page .filter-container {
         margin-left: 12px;
     }.skin-crater .dropdown-group .dropdown-activator a {
          color: #040405;
      }body .estimate-view-page .side-header .inv-button {
           background-color: #f9fbff !important;
           border: 1px solid #eaf1fb;
           box-sizing: border-box;
           color: #a5acc1 !important;
        font-size:16px;
       }
    .estimate-view-page .filter-container .dropdown-container {
          padding: 11px;
          left: auto;
          right: 0;
          width: 166px;
      }.estimate-view-page .filter-container .filter-items:first-child {
           margin-top: auto;
       }
    .estimate-view-page .filter-container .filter-items {
        display: flex;
    }.estimate-view-page .filter-container .inv-label {
         font-style: normal;
         font-weight: 400;
         font-size: 14px;
         line-height: 18px;
         text-transform: capitalize;
         color: #040405;
         margin-bottom: 6px;
         margin-left: 10px;
     }svg:not(:root).svg-inline--fa {
           overflow: visible;
       }

     .svg-inline--fa.fa-w-16 {
         width: 1em;
     }


     .svg-inline--fa {
         display: inline-block;
         font-size: inherit;
         height: 1em;
         overflow: visible;
         vertical-align: -0.125em;
     }.estimate-view-page .side-content {
          overflow-y: scroll;
          height: 100%;
      }.estimate-view-page .side-estimate {
           padding: 12px 16px;
           display: flex;
           justify-content: space-between;
           border-bottom: 1px solid rgba(185,193,209,.41);
           cursor: pointer;
        text-decoration: none;
       }
    /*.estimate-view-page .side-estimate:last-child {*/
            /*margin-bottom: 98px;*/
        /*}*/

   .estimate-view-page .side-estimate .left .inv-name {
         font-style: normal;
         font-weight: 400;
         font-size: 14px;
         line-height: 21px;
         text-transform: capitalize;
         color: #040405;
         margin-bottom: 6px;
     }.estimate-view-page .side-estimate .left .inv-number {
          font-style: normal;
          font-weight: 500;
          font-size: 12px;
          line-height: 18px;
          color: #595959;
          margin-bottom: 6px;
      }.estimate-view-page .side-estimate .left .inv-status {
           font-style: normal;
           font-weight: 400;
           font-size: 10px;
           line-height: 15px;
           padding: 2px 10px;
           display: inline-block;
       }
    .est-status-draft {
        background: #f8edcb;
        font-size: 13px;
        color: #6c432e;
        padding: 5px 10px;
    }.estimate-view-page .side-estimate .right .inv-amount {
         font-style: normal;
         font-weight: 600;
         font-size: 20px;
         line-height: 30px;
         text-align: right;
         color: #263b5e;
     }.estimate-view-page .side-estimate .right .inv-date {
          font-style: normal;
          font-weight: 400;
          font-size: 14px;
          line-height: 21px;
          text-align: right;
          color: #595959;
      }
    /*.estimate-view-page .side-estimate:last-child {*/
        /*margin-bottom: 98px;*/
    /*}*/
    .estimate-view-page .side-estimate {
        padding: 12px 16px;
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid rgba(185,193,209,.41);
        cursor: pointer;
    }.estimate-view-page .estimate-view-page-container {
         display: flex;
         flex-direction: column;
         height: 75vh;
         min-height: 0;
         overflow: hidden;

     }
    .estimate-view-page .side-estimate:hover {
        background-color: #f9fbff;
    }
    .estimate-view-page .frame-style {
          flex: 1 1 auto;
          border: 1px solid #b9c1d1;
          border-radius: 7px;
      }
    .page-title{

        position: relative;
        top:-10px;
    }
    .btn-outline-primary{
        color: #5851d8 !important;
        padding: 8px 20px !important;
        border-color: #5851d8 !important;
    }
    .btn-outline-primary:hover,.btn-outline-primary:focus{
        color: #fff !important;
        background-color: #5851d8 !important;
        border-color: #5851d8 !important;
    }
    div.dataTables_wrapper div.dataTables_info{
            top:0px !important;
        position: relative !important;
    }
    table.dataTable tbody tr:hover {
        box-shadow:none !important;
    }
    .table > tbody > tr > td{
        padding:0px !important;
    }
    .dropdown-menu .dropdown-item{
        font-size:16px;
    }
    div.dataTables_wrapper div.dataTables_filter{
        top: 26px !important;
        left: -1em !important;
    }
</style>
@section('content')

    <div data-v-a7a79d92="" class="main-content estimate-view-page"><div class="page-header"><h3 class="page-title"> REF.NO-{{$document->code}}</h3> <div class="page-actions row"><div class="col-xs-2 mr-3"><button type="button" class="base-button btn btn-outline-primary default-size "><!----> <!---->
                        <a id="quote-mail" data-toggle="modal" data-target="#myModal{{$document->id}}"><span class="fa fa-envelope" aria-hidden="true"></span>  Send Mail</a>
                        <!----></button></div> <div class="col-xs-2">
                          <a href="{{ route('quotes.quote.copy', $document->id ) }}">
                          <button type="button" class="base-button btn btn-outline-primary default-size "><!----> <!---->
                        Copy Quote
                        <!----></button></a>
                </div>
                <div class="filter-container dropdown-group has-child toggle-arrow" align="left">
                    {{--<div class="dropdown-activator">--}}
                        {{--<a href="#">--}}
                            {{--<button type="button" class="base-button btn btn-primary default-size "><!----> <!----> --}}
                                {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="ellipsis-h" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-ellipsis-h fa-w-16"><path fill="currentColor" d="M328 256c0 39.8-32.2 72-72 72s-72-32.2-72-72 32.2-72 72-72 72 32.2 72 72zm104-72c-39.8 0-72 32.2-72 72s32.2 72 72 72 72-32.2 72-72-32.2-72-72-72zm-352 0c-39.8 0-72 32.2-72 72s32.2 72 72 72 72-32.2 72-72-32.2-72-72-72z" class=""></path></svg> <!---->--}}
                            {{--</button>--}}
                        {{--</a>--}}
                    {{--</div> --}}
                    {{--<div class="dropdown-container align-right" style="display: none;"> --}}
                        {{--<div class="dropdown-group-item">--}}
                            {{--<a href="/admin/estimates/3/edit" class="dropdown-item">--}}
                                {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="dropdown-item-icon svg-inline--fa fa-pencil-alt fa-w-16"><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z" class=""></path></svg>--}}
                                {{--Edit--}}
                            {{--</a> --}}
                            {{--<div class="dropdown-item">--}}
                                {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="dropdown-item-icon svg-inline--fa fa-trash fa-w-14"><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z" class=""></path></svg>--}}
                                {{--Delete--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                        <div class="dropdown">
                            <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="dot-icon show-icon  ">
                                    <span class="dot dot1 show-dot show-dot1"></span>
                                    <span class="dot dot2 show-dot show-dot2"></span>
                                    <span class="dot dot3 show-dot show-dot3"></span>
                                </div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >


                                <a href="{{ route('quotes.quote.edit', $document->id ) }}" class="dropdown-item btn  btn-xs">
                                    <span class="fa fa-pencil" aria-hidden="true"></span> Edit
                                </a>
                                 <form method="POST" action="" accept-charset="UTF-8">

                                <button type="button" class="dropdown-item btn btn-xs" title="Delete Quote" data-toggle="modal" data-target="#deleteModel{{$document->id}}">
                                    <span class="fa fa-trash" aria-hidden="true"></span> Delete
                                </button>
                                 </form>
                            </div>
                        </div>
                      
                </div>
            </div>
           <form method="POST" action="{!! route('quotes.quote.destroy', $document->id) !!}" accept-charset="UTF-8">
                        <input name="_method" value="DELETE" type="hidden">
                                                {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$document->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
                                </form>
        </div>
        <div class="estimate-sidebar">
          
            {{--<div class="side-header">--}}
                {{--<div class="base-input">--}}
                    {{--<input name="" tabindex="" placeholder="Search" autocomplete="on" type="text" class="input-field input-field-right-icon inv-search">--}}
                    {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="right-icon svg-inline--fa fa-search fa-w-16">--}}
                      {{--<path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" class="">--}}
                      {{--</path>--}}
                    {{--</svg>--}}
                {{--</div>--}}
                {{--<div role="group" aria-label="First group" class="btn-group ml-3">--}}
                    {{--<div class="filter-container dropdown-group has-child toggle-arrow" align="left">--}}
                        {{--<div class="dropdown-activator">--}}
                            {{--<a href="#">--}}
                                {{--<button type="button" class="inv-button inv-filter-fields-btn base-button btn btn-dark default-size ">--}}
                                    {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="filter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-filter fa-w-16">--}}
                                        {{--<path fill="currentColor" d="M487.976 0H24.028C2.71 0-8.047 25.866 7.058 40.971L192 225.941V432c0 7.831 3.821 15.17 10.237 19.662l80 55.98C298.02 518.69 320 507.493 320 487.98V225.941l184.947-184.97C520.021 25.896 509.338 0 487.976 0z" class="">--}}
                                        {{--</path>--}}
                                    {{--</svg>--}}
                                {{--</button>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="dropdown-container" style="display: none;">--}}
                            {{--<div class="filter-items">--}}
                                {{--<input id="filter_estimate_date" type="radio" name="filter" value="estimate_date" class="inv-radio">--}}
                                {{--<label for="filter_estimate_date" class="inv-label">Estimate Date</label>--}}
                            {{--</div>--}}
                            {{--<div class="filter-items">--}}
                                {{--<input id="filter_due_date" type="radio" name="filter" value="expiry_date" class="inv-radio">--}}
                                {{--<label for="filter_due_date" class="inv-label">Due Date</label>--}}
                            {{--</div>--}}
                            {{--<div class="filter-items">--}}
                                {{--<input id="filter_estimate_number" type="radio" name="filter" value="estimate_number" class="inv-radio">--}}
                                {{--<label for="filter_estimate_number" class="inv-label">Estimate Number</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<button type="button" class="inv-button inv-filter-sorting-btn base-button btn btn-dark default-size ">--}}
                        {{--<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sort-amount-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sort-amount-up fa-w-16">--}}
                            {{--<path fill="currentColor" d="M304 416h-64a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM16 160h48v304a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16V160h48c14.21 0 21.38-17.24 11.31-27.31l-80-96a16 16 0 0 0-22.62 0l-80 96C-5.35 142.74 1.77 160 16 160zm416 0H240a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h192a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm-64 128H240a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM496 32H240a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h256a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z" class="">--}}

                            {{--</path>--}}
                        {{--</svg>--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--</div>--}}




                        <div class="side-content" >
                          <table class="table table-striped full-width" id="datatable-quotes">
                            <thead>
                              <th style="width:60px"></th>
                            </thead>
                            <tbody>
                             @if(count($quotes))
                            @foreach($quotes as $quote)
                            <tr>
                              <td >
                                  <a href="{{ route('quotes.quote.pdf',$quote->id) }}" class="side-estimate router-link-exact-active active">
                                      <div class="left">
                                          <div class="inv-name">{{ ucfirst($quote->Customer->cust_code) }}</div>
                                          <div class="inv-number">{{ $quote->code }}</div>
                                          <div class="inv-status est-status-draft">{{ $quote->status }}</div>
                                      </div> <div class="right">
                                          <div class="inv-amount">
                                              <span style="font-family: sans-serif">₹</span> {{ number_format($quote->govt_fee+$quote->prof_fees)}}</div>
                                          <div class="inv-date">{{ $quote->created_at->format("F j, Y")}}</div>
                                      </div>
                                  </a>
                              </td>
                            </tr>
                               @endforeach
                               @endif
                               </tbody>
                               </table>
                           </div>

                      </div>

        <div class="estimate-view-page-container">
            <iframe src="{{$fileName}}" class="frame-style"></iframe></div></div>



                        <div id="myModal{{ $document->id }}" class="modal fade" role="dialog">
                            <div class="overlay-wrap" ></div>
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title">Qoute Form</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" method="post" id="reused_form{{$document->id}}" action="{{ route('quotes.quote.sendQuoteMail', $document->id ) }}" enctype="multipart/form-data">
                                            <p></p>
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="name">Subject:</label>
                                                <input type="text" class="form-control" id="subj" name="subj" value="{{ 'Quotation to  '.$document->Customer->prefix.' '.$document->Customer->first_name .' '.$document->Customer->last_name.' for '.$document->Service['title'] }}"  required maxlength="100">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Email:</label>
                                                <input type="email" class="form-control" id="email" name="email" value="{{$document->Customer->email}}"  required maxlength="100">
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Message:</label>
                                                <textarea class="form-control" type="textarea" name="message"
                                                          id="message" placeholder="Your Message Here"
                                                          maxlength="6000" rows="7">
                                                          <p>Greeting from Tradenfill !</p>

<p>Hope you are doing well!!!</p>

<p>Thank you for your quotation request, as per discuss with you, please find enclosed the list of required documents for requested quotation.</p>

<p>If you need any further clarification, please feel free to contact us.</p>

<p>Waiting for your positive response.</p> 

                                                </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">Attach File</label>
                                                <input type="file" class="form-control" id="otherfile" name="otherfile"  maxlength="100">
                                            </div>
                                            <button type="submit" class="btn btn-lg btn-blue btn-block" id="sendMail">Send Quote</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


@endsection


@section('styles')
    <!-- Datatables-->
    <link rel="stylesheet" href="{{url('theme-angle')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">

@endsection
@section('javascript')
    <script src="{{url('theme-angle')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
    <script src="{{url('theme-angle')}}/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{url('theme-angle')}}/vendor/jszip/dist/jszip.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/pdfmake.js"></script>
    <script src="{{url('theme-angle')}}/vendor/pdfmake/build/vfs_fonts.js"></script>

<script>

    // $(document).ready(function() {
    //     // Setup - add a text input to each footer cell
    //     $('#datatable-quotes thead tr').clone(true).appendTo( '#datatable-quotes thead' );
    //     $('#datatable-quotes thead tr:eq(1) th').each( function (i) {
    //         var title = $(this).text();
    //         $(this).removeClass('sorting');
    //         if(title == 'Reference No'|| title == 'Customer' || title == 'Customer Code') {
    //             $(this).html('<input type="text" placeholder="Search ' + title + '" style="width:100%"/>');
    //             $(this).removeClass('sorting');
    //         }
    //         else {
    //             $(this).html('');
    //         }
    //         var table = $('#datatable-quotes').DataTable();
    //         $( 'input', this ).on( 'keyup change', function () {
    //             if ( table.column(i).search() !== this.value ) {
    //                 table
    //                     .column(i)
    //                     .search( this.value )
    //                     .draw();
    //             }
    //         } );
    //     } );
    //
    //
    // } );

    $('#datatable-quotes').DataTable({
        'paging': false, // Table pagination
        'ordering': false, // Column ordering
        'info': false, // Bottom left status text
        responsive: true,
        orderCellsTop: true,
        fixedHeader: false,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],

        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch: '',
            sLengthMenu: '_MENU_ records per page',
            info: 'Showing page _PAGE_ of _PAGES_',
            zeroRecords: 'Nothing found - sorry',
            infoEmpty: 'No records available',
            infoFiltered: '(filtered from _MAX_ total records)',
            oPaginate: {
                sNext: '<em class="fa fa-caret-right"></em>',
                sPrevious: '<em class="fa fa-caret-left"></em>'
            }
        },
        // Datatable Buttons setup
        dom: 'Bfrtip',
        buttons: [
            //{ extend: 'copy', className: 'btn-green' },
            //{ extend: 'csv', className: 'btn-green' },
            //{ extend: 'excel', className: 'btn-green', title: 'XLS-File' },
            //{ extend: 'pdf', className: 'btn-green', title: $('title').text() },
            //{ extend: 'print', className: 'btn-green' }
        ]
    });

</script>

<script language="JavaScript">
// function check(source) {
//   checkboxes = $('[id^="customCheck"]');
//   for(var i=0, n=checkboxes.length;i<n;i++) {
//     checkboxes[i].checked = source.checked;
//   }
// }
//
//
// function excelgen(var1)
// {
//     var selected = [];
//     $('[name^="customername"]').each(function() {
//         if ($(this).is(":checked")) {
//         selected.push($(this).attr('value'));
//         }
//     });
//     selected=selected.toString()
//     window.location.href = var1+"?id="+selected;
// }
$('input[type="search"]').attr('placeholder','Search here');
</script>
@endsection