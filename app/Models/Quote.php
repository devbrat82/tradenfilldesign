<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Auth;
use App\Mail\WelcomeAccountConfirm;
use App\MailToken;
use Zend\Diactoros\Request;

class Quote extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quotes';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer',
        'act',
        'service',
        'fotmation',
        'govt_fee',
        'prof_fees',
        'split_price',
        'duration',
        'unit',
        'docs'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];


    /**
     * Get created_at in array format
     *
     * @param  string $value
     * @return array
     */
//    public function getCreatedAtAttribute($value)
//    {
//        return \DateTime::createFromFormat('j/n/Y g:i A', $value);
//
//    }

    /**
     * Get updated_at in array format
     *
     * @param  string $value
     * @return array
     */
//    public function getUpdatedAtAttribute($value)
//    {
//        return \DateTime::createFromFormat('j/n/Y g:i A', $value);
//
//    }

    public function Act()
    {
        return $this->hasOne(Act::Class, 'id', 'act');
    }

    public function Service()
    {
        return $this->hasOne(Service::Class, 'id', 'service');
    }

    public function FormationType()
    {
        return $this->hasOne(FormationType::Class, 'id', 'fotmation');
    }

    public function Customer()
    {
        return $this->hasOne(Customer::Class, 'id', 'customer');
    }

    public function User()
    {
        return $this->hasOne('App\User', 'id', 'sent_by');
    }

    public function CreatedUser()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public static function saveRecord($request, $id = 0)
    {

        $quote = ($id == 0) ? new Quote() : Quote::FindOrFail($id);

        $quote->code = (string)self::getQuoteCode();
        $quote->customer = (integer)$request->customer;

        $quote->act = (integer)$request->act;

        $quote->service = (integer)$request->service;
        $quote->fotmation = (integer)$request->fotmation;

        $quote->govt_fee = $request->govt_fee;

        $quote->prof_fees = $request->prof_fees;

        $quote->duration = (integer)$request->duration;
        $quote->unit = (string)$request->unit;
        $quote->split_price = (integer)$request->split_price;
        $quote->comment = (string)$request->comment;

        if ($request->has('docs') && $request->docs != '' && !empty($request->docs)) {
            $quote->docs = json_encode($request->docs);
        }

        //dd($quote->toArray());
        $quote->created_by = Auth::user()->id;

        $quote->save();

        return $quote->id;
    }

    public static function getData()
    {
        return self::with('Act')->with('Service')->with('FormationType')->with('Customer')->with('User')->get();
    }

    public static function getQuoteCode()
    {
        $rcd = self::latest()->first();
        if ($rcd != null)
            $id = $rcd->id;
        else
            $id = 1;

        $id += 1;
        return 'Q0000' . $id;
    }

    public static function sendWelcomeConfirmEmail($email=null, $user, $subject, $msg, $file, $otherAttachment=null)
    {

       // $data = new WelcomeAccountConfirm($user, $subject, $msg,$file, $otherAttachment);
        $cc = 'info@tradenfill.com';
        $mailId = ($email!=null?$email:$user->email);
           \Mail::to($mailId)->cc($cc)->send(new WelcomeAccountConfirm($user, $subject, $msg,$file,$otherAttachment));
    }

    public function excel($id)
    {
        $id = $id;
        $service_documents = Quote::select('quotes.id as Id','quotes.code as Code','customers.cust_code as Customer Code','acts.name as Main','services.title as Service','formation_types.name as Formation','quotes.status as Status','quotes.created_at as Created Date')
        ->leftjoin('acts', 'acts.id', '=', 'quotes.act')
        ->leftjoin('customers', 'customers.id', '=', 'quotes.customer')
        ->leftjoin('services', 'services.id', '=', 'quotes.service')
        ->leftjoin('formation_types', 'formation_types.id', '=', 'quotes.fotmation')
         ->when($id, function ($query) use ($id) {
         return $query->whereIn('quotes.id',$id);
           })
        ->orderBy('quotes.id')
        ->get();
        return $service_documents; 
    }


}

