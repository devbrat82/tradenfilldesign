@extends('layouts.app')
@section('content')
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <div>Dashboard
                    <small data-localize="dashboard.WELCOME"></small>
                </div>
                <!-- END Language list-->
            </div>
			
			
			<div class="row">
               <div class="col-xl-3 col-md-6">
                  <!-- START card-->
                   <a href="{{route('services.service.index')}}" style='text-decoration: none;'>
                  <div class="card dashbord-card flex-row align-items-center align-items-stretch border-0">
                     <div class="col-8 py-3 bg-primary rounded-right">
                        <div class="h2 mt-0 dashboard-text-title">{{ $serviceCount }}</div>
                        <div class="text-uppercase dashboard-text">Services</div>
                     </div>
                     <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                        <em class="icon-briefcase fa-3x dashboard-icons"></em>
                     </div>
                  </div>
                   </a>
               </div>
               <div class="col-xl-3 col-md-6">
                  <!-- START card-->
                   <a href="{{route('documents.document.index')}}" style='text-decoration: none;'>
                  <div class="card dashbord-card flex-row align-items-center align-items-stretch border-0">
                     <div class="col-8 py-3 bg-purple rounded-right">
                        <div class="h2 mt-0 dashboard-text-title">{{ $docs }}
                          
                        </div>
                        <div class="text-uppercase dashboard-text">Documents</div>
                     </div>
                     
                     <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                        <em class="icon-doc fa-3x dashboard-icons"></em>
                     </div>
                  </div>
                   </a>
               </div>
                <div class="col-xl-3 col-md-6">
                    <!-- START card-->
                    <a href="{{route('customers.customer.index')}}" style='text-decoration: none;'>
                        <div class="card  dashbord-card flex-row align-items-center align-items-stretch border-0">

                            <div class="col-8 py-3 bg-primary rounded-right">
                                <div class="h2 mt-0 dashboard-text-title">{{ $customerCount }}</div>
                                <div class="text-uppercase dashboard-text">Customers</div>
                            </div>
                            <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                                <em class="icon-user fa-3x dashboard-icons"></em>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-md-6">
                    <!-- START card-->
                    <a href="{{route('quotes.quote.index')}}" style='text-decoration: none;'> 
                        <div class="card  dashbord-card flex-row align-items-center align-items-stretch border-0">

                            <div class="col-8 py-3 bg-primary rounded-right">
                                <div class="h2 mt-0 dashboard-text-title">{{ $quoteCount }}</div>
                                <div class="text-uppercase dashboard-text">Quotes</div>
                            </div>
                            <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                                <em class="icon-briefcase fa-3x dashboard-icons"></em>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="row">

                <div class="col-xl-4">
                    <!-- START List group-->
                    <div class="list-group mb-3">

                        <div class="list-group-item">
                            <div class="d-flex align-items-right py-3">
                                <div class="w-100 px-3">
                                    <p class="m-0 lead"><b>{{$total_quote_this_month}}</b></p>
                                    <p class="m-0 text-sm"> Quotes Generated this month</p>
                                </div>

                            </div>
                        </div>


                        <div class="list-group-item">
                            <div class="d-flex align-items-right py-3">
                                <div class="w-100 px-3">
                                    <p class="m-0 lead"><b>{{$this_month_customers}}</b></p>
                                    <p class="m-0 text-sm"> Customers Registered this month</p>
                                </div>

                            </div>
                        </div>

                        {{--<div class="list-group-item">--}}
                            {{--<div class="d-flex align-items-right py-3">--}}
                                {{--<div class="w-100 px-3">--}}
                                    {{--<p class="m-0 lead"><b>{{'value'}}</b></p>--}}
                                    {{--<p class="m-0 text-sm"> Staff Registered this month</p>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}




                    </div>
                    <!-- END List group-->
                </div>



                <div class="col-xl-8">
                    <!-- START bar chart-->
                    <div class="card" id="cardChart3">
                        <div class="card-header">
                            <!-- END button group-->
                            <div class="card-title">Quotes Generated</div>
                        </div>
                        <div class="card-wrapper">
                            <div class="card-body">
                                <div class="chart-bar-splinev223 flot-chart"></div>
                            </div>
                        </div>
                    </div>
                    <!-- END bar chart-->
                </div>

            </div>
            {{--<div class="unwrap my-3">--}}
                {{--<!-- START chart-->--}}
                {{--<div class="card" id="cardChart9">--}}
                    {{--<div class="card-header">--}}

                        {{--<div class="card-title">Customer Registered</div>--}}
                    {{--</div>--}}
                    {{--<div class="card-wrapper">--}}
                        {{--<div class="card-body">--}}
                            {{--<div class="chart-splinev2 flot-chart"></div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- END chart-->--}}
            {{--</div>--}}
			
			
			

        </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        (function(window, document, $, undefined) {
            $(function() {
                
                var datav22 = [{
                    "label": "<span onclick='changedataset(2)'>Previous Year</span>",
                    "color": "#03fc9d",
                    "data": []
                },{
                    "label": "<span onclick='changedataset(1)'>This Year</span>",
                    "color": "#2196F3",
                    "data": [
                            @if(count($this_year_quotes))
                            @foreach($this_year_quotes as $key=>$data)
                        ["{{ $key }}", {{ $data }}],
                        @endforeach
                        @endif
                    ]
                }];

                var datav23 = [{
                    "label": "<span onclick='changedataset(1)'>This Year</span>",
                    "color": "#2196F3",
                    "data": []
                },{
                    "label": "<span onclick='changedataset(2)'>Previous Year</span>",
                    "color": "#03fc9d",
                    "data": [
                            @if(count($this_previous_quotes))
                            @foreach($this_previous_quotes as $key=>$datapre)
                        ["{{ $key }}", {{ $datapre }}],
                        @endforeach
                        @endif
                    ]
                }];



                var optionsss = {
                    series: {
                        stack: true,
                        bars: {
                            align: 'center',
                            lineWidth: 0,
                            show: true,
                            barWidth: 0.6,
                            fill: 0.9
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: function(label, x, y) { return  x + ' : ' + y; }
                    },
                    xaxis: {
                        tickColor: '#fcfcfc',
                        mode: 'categories',
                    },
                    yaxis: {
                        // position: 'right' or 'left'
                        tickColor: '#eee',
                    },
                    shadowSize: 1
                };


              changedataset = function(var1){
                    if(var1==1){
                     $.plot(chartv2ss, datav22, optionsss);   
                    }
                    else{
                      $.plot(chartv2ss, datav23, optionsss);  
                    }
              }


                var chartv2ss = $('.chart-bar-splinev223');
                
                    $.plot(chartv2ss, datav22, optionsss);
                

                



            });

        })(window, document, window.jQuery);

        

    </script>
@endsection


