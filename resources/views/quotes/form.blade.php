
<div class="row ct">
    <div class="col-md-2">
        <h3>Basic Info</h3>

    </div>
    <div class="col-md-5">
        <label for="customer" class="control-label">Customer</label>
        <div class="input-group">
            <select class="form-control" id="customer" name="customer" required>
                    <option value="" style="display: none;" {{ old('customer', optional($quote)->customer ?: '') == '' ? 'selected' : '' }} disabled selected>Select customer</option>
                @foreach ($customer as $key => $text)
                    <option value="{{ $key }}" {{ old('customer', optional($quote)->customer) == $key ? 'selected' : '' }}>
                        {{ $text }}
                    </option>
                @endforeach
            </select>
            @if($quote==null)
                <a data-toggle="modal" data-target="#myModal" class="btn btn-primary" type="button" tabindex="-1" title="Add New Customer"><span class="fa fa-user"></span></a>
            @endif
        </div>
        
        {!! $errors->first('customer', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="col-md-5">
        <label for="act" class=" control-label">Main</label>
        <select class="form-control" id="act" name="act" required>
            <option value="" style="display: none;" {{ old('act', optional($quote)->act ?: '') == '' ? 'selected' : '' }} disabled selected>Select act</option>
            @foreach ($act as $key => $text)
                <option value="{{ $key }}" {{ old('act', optional($quote)->act) == $key ? 'selected' : '' }}>
                    {{ $text }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('act', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="row ct">
    <div class="col-md-2"></div>
    <div class="col-md-5">
        <label for="service" class="control-label">Service</label>
        <select class="form-control" id="service" name="service" required>
        	    <option value="" style="display: none;" {{ old('service', optional($quote)->service ?: '') == '' ? 'selected' : '' }} disabled selected>Select service</option>
        	@foreach ($service as $key => $text)
			    <option value="{{ $key }}" {{ old('service', optional($quote)->service) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('service', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="col-md-5">
        <label for="fotmation" class="control-label">Formation</label>
        <select class="form-control" id="fotmation" name="fotmation" required>
            <option value="" style="display: none;" {{ old('fotmation', optional($quote)->fotmation ?: '') == '' ? 'selected' : '' }} disabled selected>Select formation</option>
            @foreach ($formation as $key => $text)
                <option value="{{ $key }}" {{ old('fotmation', optional($quote)->fotmation) == $key ? 'selected' : '' }}>
                    {{ $text }}
                </option>
            @endforeach
        </select>

        {!! $errors->first('fotmation', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<hr>
<div class="ct " style="display:none" id = 'reqDocs'>
    @include('quotes.docs_details')
</div>

@section('popup')
    @if($quote==null)
        <div id="myModal" class="modal fade" role="dialog">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Create New Customer</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="post" id="reused_form" action="{{ route('customers.customer.store') }}">
                            <p></p>
                            {{ csrf_field() }}
                            <input type="hidden" name="from_quote" value="1">
                            @include ('customers.form', [
                                            'customer' => null,
                                            'customerType' => $customerTypes,
                                          ])

                            <div class="row">
                                <div class=" col-md-6">
                                    <input class="btn btn-primary" type="submit" value="Save Customer">
                                    <button type="button" class="ml-2 base-button btn btn-outline-primary default-size ">Cancel</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div id="docList" class="modal fade" role="dialog">
        <div class="overlay-wrap" ></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <center><h4 class="modal-title">Document List</h4></center>
                </div>
                <div class="modal-body">
                    <form role="form" method = "POST" id="docs_add_form" >
                        <p></p>
                        {{ csrf_field() }}
                        <input type="hidden" name="act" id="actDoc">
                        <input type="hidden" name="serv" id="servDoc">
                        <input type="hidden" name="fmt"  id="fmtDoc">
                        <div id="list"> </div>

                        <div class="row">
                            <div class=" col-md-12">
                                <button class="btn btn-primary" id="submitDocs" style="float:right"> Add </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            var v1 = $('#act').val();
            var v2 = $('#service').val();
            var v3 = $('#fotmation').val();
            var v4 = "<?php echo (($quote!=null)?$quote->id:0); ?>";
            if(v1 !=null && v2!=null && v3 !=null)
            {
                calculate(v3,v2,v1,v4);
                $('#reqDocs').css('display','block');
            }



            $('#gvt').click(function(){
                $('#govt_fee').removeAttr('readonly');
            })

            $('#prof').click(function(){
                $('#prof_fees').removeAttr('readonly');
            })

            $("#act").on('change', function() {
                const val = $( this).val();
                let ajaxData = {"_token": "{{ csrf_token() }}",}
                ajaxData['act_id'] = val;

                var request = $.ajax({
                    url: "<?php echo route('quotes.quote.actService'); ?>",
                    method: "GET",
                    data: ajaxData,
                    dataType: "json"
                });

                request.done(function(serv) {
                    var trHTML =[];
                    $('#service').html('');
                    $.each(serv, function (k, v)
                    {

                       trHTML += '<option value="' + k + '">' + v + ' </option>';

                    })
                    $('#service').append(trHTML);
                    // $.notify(message, {status: "success", pos: "bottom-right"});
                })
            })

            $("#fotmation").on('change', function() {
                const formtn = $(this).val();
                const serv = $('#service').val();
                const act = $('#act').val();

                // ajax call

                calculate(formtn,serv,act,0);
            })
        } );

       function calculate(val1,val2,vl3,val4) {

           let ajaxData = {"_token": "{{ csrf_token() }}",}
           ajaxData['formtn'] = val1;
           ajaxData['serv'] = val2;
           ajaxData['act'] = vl3;
           ajaxData['quoteId'] =val4;

           var request = $.ajax({
               url: "<?php echo route('quotes.quote.getDocs'); ?>",
               method: "GET",
               data: ajaxData,
               dataType: "json"
           });

           request.done(function(data) {
               console.log(data);
               var trHTML =['<div class="row">'];
               if(data.reqd)
               {
                   //trHTML = '<ul>';
                   $.each(data.reqd, function (k, v)
                   {
                       trHTML += '<div class="col-md-6 col-sm-12"><div class="alert alert-success alert-dismissible fade show" role="alert"> <input style="opacity: 0" type="checkbox" checked readonly name="docs[]" value="' + k + '"> ' + v + ' </input> <button type="button" class="btn close doc_remove" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true" class="cross">×</span></button></div></div>';
                       //trHTML += '<li><input type="checkbox" checked readonly name="docs[]" value="' + k + '"> ' + v + ' </input></li>';
                       $('#docs').html(trHTML);
                   })
                   //trHTML += '</ul>';
                   trHTML += '</div>';
               }else{
                   $('#docs').html('No documents found.');
               }
               if(data.docs)
               {

                   $('#govt_fee').val(data.docs.govt_fee);
                   $('#prof_fees').val(data.docs.prof_fees);
                   $('#duration').val(data.docs.duration);
                   $('#unit').val(data.docs.unit);
                   if(data.docs.unit)
                   {
                       $('#unit').html('');
                       var html =  '<option value="' + data.docs.unit + '">' + data.docs.unit + ' </option>';
                       $('#unit').append(html);
                   }
                   // else {
                   //     alert('jhkjhdk');
                   //     var html =  '<option value="Year">Year</option>';
                   //     html +=  '<option value="Month">Month</option>';
                   //     html +=  '<option value="Days">Days</option>';
                   //     $('#unit').append(html);
                   // }

               }else {
                   $('#govt_fee').val('');
                   $('#prof_fees').val('');
                   $('#duration').val('');
                   $('#unit').val('');
               }

               $('#reqDocs').css('display','block');



           })
        }

        $('.add_doc').click(function () {

            var act = $('#act').val();
            var service = $('#service').val();
            var fotmation = $('#fotmation').val();

            let ajaxData = {"_token": "{{ csrf_token() }}",}
            ajaxData['act_id'] = act;
            ajaxData['service_id'] = service;
            ajaxData['fotmation_id'] = fotmation;

            var request = $.ajax({
                url: "<?php echo route('quotes.quote.getAllDocs'); ?>",
                method: "GET",
                data: ajaxData,
                dataType: "json"
            });

            request.done(function(docs) {
                console.log(docs);
                var trHTML =['<table class=\'table-striped\' style="width:100%">'];
                $('#list').html('');
                $.each(docs, function (k, v)
                {
                    trHTML += '<tr><td><li style="list-style: none; padding:10px 32px;" class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="defaultUnchecked' +k + '" name="list[]" value="' + k + '"> <label class="custom-control-label" for="defaultUnchecked' +k + '"> ' + v + ' </label></li></td></tr>';
                })
                trHTML += '</table>';
                $('#list').append(trHTML);
                $('#actDoc').val(act);
                $('#servDoc').val(service);
                $('#fmtDoc').val(fotmation);

                $('#docList').modal('show');
                // $.notify(message, {status: "success", pos: "bottom-right"});
            })
        })

        $('#submitDocs').click(function (e) {

            e.preventDefault();
            // var act = $('#act').val();
            // var service = $('#service').val();
            // var fotmation = $('#fotmation').val();

            {{--let ajaxData = {"_token": "{{ csrf_token() }}",}--}}
            let ajaxData = $('#docs_add_form').serialize();


            var request = $.ajax({
                url: "<?php echo route('quotes.quote.saveDocs'); ?>",
                method: "POST",
                data: ajaxData,
                dataType: "json"
            });


            request.done(function(docs) {
                console.log(docs);
                var trHTML =['<div class="row">'];
                $('#docs').html('');
                $.each(docs, function (k, v)
                {
                    trHTML += '<div class="col-md-6 col-sm-12"><div class="alert alert-success alert-dismissible fade show" role="alert"> <input style="opacity: 0" type="checkbox" checked readonly name="docs[]" value="' + k + '"> ' + v + ' </input> <button type="button" class="btn close doc_remove" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true" class="cross">×</span></button></div></div>';
                })
                trHTML +=['</div>'];
                $('#docs').append(trHTML);
                $('#docList').modal('hide');
                // $.notify(message, {status: "success", pos: "bottom-right"});
            })
        })
        //@ sourceURL=edit_skills.js
    </script>
@endsection




