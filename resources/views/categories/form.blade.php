
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title" class="col-md-2 control-label">Title</label>
    <div class="col-md-10">
        <input class="form-control" name="title" type="text" id="title" value="{{ old('title', optional($category)->title) }}" minlength="1" maxlength="255" placeholder="Enter title here...">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('published') ? 'has-error' : '' }}">
    <label for="published" class="col-md-2 control-label">Published</label>
    <div class="col-md-10">
        <select class="form-control" id="published" name="published">
        	    <option value="" style="display: none;" {{ old('published', optional($category)->published ?: '') == '' ? 'selected' : '' }} disabled selected>Select published</option>
        	@foreach (['1' => 'True',
'0' => 'False'] as $key => $text)
			    <option value="{{ $key }}" {{ old('published', optional($category)->published) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
    </div>
</div>

