<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer')->nullable();
            $table->integer('act')->nullable();
            $table->integer('service')->nullable();
            $table->integer('fotmation')->nullable();
            $table->string('govt_fee')->nullable();
            $table->string('prof_fees')->nullable();
            $table->boolean('split_price')->nullable();
            $table->string('file_path')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotes');
    }
}
