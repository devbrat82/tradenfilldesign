
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" required value="{{ old('name', optional($document)->name) }}" minlength="1" maxlength="255" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('alias') ? 'has-error' : '' }}">
    <label for="alias" class="col-md-2 control-label">Alias</label>
    <div class="col-md-10">
        <input class="form-control" name="alias" type="text" required id="alias" value="{{ old('alias', optional($document)->alias) }}" minlength="1" placeholder="Enter alias here...">
        {!! $errors->first('alias', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="col-md-2 control-label">Description</label>
    <div class="col-md-10">
        <textarea class="form-control" name="description" cols="50" rows="10" id="description" minlength="1" maxlength="1000">{{ old('description', optional($document)->description) }}</textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('documenttype') ? 'has-error' : '' }}">
    <label for="documenttype" class="col-md-2 control-label">Document type</label>
    <div class="col-md-10">
        <select class="form-control" id="documenttype" name="documenttype" required>
        	    <option value="" style="display: none;" {{ old('documenttype', optional($document)->documenttype ?: '') == '' ? 'selected' : '' }} disabled selected>Select document type</option>
            @foreach ($docType as $key => $text)
			    <option value="{{ $key }}" {{ old('documenttype', optional($document)->documenttype) == $key ? 'selected' : '' }}>
			    	{{ $text }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('documenttype', '<p class="help-block">:message</p>') !!}
    </div>
</div>

