<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\DocumentCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class DocumentsController extends Controller
{

    /**
     * Display a listing of the documents.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $documents = Document::get();
        $docType = DocumentCategory::where('status',1)->pluck('name','id');
        return view('documents.index', compact(['documents','docType']));
    }

    /**
     * Show the form for creating a new document.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {

        $docType = DocumentCategory::where('status',1)->pluck('name','id');
        return view('documents.create', compact('docType'));
    }

    /**
     * Store a new document in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Document::create($data);

            return redirect()->route('documents.document.index')
                             ->with('success_message', 'Document was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified document.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);

        return view('documents.show', compact('document'));
    }

    /**
     * Show the form for editing the specified document.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);
        $docType = DocumentCategory::where('status',1)->pluck('name','id');

        return view('documents.edit', compact(['document','docType']));
    }

    /**
     * Update the specified document in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
        try {
            
            $data = $this->getData($request,$id);
            
            $document = Document::findOrFail($id);
            $document->update($data);

            return redirect()->route('documents.document.index')
                             ->with('success_message', 'Document was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }        
    }

    /**
     * Remove the specified document from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $document = Document::findOrFail($id);
            $document->delete();

            return redirect()->route('documents.document.index')
                             ->with('success_message', 'Document was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request,$id=null)
    {
        $rules = [
            'name' => 'required|string|min:1|max:255|unique:documents,name,'.$id,
            'alias' => 'required|string|min:1|nullable',
            'description' => 'string|min:1|max:1000|nullable',
            'documenttype' => 'required|nullable',
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
