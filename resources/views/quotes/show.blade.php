@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            {{--<h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Quote' }}</h4>--}}
            <div>
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('quotes.quote.index')}}">Quotes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Quotes</li>
  </ol>
</nav>
                </div>
        </span>

        <div class="pull-right">

            <!-- <form method="POST" action="{!! route('quotes.quote.destroy', $quote->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('quotes.quote.index') }}" class="btn btn-primary" title="Show All Quote">
                        <span class="fa fa-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('quotes.quote.create') }}" class="btn btn-success" title="Create New Quote">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('quotes.quote.edit', $quote->id ) }}" class="btn btn-primary" title="Edit Quote">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Quote" data-toggle="modal" data-target="#deleteModel">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form> -->



            <form method="POST" action="" accept-charset="UTF-8">
                                    <div class="dropdown">
                                    <a  type="text" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="dot-icon show-icon">
                                        <span class="dot dot1 show-dot show-dot1"></span>
                                        <span class="dot dot2 show-dot show-dot2"></span> 
                                        <span class="dot dot3 show-dot show-dot3"></span>
                                    </div>
                                     </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" >
                                        
                                    <a href="{{ route('quotes.quote.index') }}" class="btn dropdown-item" title="Show All Quote">
                        <span class="fa fa-list" aria-hidden="true"></span> Show All Quote
                    </a>
                    <a href="{{ route('quotes.quote.create') }}" class="dropdown-item btn" title="Create New Quote">
                        <span class="fa fa-plus" aria-hidden="true"></span> Create New Quote
                    </a>
                    <a href="{{ route('quotes.quote.edit', $quote->id ) }}" class="btn dropdown-item" title="Edit Quote">
                        <span class="fa fa-pencil" aria-hidden="true"></span> Edit Quote
                    </a>

                    <button type="button" class="btn dropdown-item" title="Delete Quote" data-toggle="modal" data-target="#deleteModel{{$quote->id }}">
                        <span class="fa fa-trash" aria-hidden="true"></span> Delete Quote
                    </button>
                                
                                    </div>
                                </div>
                            </form>


                       

       
        <form method="POST" action="{!! route('quotes.quote.destroy', $quote->id) !!}" accept-charset="UTF-8">
        <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
            <div class="modal fade deleteModel" id="deleteModel{{$quote->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="overlay-wrap" ></div>
            <div class="modal-dialog" role="document">
     
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <i class="fas fa-trash" ></i>
    <div class='modelHeading'>
    <div class='modelMainHeading'>
        Are you sure?
        </div>
        <div class='modelSubHeading'> You will not be able to recover this data</div>
    </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>    
    </div>
  </div>
</div>
</form>








        </div>

    </div>

    {{--<div class="panel-body">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Customer</dt>--}}
                {{--<dd>{{ ucfirst($quote->Customer->first_name. ' '.$quote->Customer->last_name) }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Customer Code</dt>--}}
                {{--<dd>{{ $quote->Customer->cust_code }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Act</dt>--}}
                {{--<dd>{{ $quote->Act->name }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Service</dt>--}}
                {{--<dd>{{ $quote->Service->title }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Fotmation</dt>--}}
                {{--<dd>{{ $quote->FormationType->name }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Govt Fee</dt>--}}
                {{--<dd>{{'₹'. $quote->govt_fee }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Prof Fees</dt>--}}
                {{--<dd>{{'₹'. $quote->prof_fees }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Quote File</dt>--}}
                {{--<dd>{{ $quote->file_path }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Created At</dt>--}}
                {{--<dd>{{ $quote->created_at }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Updated At</dt>--}}
                {{--<dd>{{ $quote->updated_at }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Status</dt>--}}
                {{--<dd>{{ $quote->status }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Comment</dt>--}}
                {{--<dd>{{ $quote->comment}}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Mail sent by</dt>--}}
                {{--<dd>{{ $quote->User!='' && $quote->User!=null?$quote->User->name:'NA' }}</dd>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-sm-6 col-lg-6">--}}
                {{--<dt>Mail sent at</dt>--}}
                {{--<dd>{{ $quote->sent_date }}</dd>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="panel-body bak-white">

        <div class="row">
            <div class="col-md-4">
                <h4><span><li class="fa fa-quote-left"></li></span>  Quote Details</h4>

            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Act </dt><dd>{{ $quote->Act->name }}</dd>

            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Service</dt>
                <dd>{{ $quote->Service->title }}</dd>
            </div>
        </div>
        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Fotmation</dt>
                <dd>{{ $quote->FormationType->name }}</dd>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Quote File</dt>
                <dd>{{ $quote->file_path }}</dd>
            </div>
        </div>


        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
                <h4><span><li class="fa fa-user"></li></span>  Customer Details</h4>

            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Customer Name</dt>
                <dd>{{ ucfirst($quote->Customer->first_name. ' '.$quote->Customer->last_name) }}</dd>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Customer Code</dt>
                <dd><a href="{{ route('customers.customer.show', $quote->Customer->id ) }}">{{ ucfirst($quote->Customer->cust_code) }}</a></dd>
            </div>
        </div>
        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
                <h4><span><li class="fa fa-money"></li></span>  Quote Fees</h4>

            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Govt Fees</dt>
                <dd>{{'₹'. $quote->govt_fee }}</dd>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Professional Fees</dt>
                <dd>{{'₹'. $quote->prof_fees }}</dd>
            </div>
        </div>

        <div class="row" style="margin-bottom: 1.8rem;    padding-bottom: 15px;
    border-bottom: 2px solid #c3b9b952;">
            <div class="col-md-4">
                <h4><span><li class="fa fa-comment"></li></span>  Quote Comment</h4>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Comment</dt>
                <dd>{{ $quote->comment!=''?$quote->comment:'NA'}}</dd>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <h4><span><li class="fa fa-file"></li></span>  Other Details</h4>

            </div>

            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Status</dt>
                <dd>{{ $quote->status }}</dd>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Mail sent by</dt>
                <dd>{{ $quote->User!='' && $quote->User!=null?$quote->User->name:'NA' }}</dd>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Mail sent at</dt>
                <dd>{{ $quote->sent_date }}</dd>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Created At</dt>
                <dd>{{ $quote->created_at }}</dd>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4">
                <dt>Created By</dt>
                <dd>{{ $quote->CreatedUser!='' && $quote->CreatedUser!=null?$quote->CreatedUser->name:'NA' }}</dd>
            </div>
        </div>

    </div>





</div>

@endsection